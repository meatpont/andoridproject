package com.souvik.tendercuts.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.souvik.tendercuts.activity.ItemListingActivity;
import com.souvik.tendercuts.activity.LoginActivity;
import com.souvik.tendercuts.activity.ProductDetailsActivity;
import com.souvik.tendercuts.activity.farhan.ListActivity;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.OnRefreshCartViewListner;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.model.CartModel;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.utils.CartAddNotify;
import com.souvik.tendercuts.utils.MeatPointUtils;
import com.squareup.picasso.Picasso;

import java.util.List;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class ItemListingAdapter extends RecyclerView.Adapter<ItemListingAdapter.MyViewHolder> {

    private Context mContext;
    private List<ItemModel> itemModelList;
    private ListActivity adapterCallback;
    private CartAddNotify yourIntrface;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, smalldesc, price, quantity, netWeight, tv_delivery, deal_price;
        public LinearLayout layout_add_item;
        public TextView outStock;
        public ImageView thumbnail, overflow;
        public CardView card_view_category;
        public Button add_to_cart, added_to_cart, buttonMinus, buttonPlus;
        public LinearLayout quantityAddLayout, productList;
        public DatabaseHandler db;
        SessionManager session;
        public String userid = "";
        RelativeLayout layout_is_new;

        public MyViewHolder(View view) {
            super(view);

            setFontAwesome((LinearLayout) view.findViewById(R.id.productList), mContext);

            title = (TextView) view.findViewById(R.id.title);
            tv_delivery = (TextView) view.findViewById(R.id.tv_delivery);
            netWeight = (TextView) view.findViewById(R.id.netWeight);
            outStock = (TextView) view.findViewById(R.id.tv_out_stock);
            layout_add_item = (LinearLayout) view.findViewById(R.id.layout_add_item);

            smalldesc = (TextView) view.findViewById(R.id.smalldesc);
            price = (TextView) view.findViewById(R.id.price);
            deal_price = view.findViewById(R.id.deal_price);

            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            //overflow = (ImageView) view.findViewById(R.id.overflow);
            card_view_category = (CardView) view.findViewById(R.id.card_view_category);

            add_to_cart = (Button) view.findViewById(R.id.add_to_cart);
            added_to_cart = (Button) view.findViewById(R.id.added_to_cart);
            quantityAddLayout = (LinearLayout) view.findViewById(R.id.quantityAddLayout);
            buttonMinus = (Button) view.findViewById(R.id.buttonMinus);
            buttonPlus = (Button) view.findViewById(R.id.buttonPlus);
            quantity = (TextView) view.findViewById(R.id.quantityitem);
            adapterCallback = (ListActivity) mContext;
//            adapterCallback = (ItemListingActivity) mContext;
            productList = (LinearLayout) view.findViewById(R.id.productList);
            layout_is_new = view.findViewById(R.id.layout_is_new);

            db = new DatabaseHandler(mContext);
            session = SessionManager.getInstance(mContext);
        }
    }


    public ItemListingAdapter(Context mContext, List<ItemModel> itemModelList) {
        this.mContext = mContext;
        this.itemModelList = itemModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_listing_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ItemModel itemModel = itemModelList.get(position);

        holder.title.setText(decode(itemModel.getProdName()));

        if (itemModel.getTomorrowDelivery().equalsIgnoreCase("Delivery for Tomorrow")) {
            holder.tv_delivery.setVisibility(View.VISIBLE);
            holder.tv_delivery.setText("Scheduled Delivery Only");
        } else {
            holder.tv_delivery.setVisibility(View.GONE);
        }
        if (itemModel.getIsNew().equalsIgnoreCase("1")) {
            holder.layout_is_new.setVisibility(View.VISIBLE);
        } else {
            holder.layout_is_new.setVisibility(View.GONE);
        }
        holder.smalldesc.setText(itemModel.getProdDesc());
        if (itemModel.getDeal_price().equals("0.00")) {
            holder.deal_price.setText("₹" + itemModel.getSellPrice());
            holder.deal_price.setVisibility(View.VISIBLE);
            holder.price.setVisibility(View.GONE);
        } else {
            holder.price.setText("₹" + itemModel.getSellPrice());
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.price.setTextColor((mContext).getResources().getColor(R.color.cb_errorRed));
            holder.deal_price.setText("₹" + itemModel.getDeal_price());
            holder.deal_price.setVisibility(View.VISIBLE);
        }
        holder.netWeight.setText(itemModel.getNetWeight());

        if (holder.db.getCartCount(new ItemModel(itemModel.getProdToken().toString(), itemModel.getUserId())) == 0) {
            holder.add_to_cart.setVisibility(View.VISIBLE);
            //holder.added_to_cart.setVisibility(View.GONE);
            holder.quantityAddLayout.setVisibility(View.GONE);
        } else {
            holder.add_to_cart.setVisibility(View.GONE);
            //holder.added_to_cart.setVisibility(View.VISIBLE);
            holder.quantityAddLayout.setVisibility(View.VISIBLE);
        }
        if (itemModel.getOutStock().equals("Out of Stock")) {
            holder.layout_add_item.setVisibility(View.GONE);
            holder.outStock.setVisibility(View.VISIBLE);
            holder.outStock.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.outStock.setText(itemModel.getOutStock());
        } else {
            holder.layout_add_item.setVisibility(View.VISIBLE);
            holder.outStock.setVisibility(View.GONE);
        }

        /*if(itemModel.isAddedCart()){
            holder.add_to_cart.setVisibility(View.GONE);
            holder.added_to_cart.setVisibility(View.VISIBLE);
        }else{

        }*/

        try {
            Picasso
                    .with(mContext)
                    .load(itemModel.getPic())
                    .into(holder.thumbnail);

            holder.thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterCallback.setItemModelList(itemModelList, itemModel.getIndex());
                    //Log.d("creation","dd => "+itemModel.getName());
//                    //itemModel.getProdName();
                    Intent i = new Intent(mContext, ProductDetailsActivity.class);
                    i.putExtra("catToken", String.valueOf(itemModel.getProdToken()));
                    i.putExtra("netWeight", itemModel.getNetWeight().toString());
                    i.putExtra("\"₹\"+", holder.netWeight.getText());
                    i.putExtra("out", itemModel.getOutStock());
                    mContext.startActivity(i);
                }
            });

//            holder.added_to_cart.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (holder.session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
//                        holder.userid = itemModel.getUserId();
//                    }else{
//                        holder.userid = "demouser";
//                    }
//
//                    if (holder.db.getCartCount(new ItemModel(itemModel.getProdToken().toString(), itemModel.getUserId())) > 0) {
//
//                        int cartID = holder.db.getCartID(new ItemModel(itemModel.getProdToken().toString(), holder.userid));
//                        holder.db.deleteItemFromCart(cartID);
//
//                        holder.add_to_cart.setVisibility(View.VISIBLE);
//                        holder.added_to_cart.setVisibility(View.GONE);
//                        //holder.db.addProductInCart(new ItemModel(itemModel.getProdToken().toString(),itemModel.getCartAddedQuantity(),itemModel.getUserId()));
//                    }
//                }
//            });

            holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                        holder.userid = itemModel.getUserId();
                    } else {
                        holder.userid = "demouser";
                    }


                    itemModel.setAddedCart(true);
                    holder.add_to_cart.setVisibility(View.GONE);
                    //holder.added_to_cart.setVisibility(View.VISIBLE);
                    holder.quantityAddLayout.setVisibility(View.VISIBLE);
                    holder.quantity.setText("1");
                    if (holder.db.getCartCount(new ItemModel(itemModel.getProdToken().toString(), holder.userid)) <= 0) {
                        holder.db.addProductInCart(new ItemModel(itemModel.getProdToken().toString(), 1, holder.userid));
                        yourIntrface.onItemSelected(1);
                    }

                }
            });

            int cartquantity = holder.db.getCartAddedQuantity(new ItemModel(itemModel.getProdToken(), holder.session.get_customerToken()));
            if (cartquantity > 0) {
                holder.quantity.setText("" + cartquantity);
            }

            holder.buttonMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //   Toast.makeText(mContext, "click", Toast.LENGTH_SHORT).show();
                    if (holder.session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                        holder.userid = holder.session.get_customerToken();
                    } else {
                        holder.userid = "demouser";
                    }

//                    if (holder.session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                    int quant = Integer.parseInt(holder.quantity.getText().toString());

                    if (quant > 1) {
                        quant = quant - 1;
                        holder.quantity.setText("" + quant);
                        if (holder.db.getCartCount(new ItemModel(itemModel.getProdToken(), holder.userid)) > 0) {
                            int cartID = holder.db.getCartID(new ItemModel(itemModel.getProdToken(), holder.userid));
                            holder.db.updateCartProductQuantity(new ItemModel(quant, cartID));
                            yourIntrface.onItemSelected(quant);
                        }

                    } else {
                        if (quant == 1) {
                            quant = quant - 1;
                            holder.quantity.setText("" + quant);
                            //holder.quantityAddLayout.setVisibility(View.GONE);

                            //itemModelList.remove(position);
                            holder.add_to_cart.setVisibility(View.VISIBLE);
                            holder.quantityAddLayout.setVisibility(View.GONE);

                            int cartID = holder.db.getCartID(new ItemModel(itemModel.getProdToken(), holder.userid));
                            holder.db.deleteItemFromCart(cartID);

                            yourIntrface.onItemSelected(quant);
                            //notifyItemRemoved(position);
                            //notifyItemRangeChanged(position, itemModelList.size());
                            //holder.productList.setVisibility(View.GONE);

                        }

                    }

//                    }
//                    else {
//                        Intent i = new Intent(mContext, LoginActivity.class);
//                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        mContext.startActivity(i);
//                    }

                }
            });

            holder.buttonPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                        holder.userid = holder.session.get_customerToken();
                    } else {
                        holder.userid = "demouser";
                    }


//                    if (holder.session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                    int quant = Integer.parseInt(holder.quantity.getText().toString());
                    if (quant >= 1) {
                        quant = quant + 1;
                        int stockquantity = itemModel.getStockAddedQuantity();

                        if (quant > stockquantity) {
                            holder.quantity.setText("" + (quant - 1));
                        } else {
                            holder.quantity.setText("" + quant);
                                /*itemModel.setCartAddedQuantity(quantity);
                                itemModelList.remove(position);
                                itemModelList.add(position,itemModel);*/
                            if (holder.db.getCartCount(new ItemModel(itemModel.getProdToken(), holder.userid)) > 0) {
                                int cartID = holder.db.getCartID(new ItemModel(itemModel.getProdToken(), holder.userid));
                                //Log.d("creation","ddd => "+cartID);
                                holder.db.updateCartProductQuantity(new ItemModel(quant, cartID));
                                yourIntrface.onItemSelected(quant);
                            }
//                                adapterCallback.itemQuantitychange(itemModelList,position);
                        }
                    }
//                        Log.e("Quant ", String.valueOf(quant));
//                    }else {
//                        Intent i = new Intent(mContext, LoginActivity.class);
//                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        mContext.startActivity(i);
//                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });*/
    }

    public void setOnItemSelected(CartAddNotify yourIntrface) {
        this.yourIntrface = yourIntrface;

    }


    public interface ItemAdapterCallback {
        void setItemModelList(List<ItemModel> itemModelList, int pos);
    }

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }

    private String decode(String url) {
        return url.replace("&amp;", "&");
    }
}
