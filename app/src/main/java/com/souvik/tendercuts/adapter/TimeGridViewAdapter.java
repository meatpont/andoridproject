package com.souvik.tendercuts.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.souvik.tendercuts.model.TimeItemsModel;

import java.util.ArrayList;

import in.co.meatpoint.R;


public class TimeGridViewAdapter extends ArrayAdapter {

    private Context context;
    private int layoutResourceId;
    private ArrayList data = new ArrayList();
    private int lastSelectedPosition = -1;

    public TimeGridViewAdapter(Context context, int layoutResourceId, ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    TextView lastSelectedButton;
    ArrayList<TextView> textViewArray = new ArrayList<TextView>();

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        final int justSelectedPos = position+1;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder(row);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        lastSelectedButton = (TextView) row.findViewById(R.id.image);

        textViewArray.add(lastSelectedButton);
        final TimeItemsModel item = (TimeItemsModel) data.get(position);
        holder.image.setText(item.getTitle());

        final ViewHolder finalHolder = holder;
        holder.image.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if(lastSelectedPosition !=justSelectedPos) {

                    final int sdk = android.os.Build.VERSION.SDK_INT;
                    if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        textViewArray.get(lastSelectedPosition+1).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.tags_rounded_corners_white) );
                        textViewArray.get(lastSelectedPosition+1).setTextColor(Color.parseColor("#8b8b8b"));

                        textViewArray.get(justSelectedPos+1).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.tags_rounded_corners_selected) );
                        textViewArray.get(justSelectedPos+1).setTextColor(Color.WHITE);

                    } else {
                        textViewArray.get(lastSelectedPosition+1).setBackground( context.getResources().getDrawable(R.drawable.tags_rounded_corners_white));
                        textViewArray.get(lastSelectedPosition+1).setTextColor(Color.parseColor("#8b8b8b"));

                        textViewArray.get(justSelectedPos+1).setBackground( context.getResources().getDrawable(R.drawable.tags_rounded_corners_selected));
                        textViewArray.get(justSelectedPos+1).setTextColor(Color.WHITE);
                    }

                    textViewArray.get(lastSelectedPosition+1).setBackgroundColor(Color.WHITE);
                    textViewArray.get(lastSelectedPosition+1).setTextColor(Color.parseColor("#8b8b8b"));

                    textViewArray.get(justSelectedPos+1).setBackgroundColor(Color.parseColor("#009EF8"));
                    textViewArray.get(justSelectedPos+1).setTextColor(Color.WHITE);

//                    ((BookingDatePeopleTimeActivity)context).setTime(item.getTitle());
                }
                lastSelectedPosition = justSelectedPos;
            }
        });

        return row;
    }

    static class ViewHolder {
        TextView image;
        LinearLayout gridTimeLayout;

        public ViewHolder(View row) {
            image = (TextView) row.findViewById(R.id.image);
            gridTimeLayout = (LinearLayout) row.findViewById(R.id.gridTimeLayout);
        }
    }
}

