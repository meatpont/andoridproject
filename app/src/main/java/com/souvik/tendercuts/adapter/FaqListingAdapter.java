package com.souvik.tendercuts.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.souvik.tendercuts.activity.ItemListingActivity;
import com.souvik.tendercuts.activity.ProductDetailsActivity;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.model.FaqModel;
import com.souvik.tendercuts.model.ItemModel;
import com.squareup.picasso.Picasso;

import java.util.List;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

/**
 * Created on 02/09/16.
 */
public class FaqListingAdapter extends RecyclerView.Adapter<FaqListingAdapter.MyViewHolder> {

    private Context mContext;
    private List<FaqModel> itemModelList;
    private ItemListingActivity adapterCallback;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView faqQuestion, faqDescription;
        public DatabaseHandler db;
        SessionManager session;

        public MyViewHolder(View view) {
            super(view);
            setFontAwesome((LinearLayout)view.findViewById(R.id.productList),mContext);
            faqQuestion = (TextView) view.findViewById(R.id.faqQuestion);
            faqDescription = (TextView) view.findViewById(R.id.faqDescription);
        }
    }

    public FaqListingAdapter(Context mContext, List<FaqModel> itemModelList) {
        this.mContext = mContext;
        this.itemModelList = itemModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.faq_listing_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final FaqModel itemModel = itemModelList.get(position);
        holder.faqQuestion.setText(itemModel.getQuestion());
        holder.faqDescription.setText(itemModel.getAnswer());
    }

    public  interface ItemAdapterCallback {
        void setItemModelList(List<FaqModel> itemModelList, int pos);
    }

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }
}
