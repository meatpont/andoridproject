package com.souvik.tendercuts.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.OrderDetailsActivity;
import com.souvik.tendercuts.activity.OrderListingActivity;
import com.souvik.tendercuts.activity.OrderTrackActivity;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.model.OrderHistoryModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.CANCEL_ORDER;
import static com.souvik.tendercuts.config.AppConfig.GET_ORDER_DETAILS;
import static com.souvik.tendercuts.config.AppController.context;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

/**
 * Created on 02/09/16.
 */
public class OrderItemListingAdapter extends RecyclerView.Adapter<OrderItemListingAdapter.MyViewHolder> {

    private Context mContext;
    private List<OrderHistoryModel> itemModelList;
    // private ItemListingActivity adapterCallback;
    SessionManager session;
    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView  productname, productPrice, quantity, price;
        public DatabaseHandler db;
        public ImageView prodImage;

        //
        public MyViewHolder(View view) {
            super(view);

            setFontAwesome((LinearLayout) view.findViewById(R.id.productList), mContext);

            prodImage = (CircleImageView) view.findViewById(R.id.prodImage);
            productname = (TextView) view.findViewById(R.id.productname);
            productPrice = (TextView) view.findViewById(R.id.productPrice);
            quantity = (TextView) view.findViewById(R.id.quantity);
            price = view.findViewById(R.id.price);

            db = new DatabaseHandler(mContext);
            session = SessionManager.getInstance(mContext);

        }
    }


    public OrderItemListingAdapter(Context mContext, List<OrderHistoryModel> itemModelList) {
        this.mContext = mContext;
        this.itemModelList = itemModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_listing_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final OrderHistoryModel itemModel = itemModelList.get(position);


//        holder.order_date.setText(itemModel.getOrderDate());
//        holder.order_date.setTypeface(null, 1);

        holder.productPrice.setTypeface(null, 1);
        holder.quantity.setTypeface(null,1);

        holder.productname.setText(itemModel.getProductName());
        holder.productname.setTypeface(null, 1);

        holder.productPrice.setText("₹"+itemModel.getProductPrice());
        holder.quantity.setText("Qty " + itemModel.getProductQuantity()+" of "+itemModel.getProductQuantity());
        //if is calcel =0 and can cancel =1 then cancel order
//        if(itemModel.getOrderStatus().equalsIgnoreCase("Received")) {
//            holder.orderTrack.setVisibility(View.VISIBLE);
//
//        }else {
//            holder.orderTrack.setVisibility(View.GONE);
//        }

        try {
            Picasso
                    .with(mContext)
                    .load(itemModel.getProductPic())
                    .into(holder.prodImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String[] amt = itemModel.getProductPrice().split("\"");
//        String []qty=itemModel.getTotQuan().split("\"");
//        double amount = Double.parseDouble(amt[1])* Double.parseDouble(qty[1]);
//        holder.price.setText(""+amount);
//        holder.orderTrack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(mContext, OrderTrackActivity.class);
//                i.putExtra("order_id", String.valueOf(itemModel.getTrackUrl()));
//                mContext.startActivity(i);
//                ((Activity)mContext).finish();
//            }
//        });
    }

    public interface ItemAdapterCallback {
        void setItemModelList(List<ItemModel> itemModelList, int pos);
    }

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }

   }
