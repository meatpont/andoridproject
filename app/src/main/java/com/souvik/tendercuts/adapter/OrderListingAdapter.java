package com.souvik.tendercuts.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.souvik.tendercuts.activity.ItemListingActivity;
import com.souvik.tendercuts.activity.OrderDetailsActivity;
import com.souvik.tendercuts.activity.OrderTrackActivity;
import com.souvik.tendercuts.activity.ProductDetailsActivity;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.model.OrderHistoryModel;
import com.squareup.picasso.Picasso;

import java.util.List;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

/**
 * Created on 02/09/16.
 */
public class OrderListingAdapter extends RecyclerView.Adapter<OrderListingAdapter.MyViewHolder> {

    private Context mContext;
    private List<OrderHistoryModel> itemModelList;
   // private ItemListingActivity adapterCallback;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView total_price,delivery_time,payment_status_desc,payment_status,order_no,order_date,
        items, status, detail;
        public Button orderTrack;
        public LinearLayout orderLayout;
        public DatabaseHandler db;
        SessionManager session;

        public MyViewHolder(View view) {
            super(view);

            setFontAwesome((LinearLayout)view.findViewById(R.id.productList),mContext);


           // total_price = (TextView) view.findViewById(R.id.total_price);
            delivery_time = (TextView) view.findViewById(R.id.delivery_time);//time
           // payment_status_desc = (TextView) view.findViewById(R.id.payment_status_desc);

           // payment_status = (TextView) view.findViewById(R.id.payment_status);
          //  order_no = (TextView)view.findViewById(R.id.order_no);
            order_date = (TextView) view.findViewById(R.id.order_date);//date
            items = (TextView) view.findViewById(R.id.items);//items
            status = (TextView) view.findViewById(R.id.status);//status

            detail = (TextView) view.findViewById(R.id.detail);//detail
            //orderLayout = (LinearLayout)view.findViewById(R.id.orderLayout);

           // orderTrack = (Button)view.findViewById(R.id.orderTrack);


            //adapterCallback = (ItemListingActivity)mContext;

            db = new DatabaseHandler(mContext);
            session = SessionManager.getInstance(mContext);
        }
    }


    public OrderListingAdapter(Context mContext, List<OrderHistoryModel> itemModelList) {
        this.mContext = mContext;
        this.itemModelList = itemModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_listing_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final OrderHistoryModel itemModel = itemModelList.get(position);


//        holder.total_price.setText("₹"+itemModel.getPaidAmount());
        holder.delivery_time.setText(itemModel.getDeliveryDate());
//        holder.payment_status_desc.setText(itemModel.getOrderStatus());
//        holder.payment_status.setText(itemModel.getPaymentStatus());
//        holder.order_no.setText(itemModel.getOrderNumber());
        holder.order_date.setText("ORDER PLACED: "+itemModel.getOrderDate());
        holder.order_date.setTypeface(null, Typeface.BOLD);
        holder.items.setText("Item "+itemModel.getTotQuan());
        if(itemModel.getOrderStatus().toString().equalsIgnoreCase("Received")){
            holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.appThemegreencolor));
            holder.status.setText(itemModel.getOrderStatus());
        }

        else {
            holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.cb_errorRed));
            holder.status.setText(itemModel.getOrderStatus());

        }

        holder.detail.setTypeface(null, 1);

        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, OrderDetailsActivity.class);
                i.putExtra("orderToken",String.valueOf(itemModel.getOrderToken()));
                i.putExtra("orderStatus", itemModel.getOrderStatus().toString());
                Log.e("OrderToken ", ""+itemModel.getOrderToken());
                mContext.startActivity(i);
//                ((Activity)mContext).finish();
            }
        });

//        holder.orderTrack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(mContext, OrderTrackActivity.class);
//                i.putExtra("order_id",String.valueOf(itemModel.getOrderToken()));
//                mContext.startActivity(i);
//            }
//        });
    }

    public  interface ItemAdapterCallback {
        void setItemModelList(List<ItemModel> itemModelList, int pos);
    }

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }
}
