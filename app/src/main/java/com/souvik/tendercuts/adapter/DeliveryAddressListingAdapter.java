package com.souvik.tendercuts.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.AddNewDeliveryAddress;
import com.souvik.tendercuts.activity.DeliveryAddress;
import com.souvik.tendercuts.activity.ItemListingActivity;
import com.souvik.tendercuts.activity.LoginActivity;
import com.souvik.tendercuts.activity.OrderDetailsActivity;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.OnRefreshCartViewListner;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.AddressModel;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.utils.MeatPointUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.ADD_DELIVERY_ADDRESS;
import static com.souvik.tendercuts.config.AppConfig.CANCEL_ORDER;
import static com.souvik.tendercuts.config.AppConfig.DELETE_DELIVERY_ADDRESS;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

/**
 * Created on 02/09/16.
 */
public class DeliveryAddressListingAdapter extends RecyclerView.Adapter<DeliveryAddressListingAdapter.MyViewHolder> {

    private Context mContext;
    private List<AddressModel> itemModelList;
    private DeliveryAddress adapterCallback;
    private OnRefreshCartViewListner mRefreshListner;
    VolleyService mVolleyService;
    FccResult mResultCallback = null;
    DeliveryAddressListingAdapter adapter;

    SessionManager session;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public DatabaseHandler db;

        public ImageButton chkRadio,unchkRadio,editAddress,deleteAddress;
        public TextView addressDesc;

        public MyViewHolder(View view) {
            super(view);
            setFontAwesome((LinearLayout)view.findViewById(R.id.cartListingIndRow),mContext);

            chkRadio = (ImageButton)view.findViewById(R.id.chkRadio);
            unchkRadio = (ImageButton)view.findViewById(R.id.unchkRadio);
            editAddress  = (ImageButton)view.findViewById(R.id.editAddress);
            deleteAddress = (ImageButton)view.findViewById(R.id.deleteAddress);
            addressDesc = (TextView)view.findViewById(R.id.addressDesc);

            db = new DatabaseHandler(mContext);
            session = SessionManager.getInstance(mContext);

            adapterCallback = (DeliveryAddress)mContext;

           // mRefreshListner = (OnRefreshCartViewListner)mContext;
           /* //overflow = (ImageView) view.findViewById(R.id.overflow);
            card_view_category = (CardView)view.findViewById(R.id.card_view_category);
            add_to_cart = (Button)view.findViewById(R.id.add_to_cart);
            added_to_cart = (Button)view.findViewById(R.id.added_to_cart);
            */
        }
    }


    public DeliveryAddressListingAdapter(Context mContext, List<AddressModel> itemModelList) {
        this.mContext = mContext;
        this.itemModelList = itemModelList;
        this.adapter=this;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.delivery_address_listing_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final AddressModel itemModel = itemModelList.get(position);
        Log.e("Default", itemModel.getShipDefault());
        holder.addressDesc.setText("Name: "+itemModel.getShipName()+"\nEmail: "+itemModel.getShipEmail()+"" +
                "\nMobile: "+itemModel.getShipPhone()+"\nAddress: "+itemModel.getShipAddress()+"\n" +
                "City: "+itemModel.getShipCity()+"\nPostCode: "+
                itemModel.getShipPostcode());

//        if(itemModel.getShipDefault().equalsIgnoreCase("yes")){
//            holder.chkRadio.setVisibility(View.VISIBLE);
//            holder.unchkRadio.setVisibility(View.GONE);
//
//        }else {
//            holder.unchkRadio.setVisibility(View.VISIBLE);
//            holder.chkRadio.setVisibility(View.GONE);
//        }


        holder.unchkRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.chkRadio.setVisibility(View.VISIBLE);
                holder.unchkRadio.setVisibility(View.GONE);
                adapterCallback.setDeliveryAddress(itemModelList,position);
            }
        });
        holder.chkRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.chkRadio.setVisibility(View.GONE);
                holder.unchkRadio.setVisibility(View.VISIBLE);
            }
        });

        holder.editAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext,AddNewDeliveryAddress.class);
                i.putExtra("addToken", itemModel.getShipToken());
                i.putExtra("type","edit");
                i.putExtra("name", itemModel.getShipName());
                i.putExtra("email", itemModel.getShipEmail());
                i.putExtra("phone", itemModel.getShipPhone());
                i.putExtra("address", itemModel.getShipAddress());
                i.putExtra("city", itemModel.getShipCity());
                i.putExtra("post", itemModel.getShipPostcode());
//                i.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                ((Activity)mContext).startActivityForResult(i, 1);
//                ((Activity)mContext).finish();
            }
        });

        holder.deleteAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialog(itemModel.getShipToken());
            }
        });

    }


   public  interface DeliveryAddressSelectedInterface {
        void setDeliveryAddress(List<AddressModel> itemModelList, int pos);
   }
    public  interface DefaultAddress {
        void setDefaultAddress(List<AddressModel> itemModelList, int pos);
    }

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");

                    if(status_code==1){
                        //JSONObject responseObject = new JSONObject(jsonObject.getString("response"));

                       // Toast.makeText(mContext, "Address deleted", Toast.LENGTH_LONG).show();
                        Intent intent = ((Activity)mContext).getIntent();
                        ((Activity)mContext).finish();
                        mContext.startActivity(intent);
                    }else{
                        Toast.makeText(mContext, "Internal error", Toast.LENGTH_LONG).show();
                    }
                    mContext.getApplicationContext();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    public void dialog(final String token) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(mContext);
            builder.setCancelable(false);
        }
        builder.setTitle("Delete")
                .setMessage("Do you want to delete address.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //call api
                        initVolleyCallback();
                        Map<String, String> paramse = new HashMap<String, String>();
                        paramse.put("cusToken", session.get_customerToken());
                        paramse.put("addressToken",token);

                        RequestQueue mRequestQueueg = Volley.newRequestQueue(mContext);
                        mVolleyService = new VolleyService(mResultCallback, mContext);

                        mVolleyService.postStringDataVolley("DELIVERY_ADDRESS", DELETE_DELIVERY_ADDRESS, paramse, mRequestQueueg);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
