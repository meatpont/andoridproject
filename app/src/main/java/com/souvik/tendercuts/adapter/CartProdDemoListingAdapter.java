package com.souvik.tendercuts.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.souvik.tendercuts.activity.ItemListingActivity;
import com.souvik.tendercuts.activity.LoginActivity;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.OnRefreshCartViewListner;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.utils.MeatPointUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

/**
 * Created on 02/09/16.
 */
public class CartProdDemoListingAdapter extends RecyclerView.Adapter<CartProdDemoListingAdapter.MyViewHolder> {

    private Context mContext;
    private List<ItemModel> itemModelList;
    private ItemListingActivity adapterCallback;
    private OnRefreshCartViewListner mRefreshListner;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, smalldesc,price;//,quantity;
        public ImageView overflow;
        public CircleImageView thumbnail;
        public CardView card_view_category;
//        public Button add_to_cart,added_to_cart,buttonMinus,buttonPlus;
        public LinearLayout quantityAddLayout,cartListingIndRow;
        public DatabaseHandler db;
        SessionManager session;

        public MyViewHolder(View view) {
            super(view);
            setFontAwesome((LinearLayout)view.findViewById(R.id.cartListingIndRow),mContext);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (CircleImageView) view.findViewById(R.id.thumbnail);
//            quantity = (TextView)view.findViewById(R.id.quantityitem);
            smalldesc = (TextView) view.findViewById(R.id.smalldesc);
            price = (TextView) view.findViewById(R.id.price);
            quantityAddLayout = (LinearLayout)view.findViewById(R.id.quantityAddLayout);
//            buttonMinus = (Button)view.findViewById(R.id.buttonMinus);
//            buttonPlus = (Button)view.findViewById(R.id.buttonPlus);
            cartListingIndRow = (LinearLayout)view.findViewById(R.id.cartListingIndRow);

            db = new DatabaseHandler(mContext);
            session = SessionManager.getInstance(mContext);

           // mRefreshListner = (OnRefreshCartViewListner)mContext;

           /* //overflow = (ImageView) view.findViewById(R.id.overflow);
            card_view_category = (CardView)view.findViewById(R.id.card_view_category);

            add_to_cart = (Button)view.findViewById(R.id.add_to_cart);
            added_to_cart = (Button)view.findViewById(R.id.added_to_cart);

            adapterCallback = (ItemListingActivity)mContext;*/
        }
    }


    public CartProdDemoListingAdapter(Context mContext, List<ItemModel> itemModelList) {
        this.mContext = mContext;
        this.itemModelList = itemModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_prod_listing_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ItemModel itemModel = itemModelList.get(position);
        holder.title.setText(itemModel.getProdName());
        Picasso
                .with(mContext)
                .load(itemModel.getPic())
                .into(holder.thumbnail);
//        holder.quantity.setText(""+itemModel.getCartAddedQuantity());
        holder.smalldesc.setText(itemModel.getNetWeight());
        holder.price.setText("₹"+itemModel.getSellPrice());

//        holder.buttonMinus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            if (holder.session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
//                int quant = Integer.parseInt(holder.quantity.getText().toString());
//                if(quant>1){
//                    quant = quant - 1;
//                    holder.quantity.setText(""+quant);
//
//                    if(holder.db.getCartCount(new ItemModel(itemModel.getProdToken(),holder.session.get_customerToken()))>0) {
//                        int cartID = holder.db.getCartID(new ItemModel(itemModel.getProdToken(), holder.session.get_customerToken()));
//                        holder.db.updateCartProductQuantity(new ItemModel(quant, cartID));
//                        mRefreshListner.refreshView();
//                    }
//
//                }else {
//                    if(quant==1) {
//                        quant = quant - 1;
//                        holder.quantity.setText(""+quant);
//                        //holder.quantityAddLayout.setVisibility(View.GONE);
//                        int cartID = holder.db.getCartID(new ItemModel(itemModel.getProdToken(),holder.session.get_customerToken()));
//                        holder.db.deleteItemFromCart(cartID);
//
//                        itemModelList.remove(position);
//                        notifyItemRemoved(position);
//                        notifyItemRangeChanged(position, itemModelList.size());
//                        holder.cartListingIndRow.setVisibility(View.GONE);
//                        mRefreshListner.refreshView();
//                    }
//                }
//            }else{
//                Intent i = new Intent(mContext, LoginActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                mContext.startActivity(i);
//            }
//
//
//            }
//        });
//
//        holder.buttonPlus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            if (holder.session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
//                int quant = Integer.parseInt(holder.quantity.getText().toString());
//                if(quant>=1){
//                    quant = quant + 1;
//                    if(quant>itemModel.getStockAddedQuantity()){
//                        holder.quantity.setText("" + (quant-1));
//                        MeatPointUtils.decisionAlertOnMainThread(mContext,
//                                android.R.drawable.ic_dialog_info,
//                                R.string.error,
//                                "Product Quantity is not exceed from stock limit.",
//                                "Ok", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
//                                        dialog.dismiss();
//                                        //finish();
//                                    }
//                                }, "", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                        );
//                    }else {
//                        holder.quantity.setText("" + quant);
//                    /*itemModel.setCartAddedQuantity(quantity);
//                    itemModelList.remove(position);
//                    itemModelList.add(position,itemModel);*/
//                        if (holder.db.getCartCount(new ItemModel(itemModel.getProdToken(), holder.session.get_customerToken())) > 0) {
//                            int cartID = holder.db.getCartID(new ItemModel(itemModel.getProdToken(), holder.session.get_customerToken()));
//                            //Log.d("creation","ddd => "+cartID);
//                            if(holder.db.updateCartProductQuantity(new ItemModel(quant, cartID))>0){
//                                mRefreshListner.refreshView();
//                            }
//                            //Log.d("creation","ddio => "+holder.db.updateCartProductQuantity(new ItemModel(quant, cartID))+"");
//                        }
//
//                        //adapterCallback.itemQuantitychange(itemModelList,position);
//                    }
//                }
//            }else{
//                Intent i = new Intent(mContext, LoginActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                mContext.startActivity(i);
//            }
//
//            }
//        });

        /*if(holder.db.getCartCount(new ItemModel(itemModel.getProdToken().toString(),itemModel.getUserId()))<=0) {
            holder.add_to_cart.setVisibility(View.VISIBLE);
            holder.added_to_cart.setVisibility(View.GONE);
        }else{
            holder.add_to_cart.setVisibility(View.GONE);
            holder.added_to_cart.setVisibility(View.VISIBLE);
        }
        if(itemModel.isAddedCart()){
            holder.add_to_cart.setVisibility(View.GONE);
            holder.added_to_cart.setVisibility(View.VISIBLE);
        }else{ }

        try {
            Picasso
                    .with(mContext)
                    .load(itemModel.getPic())
                    .into(holder.thumbnail);

            holder.card_view_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d("creation","dd => "+itemModel.getName());
                    //itemModel.getProdName();
                    Intent i = new Intent(mContext, ProductDetailsActivity.class);
                    i.putExtra("catToken",String.valueOf(itemModel.getProdToken()));
                    mContext.startActivity(i);
                }
            });

            holder.added_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(holder.db.getCartCount(new ItemModel(itemModel.getProdToken().toString(),itemModel.getUserId()))>0){

                        int cartID = holder.db.getCartID(new ItemModel(itemModel.getProdToken().toString(),itemModel.getUserId()));
                        holder.db.deleteItemFromCart(cartID);

                        holder.add_to_cart.setVisibility(View.VISIBLE);
                        holder.added_to_cart.setVisibility(View.GONE);
                        //holder.db.addProductInCart(new ItemModel(itemModel.getProdToken().toString(),itemModel.getCartAddedQuantity(),itemModel.getUserId()));
                    }
                }
            });*/

           /* holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemModel.setAddedCart(true);
                    //Intent i = new Intent(mContext, ProductDetailsActivity.class);
                    //i.putExtra("catToken",String.valueOf(itemModel.getProdToken()));
                    holder.add_to_cart.setVisibility(View.GONE);
                    holder.added_to_cart.setVisibility(View.VISIBLE);



                    if(holder.db.getCartCount(new ItemModel(itemModel.getProdToken().toString(),itemModel.getUserId()))<=0){

                        holder.db.addProductInCart(new ItemModel(itemModel.getProdToken().toString(),itemModel.getCartAddedQuantity(),itemModel.getUserId()));

                    }



                    //adapterCallback.setItemModelList(itemModelList,itemModel.getIndex());

                    *//*HashMap<String, ItemModel> itemlistsHashmap = new HashMap<String, ItemModel>();
                    itemlistsHashmap.put("itemsDataHash",itemModel);
                    Bundle extras = new Bundle();
                    extras.putSerializable("itemsData", itemlistsHashmap);
                    i.putExtras(extras);
                    //i.putExtra("",String.valueOf(itemModel.getProdToken()));
                    mContext.startActivity(i);*//*
                }
            });*/

        /*} catch (Exception e) {
            e.printStackTrace();
        }*/

        /*holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });*/
    }

   /* public  interface ItemAdapterCallback {
        void setItemModelList(List<ItemModel> itemModelList, int pos);
    }*/

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }

    /*private void deleteItem(int position) {
        mDataSet.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mDataSet.size());
        holder.itemView.setVisibility(View.GONE);
    }*/
}
