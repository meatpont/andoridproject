package com.souvik.tendercuts.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.souvik.tendercuts.model.CartModel;
import com.souvik.tendercuts.model.ItemModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.co.meatpoint.R;

/**
 * Created on 02/09/16.
 */

public class ProdListingOrdSumAdapter extends ArrayAdapter<ItemModel> {

    public ProdListingOrdSumAdapter(Context context, ArrayList<ItemModel> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ItemModel cartitem = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.prod_listing_order_sum_row, parent, false);
        }


        // Lookup view for data population
        ImageView thumbnail = (ImageView)convertView.findViewById(R.id.thumbnail);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView price = (TextView) convertView.findViewById(R.id.price);
        TextView quantityitem = (TextView) convertView.findViewById(R.id.quantityitem);

        Picasso
                .with(getContext())
                .load(cartitem.getPic())
                .into(thumbnail);

        // Populate the data into the template view using the data object
        title.setText(cartitem.getProdName()+" ("+cartitem.getCartAddedQuantity()+")");
        price.setText(cartitem.getSellPrice());
//        quantityitem.setText(cartitem.getCartAddedQuantity());
        // Return the completed view to render on screen
        return convertView;
    }
}
