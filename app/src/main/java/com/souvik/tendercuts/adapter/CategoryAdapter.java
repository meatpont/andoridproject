package com.souvik.tendercuts.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.souvik.tendercuts.activity.BulkOrderActivity;
import com.souvik.tendercuts.activity.ItemListingActivity;
import com.souvik.tendercuts.activity.farhan.ListActivity;
import com.souvik.tendercuts.activity.farhan.TimeSlotActivity;
import com.souvik.tendercuts.model.CategoryModel;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import in.co.meatpoint.R;

/**
 * Created on 02/09/16.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<CategoryModel> categoryModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail, overflow;
        public CardView card_view_category;
        public RelativeLayout categoryLayout;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            //count = (TextView) view.findViewById(R.id.count);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            //overflow = (ImageView) view.findViewById(R.id.overflow);
            card_view_category = (CardView)view.findViewById(R.id.card_view_category);
            categoryLayout = (RelativeLayout)view.findViewById(R.id.categoryLayout);
        }
    }


    public CategoryAdapter(Context mContext, List<CategoryModel> categoryModelList) {
        this.mContext = mContext;
        this.categoryModelList = categoryModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_listing_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CategoryModel categoryModel = categoryModelList.get(position);
        holder.title.setText(categoryModel.getName());

        //holder.count.setText(categoryModel.getCatToken() + " songs");

        // loading categoryModel cover using Glide library
        //Glide.with(mContext).load(categoryModel.getThumbnail()).into(holder.thumbnail);
        try {
            Picasso
                    .with(mContext)
                    .load(categoryModel.getThumbnail())
                    .into(holder.thumbnail);

            holder.thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d("creation","dd => "+categoryModel.getCatToken());
                    //categoryModel.getCatToken();
                    if(categoryModel.getIs_bulk().equalsIgnoreCase("yes")){
                        Intent i = new Intent(mContext, BulkOrderActivity.class);
                        i.putExtra("catDetails", String.valueOf(categoryModel.getCategoryContent()));
                        mContext.startActivity(i);
                    }/*else if(categoryModel.getCatToken()==999){
                        Toast.makeText(mContext,"Todays Deals",Toast.LENGTH_LONG);
                    }*/else{
//                        Log.e("Token ", String.valueOf(categoryModel.getCategoryContent()));
                        Intent i = new Intent(mContext, ListActivity.class);// ItemListingActivity.class);
                        i.putExtra("catToken",Integer.parseInt(String.valueOf(categoryModel.getCatToken())));
                        i.putExtra("catName",String.valueOf(categoryModel.getName()));
                        i.putExtra("pos", position-1);
                        i.putExtra("catDetails", String.valueOf(categoryModel.getCategoryContent()));
                        mContext.startActivity(i);
                    }

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

        /*holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });*/
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
   /* private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }*/

    /**
     * Click listener for popup menu items
     */
    /*class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }*/

    @Override
    public int getItemCount() {
        return categoryModelList.size();
    }
}
