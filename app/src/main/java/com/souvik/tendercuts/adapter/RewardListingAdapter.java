package com.souvik.tendercuts.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.souvik.tendercuts.activity.OrderDetailsActivity;
import com.souvik.tendercuts.activity.OrderTrackActivity;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.model.RewardHistoryModel;

import java.util.List;

import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

/**
 * Created on 02/09/16.
 */
public class RewardListingAdapter extends RecyclerView.Adapter<RewardListingAdapter.MyViewHolder> {

    private Context mContext;
    private List<RewardHistoryModel> itemModelList;
   // private ItemListingActivity adapterCallback;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView pointdate,points,pointType;
        public LinearLayout rewardtransactionRow;
        public DatabaseHandler db;
        SessionManager session;

        public MyViewHolder(View view) {
            super(view);

            setFontAwesome((LinearLayout)view.findViewById(R.id.rewardtransactionRow),mContext);

            pointdate = (TextView)view.findViewById(R.id.pointdate);
            points = (TextView)view.findViewById(R.id.points);
            pointType = (TextView)view.findViewById(R.id.pointType);

            //adapterCallback = (ItemListingActivity)mContext;

            db = new DatabaseHandler(mContext);
            session = SessionManager.getInstance(mContext);
        }
    }


    public RewardListingAdapter(Context mContext, List<RewardHistoryModel> itemModelList) {
        this.mContext = mContext;
        this.itemModelList = itemModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reward_transaction_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final RewardHistoryModel itemModel = itemModelList.get(position);

        holder.pointdate.setText(itemModel.getPointdate());
        //holder.points.setText(itemModel.getPoints());
        holder.pointType.setText(itemModel.getPointType());
        if(itemModel.getPointType().equalsIgnoreCase("redeem")) {
            holder.points.setText("-₹"+itemModel.getPoints());
        }else{

            holder.points.setText("+₹"+itemModel.getPoints());
        }

    }

    public  interface ItemAdapterCallback {
        void setItemModelList(List<RewardHistoryModel> itemModelList, int pos);
    }

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }
}
