package com.souvik.tendercuts.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;

import com.souvik.tendercuts.helper.FontManager;

import java.io.IOException;

import in.co.meatpoint.R;


/**
 * Created by Anoop on 16-06-2017.
 */

public class MeatPointUtils {

    private static AlertDialog alert;
    private static AlertDialog decisionAlert;
    //================================================================

    public static void alert(Context c, int titleRes, String message) {
        alert(c, R.drawable.del_aa_common_error, titleRes, message, true, null);
    }

    public static void alert(Context c, int iconRes, int titleRes, String message) {
        alert(c, iconRes, titleRes, message, true, null);
    }

    public static void alert(Context c, int iconRes, int titleRes, String message, boolean foreground, DialogInterface.OnCancelListener cancelListener) {

        if (foreground) {

            try {
                if (alert != null && alert.isShowing()) {
                    //======Do nothing
                } else {

//                    AlertDialog.Builder alert = new AlertDialog.Builder(c, R.style.MyAlertDialogStyle);
                    AlertDialog.Builder alert = new AlertDialog.Builder(c);

                    try {
                        alert.setIcon(iconRes);
                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                        Log.i("", e.getMessage() + " at alert(...) of MeatPointUtils");
                    }

                    try {
                        alert.setTitle(titleRes);
                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                        Log.i("", e.getMessage() + " at alert(...) of MeatPointUtils");
                    }

                    alert.setMessage(message);
                    alert.setOnCancelListener(cancelListener);

                    MeatPointUtils.alert = alert.create();
                    MeatPointUtils.alert.setCanceledOnTouchOutside(true);
                    MeatPointUtils.alert.show();

                }
            } catch (NullPointerException e) {
                e.printStackTrace();
                Log.e("", e.getMessage() + " at alert(...) of MeatPointUtils");
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("", e.getMessage() + " at decisionAlert(...) of MeatPointUtils");
            }

        }

    }

    //================================================================
    /*public static void alert(NavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener, Object o, Context c, String message, boolean b) {
        alert(c, R.string.error, message);
    }*/


    //================================================================
    public static void alert(Context c, String message, DialogInterface.OnCancelListener cancelListener) {
        alert(c, R.drawable.del_aa_common_error, R.string.error, message, true, cancelListener);
    }

    public static void alert(Context c, int title, String message, DialogInterface.OnCancelListener cancelListener) {
        alert(c, R.drawable.del_aa_common_error, title, message, true, cancelListener);
    }


    //======================================================================
    public static boolean isNetworkConnected(Context ctx, boolean showAlert) {

        boolean status = false;
        try {

            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = cm.getActiveNetworkInfo();

            //Log.d("creation","ni.getState() => "+ni.getState());
            //Log.d("creation","ni.getType() => "+ni.getType());
            //Log.d("creation","ni.isAvailable() => "+ni.isAvailable());
            //Log.d("creation","ni.isAvailable() => "+ni.isConnected());
            //Log.d("creation","ni.isFailover() => "+ni.isFailover());
            //Log.d("creation","ni.getReason() => "+ni.getReason());

            status = false;

            if (ni != null && ni.isConnectedOrConnecting()) {
                status = true;
            } else {
                if (showAlert)
                    MeatPointUtils.showAlertOnMainThread(ctx, R.string.error, ctx.getResources().getString(R.string.no_internet));
                status = false;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
          //  Log.e(e.getMessage() + "At isNetworkConnected(...) of MeatPointUtils");
        } catch (Exception e) {
            e.printStackTrace();
            //Log.e(e.getMessage() + "At isNetworkConnected(...) of MeatPointUtils");
        }

        return status;

    }

    //======================================================================
    public static void showAlertOnMainThread(final Context ctx, final int titleRes, final String text) {

        try {
            if (ctx != null) {
                ((Activity) ctx).runOnUiThread(new Runnable() {

                    public void run() {
                        // TODO Auto-generated method stub
                        alert(ctx, titleRes, text);
                    }
                });
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.e("", e.getMessage() + " At showAlertOnMainThread(...) module of Utils class");
        } catch (ClassCastException e) {
            e.printStackTrace();
            Log.e("", e.getMessage() + " At showAlertOnMainThread(...) module of Utils class");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("", e.getMessage() + " At showAlertOnMainThread(...) module of Utils class");
        }

    }

    //======================================================
    public static boolean isInternetConnectionAvailable(Context ctx, boolean showAlert) {
        Runtime runtime = Runtime.getRuntime();
        boolean status = false;

        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            if(exitValue == 0) {
                status = true;
            }
            //status = false;

        } catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        if (!status) {
            if (showAlert) {
                MeatPointUtils.showAlertOnMainThread(ctx, R.string.error, ctx.getResources().getString(R.string.no_internet));
            }
        }

        return status;
        //return false;
    }

    //==============================================================
    /*public static void snackbarShow(String msg, View v){

        Snackbar snackbar = Snackbar.make(v, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }*/

    //======================================================================
    public static void decisionAlertOnMainThread(final Context c, final int titleRes,
                                                 final CharSequence message, final String positiveTitle, final DialogInterface.OnClickListener onPositiveClick,
                                                 final String negativeTitle, final DialogInterface.OnClickListener onNegativeClick) {
        decisionAlertOnMainThread(c, R.drawable.del_aa_common_error, titleRes, message, positiveTitle, onPositiveClick, negativeTitle, onNegativeClick);
    }

    public static void decisionAlertOnMainThread(final Context c, final int iconRes, final int titleRes,
                                                 final CharSequence message, final String positiveTitle, final DialogInterface.OnClickListener onPositiveClick,
                                                 final String negativeTitle, final DialogInterface.OnClickListener onNegativeClick) {

        try {
            ((Activity) c).runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    decisionAlert(c, iconRes, titleRes, message, positiveTitle, onPositiveClick, negativeTitle, onNegativeClick);
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.e("MeatPointUtils", e.getMessage() + "At decisionAlertOnMainThread(...) of Utils");
        } catch (ClassCastException e) {
            e.printStackTrace();
            Log.e("MeatPointUtils", e.getMessage() + "At decisionAlertOnMainThread(...) of Utils");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("MeatPointUtils", e.getMessage() + "At decisionAlertOnMainThread(...) of Utils");
        }

    }

    public static void decisionAlert(Context c, int titleRes, CharSequence message, String positiveTitle, DialogInterface.OnClickListener onPositiveClick, String negativeTitle, DialogInterface.OnClickListener onNegativeClick) {
        decisionAlert(c, R.drawable.del_aa_common_error, titleRes, message, positiveTitle, onPositiveClick, negativeTitle, onNegativeClick);
    }

    public static void decisionAlert(Context c, int iconRes, int titleRes, CharSequence message, String positiveTitle, DialogInterface.OnClickListener onPositiveClick, String negativeTitle, DialogInterface.OnClickListener onNegativeClick) {

        try {
            if (decisionAlert != null && decisionAlert.isShowing()) {
                //======Do nothing
            } else {

                //AlertDialog.Builder builder = new AlertDialog.Builder(c, R.style.MyAlertDialogStyle);
                AlertDialog.Builder builder = new AlertDialog.Builder(c);

                try {
                    builder.setIcon(iconRes);
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                    Log.e("MeatPointUtils", e.getMessage() + " At decisionAlert(...) of Utils");
                }

                try {
                    builder.setTitle(titleRes);
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                    Log.e("MeatPointUtils", e.getMessage() + " At decisionAlert(...) of Utils");
                }

                builder.setMessage(message);
                builder.setCancelable(!(onPositiveClick != null || onNegativeClick != null));

                if (onPositiveClick != null) {
                    builder.setPositiveButton(positiveTitle, onPositiveClick);
                }

                if (onNegativeClick != null) {
                    builder.setNegativeButton(negativeTitle, onNegativeClick);
                }


                decisionAlert = builder.create();
                decisionAlert.show();

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.e("MeatPointUtils", e.getMessage() + " at decisionAlert(...) of Utils");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("MeatPointUtils", e.getMessage() + " at decisionAlert(...) of Utils");
        }

    }

    //======================================================================

    public static void setFontAwesome(View layout, Context activitythis){
        //LinearLayout l = (LinearLayout)findViewById(R.id.referalLinear);
        Typeface iconFont = FontManager.getTypeface(activitythis, FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(layout, iconFont);

    }
}
