package com.souvik.tendercuts.utils;

/**
 * Created by Souvik Chaudhury on 08-12-2017.
 */

public interface CartAddNotify {
    void onItemSelected(int k);
}
