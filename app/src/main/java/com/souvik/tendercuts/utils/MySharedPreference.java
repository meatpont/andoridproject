package com.souvik.tendercuts.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class MySharedPreference {

	private static final String TAG = "MySharedPreference";
	private static String deviceTknForFcm;

	//===================================
//	public static void storeLocalLoginUsername(Context context, String username)
//	{
//	    Preferences pref = new Preferences(context);
//	    pref.set(keychainKeys.usrName.toString(), username);
//	    pref.commit();
//	}
//
//	public static String storedLocalLoginUsername(Context context)
//	{
//	    Preferences pref = new Preferences(context);
//	    return pref.get(keychainKeys.usrName.toString());
//	}
	//===================================


	/*public static void saveDeviceToken(Context context, String token){
		Preferences pref = new Preferences(context);
		String tokenStr;

		if(token == null){
			tokenStr = "FCM token is null";
		}
		else{
			tokenStr = token;
		}

		pref.set(MySharedPreference.deviceTknForFcm.toString(), tokenStr);
		pref.commit();
	}
	public static String getDeviceToken(Context context) throws Exception {

		Preferences pref = new Preferences(context);
		String accTokenFcm = pref.get(MySharedPreference.deviceTknForFcm.toString());

		if(accTokenFcm.isEmpty()){
			//throw new JResponseError("Auth Token is empty. ");
		}
		return accTokenFcm;
	}*/


	private static final String SHARED_PREF_NAME = "FCMSharedPref";
	private static final String TAG_TOKEN = "tagtoken";

	private static MySharedPreference mInstance;
	private static Context mCtx;

	private MySharedPreference(Context context) {
		mCtx = context;
	}

	public static synchronized MySharedPreference getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new MySharedPreference(context);
		}
		return mInstance;
	}

	//this method will save the device token to shared preferences
	public boolean saveDeviceToken(String token){
		SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(TAG_TOKEN, token);
		editor.apply();
		return true;
	}

	//this method will fetch the device token from shared preferences
	public String getDeviceToken(){
		SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return  sharedPreferences.getString(TAG_TOKEN, null);
	}
	public boolean setDeviceRegisteredToServer(Boolean check){
		SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean("isDeviceRegistered", check);
		editor.apply();
		return true;
	}

	//this method will fetch the device token from shared preferences
	public boolean isDeviceRegistered(){
		SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return  sharedPreferences.getBoolean("isDeviceRegistered", false);
	}
	public boolean setMemberShipData(String memberSgipName, String price){
		SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString("mamberShipName", memberSgipName);
		editor.putString("mamberShipPrice", price);
		editor.apply();
		return true;
	}

	//this method will fetch the device token from shared preferences
	public String getMemberShipName(){
		SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return  sharedPreferences.getString("mamberShipName", null);
	}
	public String getMemberShipPrice(){
		SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return  sharedPreferences.getString("mamberShipPrice", null);
	}
	public void setSearchingDataSavedInDB(boolean status)
	{
		SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean("SETDATASAVEDINDB", status);
		editor.apply();
	}
	public boolean getSearchingDataSavedStatusInDB() {
		SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return  sharedPreferences.getBoolean("SETDATASAVEDINDB", false);
	}

}

