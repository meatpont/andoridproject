package com.souvik.tendercuts.activity;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.souvik.tendercuts.activity.farhan.model.TimeHolder;
import com.souvik.tendercuts.adapter.CartListingAdapter;
import com.souvik.tendercuts.adapter.CartProdDemoListingAdapter;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.payyoumoney.AppPreference;
import com.souvik.tendercuts.utils.MeatPointUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.CALCULATE_CART;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class OrderSumActivity extends AppCompatActivity {

    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
    private AppPreference mAppPreference;
    ArrayList<ItemModel> firstItem;
    ArrayList<ItemModel> secondItem;

    private ProgressDialog pDialog;
    CardView cardView, cardView2;

    LinearLayout fb_totLayout, fb_redemedPointsLayout, fb_deliveryOptionLayout,
            fb_gstOptionLayout, fb_discountOptionLayout, fb_earnPointsLayout,
            fb_couponlayout, fb_grandTotalLayout, couponappliedlayout, payableAmountLayout;

    TextView fb_totalPrice, fb_redeemedPoint, fb_deliveryFee, fb_gstValue, fb_discountFee, fb_points, fb_couponpriceminus,
            fb_grandTotal, payableAmount, shippingname, shippingmobile, shippingaddress, fb_deliveredDate, fb_deliveredDate2, fb_cartaddedQuantity, fb_totalPriceButtom, extraInfoText, specialInstructionHeading;

    private String pm_custToken = "", pm_products = "", pm_deliveryMode = "", pm_deliveryDate = "",
            pm_deliveryTime = "", pm_deliveryDate2 = "", pm_deliveryTime2 = "", pm_deliveryAddres = "",
            pm_checkCouponCode = "no", pm_couponCode = "", pm_checkRewardPoint = "no", pm_paymentMethod = "no",
            pm_extraInfo = "", title1 = "", title2 = "";
    ArrayList<ItemModel> items, cartItems;

    private RecyclerView recyclerView, recyclerViewSecond;
    CartProdDemoListingAdapter adapter, adaptersecond;

    @BindView(R.id.firstDeliveryValue)
    TextView firstDeliveryValue;
    @BindView(R.id.firstDeliveryCharge)
    TextView firstDeliveryCharge;
    @BindView(R.id.secondDeliveryValue)
    TextView secondDeliveryValue;
    @BindView(R.id.secondDeliveryCharge)
    TextView secondDeliveryCharge;

    //first delivery slot
    @BindView(R.id.tv_day1)
    TextView day1;
    @BindView(R.id.tv_date1)
    TextView date1;
    @BindView(R.id.tv_time1)
    TextView time1;
    //second delivery slot
    @BindView(R.id.tv_day)
    TextView day;
    @BindView(R.id.tv_date)
    TextView date;
    @BindView(R.id.tv_time)
    TextView time;

    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.layout_parent)
    LinearLayout layout_parent;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_sum);

        mAppPreference = new AppPreference();
        ButterKnife.bind(OrderSumActivity.this);
        setFontAwesome((RelativeLayout) findViewById(R.id.orderSummaryLayout), OrderSumActivity.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(OrderSumActivity.this);
        pDialog = new ProgressDialog(OrderSumActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setVisibility(View.VISIBLE);
        titleToolbar.setText("Order Summary");

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        ImageView imageView5 = (ImageView) findViewById(R.id.imageView5);
        imageView5.setVisibility(View.GONE);

        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layout_parent.setVisibility(View.GONE);

        initVolleyCallback();

        firstItem = new ArrayList<ItemModel>();
        secondItem = new ArrayList<ItemModel>();

        cardView = findViewById(R.id.card_view_product);
        cardView2 = findViewById(R.id.card_view_product2);

        fb_totLayout = (LinearLayout) findViewById(R.id.totLayout);
        fb_redemedPointsLayout = (LinearLayout) findViewById(R.id.redemedPointsLayout);
        fb_deliveryOptionLayout = (LinearLayout) findViewById(R.id.deliveryOptionLayout);
        fb_gstOptionLayout = (LinearLayout) findViewById(R.id.gstOptionLayout);
        fb_earnPointsLayout = (LinearLayout) findViewById(R.id.earnPointsLayout);
        fb_couponlayout = (LinearLayout) findViewById(R.id.couponlayout);
        fb_grandTotalLayout = (LinearLayout) findViewById(R.id.grandTotalLayout);
        payableAmountLayout = (LinearLayout) findViewById(R.id.payableAmountLayout);
        fb_discountOptionLayout = (LinearLayout) findViewById(R.id.discountOptionLayout);
        couponappliedlayout = (LinearLayout) findViewById(R.id.couponlayout);

        fb_totalPrice = (TextView) findViewById(R.id.totalPrice);
        fb_redeemedPoint = (TextView) findViewById(R.id.redeemedPoint);
        fb_deliveryFee = (TextView) findViewById(R.id.deliveryFee);
        fb_gstValue = (TextView) findViewById(R.id.gstValue);
        fb_points = (TextView) findViewById(R.id.points);
        fb_couponpriceminus = (TextView) findViewById(R.id.couponpriceminus);
        fb_grandTotal = (TextView) findViewById(R.id.grandTotal);
        payableAmount = (TextView) findViewById(R.id.payableAmount);
        fb_discountFee = (TextView) findViewById(R.id.discountFee);
        shippingname = (TextView) findViewById(R.id.shippingname);
        shippingmobile = (TextView) findViewById(R.id.shippingmobile);
        shippingaddress = (TextView) findViewById(R.id.shippingaddress);
        fb_deliveredDate = (TextView) findViewById(R.id.deliveredDate);
        fb_deliveredDate2 = (TextView) findViewById(R.id.deliveredDate2);
        fb_cartaddedQuantity = (TextView) findViewById(R.id.cartaddedQuantity);
        fb_totalPriceButtom = (TextView) findViewById(R.id.totalPriceButtom);

        extraInfoText = (TextView) findViewById(R.id.extraInfoText);
        specialInstructionHeading = (TextView) findViewById(R.id.specialInstructionHeading);

        items = new ArrayList<ItemModel>();
        cartItems = new ArrayList<ItemModel>();
        firstItem = new ArrayList<ItemModel>();
        secondItem = new ArrayList<ItemModel>();

        adapter = new CartProdDemoListingAdapter(OrderSumActivity.this, firstItem);
        recyclerView = (RecyclerView) findViewById(R.id.prodListsRecycler);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager1);
        recyclerView.setAdapter(adapter);

        adaptersecond = new CartProdDemoListingAdapter(OrderSumActivity.this, secondItem);
        recyclerViewSecond = (RecyclerView) findViewById(R.id.prodListsRecycler2);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewSecond.setLayoutManager(mLayoutManager2);
        recyclerViewSecond.setAdapter(adaptersecond);
        Log.e("Flag ", "" + getIntent().getStringExtra("flag"));
        if (getIntent().getStringExtra("flag").equalsIgnoreCase("1"))
            cardView2.setVisibility(View.VISIBLE);
        else
            cardView2.setVisibility(View.GONE);

//        recyclerView.setAdapter(adapter);


        pm_custToken = session.get_customerToken();
        pm_products = getIntent().getStringExtra("products");
        pm_deliveryMode = getIntent().getStringExtra("deliveryMode");
        // pm_deliveryDate = getIntent().getStringExtra("deliveryDate");
        pm_deliveryTime = getIntent().getStringExtra("deliveryTime");
        // pm_deliveryDate2 = getIntent().getStringExtra("deliveryDate2");
        pm_deliveryTime2 = getIntent().getStringExtra("deliveryTime2");
        pm_deliveryAddres = getIntent().getStringExtra("deliveryAddress");
        pm_checkCouponCode = getIntent().getStringExtra("checkCouponCode");
        pm_couponCode = getIntent().getStringExtra("couponCode");
        pm_checkRewardPoint = getIntent().getStringExtra("checkRewardPoint");
        pm_extraInfo = getIntent().getStringExtra("extraInfo");

        title1 = getIntent().getStringExtra("titledt1");
        title2 = getIntent().getStringExtra("titledt2");
        pm_paymentMethod = "yes";


//        if(pm_deliveryMode.equalsIgnoreCase("1")){
//            // Express Delivery Mode
////        Log.e("Time ", new TimeHolder().getTime());
//            fb_deliveredDate.setText("Delivered Today within 90 min");
//        }else{
//            //Regular Delivery Mode
        Log.e("time ", title1 + " " + title2);
        //for deliver first
        String[] dy = title1.split(" ");
        day1.setText(dy[0]);

        pm_deliveryDate = dy[1] + " " + dy[2];

        date1.setText(dy[1] + " " + dy[2]);
//        date1.setTypeface(null, 1);

        // time1.setTypeface(null, 1);
        time1.setText(pm_deliveryTime);

//            fb_deliveredDate.setText("Delivered on "+builder);//+pm_deliveryDate+" - "+pm_deliveryTime);
//        fb_deliveredDate.setTypeface(null, 1);
//for deliver second

        String[] dyy = title2.split(" ");
        day.setText(dyy[0] + " ");

        date.setText(dyy[1] + " " + dyy[2]);
        pm_deliveryDate2 = dyy[1] + " " + dyy[2];
//        time.setTypeface(null, 1);

        time.setText(pm_deliveryTime2);


//
// b_deliveredDate2.setTypeface(null,1);
//        }

        paymentConfirmPageLoad(pm_custToken, pm_products, pm_deliveryMode, pm_deliveryDate, pm_deliveryTime, pm_deliveryDate2, pm_deliveryTime2, pm_deliveryAddres, pm_checkCouponCode, pm_couponCode, pm_checkRewardPoint, pm_extraInfo);
        //setupCitrusConfigs();

        final Button proceedBtn = (Button) findViewById(R.id.proceedBtn);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceed();
            }
        });

        TextView proceedText = (TextView) findViewById(R.id.proceedText);
        proceedText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceed();

            }
        });

    }

    private void paymentConfirmPageLoad(String custToken, String products, String deliveryMode, String deliveryDate, String deliveryTime, String deliveryDate2, String deliveryTime2, String deliveryAddres, String checkCouponCode, String couponCode, String checkRewardPoint, String extraInfo) {
        initVolleyCallback();
        showDialog();


        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", custToken);
        paramse.put("products", products);
        paramse.put("delivery_mode", deliveryMode);
        paramse.put("first_delivery_date", deliveryDate);
        paramse.put("first_delivery_time", deliveryTime);
        paramse.put("second_delivery_date", deliveryDate2);
        paramse.put("second_delivery_time", deliveryTime2);
        paramse.put("delivery_address", deliveryAddres);
        paramse.put("check_coupon_code", checkCouponCode);
        paramse.put("coupon_code", couponCode);
        paramse.put("check_reward_point", checkRewardPoint);
        paramse.put("extra_info", extraInfo);
        Log.e("Order sum post ", String.valueOf(paramse));

        RequestQueue mRequestQueueg = Volley.newRequestQueue(OrderSumActivity.this);
        mVolleyService = new VolleyService(mResultCallback, OrderSumActivity.this);
        mVolleyService.postStringDataVolley("CALCULATE_CART", CALCULATE_CART, paramse, mRequestQueueg);
    }

    void initVolleyCallback() {

        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

                if (requestType.equalsIgnoreCase("CALCULATE_CART")) {
                    try {
                        hideDialog();
                        layout_parent.setVisibility(View.VISIBLE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status_code = jsonObject.getInt("status");
                            if (status_code == 1) {

                                JSONObject responseObject = new JSONObject(jsonObject.getString("response"));

                                fb_totalPrice.setText("₹" + responseObject.getString("sub_total"));
                                if (!responseObject.getString("sub_total").isEmpty() && Double.parseDouble(responseObject.getString("sub_total")) > 0) {
                                    fb_totLayout.setVisibility(View.VISIBLE);
                                }

                                fb_discountFee.setText("(-)₹" + responseObject.getString("discount_amount"));
                                if (!responseObject.getString("discount_amount").isEmpty() && Double.parseDouble(responseObject.getString("discount_amount")) > 0) {
                                    fb_discountOptionLayout.setVisibility(View.VISIBLE);
                                }

                                fb_deliveryFee.setText("₹" + responseObject.getString("delivery_charge"));
                                if (!responseObject.getString("delivery_charge").isEmpty() && Double.parseDouble(responseObject.getString("delivery_charge")) > 0) {
                                    fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
                                }

                                fb_gstValue.setText("₹" + responseObject.getString("gst_value"));
                                if (!responseObject.getString("gst_value").isEmpty() && Double.parseDouble(responseObject.getString("gst_value")) > 0) {
                                    fb_gstOptionLayout.setVisibility(View.VISIBLE);
                                }

                                //fb_grandTotal.setText("₹"+responseObject.getString("grand_total"));
                                fb_grandTotal.setText("₹" + responseObject.getString("grand_total"));
                                if (!responseObject.getString("grand_total").isEmpty() && Double.parseDouble(responseObject.getString("grand_total")) > 0) {
                                    fb_grandTotalLayout.setVisibility(View.VISIBLE);
                                }

                                fb_redeemedPoint.setText("(-)₹" + responseObject.getString("redeemed_point"));
                                if (!responseObject.getString("redeemed_point").isEmpty() && Double.parseDouble(responseObject.getString("redeemed_point")) > 0) {
                                    fb_redemedPointsLayout.setVisibility(View.VISIBLE);
                                }

                                fb_points.setText(responseObject.getString("earn_points") + " Points");
//                                if (!responseObject.getString("earn_points").isEmpty() && Double.parseDouble(responseObject.getString("earn_points")) > 0) {
                                    fb_earnPointsLayout.setVisibility(View.VISIBLE);
//                                }

                                payableAmount.setText("₹" + responseObject.getString("payable_amount"));
                                fb_totalPriceButtom.setText("₹" + responseObject.getString("payable_amount"));
                                if (!responseObject.getString("payable_amount").isEmpty() && Double.parseDouble(responseObject.getString("payable_amount")) > 0) {
                                    payableAmountLayout.setVisibility(View.VISIBLE);
                                }

                                int quantity = 0;
                                JSONArray productlistArrayy = responseObject.getJSONArray("in_stock");
                                for (int i = 0; i < productlistArrayy.length(); i++) {
                                    JSONObject productEachRow = productlistArrayy.getJSONObject(i);
                                    quantity = quantity + Integer.parseInt(productEachRow.getString("cartQuan"));
                                    Log.e("Quantity", productEachRow.getString("cartQuan"));
                                }
                                fb_cartaddedQuantity.setText("" + quantity);


                                if (!responseObject.getString("coupon_code").isEmpty()) {
                                    if (responseObject.getInt("coupon_applied")==1)
                                        couponappliedlayout.setVisibility(View.VISIBLE);
                                    else
                                        couponappliedlayout.setVisibility(View.GONE);
//                                    redemedPointsLayout.setVisibility(View.GONE);
//                                    couponTextlayout.setVisibility(View.GONE);
                                }else {
                                    couponappliedlayout.setVisibility(View.GONE);
                                }
                                //responseObject.getString("coupon_code");
                                //responseObject.getString("payable_amount");
                                JSONObject jj = new JSONObject(responseObject.getString("default_address"));
                                pm_deliveryAddres = jj.getString("shipToken");
                                String fullAdress = jj.getString("shipAddress").trim() + ",\n" +
                                        jj.getString("shipCity").trim() + ",\n" +
                                        jj.getString("shipPostcode").trim() + ",\n" +
                                        jj.getString("shipEmail").trim() + "\n" +
                                        jj.getString("shipPhone").trim();

                                shippingname.setText(jj.getString("shipName"));
                                shippingmobile.setVisibility(View.GONE);//.setText("("+jj.getString("")+")");
                                shippingaddress.setText(fullAdress);

                                JSONArray productlistArray = responseObject.getJSONArray("in_stock");
//                                for (int i = 0; i < productlistArray.length(); i++) {
//                                    JSONObject productEachRow = productlistArray.getJSONObject(i);
//
//                                    //ItemModel cartItemsList = items.get(i);
//
//                                    ItemModel m = new ItemModel();
//                                    m.setProdToken(productEachRow.getString("prodToken"));
//                                    m.setProdName(productEachRow.getString("prodName"));
//                                    m.setProdDesc(productEachRow.getString("prodDesc"));
////                                    m.setMarketPrice(productEachRow.getString("marketPrice"));
//                                    m.setSellPrice(productEachRow.getString("sellPrice"));
//                                    m.setPic(productEachRow.getString("pic"));
//                                    m.setCartAddedQuantity(productEachRow.getInt("cartQuan"));
//                                    m.setStockAddedQuantity(productEachRow.getInt("stockQuan"));
//                                    m.setNetWeight(productEachRow.getString("netWeight"));
//                                    cartItems.add(m);
////                                    Log.d("creation", " => " + productEachRow.getString("prodName"));
//                                }
                                try {
                                    JSONObject objectFirst = responseObject.getJSONObject("first");
                                    JSONArray arrayFirst = objectFirst.getJSONArray("items");
                                    double price = 0;
                                    firstItem.clear();
                                    for (int i = 0; i < arrayFirst.length(); i++) {
                                        JSONObject objectItem = arrayFirst.getJSONObject(i);
                                        ItemModel im = new ItemModel();
                                        im.setProdToken(objectItem.getString("prodToken"));
                                        im.setProdName(objectItem.getString("prodName"));
                                        im.setSellPrice(objectItem.getString("sellPrice"));
                                        im.setPic(objectItem.getString("pic"));
                                        im.setCartAddedQuantity(objectItem.getInt("cartQuan"));
                                        im.setStockAddedQuantity(objectItem.getInt("stockQuan"));
                                        im.setNetWeight(objectItem.getString("netWeight"));
                                        firstItem.add(im);
                                        price += Double.parseDouble(objectItem.getString("sellPrice"));
                                    }
                                    firstDeliveryValue.setText("₹" + price);
                                    firstDeliveryCharge.setText("₹" + objectFirst.getString("delivery_charge"));
                                    adapter.notifyDataSetChanged();
                                } catch (Exception e) {
                                    Log.e("Exception ", "First " + e.getMessage().toString());
                                    cardView.setVisibility(View.GONE);
                                }
                                try {
                                    JSONObject objectSecond = responseObject.getJSONObject("second");
                                    JSONArray arraySecond = objectSecond.getJSONArray("items");
                                    secondItem.clear();
                                    double price2 = 0;
                                    for (int i = 0; i < arraySecond.length(); i++) {
                                        JSONObject objectItem = arraySecond.getJSONObject(i);
                                        ItemModel im = new ItemModel();
                                        im.setProdToken(objectItem.getString("prodToken"));
                                        im.setProdName(objectItem.getString("prodName"));
                                        im.setSellPrice(objectItem.getString("sellPrice"));
                                        im.setPic(objectItem.getString("pic"));
                                        im.setCartAddedQuantity(objectItem.getInt("cartQuan"));
                                        im.setStockAddedQuantity(objectItem.getInt("stockQuan"));
                                        im.setNetWeight(objectItem.getString("netWeight"));
                                        secondItem.add(im);
                                        price2 += Double.parseDouble(objectItem.getString("sellPrice"));
                                    }
                                    secondDeliveryValue.setText("₹" + price2);
                                    secondDeliveryCharge.setText("₹" + objectSecond.getString("delivery_charge"));
                                    adaptersecond.notifyDataSetChanged();

                                } catch (Exception e) {
                                    Log.e("Exception ", "Second " + e.getMessage().toString());
                                    cardView2.setVisibility(View.GONE);
                                }

                                extraInfoText.setText(responseObject.getString("extra_info"));
                                if (!responseObject.getString("extra_info").isEmpty() && responseObject.getString("extra_info").length() > 0) {
                                    extraInfoText.setVisibility(View.VISIBLE);
                                    specialInstructionHeading.setVisibility(View.VISIBLE);
                                    view1.setVisibility(View.VISIBLE);
                                    view2.setVisibility(View.VISIBLE);

                                }

                                /*LinearLayout prodLists = (LinearLayout)findViewById(R.id.prodLists);
                                LinearLayout parent = null;

                                JSONArray productlistArray = responseObject.getJSONArray("in_stock");
                                for (int i = 0; i < productlistArray.length(); i++) {
                                    JSONObject productEachRow = productlistArray.getJSONObject(i);

                                    /*//**************************************************************************************************
                                 parent = new LinearLayout(OrderSumActivity.this);

                                 parent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                 parent.setPadding(30,5,0,0);
                                 parent.setOrientation(LinearLayout.HORIZONTAL);
                                 parent.setBackground(getResources().getDrawable(R.drawable.layout_border_plain));
                                 //children of parent linearlayout

                                 ImageView iv = new ImageView(OrderSumActivity.this);
                                 LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 200);
                                 iv.setLayoutParams(layoutParams);
                                 //iv.setBackground(getResources().getDrawable(R.drawable.layout_border_plain));
                                 iv.setPadding(5,5,5,5);
                                 iv.setScaleType(ImageView.ScaleType.FIT_XY);

                                 LinearLayout layout2 = new LinearLayout(OrderSumActivity.this);

                                 layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                 layout2.setPadding(25,35,0,0);
                                 layout2.setOrientation(LinearLayout.VERTICAL);

                                 parent.addView(iv);
                                 parent.addView(layout2);

                                 //children of layout2 LinearLayout

                                 TextView tv1 = new TextView(OrderSumActivity.this);
                                 TextView tv2 = new TextView(OrderSumActivity.this);

                                 tv1.setTextColor(getResources().getColor(R.color.appThemegreencolor));
                                 layout2.addView(tv1);
                                 layout2.addView(tv2);

                                 tv1.setText(productEachRow.getString("prodName")+" ("+Integer.parseInt(productEachRow.getString("cartQuan"))+") ");
                                 tv2.setText(productEachRow.getString("sellPrice"));

                                 Picasso
                                 .with(OrderSumActivity.this)
                                 .load(productEachRow.getString("pic"))
                                 .into(iv);

                                 prodLists.addView(parent);
                                 }*/
                            }
                        } catch (Exception e) {
                            Log.e("Exc ", e.getMessage().toString());
                        }

                        Log.d("creation", "Total Response POST: " + response);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void proceed() {

                Intent i = new Intent(OrderSumActivity.this, PaymentConfirm.class);
                i.putExtra("products", pm_products);
                i.putExtra("deliveryMode", pm_deliveryMode);
                i.putExtra("deliveryDate", pm_deliveryDate);
                i.putExtra("deliveryTime", pm_deliveryTime);
                i.putExtra("deliveryDate2", pm_deliveryDate2);
                i.putExtra("deliveryTime2", pm_deliveryTime2);
                i.putExtra("deliveryAddress", pm_deliveryAddres);
                i.putExtra("checkCouponCode", pm_checkCouponCode);
                i.putExtra("couponCode", pm_couponCode);
                i.putExtra("checkRewardPoint", pm_checkRewardPoint);
                i.putExtra("extraInfo", pm_extraInfo);

                startActivity(i);

//        Intent i = new Intent(OrderSumActivity.this, PaymentConfirm.class);
//        i.putExtra("products", pm_products);
//        i.putExtra("deliveryMode", pm_deliveryMode);
//        i.putExtra("deliveryDate", pm_deliveryDate);
//        i.putExtra("first_delivery_time", pm_deliveryTime);
//        i.putExtra("deliveryDate2", pm_deliveryDate2);
//        i.putExtra("deliveryTime2", pm_deliveryTime2);
//        i.putExtra("deliveryAddress", pm_deliveryAddres);
//        i.putExtra("checkCouponCode", pm_checkCouponCode);
//        i.putExtra("couponCode", pm_couponCode);
//        i.putExtra("checkRewardPoint", pm_checkRewardPoint);
//        i.putExtra("extraInfo", pm_extraInfo);
//        startActivity(i);
    }
}
