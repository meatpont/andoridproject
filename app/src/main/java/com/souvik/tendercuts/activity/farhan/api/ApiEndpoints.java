package com.souvik.tendercuts.activity.farhan.api;

/**
 * Created by Md Farhan Raja on 25-Feb-18.
 */

public class ApiEndpoints
{
    private static final String BASEURL="http://meatpoint.co.in/api/v1/";
    public static final String GET_TIME_SLOTS = BASEURL+ "Cart/calculate_cart";
}
