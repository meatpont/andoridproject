package com.souvik.tendercuts.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.farhan.OtpVerification;
import com.souvik.tendercuts.config.AppConfig;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.MOBILE_VERIFY;
import static com.souvik.tendercuts.config.AppConfig.SEND_OTP;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class ForgetPasswordActivity extends AppCompatActivity {

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    SessionManager session;
    public DatabaseHandler db;

    private ProgressDialog pDialog;

    Button sendotp;
    EditText phoneno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        setFontAwesome((RelativeLayout) findViewById(R.id.forgetpasswordlayout), ForgetPasswordActivity.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(ForgetPasswordActivity.this);

        pDialog = new ProgressDialog(ForgetPasswordActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setText("Forgot Password?");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ForgetPasswordActivity.this, HomeActivity.class));
                finish();
            }
        });
        phoneno = (EditText) findViewById(R.id.phoneno);
        sendotp = (Button) findViewById(R.id.sendotp);

        sendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(phoneno.length()==10) {
//                startActivity(new Intent(ForgetPasswordActivity.this, OtpVerification.class));
                    initVolleyCallback();
                    showDialog();
                    mVolleyService = new VolleyService(mResultCallback, ForgetPasswordActivity.this);

                    HashMap<String, String> loginDataPost = new HashMap<String, String>();
                    loginDataPost.put("mobile", phoneno.getText().toString());

                    RequestQueue requestQueue = Volley.newRequestQueue(ForgetPasswordActivity.this);
                    mVolleyService.postStringDataVolley("FORGET_PASSWORD", SEND_OTP, loginDataPost, requestQueue);
                }else {
                    MeatPointUtils.decisionAlertOnMainThread(ForgetPasswordActivity.this,
                            android.R.drawable.ic_dialog_info,
                            R.string.error,
                            "Enter register mobile number.",
                            "Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                    dialog.dismiss();
                                    //finish();
                                }
                            }, "", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                }
                            }
                    );
                }
            }
        });

    }

    void initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response : " + response);
                hideDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        HashMap<String, String> detailsMap = new HashMap<String, String>();


                        JSONObject loginDetails = jsonObject.getJSONObject("response");
                        Intent intent = new Intent(ForgetPasswordActivity.this, OtpVerification.class);
                        intent.putExtra("mobile", loginDetails.getString("mobile"));
                        intent.putExtra("otp", loginDetails.getString("otp"));
                        intent.putExtra("type", "reset");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        Log.d("creation", loginDetails.getString("cusToken"));
                        Log.d("creation", loginDetails.getString("otp"));
                    }
                    else if(status==0){
                        MeatPointUtils.decisionAlertOnMainThread(ForgetPasswordActivity.this,
                                android.R.drawable.ic_dialog_info,
                                R.string.error,
                                "Enter register mobile number.",
                                "Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                        dialog.dismiss();
                                        //finish();
                                    }
                                }, "", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        );

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                hideDialog();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ForgetPasswordActivity.this, HomeActivity.class));
        finish();
    }

}
