package com.souvik.tendercuts.activity.farhan.model;

/**
 * Created by Sushil on 3/14/2018.
 */

public class CategoryHolder {
//    "catToken": "888",
//            "catName": "Bulk Order",
//            "pic": "http://meatpoint.co.in/pichub/uploads/bulk.jpg",
//            "is_bulk": "yes"

    String catToken, catName, pic, is_bulk;

    public String getCatToken() {
        return catToken;
    }

    public void setCatToken(String catToken) {
        this.catToken = catToken;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getIs_bulk() {
        return is_bulk;
    }

    public void setIs_bulk(String is_bulk) {
        this.is_bulk = is_bulk;
    }
}
