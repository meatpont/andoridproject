package com.souvik.tendercuts.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.adapter.OrderListingAdapter;
import com.souvik.tendercuts.common.Action_menu_button;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.AddressModel;
import com.souvik.tendercuts.model.OrderHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.GET_DELIVERY_ADDRESS;
import static com.souvik.tendercuts.config.AppConfig.GET_ORDER;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class OrderListingActivity extends AppCompatActivity {

    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    private OrderListingAdapter adapter;
    private List<OrderHistoryModel> orderListsItems;
    private RecyclerView recyclerView;

    TextView nororderfound;
    ProgressBar orderListingProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_listing);

        setFontAwesome((LinearLayout)findViewById(R.id.orderListLayout),OrderListingActivity.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(OrderListingActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setText("My Orders");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OrderListingActivity.this, HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        nororderfound = (TextView)findViewById(R.id.nororderfound);
        orderListingProgress = (ProgressBar)findViewById(R.id.orderListingProgress);


        if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
            initVolleyCallback();
            Map<String, String> paramse = new HashMap<String, String>();
            paramse.put("cusToken", session.get_customerToken());
            RequestQueue mRequestQueueg = Volley.newRequestQueue(OrderListingActivity.this);
            mVolleyService = new VolleyService(mResultCallback,OrderListingActivity.this);
            Log.e("Post param ", String.valueOf(paramse));
            mVolleyService.postStringDataVolley("DELIVERY_ADDRESS",GET_ORDER,paramse,mRequestQueueg);

            orderListsItems = new ArrayList<OrderHistoryModel>();
            adapter = new OrderListingAdapter(this, orderListsItems);

            recyclerView = (RecyclerView)findViewById(R.id.orderRecycler);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrderListingActivity.this,LinearLayoutManager.VERTICAL,false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(adapter);
        }else{
            Intent i = new Intent(OrderListingActivity.this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }


        /*********/
        Action_menu_button n = new Action_menu_button(getApplicationContext(),OrderListingActivity.this);
        n.actionMenuButton(2);
    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if(status_code==1) {
                        recyclerView.setVisibility(View.VISIBLE);
                        orderListingProgress.setVisibility(View.GONE);
                        JSONObject responseArray=new JSONObject(jsonObject.getString("response"));

                        JSONArray upcoming = responseArray.getJSONArray("upcoming");
                        JSONArray history = responseArray.getJSONArray("history");
                        if(history.length()==0 && upcoming.length()==0){
                            nororderfound.setVisibility(View.VISIBLE);
                        } else {
                            nororderfound.setVisibility(View.GONE);
                        }

                        for(int i=0;i<upcoming.length();i++){
                            JSONObject upcomingEachRow = upcoming.getJSONObject(i);
                            OrderHistoryModel a = new OrderHistoryModel();

                            a.setOrderToken(upcomingEachRow.getString("orderToken"));
                            a.setOrderNumber(upcomingEachRow.getString("orderNumber"));
                            a.setTotProd(upcomingEachRow.getString("totProd"));
                            a.setTotQuan(upcomingEachRow.getString("totQuan"));
                            a.setPaidAmount(upcomingEachRow.getString("paidAmount"));
                            a.setOrderDate(upcomingEachRow.getString("orderDate"));
                            a.setOrderStatus(upcomingEachRow.getString("orderStatus"));
                            a.setPaymentStatus(upcomingEachRow.getString("paymentStatus"));
                            a.setDeliveryDate(upcomingEachRow.getString("deliveryDate"));

                            orderListsItems.add(a);

                        }
                        for (int k=0;k<history.length();k++){
                            JSONObject historyEachRow = history.getJSONObject(k);
                            OrderHistoryModel ab = new OrderHistoryModel();

                            ab.setOrderToken(historyEachRow.getString("orderToken"));
                            ab.setOrderNumber(historyEachRow.getString("orderNumber"));
                            ab.setTotProd(historyEachRow.getString("totProd"));
                            ab.setTotQuan(historyEachRow.getString("totQuan"));
                            ab.setPaidAmount(historyEachRow.getString("paidAmount"));
                            ab.setOrderDate(historyEachRow.getString("orderDate"));
                            ab.setOrderStatus(historyEachRow.getString("orderStatus"));
                            ab.setPaymentStatus(historyEachRow.getString("paymentStatus"));
                            ab.setDeliveryDate(historyEachRow.getString("deliveryDate"));

                            orderListsItems.add(ab);
                        }
                        adapter.notifyDataSetChanged();
                    }else{
                        recyclerView.setVisibility(View.GONE);
                        orderListingProgress.setVisibility(View.GONE);
                        nororderfound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(OrderListingActivity.this, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }
}
