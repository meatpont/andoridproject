package com.souvik.tendercuts.activity.farhan.model;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.co.meatpoint.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentChild extends Fragment {


    public FragmentChild() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_child, container, false);
    }

    public static Fragment newInstance(String title) {
        FragmentChild myFragment = new FragmentChild();

        Bundle args = new Bundle();
        args.putString("title", title);
        myFragment.setArguments(args);

        return myFragment;
    }
}
