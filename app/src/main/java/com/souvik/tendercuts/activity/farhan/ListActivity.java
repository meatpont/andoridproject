package com.souvik.tendercuts.activity.farhan;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.souvik.tendercuts.activity.CartActivity;
import com.souvik.tendercuts.activity.ItemListingActivity;
import com.souvik.tendercuts.activity.LoginActivity;
import com.souvik.tendercuts.activity.ProductDetailsActivity;
import com.souvik.tendercuts.activity.farhan.model.CategoryHolder;
import com.souvik.tendercuts.activity.farhan.model.TimeHolder;
import com.souvik.tendercuts.adapter.ItemListingAdapter;
import com.souvik.tendercuts.common.Action_menu_button;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.fragment.ItemListFragment;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.utils.CartAddNotify;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class ListActivity extends AppCompatActivity implements ItemListingAdapter.ItemAdapterCallback {
    TabLayout tabLayout;
    ViewPager viewPager;
    Toolbar toolbar;
    int no_of_categories = -1;
    int tab_pos = 0;
    ArrayList<CategoryHolder> list = new ArrayList<CategoryHolder>();

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    private RecyclerView recyclerView;
    //    private ItemListingAdapter adapter;
    private List<ItemModel> itemModelList;
    private ProgressBar listingProgress;
    private TextView noItemFound, cartaddedQuantity, totalPriceCart;

    private String itemName, userid = "";
    private JSONArray categoryContent;
    private String cat;
    List<CategoryList> catName = new ArrayList<CategoryList>();

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    LinearLayout fragmentContainer;

    SessionManager session;
    public DatabaseHandler db;

    //    LinearLayout proceedLayout;
    List<ItemModel> items = new ArrayList<ItemModel>();
    int sdk = android.os.Build.VERSION.SDK_INT;
    //    ItemListFragment itemlistfragment;
//    ViewPagerAdapter adapter;
    Intent customerResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        setFontAwesome((RelativeLayout) findViewById(R.id.item_listing_toolbar), ListActivity.this);
        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(ListActivity.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        customerResponse = getIntent();
        try {
            categoryContent = new JSONArray(customerResponse.getStringExtra("catDetails"));
            no_of_categories = new CatagoryHolder().getCatagory().size();
            tab_pos = customerResponse.getIntExtra("pos", 0);
        } catch (JSONException e) {
            Log.e("Exp ", e.getMessage());
        }
        tabItem();
//        fragmentload();

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.pager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        tabLayout.getTabAt(tab_pos).select();
                    }
                }, 20);



//        tabItem();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
                public void onPageSelected(int position) {
                    Log.e("pos ", ""+position);

                }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//
//            @Override
//            public void onTabSelected(final TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//                fragmentload(catName.get(tab.getPosition()).getToken(), tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

        //proceed layout
//        TextView proceedText = (TextView)findViewById(R.id.proceedText);
//
//        proceedText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
//                    Intent i = new Intent(ListActivity.this, CartActivity.class);
//                    startActivity(i);
//                } else {
//                    Intent i = new Intent(ListActivity.this, LoginActivity.class);
//
//                    HashMap<String, String> detailsMap = new HashMap<String, String>();
//                    detailsMap.put("isCustomerLoggedIn", "4");
//                    session.set_UserDetails(detailsMap);
//
//                    startActivity(i);
//                }
//
//            }
//        });

//        Button proceedBtn = (Button) findViewById(R.id.proceedBtn);
//        proceedBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
//                    Intent i = new Intent(ListActivity.this, CartActivity.class);
//                    startActivity(i);
//                } else {
//                    Intent i = new Intent(ListActivity.this, LoginActivity.class);
//
//                    HashMap<String, String> detailsMap = new HashMap<String, String>();
//                    detailsMap.put("isCustomerLoggedIn", "4");
//                    session.set_UserDetails(detailsMap);
//
//                    startActivity(i);
//                }
//            }
//        });
//        proceedLayout = (LinearLayout) findViewById(R.id.proceedLayout);

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);
        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListActivity.this, CartActivity.class);
                startActivity(i);
            }
        });

        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        /*********/
        Action_menu_button n = new Action_menu_button(getApplicationContext(), ListActivity.this);
        n.actionMenuButton(1);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i=0; i<catName.size(); i++){

            ItemListFragment fView = new ItemListFragment(catName.get(i).getToken());
            View view = fView.getView();

//            TextView txtTabItemNumber = (TextView)view.findViewById(R.id.txtTabItemNumber);
//            txtTabItemNumber.setText("TAB " + i);
            adapter.addFrag(fView,catName.get(i).getName());
        }
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void fragmentload() {
        try {
            categoryContent = new JSONArray(customerResponse.getStringExtra("catDetails"));
            Log.e("Detail ", String.valueOf(categoryContent));
            list.clear();
            for (int i = 1; i < categoryContent.length(); i++) {
                JSONObject object = categoryContent.getJSONObject(i);
                CategoryHolder ch = new CategoryHolder();
                if (catName.get(i).getName().equalsIgnoreCase(object.getString("catName"))) {
                    ch.setCatToken(object.getString("catToken"));
                    ch.setCatName(object.getString("catName"));
                    ch.setPic(object.getString("pic"));
                    ch.setIs_bulk(object.getString("is_bulk"));
                    list.add(ch);
                }
            }
           // adapter = new ViewPagerAdapter(getSupportFragmentManager(), list);
          //  viewPager.setAdapter(adapter);

//            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    public void tabItem() {
        try {

            catName.clear();
            for (int i = 1; i < categoryContent.length(); i++) {
                JSONObject object = categoryContent.getJSONObject(i);

                CategoryList cl = new CategoryList();
                cl.setToken(object.getString("catToken"));
                cl.setName(object.getString("catName"));
                catName.add(cl);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
//        for (int i = 0; i < catName.size(); i++) {
//            tabLayout.addTab(tabLayout.newTab().setText(catName.get(i).getName()));
//
//        }
//        new Handler().postDelayed(
//                new Runnable() {
//                    @Override
//                    public void run() {
//                        tabLayout.getTabAt(tab_pos).select();
//                    }
//                }, 100);
//
//        fragmentload(catName.get(tab_pos).getToken(), tab_pos);
    }


    @Override
    public void setItemModelList(List<ItemModel> itemModelList, int pos) {
        ItemModel itemModel = itemModelList.get(pos);
        /*Log.d("creation","Product Name => "+itemModel.getProdName());
        Log.d("creation","Product Token => "+itemModel.getProdToken());*/

        //Toast.makeText(this, "detail", Toast.LENGTH_SHORT).show();

//        Intent i = new Intent(ListActivity.this, ProductDetailsActivity.class);
//
//        HashMap<String, ItemModel> itemlistsHashmap = new HashMap<String, ItemModel>();
//        itemlistsHashmap.put("itemsDataHash", itemModel);
//
//        HashMap<String, List<ItemModel>> itemTotalHashmap = new HashMap<String, List<ItemModel>>();
//
//        itemTotalHashmap.put("totalItems", itemModelList);
//
//        Bundle extras = new Bundle();
//        extras.putSerializable("itemsData", itemlistsHashmap);
//        extras.putSerializable("allItems", itemTotalHashmap);
//        extras.putInt("position", pos);
//
//        i.putExtras(extras);
//        startActivityForResult(i, 1000);
        //startActivity(i);
        /*Log.d("creation","Product Name => "+itemModel.getProdName());
        Log.d("creation","Addedd Cart => "+itemModel.isAddedCart());
        //i.putExtra("catToken",String.valueOf(itemModel.getProdToken()));
        Bundle extras = new Bundle();
        extras.putSerializable("itemsData", itemlistsHashmap);
        */
    }


}