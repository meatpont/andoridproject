package com.souvik.tendercuts.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.file.WatchKey;
import java.util.HashMap;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.ADD_DELIVERY_ADDRESS;
import static com.souvik.tendercuts.config.AppConfig.EDIT_DELIVERY_ADDRESS;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class AddNewDeliveryAddress extends AppCompatActivity {
    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;
    String type=null;

    private Button addnewaddress;
   // TextView title;
    private EditText fname,lname,email,phone,address,city,postcode;
    String strName, strEmail, strPhone, strAddress, strCity, strPost;
    String []aryName;
    CheckBox chk_default;
    int flag=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_delivery_address);

        setFontAwesome((LinearLayout)findViewById(R.id.addnewDeliveryAddr),AddNewDeliveryAddress.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(AddNewDeliveryAddress.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setVisibility(View.VISIBLE);
        chk_default= findViewById(R.id.chk_default);
       chk_default.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
               if(chk_default.isChecked()){
                   flag=1;
               }
               else {
                   flag=0;
               }
           }
       });


       // title = findViewById(R.id.tv_title);
        type=getIntent().getStringExtra("type").toString();
        if(type.equalsIgnoreCase("add")) {
            titleToolbar.setText("Add New Delivery Address");
        }else if (type.equalsIgnoreCase("edit")) {
            titleToolbar.setText("Edit Delivery Address");
            strName = getIntent().getStringExtra("name");
            strEmail = getIntent().getStringExtra("email");
            strPhone = getIntent().getStringExtra("phone");
            strAddress = getIntent().getStringExtra("address");
            strCity = getIntent().getStringExtra("city");
            strPost = getIntent().getStringExtra("post");

        }


        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(AddNewDeliveryAddress.this,DeliveryAddress.class);
//                startActivity(i);
                finish();
            }
        });
        fname = (EditText)findViewById(R.id.fname);
        lname = (EditText)findViewById(R.id.lname);
        email = (EditText)findViewById(R.id.email);
        phone = (EditText)findViewById(R.id.phone);
        address = (EditText)findViewById(R.id.address);
        city = (EditText)findViewById(R.id.city);
        postcode = (EditText)findViewById(R.id.postcode);
        if(strName!=null && strEmail!=null){
//            strName, strEmail, strPhone, strAddress, strCity, strPost
            aryName=strName.split(" ");
            fname.setText(aryName[0]);
//            fname.setEnabled(false);
            lname.setText(aryName[1]);
//            lname.setEnabled(false);
            email.setText(strEmail);
//            email.setEnabled(false);
            phone.setText(strPhone);
//            phone.setEnabled(false);
            address.setText(strAddress);
            city.setText(strCity);
            postcode.setText(strPost);
        }

        addnewaddress = (Button)findViewById(R.id.addnewaddress);
        addnewaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!fname.getText().toString().isEmpty() && !lname.getText().toString().isEmpty() && !email.getText().toString().isEmpty()
                        && phone.getText().toString().length()==10 && !phone.getText().toString().isEmpty() &&
                        !city.getText().toString().isEmpty() && !postcode.getText().toString().isEmpty()) {
                    if(isValidEmail(email.getText().toString())) {
                        initVolleyCallback();
                        Map<String, String> paramse = new HashMap<String, String>();
                        if (type.equalsIgnoreCase("add")) {
                            paramse.put("cusToken", session.get_customerToken());
                            paramse.put("fname", fname.getText().toString());
                            paramse.put("lname", lname.getText().toString());
                            paramse.put("email", email.getText().toString());
                            paramse.put("phone", phone.getText().toString());
                            paramse.put("address", address.getText().toString());
                            paramse.put("address_optional", "");
                            paramse.put("city", city.getText().toString());
                            paramse.put("postcode", postcode.getText().toString());
                            paramse.put("make_default", String.valueOf(flag));

                            RequestQueue mRequestQueueg = Volley.newRequestQueue(AddNewDeliveryAddress.this);
                            mVolleyService = new VolleyService(mResultCallback, AddNewDeliveryAddress.this);

                            mVolleyService.postStringDataVolley("DELIVERY_ADDRESS", ADD_DELIVERY_ADDRESS, paramse, mRequestQueueg);
                        } else if (type.equalsIgnoreCase("edit")) {
                            paramse.put("cusToken", session.get_customerToken());
                            paramse.put("addressToken", getIntent().getStringExtra("addToken"));
                            paramse.put("fname", fname.getText().toString());
                            paramse.put("lname", lname.getText().toString());
                            paramse.put("email", email.getText().toString());
                            paramse.put("phone", phone.getText().toString());
                            paramse.put("address", address.getText().toString());
                            paramse.put("address_optional", "");
                            paramse.put("city", city.getText().toString());
                            paramse.put("postcode", postcode.getText().toString());
                            paramse.put("make_default", String.valueOf(flag));

                            RequestQueue mRequestQueueg = Volley.newRequestQueue(AddNewDeliveryAddress.this);
                            mVolleyService = new VolleyService(mResultCallback, AddNewDeliveryAddress.this);
                            mVolleyService.postStringDataVolley("DELIVERY_ADDRESS", EDIT_DELIVERY_ADDRESS, paramse, mRequestQueueg);
                        }
                    }
                    else {
                        Snackbar.make(findViewById(android.R.id.content), "Enter valid email", Snackbar.LENGTH_SHORT).show();
                    }
                }else{
                    Snackbar.make(findViewById(android.R.id.content), "Enter valid information", Snackbar.LENGTH_SHORT).show();

                }
            }
        });
    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");

                    if(status_code==1){
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("fname", fname.getText().toString());
                        returnIntent.putExtra("lname", lname.getText().toString());
                        returnIntent.putExtra("email", email.getText().toString());
                        returnIntent.putExtra("phone", phone.getText().toString());
                        returnIntent.putExtra("address", address.getText().toString());
                        returnIntent.putExtra("address_optional", "");
                        returnIntent.putExtra("city", city.getText().toString());
                        returnIntent.putExtra("postcode", postcode.getText().toString());
                        returnIntent.putExtra("make_default", String.valueOf(flag));
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
//                        Intent i = getIntent();//AddNewDeliveryAddress.this,DeliveryAddress.class);
//                        startActivity(i);
//                        finish();
                    }else{
                        String message = jsonObject.getString("message");
                        MeatPointUtils.decisionAlertOnMainThread(AddNewDeliveryAddress.this,
                                android.R.drawable.ic_dialog_info,
                                R.string.error,
                                message,
                                "Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                        dialog.dismiss();
                                        //finish();
                                    }
                                }, "", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

}
