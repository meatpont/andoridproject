package com.souvik.tendercuts.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.adapter.OrderItemListingAdapter;
import com.souvik.tendercuts.adapter.OrderListingAdapter;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.OrderHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.CANCEL_ORDER;
import static com.souvik.tendercuts.config.AppConfig.GET_ORDER;
import static com.souvik.tendercuts.config.AppConfig.GET_ORDER_DETAILS;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class OrderDetailsActivity extends AppCompatActivity {

    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;
    int flg = 0;
    @BindView(R.id.firstDeliveryValue)
    TextView firstDeliveryValue;
    @BindView(R.id.firstDeliveryCharge)
    TextView firstDeliveryCharge;
    @BindView(R.id.secondDeliveryValue)
    TextView secondDeliveryValue;
    @BindView(R.id.secondDeliveryCharge)
    TextView secondDeliveryCharge;
    @BindView(R.id.tv_sub_total)
    TextView sub_total;
    @BindView(R.id.tv_note)
    TextView tv_note;
    CardView card, card2;
    @BindView(R.id.view1) View view1;
    @BindView(R.id.view2) View view2;

    @BindView(R.id.layout_parent)
    LinearLayout layout_payent;

    private OrderItemListingAdapter adapter, adapter2;
    private List<OrderHistoryModel> orderListsItems, orderListsItems2;
    private RecyclerView recyclerView, recyclerView2;
    Button btn_cancel, btn_track, btn_track2;
    String odrToken = "", trk_url = "", trk_url2 = "";
    ProgressBar orderListingProgress;

    private TextView payableamount, discount, shipping, gst, redeemedPoint, grandTotal, earnedPoint,
            tv_details, tv_cancelMessage, slot1, slot2, status, status2, orderid, orderidtitle, orderid2, orderidtitle2;
    LinearLayout layout_shiping_charge, layout_discount, layout_gst, layout_gTotal, layout_redeem_point,
            layout_paid_amount, layout_rewards_point, layout_coupon;
    View viewredeem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        setFontAwesome((LinearLayout) findViewById(R.id.orderDetailsLayout), OrderDetailsActivity.this);
        ButterKnife.bind(OrderDetailsActivity.this);
        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(OrderDetailsActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setVisibility(View.VISIBLE);
        titleToolbar.setText("Order Details");

        layout_payent.setVisibility(View.GONE);

        orderListingProgress = (ProgressBar) findViewById(R.id.orderListingProgress);

        discount = findViewById(R.id.tv_discount);
        shipping = findViewById(R.id.tv_shipping_charge);
        gst = findViewById(R.id.tv_gst);
        redeemedPoint = findViewById(R.id.tv_redeemed_point);
        payableamount = findViewById(R.id.tv_payable_amount);
        earnedPoint = findViewById(R.id.tv_earn_point);
        tv_details = findViewById(R.id.tv_details);
        tv_details.setVisibility(View.GONE);
        btn_cancel = findViewById(R.id.btn_cancle);
        btn_track = findViewById(R.id.orderTrack);
        btn_track2 = findViewById(R.id.orderTrack2);
        tv_cancelMessage = findViewById(R.id.tv_cancelMessage);

        layout_coupon = findViewById(R.id.couponlayout);
        layout_coupon.setVisibility(View.GONE);

        card = findViewById(R.id.card);
        card2 = findViewById(R.id.card2);
        viewredeem = findViewById(R.id.view_redeem_point);

        orderid = findViewById(R.id.order_no);
        orderidtitle = findViewById(R.id.tv_order_id_title);
        orderidtitle.setTypeface(null, 1);
        orderidtitle.setText("Order No.: ");
        orderid2 = findViewById(R.id.order_no2);
        orderidtitle2 = findViewById(R.id.tv_order_id_title2);
        orderidtitle2.setTypeface(null, 1);
        orderidtitle2.setText("Order No.: ");

        status = findViewById(R.id.status);
        status2 = findViewById(R.id.status2);
        tv_cancelMessage.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        slot1 = findViewById(R.id.date);
        slot2 = findViewById(R.id.date2);
        slot1.setTypeface(null, 1);
        slot2.setTypeface(null, 1);
//        slot2.setVisibility(View.GONE);
//        slot1.setVisibility(View.GONE);

        layout_shiping_charge = findViewById(R.id.layout_shipping_charge);
        layout_discount = findViewById(R.id.layout_discount);
        layout_gst = findViewById(R.id.layout_gst);
        layout_gTotal = findViewById(R.id.layout_gTotal);
        layout_redeem_point = findViewById(R.id.layout_redeem_point);
        layout_paid_amount = findViewById(R.id.layout_payed_amount);
        layout_rewards_point = findViewById(R.id.layout_rewards_point);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog();
            }
        });

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);


        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        btn_track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!trk_url.equalsIgnoreCase(null)) {
                    Intent i = new Intent(OrderDetailsActivity.this, OrderTrackActivity.class);
                    i.putExtra("url", trk_url);
                    startActivity(i);
                }

            }
        });
        btn_track2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!trk_url2.equalsIgnoreCase(null)) {
                    Intent i = new Intent(OrderDetailsActivity.this, OrderTrackActivity.class);
                    i.putExtra("url", trk_url2);
                    startActivity(i);
                }

            }
        });

        initVolleyCallback();
        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", session.get_customerToken());
        paramse.put("ordToken", getIntent().getStringExtra("orderToken"));
        RequestQueue mRequestQueueg = Volley.newRequestQueue(OrderDetailsActivity.this);
        mVolleyService = new VolleyService(mResultCallback, OrderDetailsActivity.this);
        mVolleyService.postStringDataVolley("OrderDetails", GET_ORDER_DETAILS, paramse, mRequestQueueg);

        orderListsItems = new ArrayList<OrderHistoryModel>();
        orderListsItems2 = new ArrayList<OrderHistoryModel>();
        adapter = new OrderItemListingAdapter(this, orderListsItems);
        adapter2 = new OrderItemListingAdapter(this, orderListsItems2);

        recyclerView = (RecyclerView) findViewById(R.id.orderItemListingRecycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView2 = (RecyclerView) findViewById(R.id.orderItemListingRecycler2);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(OrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView2.setLayoutManager(mLayoutManager2);
        recyclerView2.setAdapter(adapter2);

        grandTotal = (TextView) findViewById(R.id.grandTotal);
    }

    void initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if (status_code == 1) {
                        layout_payent.setVisibility(View.VISIBLE);
                        orderListingProgress.setVisibility(View.GONE);
                        JSONObject responseObject = jsonObject.getJSONObject("response");

                        JSONObject arrayOrderDetail = responseObject.getJSONObject("orderDetail");
                        JSONArray arrayProduct = responseObject.getJSONArray("orderProducts");

                        slot1.setText(arrayOrderDetail.getString("orderDate"));
                        slot1.setTypeface(null, 1);
                        slot1.setVisibility(View.VISIBLE);


                        //JSONArray history = responseArray.getJSONArray("history");
                        Double totPrice = 0.0;
                        Log.e("Length ", String.valueOf(arrayProduct.length()));
                        if (arrayProduct.length() > 1) {
                            card2.setVisibility(View.VISIBLE);
                            card.setVisibility(View.VISIBLE);
                        }
                        if (arrayProduct.length() <= 1) {
                            card.setVisibility(View.VISIBLE);
                            card2.setVisibility(View.GONE);
                        }
//                        for (int i = 0; i < arrayProduct.length(); i++) {
                        //for slot 1
                        {
                            double price = 0, charge = 0;
                            JSONObject orderEachRow = arrayProduct.getJSONObject(0);
                            slot1.setText(orderEachRow.getString("slot"));

                            status.setTypeface(null, 1);
                            if (orderEachRow.getString("orderstatus").equalsIgnoreCase("Received")) {
                                status.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.appThemegreencolor));
                            } else {
                                status.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.cb_errorRed));
                            }
                            status.setText(orderEachRow.getString("orderstatus"));
                            slot1.setVisibility(View.VISIBLE);
                            if (status.getText().toString().equalsIgnoreCase("Cancelled")) {
                                btn_track.setVisibility(View.GONE);
                            } else {
                                btn_track.setVisibility(View.VISIBLE);
                            }

                            orderid.setTypeface(null, 1);
                            orderid.setText(orderEachRow.getString("orderNumber"));
                            trk_url = orderEachRow.getString("trackingUrl");

                            JSONArray ary = orderEachRow.getJSONArray("details");
                            orderListsItems.clear();
                            for (int j = 0; j < ary.length(); j++) {
                                JSONObject obj = ary.getJSONObject(j);
                                OrderHistoryModel a = new OrderHistoryModel();

                                a.setOrderToken(obj.getString("orderToken"));
                               // a.setOrderNumber(obj.getString("orderNumber"));
                                a.setOrderDate(orderEachRow.getString("slot"));
                                a.setProductName(obj.getString("productName"));
                                a.setProductQuantity(obj.getString("quantity"));
                                a.setProductPrice(obj.getString("unitPrice"));
//                            a.setOrderStatus(orderEachRow.getString("orderStatus"));
//                            a.setPaymentStatus(orderEachRow.getString("paymentStatus"));
                                a.setProductTotalPrice(obj.getString("totalPrice"));
                                a.setProductPic(obj.getString("pic"));
                                a.setParentOrderToken(arrayOrderDetail.getString("orderToken"));
                                a.setOrderStatus(arrayOrderDetail.getString("orderStatus"));

                                a.setTrackUrl(obj.getString("trackUrl"));
                                orderListsItems.add(a);

                                price += Double.parseDouble(obj.getString("deliveryValue"));
                                charge = Double.parseDouble(obj.getString("deliveryCharge"));
                                totPrice = totPrice + obj.getDouble("totalPrice");
                                odrToken = arrayOrderDetail.getString("orderToken");
                                //}

                                if(arrayOrderDetail.getString("special_note").isEmpty())
                                {
                                    tv_note.setVisibility(View.GONE);
                                    view1.setVisibility(View.GONE);
                                    view2.setVisibility(View.GONE);
                                }
                                else {
                                    tv_note.setVisibility(View.VISIBLE);
                                    tv_note.setText("*"+arrayOrderDetail.getString("special_note"));
                                    view1.setVisibility(View.VISIBLE);
                                    view2.setVisibility(View.GONE);
                                }
                                if (arrayOrderDetail.getInt("is_cancel") == 0) {
                                    if (arrayOrderDetail.getInt("can_cancel") == 1) {
                                        tv_cancelMessage.setText(arrayOrderDetail.getString("can_cancel_msg"));
                                        btn_cancel.setVisibility(View.VISIBLE);
                                        tv_cancelMessage.setVisibility(View.VISIBLE);
                                        tv_cancelMessage.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.gray));
                                    }
                                } else {
                                    tv_cancelMessage.setText(arrayOrderDetail.getString("is_cancel_msg"));
                                    tv_cancelMessage.setVisibility(View.VISIBLE);
                                    btn_cancel.setVisibility(View.GONE);
                                    tv_cancelMessage.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.cb_errorRed));
                                    tv_cancelMessage.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                                }

                            }
                            firstDeliveryValue.setText("₹" + price);
                            firstDeliveryCharge.setText("₹" + charge);
                        }
                        if (arrayProduct.length() > 1) {
                            JSONObject orderEachRow2 = arrayProduct.getJSONObject(1);
                            slot2.setText(orderEachRow2.getString("slot"));
                            status2.setTypeface(null, 1);
                            if (orderEachRow2.getString("orderstatus").equalsIgnoreCase("Received")) {
                                status2.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.appThemegreencolor));
                            } else {
                                status2.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.cb_errorRed));
                            }
                            status2.setText(orderEachRow2.getString("orderstatus"));
                            if (status2.getText().toString().equalsIgnoreCase("Cancelled")) {
                                btn_track2.setVisibility(View.GONE);
                            } else {
                                btn_track2.setVisibility(View.VISIBLE);
                            }

                            slot2.setVisibility(View.VISIBLE);
                            orderid2.setTypeface(null, 1);
                            orderid2.setText(orderEachRow2.getString("orderNumber"));
                            trk_url2 = orderEachRow2.getString("trackingUrl");
                            JSONArray ary2 = orderEachRow2.getJSONArray("details");
                            double value = 0, charge2 = 0;
                            orderListsItems2.clear();
                            for (int j = 0; j < ary2.length(); j++) {
                                JSONObject obj2 = ary2.getJSONObject(j);
                                OrderHistoryModel a = new OrderHistoryModel();

                                a.setOrderToken(obj2.getString("orderToken"));
                               // a.setOrderNumber(obj2.getString("orderNumber"));
                                a.setProductName(obj2.getString("productName"));
                                a.setProductQuantity(obj2.getString("quantity"));
                                a.setProductPrice(obj2.getString("unitPrice"));
//                            a.setOrderStatus(orderEachRow.getString("orderStatus"));
//                            a.setPaymentStatus(orderEachRow.getString("paymentStatus"));
                                a.setProductTotalPrice(obj2.getString("totalPrice"));
                                a.setProductPic(obj2.getString("pic"));
                                a.setParentOrderToken(arrayOrderDetail.getString("orderToken"));
                                a.setOrderStatus(arrayOrderDetail.getString("orderStatus"));

                                a.setTrackUrl(obj2.getString("trackUrl"));
                                orderListsItems2.add(a);

                                value += Double.parseDouble(obj2.getString("deliveryValue"));
                                charge2 = Double.parseDouble(obj2.getString("deliveryCharge"));

                                totPrice = totPrice + obj2.getDouble("totalPrice");
                                odrToken = arrayOrderDetail.getString("orderToken");
                                //}
                                if (arrayOrderDetail.getInt("is_cancel") == 0) {
                                    if (arrayOrderDetail.getInt("can_cancel") == 1) {
                                        tv_cancelMessage.setText(arrayOrderDetail.getString("can_cancel_msg"));
                                        btn_cancel.setVisibility(View.VISIBLE);
                                        tv_cancelMessage.setVisibility(View.VISIBLE);
                                        tv_cancelMessage.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.gray));
                                    }
                                } else {
                                    tv_cancelMessage.setText(arrayOrderDetail.getString("is_cancel_msg"));
                                    tv_cancelMessage.setVisibility(View.VISIBLE);
                                    btn_cancel.setVisibility(View.GONE);
                                    tv_cancelMessage.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.cb_errorRed));
                                    tv_cancelMessage.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                                }

                            }

                            secondDeliveryValue.setText("₹" + value);
                            secondDeliveryCharge.setText("₹" + charge2);
                        }
//                        total_amount.setText("₹"+arrayOrderDetail.getString("Total"));
//                        if (!tos.isEmpty() && Double.parseDouble(c.getSub_total()) > 0) {
//                            fb_totLayout.setVisibility(View.VISIBLE);
//                        }
//
//                        fb_discountFee.setText("₹" + c.getDiscount_amount());
//                        if (!c.getDiscount_amount().isEmpty() && Double.parseDouble(c.getDiscount_amount()) > 0) {
//                            fb_discountOptionLayout.setVisibility(View.VISIBLE);
//                        }
//
//                        fb_deliveryFee.setText("₹" + c.getDelivery_charge());
//                        if (!c.getDelivery_charge().isEmpty() && Double.parseDouble(c.getDelivery_charge()) > 0) {
//                            fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
//                        }
//
//                        fb_gstValue.setText("₹" + c.getGst_value());
//                        if (!c.getGst_value().isEmpty() && Double.parseDouble(c.getGst_value()) > 0) {
//                            fb_gstOptionLayout.setVisibility(View.VISIBLE);
//                        }
//
////                                fb_grandTotal.setText("₹"+responseObject.getString("grand_total"));
//                        fb_grandTotal.setText("₹" + c.getPayable_amount());
//                        if (!c.getGrand_total().isEmpty() && Double.parseDouble(c.getGrand_total()) > 0) {
//                            fb_grandTotalLayout.setVisibility(View.VISIBLE);
//                        }
//
//                        fb_redeemedPoint.setText("(-)₹" + c.getRedeemed_point());
//                        if (!c.getRedeemed_point().isEmpty() && Double.parseDouble(c.getRedeemed_point()) > 0) {
//                            fb_redemedPointsLayout.setVisibility(View.VISIBLE);
//                        }
//
//                        fb_points.setText(c.getEarn_points() + " Points");
//                        if (!c.getEarn_points().isEmpty() && Double.parseDouble(c.getEarn_points()) > 0) {
//                            fb_earnPointsLayout.setVisibility(View.VISIBLE);
//                        }
//
//                        // payableAmount.setText("₹"+responseObject.getString("payable_amount"));
////                        totalPriceButtom.setText("₹" + c.getPayable_amount());
//                        layout_shiping_charge,layout_discount, layout_gst, layout_gTotal, layout_redeem_point, layout_paid_amount,layout_rewards_point
                        sub_total.setText("₹" + arrayOrderDetail.getString("Total"));
                        grandTotal.setText("₹" + arrayOrderDetail.getString("grandTotal"));
                        if (Double.parseDouble(arrayOrderDetail.getString("Shipping")) > 0) {
                            layout_shiping_charge.setVisibility(View.VISIBLE);
                            shipping.setText("₹" + arrayOrderDetail.getString("Shipping"));
                        } else layout_shiping_charge.setVisibility(View.GONE);

                        if (Double.parseDouble(arrayOrderDetail.getString("Discount")) > 0) {
                            layout_discount.setVisibility(View.VISIBLE);
                        } else layout_discount.setVisibility(View.GONE);
                        discount.setText("(-)₹" + arrayOrderDetail.getString("Discount"));

                        gst.setText("₹" + arrayOrderDetail.getString("GST"));

                        if (Double.parseDouble(arrayOrderDetail.getString("redeemedPoint")) > 0) {
                            layout_redeem_point.setVisibility(View.VISIBLE);
                            viewredeem.setVisibility(View.VISIBLE);
                            redeemedPoint.setText("(-)₹" + arrayOrderDetail.getString("redeemedPoint"));
                        } else {
                            layout_redeem_point.setVisibility(View.GONE);
                            viewredeem.setVisibility(View.GONE);
                        }
                        payableamount.setText("₹" + arrayOrderDetail.getString("paidAmount"));
                        earnedPoint.setText(arrayOrderDetail.getString("earnedPoint"));

                        if (arrayOrderDetail.getString("coupon_msg").isEmpty())
                            layout_coupon.setVisibility(View.GONE);
                        else
                            layout_coupon.setVisibility(View.VISIBLE);


                        adapter.notifyDataSetChanged();
                        adapter2.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    Log.e("Exception ", e.getMessage().toString());
                }
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void dialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(OrderDetailsActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(OrderDetailsActivity.this);
            builder.setCancelable(false);
        }
        builder.setTitle("Cancel Order")
                .setMessage("Do you want to cancel your order.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //call api
                        new CancelOrder().initVolleyCallback();
                        Map<String, String> paramse = new HashMap<String, String>();
                        paramse.put("cusToken", session.get_customerToken());
                        paramse.put("ordToken", odrToken);
                        RequestQueue mRequestQueueg = Volley.newRequestQueue(OrderDetailsActivity.this);
                        mVolleyService = new VolleyService(mResultCallback, OrderDetailsActivity.this);
                        mVolleyService.postStringDataVolley("CancelOrder", CANCEL_ORDER, paramse, mRequestQueueg);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public class CancelOrder {
        void initVolleyCallback() {
            mResultCallback = new FccResult() {
                @Override
                public void notifySuccess(String requestType, JSONObject response) {
                    Log.d("creation", "Total Response GET : " + response);
                }

                @Override
                public void notifySuccessString(String requestType, String response) {
                    Log.d("creation", "Total Response POST: " + response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status_code = jsonObject.getInt("status");
                        if (status_code == 1) {
                            Toast.makeText(OrderDetailsActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            startActivity(new Intent(OrderDetailsActivity.this, OrderListingActivity.class));
                            finish();
                        }

                    } catch (JSONException e) {
                        Log.e("Exception ", e.getMessage().toString());
                    }
                    //CART_LISTING
                }

                @Override
                public void notifyError(String requestType, VolleyError error) {
                    Log.d("---", "Volley requester error==" + error);
                    Log.d("---", "Volley JSON post" + "That didn't work!");
                }
            };
        }
    }

}
