package com.souvik.tendercuts.activity.farhan;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sushil on 4/8/2018.
 */

public class SessionManager {
 private static SharedPreferences pref;
 private static SharedPreferences.Editor editor;

    public static int getQuantity(Activity activity){
        int pos = 0;
        try{
            pref =activity.getSharedPreferences("session", 0);
            pos = pref.getInt("quantity",0);
        }catch (Exception e){
            e.printStackTrace();
        }
        return pos;
    }
    public static void createSession(Activity activity, int quantity){
        pref= activity.getSharedPreferences("session", 0);
        editor=pref.edit();
        editor.putInt("quantity", quantity);
        editor.commit();
    }

    public  static  void clearSession(Activity activity){
        pref =activity.getSharedPreferences("session",0);
        editor=pref.edit();
        editor.clear();
        editor.commit();
    }

}
