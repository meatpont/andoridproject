package com.souvik.tendercuts.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.farhan.ListActivity;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.utils.CartAddNotify;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.PRODUCT_DETAILS_URL;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class ProductDetailsActivity extends AppCompatActivity {

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;
    private String prodToken;
    CardView parentCart;

    ImageView prodImage;
    TextView prodName,prodDesc,textviewItems,price, netWeight, deal_price, outOfStock;
    HashMap<String, ItemModel> itemlistsHashmap;

    HashMap<String, List<ItemModel>> totalItemListsHashmap;
    ItemModel itemModel;
    List<ItemModel> itemModelList;
    private int position;
    private CartAddNotify yourIntrface;

    private ItemListingActivity adapterCallback;

    private LinearLayout productDetailsLayout,quantityAddLayout,productDetailsInnerLayout, layoutAddItem;

    private Button buttonMinus,buttonPlus,add_to_cart,added_to_cart;
    private ProgressBar detailsProgress;

    public DatabaseHandler db;

    SessionManager session;
    TextView titleToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        db = new DatabaseHandler(ProductDetailsActivity.this);
        session = SessionManager.getInstance(getApplicationContext());

        Intent customerResponse = getIntent();
        prodToken = customerResponse.getStringExtra("catToken");
        productDetailsLayout = (LinearLayout)findViewById(R.id.productDetailsLayout);
        quantityAddLayout = (LinearLayout)findViewById(R.id.quantityAddLayout);
        added_to_cart = (Button)findViewById(R.id.added_to_cart);
        add_to_cart = (Button)findViewById(R.id.add_to_cart);
        productDetailsInnerLayout = (LinearLayout)findViewById(R.id.productDetailsInnerLayout);
        detailsProgress = (ProgressBar)findViewById(R.id.detailsProgress);

        parentCart=findViewById(R.id.parentCart);
        parentCart.setVisibility(View.GONE);

        setFontAwesome((LinearLayout)findViewById(R.id.productDetailsLayout),ProductDetailsActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setText("Product Details");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(ProductDetailsActivity.this, ListActivity.class);
//                startActivity(i);
                finish();
            }
        });

        outOfStock=findViewById(R.id.tv_out_stock);
        layoutAddItem=findViewById(R.id.layout_add_item);
        textviewItems = (TextView)findViewById(R.id.textviewItems);
        buttonPlus = (Button)findViewById(R.id.buttonPlus);
        buttonMinus = (Button)findViewById(R.id.buttonMinus);
        if (customerResponse.getStringExtra("out").contains("Out of Stock")) {
            layoutAddItem.setVisibility(View.GONE);
            outOfStock.setVisibility(View.VISIBLE);
            outOfStock.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            outOfStock.setText(customerResponse.getStringExtra("out"));
        } else {
            if(db.getCartCount(new ItemModel(prodToken,session.get_customerToken()))<=0) {
                quantityAddLayout.setVisibility(View.GONE);
                add_to_cart.setVisibility(View.VISIBLE);
            }else{
                add_to_cart.setVisibility(View.GONE);
                int cartquantity = db.getCartAddedQuantity(new ItemModel(prodToken,session.get_customerToken()));
                if(cartquantity>0) {
                    textviewItems.setText("" + cartquantity);
                }
            }
        }


//        if(com.souvik.tendercuts.activity.farhan.SessionManager.getQuantity(ProductDetailsActivity.this)>0){
//            add_to_cart.setVisibility(View.GONE);
//        }else {
//            add_to_cart.setVisibility(View.VISIBLE);
//        }
        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(textviewItems.getText().toString());
                if(quantity>1){
                    quantity = quantity - 1;
                    textviewItems.setText(""+quantity);

                    if(db.getCartCount(new ItemModel(prodToken,session.get_customerToken()))>0) {
                        int cartID = db.getCartID(new ItemModel(prodToken, session.get_customerToken()));
                        db.updateCartProductQuantity(new ItemModel(quantity, cartID));

                    }
                }else {
                    if(quantity==1) {
                        quantity = quantity - 1;
                        textviewItems.setText(""+quantity);
                        quantityAddLayout.setVisibility(View.GONE);
                        add_to_cart.setVisibility(View.VISIBLE);

                        int cartID = db.getCartID(new ItemModel(prodToken,session.get_customerToken()));
                        db.deleteItemFromCart(cartID);


                    }
                }
                com.souvik.tendercuts.activity.farhan.SessionManager.createSession(ProductDetailsActivity.this, quantity);
            }
        });
        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_to_cart.setVisibility(View.GONE);
                quantityAddLayout.setVisibility(View.VISIBLE);
                textviewItems.setText("1");

                if(db.getCartCount(new ItemModel(prodToken,session.get_customerToken()))<=0){
                    db.addProductInCart(new ItemModel(prodToken,1,session.get_customerToken()));

                }
            }
        });
        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(textviewItems.getText().toString());
                if(quantity>=1){
                    quantity = quantity + 1;
                    textviewItems.setText(""+quantity);

                    /*itemModel.setCartAddedQuantity(quantity);

                    itemModelList.remove(position);
                    itemModelList.add(position,itemModel);*/
                    if(db.getCartCount(new ItemModel(prodToken,session.get_customerToken()))>0) {
                        int cartID = db.getCartID(new ItemModel(prodToken, session.get_customerToken()));
                        Log.d("creation","ddd => "+cartID);
                        db.updateCartProductQuantity(new ItemModel(quantity, cartID));

                    }

                    //adapterCallback.itemQuantitychange(itemModelList,position);
                }
                com.souvik.tendercuts.activity.farhan.SessionManager.createSession(ProductDetailsActivity.this, quantity);
            }
        });


        initVolleyCallback();
        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("prodToken", prodToken);
        RequestQueue mRequestQueueg = Volley.newRequestQueue(ProductDetailsActivity.this);
        mVolleyService = new VolleyService(mResultCallback,ProductDetailsActivity.this);
        mVolleyService.postStringDataVolley("PRODUCT_DETAILS",PRODUCT_DETAILS_URL,paramse,mRequestQueueg);

        prodImage = (ImageView)findViewById(R.id.productImg);
        prodName = (TextView)findViewById(R.id.productName);
        prodDesc = (TextView)findViewById(R.id.productDesc);
        price = (TextView)findViewById(R.id.price);
       deal_price = (TextView)findViewById(R.id.deal_price);
        netWeight =findViewById(R.id.netWeight);
        netWeight.setText(getIntent().getStringExtra("netWeight"));
    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if (status_code == 1) {
                        parentCart.setVisibility(View.VISIBLE);
                        detailsProgress.setVisibility(View.GONE);
                        productDetailsInnerLayout.setVisibility(View.VISIBLE);
                        JSONObject prodDetailsObject = jsonObject.getJSONObject("response");
                        titleToolbar.setText(prodDetailsObject.getString("prodName"));
                        prodName.setText(prodDetailsObject.getString("prodName"));
                        prodDesc.setText(prodDetailsObject.getString("prodDesc"));
                        //price.setText("₹"+prodDetailsObject.getString("sellPrice"));

                        if(!prodDetailsObject.getString("dealPrice").isEmpty() &&
                                Double.parseDouble(prodDetailsObject.getString("dealPrice"))>0) {
                            price.setText("₹" + prodDetailsObject.getString("sellPrice"));
                            price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            deal_price.setText("₹" + prodDetailsObject.getString("dealPrice"));
                            price.setTextColor(getResources().getColor(R.color.cb_errorRed));
                            deal_price.setVisibility(View.VISIBLE);
                        }
                        else {
                            price.setText("₹" + prodDetailsObject.getString("sellPrice"));
                            deal_price.setVisibility(View.GONE);
                        }


                        Picasso
                                .with(ProductDetailsActivity.this)
                                .load(prodDetailsObject.getString("pic"))
                                .into(prodImage);
                    }else{
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent i = new Intent(ProductDetailsActivity.this, ListActivity.class);
//        startActivity(i);
        finish();
    }

    public void setOnItemSelected(CartAddNotify yourIntrface) {
        this.yourIntrface = yourIntrface;
    }
    /*public  interface productDetailsCallback {
        void reloadItems(List<ItemModel> itemModelList);
        void itemQuantitychange(List<ItemModel> itemModelList,int position);
    }*/


}

