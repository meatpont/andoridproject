package com.souvik.tendercuts.activity.farhan.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Md Farhan Raja on 25-Feb-18.
 */

public class Time implements Parcelable
{
    @SerializedName("slot_id")private int slotId;
    @SerializedName("available")private int slotAvailable;
    @SerializedName("time")private String slotTime;
    @SerializedName("default_selected")private int slotSelected;

    public Time(Parcel in) {
        slotId = in.readInt();
        slotAvailable = in.readInt();
        slotTime = in.readString();
        slotSelected = in.readInt();
    }

    public static final Creator<Time> CREATOR = new Creator<Time>() {
        @Override
        public Time createFromParcel(Parcel in) {
            return new Time(in);
        }

        @Override
        public Time[] newArray(int size) {
            return new Time[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(slotId);
        parcel.writeInt(slotAvailable);
        parcel.writeString(slotTime);
        parcel.writeInt(slotSelected);
    }

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    public int getSlotAvailable() {
        return slotAvailable;
    }

    public void setSlotAvailable(int slotAvailable) {
        this.slotAvailable = slotAvailable;
    }

    public String getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(String slotTime) {
        this.slotTime = slotTime;
    }

    public int getSlotSelected() {
        return slotSelected;
    }

    public void setSlotSelected(int slotSelected) {
        this.slotSelected = slotSelected;
    }
}
