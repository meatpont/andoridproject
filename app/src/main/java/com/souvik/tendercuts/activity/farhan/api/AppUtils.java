package com.souvik.tendercuts.activity.farhan.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import in.co.meatpoint.R;

public class AppUtils {

    public static void toast(Activity activity, String message) {
        if (message != null && !message.equals("") && activity != null) {
            Snackbar snack = Snackbar.make(activity.findViewById(android.R.id.content),
                    message, Snackbar.LENGTH_LONG);
            ViewGroup group = (ViewGroup) snack.getView();
            group.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
            snack.show();
        }
    }

    private static ProgressDialog progressDialog;

    public static void showProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context, R.style.TransparentProgressDialog);
        progressDialog.setMessage("");
        progressDialog.setTitle(null);
        progressDialog.show();
        progressDialog.setProgressStyle(R.style.TransparentProgressDialog);
        View v = LayoutInflater.from(context).inflate(R.layout.custom_progress_dialog, null);
        ProgressBar progress = (ProgressBar) v.findViewById(R.id.progress);
        progress.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        progressDialog.setContentView(v);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
    }

    public static void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static void setSwipeRefreshColorScheme(SwipeRefreshLayout refreshLayout) {
        //refreshLayout.setColorSchemeColors(R.color.color1,R.color.color2,R.color.color3);
    }

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Typeface getHeaderTypeFace(Context context) {
//        Typeface t=Typeface.createFromAsset(context.getAssets(),"fonts/good_times.ttf");
//        Typeface t=Typeface.createFromAsset(context.getAssets(),"fonts/roboto_black.ttf");
        Typeface t = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_regular.ttf");
//        Typeface t=Typeface.createFromAsset(context.getAssets(),"fonts/roboto_light.ttf");

        return t;
    }

    public static String getAlphaColor(String originalColor) {
        long alphaFixed = Math.round(0.1 * 255);
        String alphaHex = Long.toHexString(alphaFixed);
        if (alphaHex.length() == 1) {
            alphaHex = "0" + alphaHex;
        }
        originalColor = originalColor.replace("#", "#" + alphaHex);
        return originalColor;
    }
}
