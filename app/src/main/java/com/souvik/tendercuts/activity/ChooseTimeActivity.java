package com.souvik.tendercuts.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.AddressModel;
import com.souvik.tendercuts.model.GetTimeModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.GET_DELIVERY_ADDRESS;
import static com.souvik.tendercuts.config.AppConfig.GET_TIMESLOT;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class ChooseTimeActivity extends AppCompatActivity {

    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    RadioGroup choose_time_slot;
    private List<GetTimeModel> timeListsItems;
    RadioButton rbutton;
    GetTimeModel timemodel = new GetTimeModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_time);

        setFontAwesome((LinearLayout)findViewById(R.id.timeslotLayout),ChooseTimeActivity.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(ChooseTimeActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setText("Choose Time Slot");

        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        choose_time_slot = (RadioGroup)findViewById(R.id.choose_time_slot);

        initVolleyCallback();
        Map<String, String> paramse = new HashMap<String, String>();
        String deliveryDate = getIntent().getStringExtra("deliveryDate");
        paramse.put("delivery_date", deliveryDate);
        RequestQueue mRequestQueueg = Volley.newRequestQueue(ChooseTimeActivity.this);
        mVolleyService = new VolleyService(mResultCallback,ChooseTimeActivity.this);
        mVolleyService.postStringDataVolley("DELIVERY_ADDRESS",GET_TIMESLOT,paramse,mRequestQueueg);

    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");

                    if(status_code==1){
                        //JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
                        timeListsItems = new ArrayList<GetTimeModel>();
                        JSONObject jobj = new JSONObject(jsonObject.getString("response"));
                        Log.d("creation",jobj.getString("available_slot"));

                        int k = jobj.getInt("total_slot");
                        JSONObject productlistobj = new JSONObject(jobj.getString("available_slot"));
                        int hj = 1,i=0;
                        while (k>0) {
                            GetTimeModel a = new GetTimeModel();
                            a.setTimeSlot(productlistobj.getString(hj+""));
                            a.setTimeSlotPresent(true);
                            timeListsItems.add(a);
                            Log.d("creation",productlistobj.getString(hj+""));

                            rbutton = new RadioButton(ChooseTimeActivity.this);
                            final int btnID  = i;
                            rbutton.setId(btnID);
                            rbutton.setText(productlistobj.getString(hj+""));
                            rbutton.setTextSize(14);
                            rbutton.setTextColor(0xFF8b8b8b);

                            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(
                                    RadioGroup.LayoutParams.MATCH_PARENT,
                                    RadioGroup.LayoutParams.MATCH_PARENT
                            );
                            params.setMargins(0, 10, 10, 2);

                            rbutton.setLayoutParams(params);
                            choose_time_slot.addView(rbutton);

                            hj++; k--; i++;
                        }

                        choose_time_slot.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                Log.d("creation", "CheckedId => " + checkedId);
                                Intent i = new Intent(ChooseTimeActivity.this,PaymentConfirm.class);
                                timemodel = timeListsItems.get(checkedId);
                                i.putExtra("timeDetails",timemodel);
                                setResult(3001, i);
                                finish();
                            }
                        });
                        /*int l=1;
                        for (int i=0;i<productlistArray.length();i++) {

                            JSONObject productEachRow = productlistArray.getJSONObject(i);

                            Log.d("creation",""+productEachRow.getString(""+l));
                            l++;*/
                            /*int k = productEachRow.getInt("total_slot");
                            while(k>0){
                                JSONObject productEachRow = productlistArray.getJSONObject(i);
                                k--;
                            }*/
                            /**/
                            //a.setTimeSlotToken();
                            /*a.setShipAddress(productEachRow.getString("shipAddress"));
                            a.setShipCity(productEachRow.getString("shipCity"));
                            a.setShipDefault(productEachRow.getString("shipDefault"));
                            a.setShipEmail(productEachRow.getString("shipEmail"));
                            a.setShipName(productEachRow.getString("shipName"));
                            a.setShipPhone(productEachRow.getString("shipPhone"));
                            a.setShipPostcode(productEachRow.getString("shipPostcode"));
                            a.setShipToken(productEachRow.getString("shipToken"));
                            a.setAddressPresent(true);*/

                            /*Log.d("creation","ff1=> "+session.get_customerToken().toString());
                            Log.d("creation","ff2=> "+session.get_customerApiKey().toString());
                            Log.d("creation","ff3=> "+session.get_customerEmail().toString());
                            Log.d("creation","ff4=> "+session.get_customerName().toString());*/



                            /*rbutton = new RadioButton(ChooseTimeActivity.this);
                            final int btnID  = i;
                            rbutton.setId(i);
                            rbutton.setText(FullAddress);
                            rbutton.setTextSize(14);
                            rbutton.setTextColor(0xFF8b8b8b);

                            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(
                                    RadioGroup.LayoutParams.MATCH_PARENT,
                                    RadioGroup.LayoutParams.MATCH_PARENT
                            );
                            params.setMargins(0, 10, 10, 2);

                            rbutton.setLayoutParams(params);
                            addressLists.addView(rbutton);*/
                        //}

                        /*addressLists.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                Log.d("creation", "CheckedId => " + checkedId);
                                Intent i = new Intent(DeliveryAddress.this,PaymentConfirm.class);
                                adrs = addressListsItems.get(checkedId);
                                //mRefreshListner.addressChange(adrs);
                                *//*i.putExtra("totalprice",getIntent().getStringExtra("totalprice"));
                                i.putExtra("grandtotalprice",getIntent().getStringExtra("totalprice"));
                                i.putExtra("products",getIntent().getStringExtra("products"));
                                *//*
                                i.putExtra("addressDetails",adrs);
                                setResult(5001, i);
                                finish();
                            }
                        });*/
                    }else{

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }
}
