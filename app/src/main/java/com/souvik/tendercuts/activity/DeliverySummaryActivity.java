package com.souvik.tendercuts.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.souvik.tendercuts.activity.farhan.TimeSlotActivity;
import com.souvik.tendercuts.activity.farhan.api.ApiEndpoints;
import com.souvik.tendercuts.activity.farhan.api.AppVolleySingleton;
import com.souvik.tendercuts.activity.farhan.api.VolleyApiRequest;
import com.souvik.tendercuts.activity.farhan.model.SaveData;
import com.souvik.tendercuts.activity.farhan.model.Time;
import com.souvik.tendercuts.activity.farhan.model.TimeHolder;
import com.souvik.tendercuts.activity.farhan.model.TimeSlot;
import com.souvik.tendercuts.adapter.CalendarSpinnerAdapter;
import com.souvik.tendercuts.adapter.CartProdDemoListingAdapter;
import com.souvik.tendercuts.common.CalculateCart;
import com.souvik.tendercuts.config.AppConfig;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.OnAddressChangeListener;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.AddressModel;
import com.souvik.tendercuts.model.CalculateCartModel;
import com.souvik.tendercuts.model.GetTimeModel;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.model.PaymentConfirmModel;
import com.souvik.tendercuts.payyoumoney.AppEnvironment;
import com.souvik.tendercuts.payyoumoney.AppPreference;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.CALCULATE_CART;
import static com.souvik.tendercuts.config.AppConfig.GET_DELIVERY_ADDRESS;
import static com.souvik.tendercuts.config.AppConfig.GET_TIMESLOT;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class DeliverySummaryActivity extends CalculateCart implements CalculateCart.outputResponseInteface {

    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;
    String flag = "0";
    double dblGTotal = 0, discount_amt;
    int resCode=0;

    private LinearLayout deliverydttmLayout, deliveryOptionLayout, redemedPointsLayout, couponlayout, couponTextlayout;
    private Button chooseDeliveryDate, chooseDeliveryTime, confirmPayment, couponbtnapply;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private TextView totalPrice, grandTotal, deliveryFee, points, redeemedPoint, couponpriceminus;
    private String totalpricestrng = "", grandtotalstrng = "", products = "", deliverySystem = "", earn_points = "", deliveryAddress = "";
    private EditText coupontext;


    HashMap<String, String> payment_confirm_details = new HashMap<String, String>();
    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
    private AppPreference mAppPreference;

    private String chdate = "";
    PaymentConfirmModel payment_confirm_modelobj = new PaymentConfirmModel();

    int currentpoint = 1000;

    private ProgressDialog pDialog;
    private List<GetTimeModel> timeListsItems;
    RadioButton rbutton;
    GetTimeModel timemodel = new GetTimeModel();
    RadioGroup choose_time_slot;

    CheckBox meatwallet, havecoupon;

    JSONObject morningObj, eveningObj;

    private String pm_custToken = "", pm_products = "", pm_deliveryMode = "", pm_deliveryDate = "",
            pm_deliveryTime = "", pm_deliveryDate2 = "", pm_deliveryTime2 = "", pm_deliveryAddres = "",
            pm_checkCouponCode = "no", pm_couponCode = "", pm_checkRewardPoint = "no", pm_paymentMethod = "no", pm_extraInfo = "";
    private Spinner chooseDeliveryDateSpin;

    //for delivery date and time
    String strfirstDate = null, strfirstTime = null, strsecondDate = null, strsecondTime = null, strTitle1 = "", strTitle2 = "";

    LinearLayout fb_totLayout, fb_redemedPointsLayout, fb_deliveryOptionLayout,
            fb_gstOptionLayout, fb_discountOptionLayout, fb_earnPointsLayout,
            fb_couponlayout, fb_grandTotalLayout, couponappliedlayout, payableAmountLayout;

    TextView fb_totalPrice, fb_redeemedPoint, fb_deliveryFee, fb_gstValue, fb_discountFee, fb_points, fb_couponpriceminus, fb_grandTotal, payableAmount, totalPriceButtom;
    TextView cartaddedQuantity, extraInfoText, specialInstructionHeading;
    ImageButton couponcancelbtn;
    View extraInfoTextLine;

    ArrayList<ItemModel> firstcartItems, secondcartItems;
    CartProdDemoListingAdapter firstadapter, secondadapter;
    SimpleDateFormat mDateFormat;
    CalendarSpinnerAdapter mSpinnerDateInAdapter;

    //Basic Details
    @BindView(R.id.deliveryAddressDetails)
    TextView dAddressDetails;
    @BindView(R.id.chooseDelivery)
    Button chooseDeliveryaddress;

    @BindView(R.id.firstProductinfoRecycler)
    RecyclerView firstrecyclerView;
    @BindView(R.id.secondProductinfoRecycler)
    RecyclerView secondrecyclerView;

    @BindView(R.id.firstlayout)
    CardView firstlayout;
    @BindView(R.id.secondlayout)
    CardView secondlayout;
//    @BindView(R.id.vw)
//    View vw;

    //First Delivery Block
    @BindView(R.id.firstitemCountHeading)
    TextView firstitemCountHeading;
    @BindView(R.id.firstTitle)
    TextView firstTitle;
    @BindView(R.id.firstDate)
    TextView firstDate;
    @BindView(R.id.firstCount)
    TextView firstCount;
    @BindView(R.id.firstCountLimit)
    TextView firstCountLimit;
    @BindView(R.id.firstDeliveryDateTime)
    Button firstDeliveryDateTime;
    @BindView(R.id.firstDeliveryValue)
    TextView firstDeliveryValue;
    @BindView(R.id.firstDeliveryCharge)
    TextView firstDeliveryCharge;
    //@BindView(R.id.firstProductinfoRecycler) RecyclerView firstProductinfoRecycler;

    //Second Delivery Block
    @BindView(R.id.seconditemCountHeading)
    TextView seconditemCountHeading;
    @BindView(R.id.secondTitle)
    TextView secondTitle;
    @BindView(R.id.secondDate)
    TextView secondDate;
    @BindView(R.id.secondCount)
    TextView secondCount;
    @BindView(R.id.secondCountLimit)
    TextView secondCountLimit;
    @BindView(R.id.secondDeliveryDateTime)
    Button secondDeliveryDateTime;
    @BindView(R.id.secondDeliveryValue)
    TextView secondDeliveryValue;
    @BindView(R.id.secondDeliveryCharge)
    TextView secondDeliveryCharge;
    @BindView(R.id.layout_delivery_amount1)
    LinearLayout layout_delivery_amount1;
    @BindView(R.id.layout_delivery_amount2)
    LinearLayout layout_delivery_amount2;
    @BindView(R.id.layout_delivery_summary)
    LinearLayout layout_delivery_summary;


    private JSONObject firstProductlistdArray, secondProductlistdArray;

    private static final int TIME_SELECT_CODE1 = 1;
    private static final int TIME_SELECT_CODE2 = 2;
    private Context context;
    String dcharge = "", dcharge2 = "";

//    @BindView(R.id.progressLayout)
//    RelativeLayout progressLayout;

//    private void setDates() {
//        Calendar calendar = Calendar.getInstance();
//        SimpleDateFormat formater = new SimpleDateFormat("dd MMM");
//        String firstdate = formater.format(calendar.getTime());
//        firstDate.setText("(" + firstdate + ")");
//        calendar.add(Calendar.DAY_OF_MONTH, 1);
//        String seconddate = formater.format(calendar.getTime());
//        secondDate.setText("(" + seconddate + ")");
//    }

//    @OnClick(R.id.firstDeliveryDateTime)
//    public void getFirstTimeSlot() {
//        Intent intent = new Intent(context, TimeSlotActivity.class);
//        intent.putExtra(TimeSlotActivity.DAY_TYPE, 1);
//        startActivityForResult(intent, TIME_SELECT_CODE);
//    }
//
//    @OnClick(R.id.secondDeliveryDateTime)
//    public void getSecondTimeSlot() {
//        Intent intent = new Intent(context, TimeSlotActivity.class);
//        intent.putExtra(TimeSlotActivity.DAY_TYPE, 2);
//        startActivityForResult(intent, TIME_SELECT_CODE);
//    }

    Button proceedBtn;
    TextView proceedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_summary);

        mAppPreference = new AppPreference();
        ButterKnife.bind(DeliverySummaryActivity.this);
        this.context = this;
        setFontAwesome((RelativeLayout) findViewById(R.id.deliverySummaryLayout), DeliverySummaryActivity.this);
        layout_delivery_summary.setVisibility(View.GONE);

//        setDates();

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(DeliverySummaryActivity.this);
        pDialog = new ProgressDialog(DeliverySummaryActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setVisibility(View.VISIBLE);
        titleToolbar.setText("Delivery Summary");

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        ImageView imageView5 = (ImageView) findViewById(R.id.imageView5);
        imageView5.setVisibility(View.GONE);

        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        firstDeliveryDateTime.setText("Select the delivery slot");
        secondDeliveryDateTime.setText("Select the delivery slot");

        //***************************************************************************************************************************
        //Value Initialization//
        //***************************************************************************************************************************
        pm_custToken = session.get_customerToken();
        pm_products = getIntent().getStringExtra("products");
        pm_deliveryMode = "1";
        pm_checkCouponCode = getIntent().getStringExtra("checkCouponCode");
        pm_couponCode = getIntent().getStringExtra("couponCode");
        pm_checkRewardPoint = getIntent().getStringExtra("checkRewardPoint");
        pm_extraInfo = getIntent().getStringExtra("extraInfo");

        //call api
        postParameter();

        //deliverydttmLayout = (LinearLayout)findViewById(R.id.deliverydttmLayout);
        deliveryOptionLayout = (LinearLayout) findViewById(R.id.deliveryOptionLayout);

        //chooseDeliveryDate = (Button)findViewById(R.id.chooseDeliveryDate);
        //chooseDeliveryTime = (Button)findViewById(R.id.chooseDeliveryTime);
        confirmPayment = (Button) findViewById(R.id.confirmPayment);
        //chooseDeliveryDateSpin = (Spinner)findViewById(R.id.chooseDeliveryDateSpin);


        totalPrice = (TextView) findViewById(R.id.totalPrice);
        grandTotal = (TextView) findViewById(R.id.grandTotal);
        deliveryFee = (TextView) findViewById(R.id.deliveryFee);
        points = (TextView) findViewById(R.id.points);

        redemedPointsLayout = (LinearLayout) findViewById(R.id.redemedPointsLayout);
        redeemedPoint = (TextView) findViewById(R.id.redeemedPoint);

        couponlayout = (LinearLayout) findViewById(R.id.couponlayout);
        couponbtnapply = (Button) findViewById(R.id.couponbtnapply);
        coupontext = (EditText) findViewById(R.id.coupontext);
        couponTextlayout = (LinearLayout) findViewById(R.id.couponTextlayout);
        cartaddedQuantity = (TextView) findViewById(R.id.cartaddedQuantity);
        couponpriceminus = (TextView) findViewById(R.id.couponpriceminus);
        /*nopointavailable = (TextView)findViewById(R.id.nopointavailable); */
        extraInfoText = (TextView) findViewById(R.id.extraInfoText);
        specialInstructionHeading = (TextView) findViewById(R.id.specialInstructionHeading);
        extraInfoTextLine = (View) findViewById(R.id.extraInfoTextLine);


        firstcartItems = new ArrayList<ItemModel>();
        secondcartItems = new ArrayList<ItemModel>();

        firstadapter = new CartProdDemoListingAdapter(DeliverySummaryActivity.this, firstcartItems);
        Log.e("Size",""+firstcartItems.size());
//        recyclerView = (RecyclerView) findViewById(R.id.prodListsRecycler);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        firstrecyclerView.setLayoutManager(mLayoutManager1);
        firstrecyclerView.setAdapter(firstadapter);

        secondadapter = new CartProdDemoListingAdapter(DeliverySummaryActivity.this, secondcartItems);
//        recyclerView = (RecyclerView) findViewById(R.id.prodListsRecycler);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        secondrecyclerView.setLayoutManager(mLayoutManager2);
        secondrecyclerView.setAdapter(secondadapter);


        initialization(DeliverySummaryActivity.this);

//        pm_deliveryDate = "Today";
//        pm_deliveryDate2=secondDate.getText().toString();
        paymentConfirmPageLoad(pm_custToken, pm_products, pm_deliveryMode, pm_deliveryDate, pm_deliveryTime, pm_deliveryDate2, pm_deliveryTime2, pm_deliveryAddres, pm_checkCouponCode, pm_couponCode, pm_checkRewardPoint, pm_extraInfo, "no", "no");

        /*try {

            JSONObject jj = new JSONObject(deliveryAddress);

            pm_deliveryAddres = jj.getString("shipToken");

            String fullAdress = jj.getString("shipName") + ", " +
                    jj.getString("shipEmail") + ", " +
                    jj.getString("shipPhone") + ", " +
                    jj.getString("shipAddress") + ", " +
                    jj.getString("shipCity") + ", " +
                    jj.getString("shipPostcode");
            addressDetails.setText(fullAdress);
            payment_confirm_details.put("deliveryAddress",fullAdress);
            payment_confirm_details.put("deliveryAddressToken",jj.getString("shipToken"));
            //addressDetails.setText(deliveryAddress);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        payment_confirm_details.put("totalPrice",totalpricestrng);
        payment_confirm_details.put("grandTotal",grandtotalstrng);
        payment_confirm_details.put("products",products);*/


        //*****************************************************************************************************************
        fb_totLayout = (LinearLayout) findViewById(R.id.totLayout);
        fb_redemedPointsLayout = (LinearLayout) findViewById(R.id.redemedPointsLayout);
        fb_deliveryOptionLayout = (LinearLayout) findViewById(R.id.deliveryOptionLayout);
        fb_gstOptionLayout = (LinearLayout) findViewById(R.id.gstOptionLayout);
        fb_discountOptionLayout = (LinearLayout) findViewById(R.id.discountOptionLayout);
        fb_earnPointsLayout = (LinearLayout) findViewById(R.id.earnPointsLayout);
        fb_couponlayout = (LinearLayout) findViewById(R.id.couponlayout);
        fb_grandTotalLayout = (LinearLayout) findViewById(R.id.grandTotalLayout);
        couponappliedlayout = (LinearLayout) findViewById(R.id.couponappliedlayout);
        payableAmountLayout = (LinearLayout) findViewById(R.id.payableAmountLayout);

        fb_totalPrice = (TextView) findViewById(R.id.totalPrice);
        fb_redeemedPoint = (TextView) findViewById(R.id.redeemedPoint);
        fb_deliveryFee = (TextView) findViewById(R.id.deliveryFee);
        fb_gstValue = (TextView) findViewById(R.id.gstValue);
        fb_discountFee = (TextView) findViewById(R.id.discountFee);
        fb_points = (TextView) findViewById(R.id.points);
        fb_couponpriceminus = (TextView) findViewById(R.id.couponpriceminus);
        fb_grandTotal = (TextView) findViewById(R.id.grandTotal);
        payableAmount = (TextView) findViewById(R.id.payableAmount);
        totalPriceButtom = (TextView) findViewById(R.id.totalPriceButtom);

        meatwallet = (CheckBox) findViewById(R.id.meatwallet);
        havecoupon = (CheckBox) findViewById(R.id.havecoupon);

        couponcancelbtn = (ImageButton) findViewById(R.id.couponcancelbftn);
        //*****************************************************************************************************************

        chooseDeliveryaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DeliverySummaryActivity.this, DeliveryAddress.class);
                i.putExtra("totalprice", totalpricestrng);
                i.putExtra("grandtotalprice", grandtotalstrng);
                i.putExtra("products", payment_confirm_details.get("products"));

                i.putExtra("date1", pm_deliveryDate);
                i.putExtra("time1", pm_deliveryTime);
                i.putExtra("charge1", firstDeliveryCharge.getText().toString());

                i.putExtra("date2", pm_deliveryDate2);
                i.putExtra("time2", pm_deliveryTime2);
                i.putExtra("charge2", secondDeliveryCharge.getText().toString());

                startActivityForResult(i, 1001);

                Log.e("tm ", pm_deliveryTime + " " + pm_deliveryTime2 + " " + firstDeliveryCharge.getText().toString() +
                        " " + secondDeliveryCharge.getText().toString());

            }
        });

        proceedText = (TextView) findViewById(R.id.proceedText);
        proceedText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!dAddressDetails.getText().toString().contains("No address available/selected. Please add/select address")) {
                    proceed();
                }else {
                    Snackbar.make(findViewById(android.R.id.content), "Select delivery address", Snackbar.LENGTH_SHORT).show();
                }

//            Log.e("add ", pm_deliveryAddres);
//                Intent i = new Intent(DeliverySummaryActivity.this, OrderSumActivity.class);
//                i.putExtra("products", pm_products);
//                i.putExtra("deliveryMode", pm_deliveryMode);
//                i.putExtra("deliveryDate", pm_deliveryDate);
//                i.putExtra("deliveryTime", pm_deliveryTime);
//                i.putExtra("deliveryAddress", pm_deliveryAddres);
//                i.putExtra("checkCouponCode", pm_checkCouponCode);
//                i.putExtra("couponCode", pm_couponCode);
//                i.putExtra("checkRewardPoint", pm_checkRewardPoint);
//                i.putExtra("extraInfo", pm_extraInfo);
//                i.putExtra("time_slot", firstDeliveryDateTime.getText().toString());
//                i.putExtra("deliveryDate2", pm_deliveryDate2);
//                i.putExtra("deliveryTime2", pm_deliveryTime2);
//                i.putExtra("titledt1", firstTitle.getText().toString()+" "+firstDate.getText().toString());
//                i.putExtra("titledt2", secondTitle.getText().toString()+" "+secondDate.getText().toString());
//                i.putExtra("flag", flag);
//                Log.e("Item length ", firstcartItems.size()+""+secondcartItems.size());
//                if(firstcartItems.size()>0 && secondcartItems.size()>0){
//                    if(!pm_deliveryTime.isEmpty() && !pm_deliveryTime2.isEmpty()){
//                        startActivity(i);
//                    }
//                    else {
//                        Snackbar.make(findViewById(android.R.id.content),"Select delivery time", Snackbar.LENGTH_SHORT).show();
//                    }
//                }
//                else  if(firstcartItems.size()>0 ) {
//                    if (!pm_deliveryTime.isEmpty()) {
//                        startActivity(i);
//                    } else {
//                        Snackbar.make(findViewById(android.R.id.content), "Select delivery time", Snackbar.LENGTH_SHORT).show();
//                    }
//                }
//                else  if(secondcartItems.size()>0 ) {
//                    if (!pm_deliveryTime2.isEmpty()) {
//                        startActivity(i);
//                    } else {
//                        Snackbar.make(findViewById(android.R.id.content), "Select delivery time", Snackbar.LENGTH_SHORT).show();
//                    }
//                }


                //checkOutFinal();
            }
        });
        String lenth;
        proceedBtn = (Button) findViewById(R.id.proceedBtn);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("add ", pm_deliveryAddres);

                if(!dAddressDetails.getText().toString().contains("No address available/selected. Please add/select address")) {
                    proceed();
                }else {
                    Snackbar.make(findViewById(android.R.id.content), "Select delivery address", Snackbar.LENGTH_SHORT).show();
                }

            }
        });

    }


    private void initVolly() {
            mResultCallback = new FccResult() {
                @Override
                public void notifySuccess(String requestType,JSONObject response) {
                    Log.e("Response " ,""+ response);
                }

                @Override
                public void notifySuccessString(String requestType, String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status_code = jsonObject.getInt("status");

                        if (status_code == 1) {
                            //JSONObject responseObject = new JSONObject(jsonObject.getString("response"));

                            JSONArray productlistArray = new JSONArray(jsonObject.getString("response"));
                            for (int i = 0; i < productlistArray.length(); i++) {
                                JSONObject productEachRow = productlistArray.getJSONObject(i);
                                if(productEachRow.getString("shipDefault").equalsIgnoreCase("yes")) {
                                    AddressModel a = new AddressModel();
                                    a.setShipAddress(productEachRow.getString("shipAddress"));
                                    a.setShipCity(productEachRow.getString("shipCity"));
                                    a.setShipDefault(productEachRow.getString("shipDefault"));
                                    a.setShipEmail(productEachRow.getString("shipEmail"));
                                    a.setShipName(productEachRow.getString("shipName"));
                                    a.setShipPhone(productEachRow.getString("shipPhone"));
                                    a.setShipPostcode(productEachRow.getString("shipPostcode"));
                                    a.setShipToken(productEachRow.getString("shipToken"));
//                            a.setShipDefault(productEachRow.getString("shipDefault"));
                                    a.setAddressPresent(true);
                                    String FullAddress = a.getShipName().trim() + ",\n" +
                                            a.getShipEmail().trim() + ",\n" +
                                            a.getShipPhone().trim() + ",\n" +
                                            a.getShipAddress().trim() + ",\n" +
                                            a.getShipCity().trim() + ",\n" +
                                            a.getShipPostcode().trim();
                                    dAddressDetails.setText(FullAddress);
                                    pm_deliveryAddres = productEachRow.getString("shipToken");
                                }
                                //8527749038 9350668598
//7678569100
                            }
                        }
                        else if (status_code == 0) {
                            dAddressDetails.setText("No address available/selected. Please add/select address");
                        }

                    } catch (JSONException e) {
                        Log.e("Exception ", e.getMessage());
                    }
                    //CART_LISTING
                }

                 @Override
                public void notifyError(String requestType,VolleyError error) {
                    Log.e("Error ", "Volley requester error==" + error.getMessage());
                    Log.e("Error ", "Volley JSON post" + "That didn't work! "+requestType);
                }
            };

    }

    private void postParameter() {
        showDialog();
//        progressLayout.setVisibility(View.VISIBLE);
        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", session.get_customerToken());
        paramse.put("products", pm_products);
        Log.e("Product ", pm_products+" token "+session.get_customerToken());
        initVolleyCallback();
        RequestQueue mRequestQueueg = Volley.newRequestQueue(DeliverySummaryActivity.this);
        mVolleyService = new VolleyService(mResultCallback, DeliverySummaryActivity.this);
        mVolleyService.postStringDataVolley("CALCULATE_CART", CALCULATE_CART, paramse, mRequestQueueg);

//        setData();
    }

//    @Override
//    protected void onPostResume() {
//        super.onPostResume();
//
//        if (!pm_deliveryTime.isEmpty() && pm_deliveryTime.length() > 0) {
//            chooseDeliveryTime.setText(pm_deliveryTime);
//        }
//
//    }


    @Override
    public void outputCalculateResponse(CalculateCartModel c) {
        Log.d("Response", "Value => " + c.getStatus());
        Log.e("Charge ", c.getFirst_delivery_date() + "  " + c.getFirst_delivery_time());
//        if(c.getFirst_delivery_time()!=""){
        // firstDeliveryDateTime.setText(c.getFirst_delivery_time());
//        }

        fb_totalPrice.setText("₹" + c.getSub_total());
        if (!c.getSub_total().isEmpty() && Double.parseDouble(c.getSub_total()) > 0) {
            fb_totLayout.setVisibility(View.VISIBLE);
        }
        discount_amt = Double.parseDouble(c.getDiscount_amount());
        fb_discountFee.setText("(-)₹" + c.getDiscount_amount());
        if (!c.getDiscount_amount().isEmpty() && Double.parseDouble(c.getDiscount_amount()) > 0) {
            fb_discountOptionLayout.setVisibility(View.VISIBLE);
        }

        fb_deliveryFee.setText("₹" + c.getDelivery_charge());
        if (!c.getDelivery_charge().isEmpty() && Double.parseDouble(c.getDelivery_charge()) > 0) {
            fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
        }

        fb_gstValue.setText("₹" + c.getGst_value());
        if (!c.getGst_value().isEmpty() && Double.parseDouble(c.getGst_value()) > 0) {
            fb_gstOptionLayout.setVisibility(View.VISIBLE);
        }

//                                fb_grandTotal.setText("₹"+responseObject.getString("grand_total"));
        dblGTotal = Double.parseDouble(c.getGrand_total());
        fb_grandTotal.setText("₹" + c.getGrand_total());
        if (!c.getGrand_total().isEmpty() && Double.parseDouble(c.getGrand_total()) > 0) {
            fb_grandTotalLayout.setVisibility(View.VISIBLE);
        }

        fb_redeemedPoint.setText("(-)₹" + c.getRedeemed_point());
        if (!c.getRedeemed_point().isEmpty() && Double.parseDouble(c.getRedeemed_point()) > 0) {
            fb_redemedPointsLayout.setVisibility(View.VISIBLE);
        }

        fb_points.setText(c.getEarn_points() + " Points");
//        if (!c.getEarn_points().isEmpty() && Double.parseDouble(c.getEarn_points()) > 0) {
            fb_earnPointsLayout.setVisibility(View.VISIBLE);
//        }

        payableAmount.setText("₹" + c.getPayable_amount());
        totalPriceButtom.setText("₹" + c.getPayable_amount());
        if (!c.getPayable_amount().isEmpty() && Double.parseDouble(c.getPayable_amount()) > 0) {
            payableAmountLayout.setVisibility(View.VISIBLE);
        }

        if(!c.getCoupon_code().isEmpty() || c.getCoupon_code().equalsIgnoreCase("")){
            Log.e("Coupon ", ""+c.getCoupon_applied());
            if(c.getCoupon_applied()==1){
                couponlayout.setVisibility(View.VISIBLE);
            }else {
                couponlayout.setVisibility(View.GONE);
            }
        }else {
            couponlayout.setVisibility(View.GONE);
        }

        Log.e("Def address ", "" + c.getDefault_address());
        try {

            JSONObject jj = new JSONObject(c.getDefault_address());
            if (jj.getString("shipToken").equalsIgnoreCase("No Address Found")) {
                dAddressDetails.setText("No address available/selected. Please add/select address");
            } else {

                String fullAdress = jj.getString("shipName").trim() + "\n" +
                        jj.getString("shipAddress").trim() + ",\n" +
                        jj.getString("shipCity").trim() + ",\n" +
                        jj.getString("shipPostcode").trim() + ",\n" +
                        jj.getString("shipEmail").trim() + ",\n" +
                        jj.getString("shipPhone").trim();
                dAddressDetails.setText(fullAdress);

                pm_deliveryAddres = jj.getString("shipToken");
            }

        } catch (JSONException e) {
            Log.e("Exception ", e.getMessage());
        }

        try {

            if (c.getFirst().equalsIgnoreCase("{}")) {
                Log.d("creation", "First -> jsonarray");
                firstlayout.setVisibility(View.GONE);
                secondCount.setText("1");
                secondCountLimit.setText("of 1");
            } else {
                firstCountLimit.setText("of 1");
                firstProductlistdArray = new JSONObject(c.getFirst());
                Log.e("first time ", firstProductlistdArray.toString());
                firstitemCountHeading.setText(firstProductlistdArray.getString("msg_1"));
                strTitle1 = firstProductlistdArray.getString("msg_3");

                firstDate.setText(firstProductlistdArray.getString("msg_4"));
                firstTitle.setText(strTitle1);

                JSONArray slotArray = firstProductlistdArray.getJSONArray("slot");
                //slotArray.getJSONObject(0).getString("day"));
                if (firstDeliveryDateTime.getText().toString().equalsIgnoreCase("Select the delivery slot")) {
                    firstDeliveryCharge.setText("₹0");
                }
                if (firstProductlistdArray.getInt("delivery_charge") != 0) {
                    firstDeliveryCharge.setText("₹" + firstProductlistdArray.getInt("delivery_charge"));
                } else if (firstProductlistdArray.getInt("delivery_charge") == 0) {
                    firstDeliveryCharge.setText("₹" + firstProductlistdArray.getInt("delivery_charge"));
                }

                firstcartItems.clear();
                int price = 0;
                JSONArray fproductlistdArray = firstProductlistdArray.getJSONArray("items");
                for (int i = 0; i < fproductlistdArray.length(); i++) {
                    JSONObject fproductaEachRow = fproductlistdArray.getJSONObject(i);

                    //ItemModel cartItemsList = items.get(i);

                    ItemModel m = new ItemModel();
                    m.setProdToken(fproductaEachRow.getString("prodToken"));
                    m.setProdName(fproductaEachRow.getString("prodName"));
                    m.setProdDesc(fproductaEachRow.getString("prodDesc"));
                    //m.setMarketPrice(productaEachRow.getString("marketPrice"));
                    m.setSellPrice(fproductaEachRow.getString("sellPrice"));
                    m.setPic(fproductaEachRow.getString("pic"));
                    m.setCartAddedQuantity(fproductaEachRow.getInt("cartQuan"));
                    m.setStockAddedQuantity(fproductaEachRow.getInt("stockQuan"));
                    m.setNetWeight(fproductaEachRow.getString("netWeight"));
                    firstcartItems.add(m);
                    price += Integer.parseInt(fproductaEachRow.getString("sellPrice"));
                    // Log.d("creation", " => " + productEachRow.getString("prodName"));
                }
                layout_delivery_amount1.setVisibility(View.VISIBLE);
                firstDeliveryValue.setText(" ₹" + price);
                firstadapter.notifyDataSetChanged();

                firstDeliveryDateTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
//                            Intent i = new Intent(DeliverySummaryActivity.this, DeliveryDateTimeActivity.class);
//                            i.putExtra("deliveryType", "first");
//                            i.putExtra("deliverySlot", firstProductlistdArray.getString("slot"));
//                            startActivity(i);

                            Intent i = new Intent(DeliverySummaryActivity.this, TimeSlotActivity.class);
                            i.putExtra("deliveryType", "first");
                            i.putExtra("day", firstTitle.getText().toString() + " " + firstDate.getText().toString());
                            i.putExtra("deliverySlot", firstProductlistdArray.getString("slot"));
                            startActivityForResult(i, TIME_SELECT_CODE1);
                        } catch (Exception e) {
                            Log.e("Error ", e.getMessage());
                        }
                    }
                });

            }

            if (c.getSecond().equalsIgnoreCase("{}")) {
                Log.d("creation", "Second -> jsonarray");
                secondlayout.setVisibility(View.GONE);
                // vw.setVisibility(View.GONE);
            } else {
                flag = "1";
//                vw.setVisibility(View.VISIBLE);
                secondProductlistdArray = new JSONObject(c.getSecond());
                Log.e("Second time ", secondProductlistdArray.toString());
                seconditemCountHeading.setText(secondProductlistdArray.getString("msg_1"));

                strTitle2 = secondProductlistdArray.getString("msg_3");
                secondTitle.setText(strTitle2);
                secondDate.setText(secondProductlistdArray.getString("msg_4"));

                JSONArray slotArray = secondProductlistdArray.getJSONArray("slot");
                // secondDeliveryDateTime.setText("Select the delivery slot");//slotArray.getJSONObject(0).getString("day"));
                if (secondDeliveryDateTime.getText().toString().equalsIgnoreCase("Select the delivery slot")) {
                    secondDeliveryCharge.setText(" ₹0");
                }
                if (secondProductlistdArray.getInt("delivery_charge") != 0) {
                    secondDeliveryCharge.setText("₹" + secondProductlistdArray.getInt("delivery_charge"));
                }
//                else if(firstProductlistdArray.getInt("delivery_charge")==0){
//                    secondDeliveryCharge.setText("₹"+secondProductlistdArray.getInt("delivery_charge"));
//                }

                int price2 = 0;
                secondcartItems.clear();
                JSONArray productlistdArray = secondProductlistdArray.getJSONArray("items");
                for (int i = 0; i < productlistdArray.length(); i++) {
                    JSONObject productaEachRow = productlistdArray.getJSONObject(i);

                    //ItemModel cartItemsList = items.get(i);

                    ItemModel m = new ItemModel();
                    m.setProdToken(productaEachRow.getString("prodToken"));
                    m.setProdName(productaEachRow.getString("prodName"));
                    m.setProdDesc(productaEachRow.getString("prodDesc"));
                    //m.setMarketPrice(productaEachRow.getString("marketPrice"));
                    m.setSellPrice(productaEachRow.getString("sellPrice"));
                    m.setPic(productaEachRow.getString("pic"));
                    m.setCartAddedQuantity(productaEachRow.getInt("cartQuan"));
                    m.setStockAddedQuantity(productaEachRow.getInt("stockQuan"));
                    m.setNetWeight(productaEachRow.getString("netWeight"));
                    secondcartItems.add(m);
                    price2 += Integer.parseInt(productaEachRow.getString("sellPrice"));
                    // Log.d("creation", " => " + productEachRow.getString("prodName"));
                }
                layout_delivery_amount2.setVisibility(View.VISIBLE);
                secondDeliveryValue.setText(" ₹" + price2);
                secondadapter.notifyDataSetChanged();

                secondDeliveryDateTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            Intent i = new Intent(DeliverySummaryActivity.this, TimeSlotActivity.class);
                            i.putExtra("deliveryType", "second");
                            i.putExtra("day", secondTitle.getText().toString() + " " + secondDate.getText().toString());
                            i.putExtra("deliverySlot", secondProductlistdArray.getString("slot"));
                            startActivityForResult(i, TIME_SELECT_CODE2);
                        } catch (Exception e) {
                            Log.e("Exception ", e.getMessage());
                        }
                    }
                });


            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        layout_delivery_summary.setVisibility(View.VISIBLE);
    }


    boolean deliveryoptionset = false;

    void initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
//                Log.d("creation DSA", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

//                layout_delivery_summary.setVisibility(View.VISIBLE);
                Log.d("Response  ",""+ response);
                hideDialog();
                //hideDialog();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if (status_code == 1) {
                        JSONObject objectResponse = jsonObject.getJSONObject("response");

                        strfirstDate = objectResponse.getString("first_delivery_date");
                        strfirstTime = objectResponse.getString("first_delivery_time");
                        strsecondDate = objectResponse.getString("second_delivery_date");
                        strfirstTime = objectResponse.getString("second_delivery_time");

                        pm_deliveryDate = strfirstDate;
                        pm_deliveryDate2 = strsecondDate;

                        int quantity = 0;
                        JSONArray productlistArray = objectResponse.getJSONArray("in_stock");
                        for (int i = 0; i < productlistArray.length(); i++) {
                            JSONObject productEachRow = productlistArray.getJSONObject(i);

                            quantity = quantity + Integer.parseInt(productEachRow.getString("cartQuan"));
                            //quantity = quantity + Integer.parseInt(productEachRow.getString("stockQuan"));
                        }
                        Log.e("Quantity ", "" + quantity);
                        cartaddedQuantity.setText("" + quantity);

                    }
                } catch (JSONException ex) {
                    Log.e("JSON Exception ", ex.getMessage());
                }

//
//
//                /*if(requestType.equalsIgnoreCase("COUPON_VALIDATE")){
//                    hideDialog();
//                    try {
//                        JSONObject jsonObject = new JSONObject(response);
//                        int status_code = jsonObject.getInt("status");
//                        if(status_code==1){
//
//                            JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
//                            couponlayout.setVisibility(View.VISIBLE);
//                            Double totalprice = Double.parseDouble(responseObject.getString("total_amount").replaceAll(",", ""));
//                            totalPrice.setText(totalprice+"");
//                            couponpriceminus.setText("-"+responseObject.getString("discount_amount"));
//                            //int grandtotal = responseObject.getInt("cart_subtotal") - responseObject.getInt("earn_points");
//                            grandTotal.setText(""+responseObject.getString("payable_amount"));
//
//                        }else{
//                            payment_confirm_details.put("coupon_code","");
//                            MeatPointUtils.decisionAlertOnMainThread(DeliverySummaryActivity.this,
//                                    android.R.drawable.ic_dialog_info,
//                                    R.string.error,
//                                    "Coupon code is not valid.",
//                                    "Ok", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            dialog.dismiss();
//                                        }
//                                    }, "", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
//                                        }
//                                    }
//                            );
//                            couponlayout.setVisibility(View.GONE);
//                            grandTotal.setText(""+grandtotalstrng);
//                        }
//                    }catch (Exception e){
//
//                    }
//                }else if(requestType.equalsIgnoreCase("DELIVERY_ADDRESS")) {
//                    hideDialog();
//                    try {
//                        JSONObject jsonObject = new JSONObject(response);
//                        int status_code = jsonObject.getInt("status");
//
//                        if(status_code==1){
//                            timeListsItems = new ArrayList<GetTimeModel>();
//                            JSONObject jobj = new JSONObject(jsonObject.getString("response"));
//                            JSONObject productlistobj = new JSONObject(jobj.getString("available_slot"));
//
//                            morningObj = new JSONObject(productlistobj.getString("morning"));
//                            eveningObj = new JSONObject(productlistobj.getString("evening"));
//
//                            LayoutInflater li = LayoutInflater.from(DeliverySummaryActivity.this);
//                            View confirmDialog = li.inflate(R.layout.dialog_choose_timeslot, null);
//                            choose_time_slot = (RadioGroup)confirmDialog.findViewById(R.id.choose_time_slot);
//                            int morningLngth = morningObj.length();
//                            int eveningLngth = eveningObj.length();
//                            int totalLngth = morningLngth+eveningLngth;
//                            int hj = 1,i=0;
//                            int morningHead = 1, eveningHead = 0;
//
//                            for(int jk=2; jk<=totalLngth+1;){
//
//                                if(morningHead==1) {
//                                    radioButtonCreation(rbutton, DeliverySummaryActivity.this,"Morning", true);
//                                    morningHead = 0;
//                                }
//
//                                while (morningLngth>0){
//                                    //Log.d("creation",morningObj.getString(jk+""));
//                                    radioButtonCreation(rbutton, DeliverySummaryActivity.this,morningObj.getString(jk+""), false);
//                                    jk++;
//                                    morningLngth--;
//                                    eveningHead = 1;
//                                }
//                                if(eveningHead==1) {
//                                    radioButtonCreation(rbutton, DeliverySummaryActivity.this,"Evening", true);
//                                    morningHead = 0;
//                                    eveningHead = 0;
//                                }
//                                while (eveningLngth>0){
//                                    //Log.d("creation",eveningObj.getString(jk+""));
//                                    radioButtonCreation(rbutton, DeliverySummaryActivity.this,eveningObj.getString(jk+""), false);
//                                    jk++;
//                                    eveningLngth--;
//                                }
//                            }
//                            AppCompatButton buttonUpdate = (AppCompatButton) confirmDialog.findViewById(R.id.choseBtn);
//                            AlertDialog.Builder alert = new AlertDialog.Builder(DeliverySummaryActivity.this);
//                            alert.setView(confirmDialog);
//                            final AlertDialog alertDialog = alert.create();
//                            buttonUpdate.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    if(choose_time_slot.getCheckedRadioButtonId()!=-1){
//                                        int id= choose_time_slot.getCheckedRadioButtonId();
//
//                                        /*//***********************************************************************************
//                 /*//***********************************************************************************
//
//                 if(morningObj.has(id+"")){
//                 try {
//                 pm_deliveryTime = morningObj.getString(id+"");
//
//                 } catch (JSONException e) {
//                 e.printStackTrace();
//                 }
//                 }else if(eveningObj.has(id+"")){
//                 try {
//                 pm_deliveryTime = eveningObj.getString(id+"");
//                 } catch (JSONException e) {
//                 e.printStackTrace();
//                 }
//                 }
//                 chooseDeliveryTime.setText(pm_deliveryTime);
//                 paymentConfirmPageLoad(pm_custToken,pm_products,pm_deliveryMode,pm_deliveryDate,pm_deliveryTime,pm_deliveryAddres,pm_checkCouponCode,pm_couponCode,pm_checkRewardPoint,pm_extraInfo);
//                 alertDialog.dismiss();
//                 }
//                 }
//                 });
//                 alertDialog.show();
//
//                 }else{
//
//                 }
//                 } catch (JSONException e) {
//                 e.printStackTrace();
//                 }
//                 }else if(requestType.equalsIgnoreCase("CALCULATE_CART")){
//                 try {
//                 hideDialog();
//                 try {
//                 JSONObject jsonObject = new JSONObject(response);
//                 int status_code = jsonObject.getInt("status");
//                 if (status_code == 1) {
//
//                 JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
//
//                 fb_totalPrice.setText("₹"+responseObject.getString("sub_total"));
//                 if(!responseObject.getString("sub_total").isEmpty() && Double.parseDouble(responseObject.getString("sub_total"))>0) {
//                 fb_totLayout.setVisibility(View.VISIBLE);
//                 }
//
//                 fb_discountFee.setText("₹"+responseObject.getString("discount_amount"));
//                 if(!responseObject.getString("discount_amount").isEmpty() && Double.parseDouble(responseObject.getString("discount_amount"))>0) {
//                 fb_discountOptionLayout.setVisibility(View.VISIBLE);
//                 }
//
//                 fb_deliveryFee.setText("₹"+responseObject.getString("delivery_charge"));
//                 if(!responseObject.getString("delivery_charge").isEmpty() && Double.parseDouble(responseObject.getString("delivery_charge"))>0) {
//                 fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
//                 }
//
//                 fb_gstValue.setText("₹"+responseObject.getString("gst_value"));
//                 if(!responseObject.getString("gst_value").isEmpty() && Double.parseDouble(responseObject.getString("gst_value"))>0) {
//                 fb_gstOptionLayout.setVisibility(View.VISIBLE);
//                 }
//
//                 //                                fb_grandTotal.setText("₹"+responseObject.getString("grand_total"));
//                 fb_grandTotal.setText("₹"+responseObject.getString("payable_amount"));
//                 if(!responseObject.getString("grand_total").isEmpty() && Double.parseDouble(responseObject.getString("grand_total"))>0) {
//                 fb_grandTotalLayout.setVisibility(View.VISIBLE);
//                 }
//
//                 fb_redeemedPoint.setText("(-)₹"+responseObject.getString("redeemed_point"));
//                 if(!responseObject.getString("redeemed_point").isEmpty() && Double.parseDouble(responseObject.getString("redeemed_point"))>0) {
//                 fb_redemedPointsLayout.setVisibility(View.VISIBLE);
//                 }
//
//                 fb_points.setText(responseObject.getString("earn_points")+" Points");
//                 if(!responseObject.getString("earn_points").isEmpty() && Double.parseDouble(responseObject.getString("earn_points"))>0) {
//                 fb_earnPointsLayout.setVisibility(View.VISIBLE);
//                 }
//
//                 //                                payableAmount.setText("₹"+responseObject.getString("payable_amount"));
//                 totalPriceButtom.setText("₹" + responseObject.getString("payable_amount"));
//                 *//*if(!responseObject.getString("payable_amount").isEmpty() && Double.parseDouble(responseObject.getString("payable_amount"))>0) {
//                                    payableAmountLayout.setVisibility(View.VISIBLE);
//                                }*//*
//
//                                *//*extraInfoText.setText(responseObject.getString("extra_info"));
//                                if(!responseObject.getString("extra_info").isEmpty() && responseObject.getString("extra_info").length()>0) {
////                                    extraInfoTextLine.setVisibility(View.VISIBLE);
//                                    extraInfoText.setVisibility(View.VISIBLE);
//                                    specialInstructionHeading.setVisibility(View.VISIBLE);
//                                }*//*
//
//
//                                *//*if(!responseObject.getString("coupon_code").isEmpty()) {
//                                    couponappliedlayout.setVisibility(View.VISIBLE);
//                                    redemedPointsLayout.setVisibility(View.GONE);
//                                    couponTextlayout.setVisibility(View.GONE);
//                                }*//*
////                                int quantity=0;
//                                JSONArray productlistArray = responseObject.getJSONArray("in_stock");
//                                for (int i = 0; i < productlistArray.length(); i++) {
//                                    JSONObject productEachRow = productlistArray.getJSONObject(i);
//                                    quantity = quantity + Integer.parseInt(productEachRow.getString("cartQuan"));
//                                    //quantity = quantity + Integer.parseInt(productEachRow.getString("stockQuan"));
//                                }
//                                cartaddedQuantity.setText(""+quantity);
//
//                                //responseObject.getString("coupon_code");
//                                //responseObject.getString("payable_amount");
//                                JSONObject jj = new JSONObject(responseObject.getString("default_address"));
//                                pm_deliveryAddres = jj.getString("shipToken");
//                                String fullAdress = jj.getString("shipName") + ", " +
//                                        jj.getString("shipEmail") + ", " +
//                                        jj.getString("shipPhone") + ", " +
//                                        jj.getString("shipAddress") + ", " +
//                                        jj.getString("shipCity") + ", " +
//                                        jj.getString("shipPostcode");
//                                addressDetails.setText(fullAdress);
//                                //payment_confirm_details.put("deliveryAddress",fullAdress);
//                                //payment_confirm_details.put("deliveryAddressToken",jj.getString("shipToken"));
//                                *//*if(!deliveryoptionset) {
//                                    deliveryOptionSet(responseObject.getString("delivery_option"));
//                                }*//*
//
//                                *//*JSONArray productlistdArray = responseObject.getJSONArray("in_stock");
//                                for (int i = 0; i < productlistdArray.length(); i++) {
//                                    JSONObject productaEachRow = productlistdArray.getJSONObject(i);
//
//                                    //ItemModel cartItemsList = items.get(i);
//
//                                    ItemModel m = new ItemModel();
//                                    m.setProdToken(productaEachRow.getString("prodToken"));
//                                    m.setProdName(productaEachRow.getString("prodName"));
//                                    m.setProdDesc(productaEachRow.getString("prodDesc"));
//                                    //m.setMarketPrice(productaEachRow.getString("marketPrice"));
//                                    m.setSellPrice(productaEachRow.getString("sellPrice"));
//                                    m.setPic(productaEachRow.getString("pic"));
//                                    m.setCartAddedQuantity(productaEachRow.getInt("cartQuan"));
//                                    m.setStockAddedQuantity(productaEachRow.getInt("stockQuan"));
//                                    m.setNetWeight(productaEachRow.getString("netWeight"));
//                                    cartItems.add(m);
////                                    Log.d("creation", " => " + productEachRow.getString("prodName"));
//                                }
//                                adapter.notifyDataSetChanged();*//*
//
//
//                                //Todo: For First Item Delivery
//                                Log.d("creation",responseObject.getString("first"));
//                                *//*if (responseObject.getJSONObject("first") instanceof JSONObject){
//                                    Log.d("creation","First -> jsonobject");
//                                }*//*
//
//                                if (responseObject.getString("first").equalsIgnoreCase("null")){
//                                    Log.d("creation","First -> jsonarray");
//                                }else{
//                                    JSONObject firstProductlistdArray = new JSONObject(responseObject.getString("first"));
//                                    Log.d("creation","msg 1 => "+firstProductlistdArray.getString("msg_1"));
//                                }
//
//                                if (responseObject.getString("second").equalsIgnoreCase("null")){
//                                    Log.d("creation","Second -> jsonarray");
//                                }else{
//                                    JSONObject secondProductlistdArray = new JSONObject(responseObject.getString("second"));
//                                    seconditemCountHeading.setText(secondProductlistdArray.getString("msg_1"));
//                                    secondTitle.setText(secondProductlistdArray.getString("msg_3"));
//                                    secondDate.setText(secondProductlistdArray.getString("msg_4"));
//
//                                    JSONArray slotArray = secondProductlistdArray.getJSONArray("slot");
//                                    secondDeliveryDateTime.setText(slotArray.getJSONObject(0).getString("day"));
//
//                                    secondDeliveryValue.setText(" ₹"+secondProductlistdArray.getString("delivery_charge"));
//                                    secondDeliveryCharge.setText(" ₹"+secondProductlistdArray.getString("delivery_charge"));
//
//
//
//                                }
//
//
//
//
//                            }
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//
//                        Log.d("creation", "Total Response POST: "+response);
//
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }else {
//
//                    Log.d("creation", "dTotal Response POST: "+response);
//
//                    try {
//                        JSONObject jsonObject = new JSONObject(response);
//                        int status_code = jsonObject.getInt("status");
//                        hideDialog();
//                        if (status_code == 1) {
//
//                            JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
//                            if(responseObject.getString("pay_status").equalsIgnoreCase("2")) {
//                                if(pm_paymentMethod.equalsIgnoreCase("no")) {
//                                    launchPayUMoneyFlow();
//                                }else{
//                                    MeatPointUtils.decisionAlertOnMainThread(DeliverySummaryActivity.this,
//                                            android.R.drawable.ic_dialog_info,
//                                            R.string.blank_msg,
//                                            "Order placed successful",
//                                            "Ok", new DialogInterface.OnClickListener() {
//                                                @Override
//                                                public void onClick(DialogInterface dialog, int which) {
//                                                    Intent i = new Intent(DeliverySummaryActivity.this,OrderListingActivity.class);
//                                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                    startActivity(i);
//                                                    finish();
//                                                    dialog.dismiss();
//                                                }
//                                            }, "", new DialogInterface.OnClickListener() {
//                                                @Override
//                                                public void onClick(DialogInterface dialog, int which) {
//                                                    //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
//                                                }
//                                            }
//                                    );
//
//                                }
//                            }else{
//                                MeatPointUtils.decisionAlertOnMainThread(DeliverySummaryActivity.this,
//                                        android.R.drawable.ic_dialog_info,
//                                        R.string.blank_msg,
//                                        "Order placed successful",
//                                        "Ok", new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                Intent i = new Intent(DeliverySummaryActivity.this,OrderListingActivity.class);
//                                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                startActivity(i);
//                                                finish();
//                                                dialog.dismiss();
//                                            }
//                                        }, "", new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
//                                            }
//                                        }
//                                );
//                            }
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }*/
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("Volley error", error.getMessage());
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
//        progressLayout.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Result code ", String.valueOf(resultCode));
        resCode=resultCode;
            if( resultCode==5001 ){
                AddressModel adrs = (AddressModel) data.getSerializableExtra("addressDetails");
                if (adrs.getAddressPresent()) {
                    pm_deliveryAddres = adrs.getShipToken();
                    //paymentConfirmPageLoad(pm_custToken, pm_products, pm_deliveryMode, pm_deliveryDate, pm_deliveryTime, pm_deliveryDate2, pm_deliveryTime2, pm_deliveryAddres, pm_checkCouponCode, pm_couponCode, pm_checkRewardPoint, pm_extraInfo, "no", "no");
                    String FullAddress = adrs.getShipName().trim() + ",\n" +
                            adrs.getShipEmail().trim() + ",\n" +
                            adrs.getShipPhone().trim() + ",\n" +
                            adrs.getShipAddress().trim() + ",\n" +
                            adrs.getShipCity().trim() + ",\n" +
                            adrs.getShipPostcode().trim();
                    dAddressDetails.setText(FullAddress);
                }

        } else if (resultCode==6001){
                initVolly();
                Map<String, String> paramse = new HashMap<String, String>();
                paramse.put("cusToken", session.get_customerToken());
                RequestQueue mRequestQueueg = Volley.newRequestQueue(DeliverySummaryActivity.this);
                mVolleyService = new VolleyService(mResultCallback,DeliverySummaryActivity.this);
                mVolleyService.postStringDataVolley("DELIVERY_ADDRESS",GET_DELIVERY_ADDRESS,paramse,mRequestQueueg);
//            AddressModel adrs = (AddressModel) data.getSerializableExtra("addressDetail");
//                pm_deliveryAddres = adrs.getShipToken();
//                Log.e("Add ", adrs.getShipName());
//                String FullAddress = adrs.getShipName().trim() + ",\n" +
//                        adrs.getShipEmail().trim() + ",\n" +
//                        adrs.getShipPhone().trim() + ",\n" +
//                        adrs.getShipAddress().trim() + ",\n" +
//                        adrs.getShipCity().trim() + ",\n" +
//                        adrs.getShipPostcode().trim();
//                dAddressDetails.setText(FullAddress);

        }
//        if (resultCode == 3001) {
//            GetTimeModel adrs = (GetTimeModel) data.getSerializableExtra("timeDetails");
//            if (adrs.getTimeSlotPresent()) {
//                chooseDeliveryTime.setText(adrs.getTimeSlot());
//                payment_confirm_details.put("deliveryTime", adrs.getTimeSlot());
//            }
//        }

        if (requestCode == TIME_SELECT_CODE1 && resultCode == TimeSlotActivity.SUCCESS) {
            firstDeliveryDateTime.setText(data.getStringExtra(TimeSlotActivity.SELECTED_TIME_SLOT));
            Log.e("get time first ", firstDeliveryDateTime.getText().toString());
            pm_deliveryTime = firstDeliveryDateTime.getText().toString();
            Log.e("Date selected ", data.getStringExtra(TimeSlotActivity.SELECTED_DATE));
            if (!data.getStringExtra(TimeSlotActivity.SELECTED_DATE).isEmpty()) {
                String[] date = data.getStringExtra(TimeSlotActivity.SELECTED_DATE).toString().split("\n");
                // String []date2 = date[1].toString().split("-");
                firstDate.setText(date[1]);//+" "+getMonth(Integer.parseInt(date2[1]))+")");
                firstTitle.setText(date[0]);

                pm_deliveryDate = firstTitle.getText().toString().concat(firstDate.getText().toString());

            }
            deliveryPostParameter(pm_deliveryDate, pm_deliveryTime, pm_deliveryDate2, pm_deliveryTime2);

        } else if (requestCode == TIME_SELECT_CODE2 && resultCode == TimeSlotActivity.SUCCESS) {
            secondDeliveryDateTime.setText(data.getStringExtra(TimeSlotActivity.SELECTED_TIME_SLOT));
            Log.e("get time second ", secondDeliveryDateTime.getText().toString());
            pm_deliveryTime2 = secondDeliveryDateTime.getText().toString();
            if (!data.getStringExtra(TimeSlotActivity.SELECTED_DATE).isEmpty()) {
                String[] date = data.getStringExtra(TimeSlotActivity.SELECTED_DATE).toString().split("\n");
                // String []date2 = date[1].toString().split("-");
                secondDate.setText(date[1]);//+" "+getMonth(Integer.parseInt(date2[1]))+")");
                secondTitle.setText(date[0]);
                Log.e("Daaate", data.getStringExtra(TimeSlotActivity.SELECTED_DATE2));
                pm_deliveryDate2 = secondTitle.getText().toString().concat(secondDate.getText().toString());


            }
            deliveryPostParameter(pm_deliveryDate, pm_deliveryTime,pm_deliveryDate2, pm_deliveryTime2);
        }

//        Log.e("Date selected ", data.getStringExtra(TimeSlotActivity.SELECTED_DATE));

    }


    public void proceed() {
        Log.e("First cart item ", String.valueOf(firstcartItems.size()));
        Intent i = new Intent(DeliverySummaryActivity.this, OrderSumActivity.class);
        i.putExtra("products", pm_products);
        i.putExtra("deliveryMode", pm_deliveryMode);
        i.putExtra("deliveryDate", pm_deliveryDate);
        i.putExtra("deliveryTime", pm_deliveryTime);
        i.putExtra("deliveryAddress", pm_deliveryAddres);
        i.putExtra("checkCouponCode", pm_checkCouponCode);
        i.putExtra("couponCode", pm_couponCode);
        i.putExtra("checkRewardPoint", pm_checkRewardPoint);
        i.putExtra("extraInfo", pm_extraInfo);
        i.putExtra("time_slot", firstDeliveryDateTime.getText().toString());
        i.putExtra("deliveryDate2", secondDate.getText().toString());
        i.putExtra("deliveryTime2", pm_deliveryTime2);
        i.putExtra("titledt1", firstTitle.getText().toString() + " " + firstDate.getText().toString());
        i.putExtra("titledt2", secondTitle.getText().toString() + " " + secondDate.getText().toString());
        i.putExtra("flag", flag);

        if (pm_deliveryAddres.length() == 0) {
            Snackbar.make(findViewById(android.R.id.content), "Select delivery address", Snackbar.LENGTH_SHORT).show();
        } else {

            if (firstcartItems.size() > 0 && secondcartItems.size() > 0) {
                if (!pm_deliveryTime.isEmpty() && !pm_deliveryTime2.isEmpty()) {
                    startActivity(i);
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Select delivery time", Snackbar.LENGTH_SHORT).show();
                }
            } else if (firstcartItems.size() > 0) {
                if (!pm_deliveryTime.isEmpty()) {
                    startActivity(i);
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Select delivery time", Snackbar.LENGTH_SHORT).show();
                }
            } else if (secondcartItems.size() > 0) {
                if (!pm_deliveryTime2.isEmpty()) {
                    startActivity(i);
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Select delivery time", Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }

    // delivery value
    private void deliveryPostParameter(String date1, String time1, String date2, String time2) {
        showDialog();
        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", session.get_customerToken());
        paramse.put("products", pm_products);
        paramse.put("check_reward_point", pm_checkRewardPoint);
        paramse.put("check_coupon_code", pm_checkCouponCode);
        paramse.put("coupon_code", pm_couponCode);
//        if (type == "first") {
            paramse.put("first_delivery_date", date1);
            paramse.put("first_delivery_time", time1);

//        } else if (type == "second") {
            paramse.put("second_delivery_date", date2);
            paramse.put("second_delivery_time", time2);
//        }
        Log.e("Product ", pm_products);
        initVolley();
        Log.e("Parameter ", paramse.toString());
        RequestQueue mRequestQueueg = Volley.newRequestQueue(DeliverySummaryActivity.this);
        mVolleyService = new VolleyService(mResultCallback, DeliverySummaryActivity.this);
        mVolleyService.postStringDataVolley("CALCULATE_CART", CALCULATE_CART, paramse, mRequestQueueg);

//        setData();
    }

    private void initVolley() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.d("creation DSA", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

                Log.d("creation smmry ", "Total Response POST: " + response);
                //hideDialog();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if (status_code == 1) {
                        JSONObject objectResponse = jsonObject.getJSONObject("response");
                        Log.e("Response ", objectResponse.getString("delivery_charge"));
                        //------payment summery -------//
                        String sub_total = objectResponse.getString("sub_total");
                        fb_totalPrice.setText("₹" + sub_total);
                        if (!sub_total.isEmpty() && Double.parseDouble(sub_total) > 0) {
                            fb_totLayout.setVisibility(View.VISIBLE);
                        }
                        String discount = objectResponse.getString("discount_amount");
                        fb_discountFee.setText("(-)₹" + discount);
                        if (!discount.isEmpty() && Double.parseDouble(discount) > 0) {
                            fb_discountOptionLayout.setVisibility(View.VISIBLE);
                        }

                        String deliverfee = objectResponse.getString("delivery_charge");
                        fb_deliveryFee.setText("₹" + deliverfee);
                        if (!deliverfee.isEmpty() && Double.parseDouble(deliverfee) > 0) {
                            fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
                        }

                        String gst = objectResponse.getString("gst_value");
                        fb_gstValue.setText("₹" + gst);
                        if (!gst.isEmpty() && Double.parseDouble(gst) > 0) {
                            fb_gstOptionLayout.setVisibility(View.VISIBLE);
                        }

//                                fb_grandTotal.setText("₹"+responseObject.getString("grand_total"));
                        String gtotal = objectResponse.getString("grand_total");
                        Log.e("GTotal ", objectResponse.getString("grand_total"));
                        fb_grandTotal.setText("₹" + gtotal);
                        if (!gtotal.isEmpty() && Double.parseDouble(gtotal) > 0) {
                            fb_grandTotalLayout.setVisibility(View.VISIBLE);
                        }

                        String redeempoint = objectResponse.getString("redeemed_point");
                        fb_redeemedPoint.setText("(-)₹" + redeempoint);
                        if (!redeempoint.isEmpty() && Double.parseDouble(redeempoint) > 0) {
                            fb_redemedPointsLayout.setVisibility(View.VISIBLE);
                        }

                        String ernpoint = objectResponse.getString("earn_points");
                        fb_points.setText(ernpoint + " Points");
//                        if (!ernpoint.isEmpty() && Double.parseDouble(ernpoint) > 0) {
                            fb_earnPointsLayout.setVisibility(View.VISIBLE);
//                        }

                        String pAmount = objectResponse.getString("payable_amount");
                        payableAmount.setText("₹" + pAmount);
                        totalPriceButtom.setText("₹" + pAmount);
                        if (!pAmount.isEmpty() && Double.parseDouble(pAmount) > 0) {
                            payableAmountLayout.setVisibility(View.VISIBLE);
                        }
                        if (!objectResponse.getString("coupon_code").isEmpty()) {
                            if (objectResponse.getInt("coupon_applied") == 1)
                                couponlayout.setVisibility(View.VISIBLE);
                            else
                                couponlayout.setVisibility(View.GONE);
                        }else {
                            couponlayout.setVisibility(View.GONE);
                        }

//                        fb_grandTotal.setText("₹" + objectResponse.getString("grand_total"));
//                        payableAmount.setText("₹" + objectResponse.getString("payable_amount"));
                        totalPriceButtom.setText("₹" + objectResponse.getString("payable_amount"));
                       // if (type.equalsIgnoreCase("first")) {
                            if (objectResponse.getJSONObject("first").length() != 0) {
                                dcharge = objectResponse.getJSONObject("first").getString("delivery_charge");
                                firstDeliveryCharge.setText("₹" + dcharge);
                                fb_deliveryFee.setText("₹" + (dcharge));
                                fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
                                int d = Integer.valueOf(dcharge);
                                dblGTotal += d;

                                new SaveData().setDcharge1(firstDeliveryCharge.getText().toString());
                            }
                       // }
//                        if (type.equalsIgnoreCase("second")) {
                            if (objectResponse.getJSONObject("second").length() != 0) {
                                dcharge2 = objectResponse.getJSONObject("second").getString("delivery_charge");
                                secondDeliveryCharge.setText(" ₹" + dcharge2);
                                new SaveData().setDcharge2(secondDeliveryCharge.getText().toString());
                                fb_deliveryFee.setText("₹" + dcharge2);
                                fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
//                                int d = Integer.valueOf(dcharge2);
//                                dblGTotal += d;
//                                fb_grandTotal.setText("₹" + dblGTotal);
//                                payableAmount.setText("₹" + dblGTotal);
//                                totalPriceButtom.setText("₹" + dblGTotal);
                            }

//                        }
                        int amt = Integer.parseInt(dcharge) + Integer.parseInt(dcharge2);
                        fb_deliveryFee.setText("₹" + (amt));
                        fb_deliveryOptionLayout.setVisibility(View.VISIBLE);


                        Log.e("Charge ", dcharge + "  " + dcharge2);
                    }


                } catch (Exception e) {
                    Log.e("Exception ", e.getMessage().toString());
                }
                hideDialog();
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
                Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        };

    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



}
