package com.souvik.tendercuts.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.farhan.OtpVerification;
import com.souvik.tendercuts.config.AppConfig;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.SEND_OTP;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class RegistrationActivity extends AppCompatActivity {

    EditText fullname, email, phone, mpassword, mconfpassword;
    TextView txtTandC;
    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;
    SessionManager session;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        session = SessionManager.getInstance(getApplicationContext());
        pDialog = new ProgressDialog(RegistrationActivity.this);

        setFontAwesome((LinearLayout) findViewById(R.id.registrationlayout), RegistrationActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setText("Registration");
        titleToolbar.setVisibility(View.VISIBLE);

        txtTandC = findViewById(R.id.txt_TandC);
        txtTandC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://meatpoint.co.in/privacy";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegistrationActivity.this, HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        Log.d("creation", "login chekc =>" + session.isCustomerLoggedIn());
        try {
            if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                Intent i = new Intent(RegistrationActivity.this, HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Button regBtn = (Button) findViewById(R.id.regBtn);
        TextView signinbtn = (TextView) findViewById(R.id.signinbtn);

        fullname = (EditText) findViewById(R.id.fullname);
        email = (EditText) findViewById(R.id.email);
        phone = (EditText) findViewById(R.id.phone);
        mpassword = (EditText) findViewById(R.id.mpassword);
        mconfpassword = (EditText) findViewById(R.id.mconfpassword);

        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MeatPointUtils.isNetworkConnected(RegistrationActivity.this, true)) {
                    if (fullname.getText().length() > 1 && email.getText().length() > 1 && phone.getText().length() > 1 && mpassword.getText().length() > 1 && mconfpassword.getText().length() > 1) {
                        if (isValidEmail(email.getText().toString())) {
                            if (mpassword.getText().length() > 4 && mconfpassword.getText().length() > 4) {
                                if (mpassword.getText().toString().equalsIgnoreCase(mconfpassword.getText().toString())) {
                                    if (phone.getText().length() > 9 && phone.getText().length() <= 10) {

                                        initVolleyCallback();
                                        mVolleyService = new VolleyService(mResultCallback, RegistrationActivity.this);
                                        showDialog();
                                        HashMap<String, String> regDataPost = new HashMap<String, String>();
                                        regDataPost.put("name", fullname.getText().toString());
                                        regDataPost.put("email", email.getText().toString());
                                        regDataPost.put("phone", phone.getText().toString());
                                        regDataPost.put("pass_strick", mpassword.getText().toString());

                                        RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
                                        mVolleyService.postStringDataVolley("Registration", AppConfig.REGISTRATION_URL, regDataPost, requestQueue);


                                    } else {
                                        //Toast.makeText(LoginActivity.this, "Give correct phone no.", Toast.LENGTH_LONG);
                                        MeatPointUtils.decisionAlertOnMainThread(RegistrationActivity.this,
                                                android.R.drawable.ic_dialog_info,
                                                R.string.error,
                                                "Give correct Mobile No.",
                                                "Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                                        dialog.dismiss();
                                                        //finish();
                                                    }
                                                }, "", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                        );
                                    }
                                } else {
                                    MeatPointUtils.decisionAlertOnMainThread(RegistrationActivity.this,
                                            android.R.drawable.ic_dialog_info,
                                            R.string.error,
                                            "Password and Confirm Password is not Match.",
                                            "Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                                    dialog.dismiss();
                                                    //finish();
                                                }
                                            }, "", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                    );
                                }
                            } else {
                                MeatPointUtils.decisionAlertOnMainThread(RegistrationActivity.this,
                                        android.R.drawable.ic_dialog_info,
                                        R.string.error,
                                        "Give Password of Minimum 5 character",
                                        "Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                                dialog.dismiss();
                                                //finish();
                                            }
                                        }, "", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                );
                            }
                        } else {
                            MeatPointUtils.decisionAlertOnMainThread(RegistrationActivity.this,
                                    android.R.drawable.ic_dialog_info,
                                    R.string.error,
                                    "Give correct email",
                                    "Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                            dialog.dismiss();
                                            //finish();
                                        }
                                    }, "", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                            );
                        }
                    } else {
                        MeatPointUtils.decisionAlertOnMainThread(RegistrationActivity.this,
                                android.R.drawable.ic_dialog_info,
                                R.string.error,
                                "Required all Fields.",
                                "Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                        dialog.dismiss();
                                        //finish();
                                    }
                                }, "", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        );
                    }
                }
                //}else{
                //bookingProgressBar.setVisibility(View.GONE);
                //v = findViewById(android.R.id.content);
                //snackbarShow("No internet Connection",v);
                //}

            }
        });

        signinbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(RegistrationActivity.this, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    void initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response : " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    hideDialog();
                    if (status == 1) {
                        JSONObject loginDetails = jsonObject.getJSONObject("response");
                        Intent intent = new Intent(RegistrationActivity.this, OtpVerification.class);
                        intent.putExtra("mobile", loginDetails.getString("mobile"));
                        intent.putExtra("otp", loginDetails.getString("otp"));
                        intent.putExtra("type", "registration");
                        startActivity(intent);
                        Log.d("creation", loginDetails.getString("cusToken"));
                        Log.d("creation", loginDetails.getString("otp"));

                       /* MeatPointUtils.decisionAlertOnMainThread(RegistrationActivity.this,
                                R.drawable.icons8_ok_48,
                                R.string.sucess,
                                "Registration is Successfull",
                                "Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        sendOtp_initVolleyCallback();
                                        showDialog();
                                        mVolleyService = new VolleyService(mResultCallback, RegistrationActivity.this);

                                        HashMap<String, String> loginDataPost = new HashMap<String, String>();
                                        loginDataPost.put("mobile", phone.getText().toString());

                                        RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
                                        mVolleyService.postStringDataVolley("SEND OTP", SEND_OTP, loginDataPost, requestQueue);
                                    }
                                }, "", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        );
                        //HashMap<String,String> detailsMap = new HashMap<>();
                        //JSONObject loginDetails = jsonObject.getJSONObject("response");

                        /*detailsMap.put("apiKey",loginDetails.getString("apiKey"));
                        detailsMap.put("customerToken",loginDetails.getString("customerToken"));
                        detailsMap.put("customerEmail",loginDetails.getString("customerEmail"));
                        detailsMap.put("customerName",loginDetails.getString("customerName"));
                        detailsMap.put("customerPhone",loginDetails.getString("customerPhone"));
                        detailsMap.put("customerPhoto",loginDetails.getString("customerPhoto"));
                        detailsMap.put("isCustomerLoggedIn","1");

                        session.set_UserDetails(detailsMap);*/


                    } else {
                        JSONObject message = jsonObject.getJSONObject("message");
                        String phone = message.getString("phone");
                        MeatPointUtils.decisionAlertOnMainThread(RegistrationActivity.this,
                                android.R.drawable.ic_dialog_info,
                                R.string.error,
                                phone,
                                "Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                        dialog.dismiss();
                                        //finish();
                                    }
                                }, "", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                hideDialog();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
