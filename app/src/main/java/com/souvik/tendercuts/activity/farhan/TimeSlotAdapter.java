package com.souvik.tendercuts.activity.farhan;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.souvik.tendercuts.activity.farhan.model.Slot;
import com.souvik.tendercuts.activity.farhan.model.Time;
import com.souvik.tendercuts.activity.farhan.model.TimeHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.co.meatpoint.R;
import in.galaxyofandroid.widgets.AdvanceTextView;

/**
 * Created by Md Farhan Raja on 25-Feb-18.
 */

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.SlotItem> {

    private Context context;
    private ArrayList<TimeHolder> slotList;
    private OnItemClickListener onItemClickListener;


    public TimeSlotAdapter(Context context, ArrayList<TimeHolder> slotList) {
        this.context = context;
        this.slotList = slotList;
    }

    @Override
    public SlotItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.slot_view,parent,false);
        return new SlotItem(view);
    }

    @Override
    public void onBindViewHolder(SlotItem holder, int position) {
        holder.timeSlot.setText(slotList.get(position).getTime());
        if(slotList.get(position).getAvailable()==0){
            holder.timeSlot.setVisibility(View.GONE);
            Log.e("Time ", slotList.get(position).getTime());
        }else {
            holder.timeSlot.setVisibility(View.VISIBLE);
            Log.e("Time 23 ", slotList.get(position).getTime());
        }

        if(slotList.get(position).getDefault_selected()==1 ){
            holder.timeSlot.setTextColor(context.getResources().getColor(R.color.appBackground));
            holder.layout.setBackground(ContextCompat.getDrawable(context, R.drawable.outline_red));
        }else {
            holder.timeSlot.setTextColor(context.getResources().getColor(R.color.colorGrey));
            holder.layout.setBackground(ContextCompat.getDrawable(context, R.drawable.outline));
        }
//        if(SessionManager.getTime((Activity) context)!=null){
//            if(SessionManager.getTime((Activity) context).equalsIgnoreCase(slotList.get(position).getTime())){
//                holder.timeSlot.setTextColor(context.getResources().getColor(R.color.appBackground));
//            }
//            else {
//                holder.timeSlot.setTextColor(context.getResources().getColor(R.color.colorGrey));
//            }
//        }
//        Log.e("Session ",SessionManager.getTime((Activity) context));


    }

    @Override
    public int getItemCount() {
        return slotList.size();
    }

    public class SlotItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.timeSlot)AdvanceTextView timeSlot;
        @BindView(R.id.layout)LinearLayout layout;

        public SlotItem(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(onItemClickListener!=null) {
                onItemClickListener.onItemClick(view, getAdapterPosition());
            }
            //SessionManager.clearSession((Activity) context);
        }
    }

    public void setItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View v, int position);
    }
}
