package com.souvik.tendercuts.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.intentfilter.androidpermissions.PermissionManager;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.souvik.tendercuts.adapter.FaqListingAdapter;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.FaqModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.CONTACT_US;
import static com.souvik.tendercuts.config.AppConfig.FAQ;
import static com.souvik.tendercuts.config.AppController.context;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;
import static java.util.Collections.singleton;

public class ContactActivity extends AppCompatActivity {

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;
    private static final int PERMISSION_CALLBACK_CONSTANT = 101;
    private static final int REQUEST_PERMISSION_SETTING = 102;

    private TextView callNo, mailNo;
    LinearLayout contactLayout;
    private ProgressBar contactProgress;
    DatabaseHandler db;

    boolean sentToSettings=false;
    private SharedPreferences permissionStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        setFontAwesome((LinearLayout) findViewById(R.id.contactOuterLayout), ContactActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setText("Contact Us");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ContactActivity.this, HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        initVolleyCallback();
        Map<String, String> paramse = new HashMap<String, String>();
        RequestQueue mRequestQueueg = Volley.newRequestQueue(ContactActivity.this);
        mVolleyService = new VolleyService(mResultCallback, ContactActivity.this);
        mVolleyService.postStringDataVolley("FAQ", CONTACT_US, paramse, mRequestQueueg);

        callNo = (TextView) findViewById(R.id.callNo);
        mailNo = (TextView) findViewById(R.id.mailNo);
        contactProgress = (ProgressBar) findViewById(R.id.contactProgress);
        contactLayout = (LinearLayout) findViewById(R.id.contactLayout);

        LinearLayout calluslayout = (LinearLayout) findViewById(R.id.calluslayout);
        calluslayout.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {

                phoneCallPermissionCheck(callNo.getText().toString().trim());
//                if(checkPermission()){
//                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    final String number=callNo.getText().toString().trim();
//                    callIntent.setData(Uri.parse("tel:"+number));//change the number
//                    startActivity(callIntent);
//                }
//                PermissionManager permissionManager = PermissionManager.getInstance(ContactActivity.this);
//                permissionManager.checkPermissions(singleton(Manifest.permission.CALL_PHONE), new PermissionManager.PermissionRequestListener() {
//                    @Override
//                    public void onPermissionGranted() {
//                        Toast.makeText(ContactActivity.this, "Permissions Granted", Toast.LENGTH_SHORT).show();
//                        Intent callIntent = new Intent(Intent.ACTION_CALL);
//                        callIntent.setData(Uri.parse("tel:" + number));
//
//                        if (ActivityCompat.checkSelfPermission(ContactActivity.this,
//                                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                            return;
//                        }
//                        startActivity(callIntent);
//                    }
//
//                    @Override
//                    public void onPermissionDenied() {
//                        Toast.makeText(context, "Permissions Denied", Toast.LENGTH_SHORT).show();
//
//                    }
//                });




            }


        });

        findViewById(R.id.emailtemplate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{mailNo.getText().toString().trim()});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT   , "");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(ContactActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void phoneCallPermissionCheck(final String trim) {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CALL_PHONE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted
                       phoneCall(trim);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }
    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }
    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
    private void phoneCall(String trim) {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(intent, 100);

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + trim));

        if (ActivityCompat.checkSelfPermission(ContactActivity.this,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivityForResult(callIntent, 100);
    }

    //

    @SuppressLint("LongLogTag")
    protected void sendEmail() {
        Log.i("Send email", "");
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("Finished sending email...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ContactActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
                /**/
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if (status_code == 1) {
                        contactProgress.setVisibility(View.GONE);
                        contactLayout.setVisibility(View.VISIBLE);
                        JSONObject productlistObject=jsonObject.getJSONObject("response");
                        callNo.setText(productlistObject.getString("call_us"));
                        mailNo.setText(productlistObject.getString("mail_us"));

                    }else{
                        contactProgress.setVisibility(View.VISIBLE);
                        contactLayout.setVisibility(View.GONE);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    contactProgress.setVisibility(View.VISIBLE);
                    contactLayout.setVisibility(View.GONE);
                    finish();
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ContactActivity.this,HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
    @Override
    public void onResume() {
        super.onResume();


//        if (sentToSettings) {
//            if (ActivityCompat.checkSelfPermission(ContactActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//                //Got Permission
//               // proceedAfterPermission();
//            }
//        }
    }



}
