package com.souvik.tendercuts.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.adapter.CartProdDemoListingAdapter;
import com.souvik.tendercuts.adapter.DeliveryAddressListingAdapter;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.OnAddressChangeListener;
import com.souvik.tendercuts.helper.OnRefreshCartViewListner;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.AddressModel;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.model.PaymentConfirmModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.GET_DELIVERY_ADDRESS;
import static com.souvik.tendercuts.config.AppController.context;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class DeliveryAddress extends AppCompatActivity implements DeliveryAddressListingAdapter.DeliveryAddressSelectedInterface{

    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    RadioGroup addressLists;
    RadioButton button;
    Button add_new_address,chooseAddress;
    String date1, time1, charge1, date2, time2, charge2;

    private OnAddressChangeListener mRefreshListner;
    int pos;
    private List<AddressModel> addressListsItems;
    DeliveryAddressListingAdapter deliveryAddrAdapter;
    private RecyclerView addressListsRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_address);

        setFontAwesome((LinearLayout)findViewById(R.id.deliveryAddrLayout),DeliveryAddress.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(DeliveryAddress.this);
//        mRefreshListner = (OnAddressChangeListener)this.getApplicationContext();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setVisibility(View.VISIBLE);
        titleToolbar.setText("Address");


        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        ImageView imageView5 = (ImageView)findViewById(R.id.imageView5);
        imageView5.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addressLists = (RadioGroup)findViewById(R.id.addressLists);
        add_new_address = (Button)findViewById(R.id.add_new_address);
        chooseAddress = (Button)findViewById(R.id.chooseAddress);

        date1= getIntent().getStringExtra("date1");
        date2= getIntent().getStringExtra("date2");
        time1= getIntent().getStringExtra("time1");
        time2= getIntent().getStringExtra("time2");
        charge1= getIntent().getStringExtra("charge1");
        charge2= getIntent().getStringExtra("charge2");

        initVolleyCallback();

        addressListsItems = new ArrayList<AddressModel>();
        deliveryAddrAdapter= new DeliveryAddressListingAdapter(DeliveryAddress.this,addressListsItems);
        addressListsRecycler = (RecyclerView) findViewById(R.id.addressListsRecycler);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        addressListsRecycler.setLayoutManager(mLayoutManager1);
        addressListsRecycler.setAdapter(deliveryAddrAdapter);



        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", session.get_customerToken());
        RequestQueue mRequestQueueg = Volley.newRequestQueue(DeliveryAddress.this);
        mVolleyService = new VolleyService(mResultCallback,DeliveryAddress.this);
        mVolleyService.postStringDataVolley("DELIVERY_ADDRESS",GET_DELIVERY_ADDRESS,paramse,mRequestQueueg);

        add_new_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DeliveryAddress.this, AddNewDeliveryAddress.class);
                i.putExtra("type", "add");
                startActivityForResult(i, 1);
//                Intent i = new Intent(DeliveryAddress.this,AddNewDeliveryAddress.class);

//                startActivity(i);
                //finish();
            }
        });

    }


    AddressModel adrs = new AddressModel();
    static DeliveryAddress INSTANCE;

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");

                    if(status_code==1){
                        //JSONObject responseObject = new JSONObject(jsonObject.getString("response"));

                        addressListsItems.clear();
                        JSONArray productlistArray = new JSONArray(jsonObject.getString("response"));
                        for (int i=0;i<productlistArray.length();i++) {
                            JSONObject productEachRow = productlistArray.getJSONObject(i);

                            AddressModel a = new AddressModel();
                            a.setShipAddress(productEachRow.getString("shipAddress"));
                            a.setShipCity(productEachRow.getString("shipCity"));
                            a.setShipDefault(productEachRow.getString("shipDefault"));
                            a.setShipEmail(productEachRow.getString("shipEmail"));
                            a.setShipName(productEachRow.getString("shipName"));
                            a.setShipPhone(productEachRow.getString("shipPhone"));
                            a.setShipPostcode(productEachRow.getString("shipPostcode"));
                            a.setShipToken(productEachRow.getString("shipToken"));
                            a.setAddressPresent(true);

                            /*Log.d("creation","ff1=> "+session.get_customerToken().toString());
                            Log.d("creation","ff2=> "+session.get_customerApiKey().toString());
                            Log.d("creation","ff3=> "+session.get_customerEmail().toString());
                            Log.d("creation","ff4=> "+session.get_customerName().toString());*/

                            addressListsItems.add(a);

                            /*button = new RadioButton(DeliveryAddress.this);
                            final int btnID  = i;
                            button.setId(i);
                            String FullAddress = productEachRow.getString("shipName")+", "+
                                    productEachRow.getString("shipEmail")+", "+
                                    productEachRow.getString("shipPhone")+", "+
                                    productEachRow.getString("shipAddress")+", "+
                                    productEachRow.getString("shipCity")+", "+
                                    productEachRow.getString("shipPostcode");
                            button.setText(FullAddress);
                            button.setTextSize(14);
                            button.setTextColor(0xFF8b8b8b);

                            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(
                                    RadioGroup.LayoutParams.MATCH_PARENT,
                                    RadioGroup.LayoutParams.MATCH_PARENT
                            );
                            params.setMargins(0, 10, 10, 2);

                            button.setLayoutParams(params);
                            addressLists.addView(button);*/
                        }
                        deliveryAddrAdapter.notifyDataSetChanged();

                        /*addressLists.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                Log.d("creation", "CheckedId => " + checkedId);
                                Intent i = new Intent(DeliveryAddress.this,DeliverySummaryActivity.class);
                                adrs = addressListsItems.get(checkedId);
                                //mRefreshListner.addressChange(adrs);
                                *//*i.putExtra("totalprice",getIntent().getStringExtra("totalprice"));
                                i.putExtra("grandtotalprice",getIntent().getStringExtra("totalprice"));
                                i.putExtra("products",getIntent().getStringExtra("products"));
                                *//*
                                i.putExtra("addressDetails",adrs);
                                setResult(5001, i);
                                finish();
                            }
                        });*/
                    }else{

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    public static DeliveryAddress getActivityInstance()
    {
        return INSTANCE;
    }

    public AddressModel getData()
    {
        return this.adrs;
    }

    @Override
    public void setDeliveryAddress(List<AddressModel> itemModelList, int pos) {
        Log.e("REsponse ","delivery address name => "+itemModelList.get(pos).getShipAddress());
        Intent i = new Intent(DeliveryAddress.this,DeliverySummaryActivity.class);
        adrs = addressListsItems.get(pos);
        i.putExtra("addressDetails",itemModelList.get(pos));

        i.putExtra("date1", date1);
        i.putExtra("time1", time1);
        i.putExtra("charge1", charge1);

        i.putExtra("date2", date2);
        i.putExtra("time2", time2);
        i.putExtra("charge2", charge2);


        setResult(5001, i);

        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            if(resultCode==RESULT_OK){
                initVolleyCallback();
                Map<String, String> paramse = new HashMap<String, String>();
                paramse.put("cusToken", session.get_customerToken());
                RequestQueue mRequestQueueg = Volley.newRequestQueue(DeliveryAddress.this);
                mVolleyService = new VolleyService(mResultCallback,DeliveryAddress.this);
                mVolleyService.postStringDataVolley("DELIVERY_ADDRESS",GET_DELIVERY_ADDRESS,paramse,mRequestQueueg);
            }
        }
    }


    @Override
    public void onBackPressed() {
        for(int i=0;i<addressListsItems.size();i++){
            if(addressListsItems.get(i).getShipDefault().equalsIgnoreCase("Yes")){
                Intent intent = new Intent(DeliveryAddress.this,DeliverySummaryActivity.class);
                adrs = addressListsItems.get(pos);
                intent.putExtra("addressDetail",addressListsItems.get(pos));
                setResult(6001, intent);
                finish();
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        initVolleyCallback();
        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", session.get_customerToken());
        RequestQueue mRequestQueueg = Volley.newRequestQueue(DeliveryAddress.this);
        mVolleyService = new VolleyService(mResultCallback,DeliveryAddress.this);
        mVolleyService.postStringDataVolley("DELIVERY_ADDRESS",GET_DELIVERY_ADDRESS,paramse,mRequestQueueg);
    }
}
