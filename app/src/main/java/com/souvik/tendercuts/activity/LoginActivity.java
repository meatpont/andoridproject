package com.souvik.tendercuts.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.SharedElementCallback;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.config.AppConfig;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class LoginActivity extends AppCompatActivity {

    EditText emailPhone,mpassword;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    SessionManager session;
    public DatabaseHandler db;

    private ProgressDialog pDialog;

    TextView skiplogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(LoginActivity.this);

        pDialog = new ProgressDialog(LoginActivity.this);

        setFontAwesome((LinearLayout)findViewById(R.id.loginlayout),LoginActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setText("Login");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();
                startActivity(i);

            }
        });

//        Log.d("creation","login chekc =>"+session.isCustomerLoggedIn());
//        try {
//            if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
//                Intent i = new Intent(LoginActivity.this, HomeActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(i);
//                finish();
//            }
//        }catch (NullPointerException e){
//            e.printStackTrace();
//        }catch (Exception e){
//            e.printStackTrace();
//        }

        Button loginBtn = (Button)findViewById(R.id.loginBtn);
        TextView newMember = (TextView)findViewById(R.id.newMember);
        TextView forgetpassword = (TextView)findViewById(R.id.forgetpassword);

        emailPhone = (EditText)findViewById(R.id.emailPhone);
        mpassword = (EditText)findViewById(R.id.mpassword);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent i=new Intent(LoginActivity.this, HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();*/
                //MeatPointUtils.isNetworkConnected(LoginActivity.this,true);
                if(MeatPointUtils.isNetworkConnected(LoginActivity.this,true)) {
                    if (emailPhone.getText().length() > 9) {
                        if (mpassword.getText().length() > 4) {
                            initVolleyCallback();
                            showDialog();
                            mVolleyService = new VolleyService(mResultCallback, LoginActivity.this);

                            HashMap<String, String> loginDataPost = new HashMap<String, String>();
                            loginDataPost.put("user_access", emailPhone.getText().toString());
                            loginDataPost.put("user_key", mpassword.getText().toString());

                            RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                            mVolleyService.postStringDataVolley("CustomerAuthenticate", AppConfig.CUSTOMER_AUTHENTICATE, loginDataPost, requestQueue);
                        } else {
                           // Toast.makeText(LoginActivity.this, "", Toast.LENGTH_LONG);
                            MeatPointUtils.decisionAlertOnMainThread(LoginActivity.this,
                                    android.R.drawable.ic_dialog_info,
                                    R.string.error,
                                    "Give Password of Minimum 4 character",
                                    "Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                            dialog.dismiss();
                                            //finish();
                                        }
                                    }, "", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                            );
                        }
                    } else {
                        //Toast.makeText(LoginActivity.this, "Give correct phone no.", Toast.LENGTH_LONG);
                        MeatPointUtils.decisionAlertOnMainThread(LoginActivity.this,
                                android.R.drawable.ic_dialog_info,
                                R.string.error,
                                "Give correct Mobile No.",
                                "Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                        dialog.dismiss();
                                        //finish();
                                    }
                                }, "", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        );
                    }
                }
                //}else{
                    //bookingProgressBar.setVisibility(View.GONE);
                    //v = findViewById(android.R.id.content);
                    //snackbarShow("No internet Connection",v);
                //}

            }
        });

        newMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LoginActivity.this, RegistrationActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        forgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        skiplogin = (TextView)findViewById(R.id.skiplogin);
        skiplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(i);
            }
        });
    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response : "+response);
                hideDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if(status==1) {
                        HashMap<String,String> detailsMap = new HashMap<String, String>();

                        JSONObject loginDetails = jsonObject.getJSONObject("response");

                        detailsMap.put("apiKey",loginDetails.getString("apiKey"));
                        detailsMap.put("customerToken",loginDetails.getString("customerToken"));
                        detailsMap.put("customerEmail",loginDetails.getString("customerEmail"));
                        detailsMap.put("customerName",loginDetails.getString("customerName"));
                        detailsMap.put("customerPhone",loginDetails.getString("customerPhone"));
                        detailsMap.put("customerPhoto",loginDetails.getString("customerPhoto"));

                        db.updateCartProductToAnotherUser(loginDetails.getString("customerToken"));
                        if(session.isCustomerLoggedIn() == null) {
                            detailsMap.put("isCustomerLoggedIn","1");
                            session.set_UserDetails(detailsMap);
                            Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        }else if(session.isCustomerLoggedIn()=="4"){
                            detailsMap.put("isCustomerLoggedIn","1");
                            session.set_UserDetails(detailsMap);
                            Intent i = new Intent(LoginActivity.this, CartActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        }else{
                            detailsMap.put("isCustomerLoggedIn","1");
                            session.set_UserDetails(detailsMap);
                            Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        }

                        /*for( Map.Entry entry : detailsMap.entrySet() ) {
                            Log.d("creation",""+entry.getKey()+" => "+entry.getValue());
                            //editor.putString(entry.getKey(), entry.getValue());
                        }*/

                    }else{
                        String message = jsonObject.getString("message");
                        MeatPointUtils.decisionAlertOnMainThread(LoginActivity.this,
                                android.R.drawable.ic_dialog_info,
                                R.string.error,
                                message,
                                "Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                        dialog.dismiss();
                                        //finish();
                                    }
                                }, "", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        );
                    }


                }catch (JSONException e){
                    Log.e("Exp ", e.getMessage().toString());
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                hideDialog();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(LoginActivity.this,HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
