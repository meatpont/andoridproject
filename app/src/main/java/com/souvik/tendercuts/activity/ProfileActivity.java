package com.souvik.tendercuts.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.farhan.OtpVerification;
import com.souvik.tendercuts.config.AppConfig;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.GET_PROFILE;
import static com.souvik.tendercuts.config.AppConfig.UPDATE_PROFILE;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class ProfileActivity extends AppCompatActivity {

    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    TextView personname, profemail, profphone, changepassword;
    EditText et_password, et_cnfpassword;
    Button updatbtn, rewardpoint, meatpointvalue;
    private ProgressDialog pDialog;
    RequestQueue requestQueue;
    LinearLayout layout_parent;
    int flag=0;
    LinearLayout layout_change_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setFontAwesome((LinearLayout) findViewById(R.id.profileLayout), ProfileActivity.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(ProfileActivity.this);
        pDialog = new ProgressDialog(ProfileActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setText("My Profile");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);
        layout_parent=findViewById(R.id.layout_parent);
        layout_parent.setVisibility(View.GONE);

        layout_change_password = findViewById(R.id.layout_change_password);
        layout_change_password.setVisibility(View.GONE);
        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                Intent i = new Intent(ProfileActivity.this, HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        personname = (TextView) findViewById(R.id.personname);
        profemail = (TextView) findViewById(R.id.profemail);
        profphone = (TextView) findViewById(R.id.profphone);
        changepassword = (TextView) findViewById(R.id.changepassword);
        updatbtn = (Button) findViewById(R.id.updatbtn);
        rewardpoint = (Button) findViewById(R.id.rewardpoint);
        meatpointvalue = (Button) findViewById(R.id.meatpointvalue);
        et_password = findViewById(R.id.et_password);
        et_cnfpassword = findViewById(R.id.et_cnfpassword);

        flag=0;

        initVolleyCallback();
        mVolleyService = new VolleyService(mResultCallback, ProfileActivity.this);
        requestQueue = Volley.newRequestQueue(ProfileActivity.this);

        if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {

            /*personname.setText(session.get_customerName());
            profemail.setText(session.get_customerEmail());
            profphone.setText(session.get_customerPhone());*/

            showDialog();
            HashMap<String, String> loginDataPost = new HashMap<String, String>();
            loginDataPost.put("cusToken", session.get_customerToken());
            mVolleyService.postStringDataVolley("GET_CUSTOMER_PROFILE", GET_PROFILE, loginDataPost, requestQueue);


            updatbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                Log.e("pass ", et_password.getText().toString()+" "+et_cnfpassword.getText().toString());
                    if(et_password.getText().toString()!=null && et_cnfpassword.getText().toString()!=null) {
                        if (et_password.getText().toString().equals(et_cnfpassword.getText().toString())) {
                            showDialog();
                            HashMap<String, String> loginDataPost = new HashMap<String, String>();
                            loginDataPost.put("cusToken", session.get_customerToken());
                            loginDataPost.put("name", session.get_customerName());
                            loginDataPost.put("email", profemail.getText().toString());
                            loginDataPost.put("pass_strick", et_password.getText().toString());
                            flag=1;
                            mVolleyService.postStringDataVolley("CustomerAuthenticate", UPDATE_PROFILE, loginDataPost, requestQueue);
                        }else {
                            et_password.setText("");
                            et_password.requestFocus();
                            et_cnfpassword.setText("");
                            Toast.makeText(ProfileActivity.this, "Password not matched.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

        } else {
            Intent i = new Intent(ProfileActivity.this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }

        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_change_password.setVisibility(View.VISIBLE);
            }
        });


    }

    void initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response : " + response);
                hideDialog();

                if (requestType.equalsIgnoreCase("GET_CUSTOMER_PROFILE")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            layout_parent.setVisibility(View.VISIBLE);
                            JSONObject responseobject = new JSONObject(jsonObject.getString("response"));

                            personname.setText(responseobject.getString("first_name") + " " + responseobject.getString("last_name"));
                            rewardpoint.setText(responseobject.getString("point"));
                            meatpointvalue.setText("1 Meat Point Reward = " + responseobject.getString("point_value") + " ₹");

                            profemail.setText(responseobject.getString("email"));
                            profphone.setText(responseobject.getString("mobile"));
//                            et_password.setText(responseobject.getString("pass_strick"));


                            //Log.d("creation","fname = > "+responseobject.getString("first_name"));
                            HashMap<String, String> detailsMap = new HashMap<String, String>();
                            detailsMap.put("customerEmail", profemail.getText().toString());
                            detailsMap.put("customerName", responseobject.getString("first_name") + " " + responseobject.getString("last_name"));
                            detailsMap.put("customerPhone", profphone.getText().toString());
                            session.set_UserDetails(detailsMap);

                        } else {
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            HashMap<String, String> detailsMap = new HashMap<String, String>();
                            detailsMap.put("customerEmail", profemail.getText().toString());
                            session.set_UserDetails(detailsMap);
                            MeatPointUtils.decisionAlertOnMainThread(ProfileActivity.this,
                                    R.drawable.icons8_ok_48,
                                    R.string.sucess,
                                    "Password updated",
                                    "Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                            dialog.dismiss();
                                            layout_change_password.setVisibility(View.GONE);
                                            //finish();
                                        }
                                    }, "", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                            );
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                hideDialog();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ProfileActivity.this, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
