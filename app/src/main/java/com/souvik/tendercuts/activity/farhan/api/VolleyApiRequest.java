package com.souvik.tendercuts.activity.farhan.api;

import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.souvik.tendercuts.activity.farhan.model.TimeSlot;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created by Farhan Raja on 18/12/2017.
 */

public class VolleyApiRequest extends BaseStringRequest {
    private Map<String, String> bodyParams;

    public VolleyApiRequest(String Url, @Nullable Map<String, String> bodyParams, Response.Listener<String> listener,
                            Response.ErrorListener errorListener) {
        super(Method.POST, Url, listener, errorListener);
        this.bodyParams = bodyParams;
        if (bodyParams != null)
            Log.e("Request Details", "API: " + Url + "\nBody Params: " + new Gson().toJson(bodyParams).toString());
    }

    @Override
    public Map<String, String> getParams() {
        return bodyParams;
    }


    /*PARSE METHODS*/

/*    public static List<WishListModel> parseWishList(JSONArray jsonArray) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<WishListModel>>() {
        }.getType();
        return gson.fromJson(jsonArray.toString(), type);
    }*/
    public static TimeSlot parseTimeSlots(JSONObject object) {
        Gson gson = new Gson();
        Type type = new TypeToken<TimeSlot>() {
        }.getType();
        return gson.fromJson(object.toString(), type);
    }
}
