package com.souvik.tendercuts.activity;

/*import android.app.FragmentManager;
import android.app.FragmentTransaction;*/

import android.app.ActionBar;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.farhan.CatagoryHolder;
import com.souvik.tendercuts.adapter.CartListingAdapter;
import com.souvik.tendercuts.adapter.ItemListingAdapter;
import com.souvik.tendercuts.common.Action_menu_button;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.fragment.ItemListFragment;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.CategoryModel;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.utils.CartAddNotify;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.CART_CHECK_URL;
import static com.souvik.tendercuts.config.AppConfig.PRODUCT_LISTING_URL;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class ItemListingActivity extends AppCompatActivity implements ItemListingAdapter.ItemAdapterCallback, CartAddNotify{

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    private RecyclerView recyclerView;
    private ItemListingAdapter adapter;
    private List<ItemModel> itemModelList;
    private ProgressBar listingProgress;
    private TextView noItemFound,cartaddedQuantity,totalPriceCart;

    private String catToken,userid="";
    private JSONArray categoryContent;
    private String cat;

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    LinearLayout fragmentContainer;

    SessionManager session;
    public DatabaseHandler db;

    LinearLayout proceedLayout;
    List<ItemModel> items = new ArrayList<ItemModel>();
    int sdk = android.os.Build.VERSION.SDK_INT;

    HorizontalScrollView horizontalscroll;
    ItemListFragment itemlistfragment;
    CatagoryHolder catHolder;
    ArrayList<CatagoryHolder> listHolder = new ArrayList<CatagoryHolder>();

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_listing);

        setFontAwesome((RelativeLayout)findViewById(R.id.item_listing_toolbar),ItemListingActivity.this);
        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(ItemListingActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView proceedText = (TextView)findViewById(R.id.proceedText);
        proceedText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                    Intent i = new Intent(ItemListingActivity.this,CartActivity.class);
                    startActivity(i);
                }else{
                    Intent i = new Intent(ItemListingActivity.this,LoginActivity.class);

                    HashMap<String,String> detailsMap = new HashMap<String, String>();
                    detailsMap.put("isCustomerLoggedIn","4");
                    session.set_UserDetails(detailsMap);

                    startActivity(i);
                }

            }
        });

        Button proceedBtn = (Button)findViewById(R.id.proceedBtn);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                    Intent i = new Intent(ItemListingActivity.this,CartActivity.class);
                    startActivity(i);
                }else{
                    Intent i = new Intent(ItemListingActivity.this,LoginActivity.class);

                    HashMap<String,String> detailsMap = new HashMap<String, String>();
                    detailsMap.put("isCustomerLoggedIn","4");
                    session.set_UserDetails(detailsMap);

                    startActivity(i);
                }
            }
        });
        proceedLayout = (LinearLayout)findViewById(R.id.proceedLayout);
       /* proceedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ItemListingActivity.this,CartActivity.class);
                startActivity(i);
            }
        });*/


        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);
        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ItemListingActivity.this,CartActivity.class);
                startActivity(i);
            }
        });

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /*Bundle bundle = this.getIntent().getExtras();
        if(bundle != null) {
            HashMap<String, CategoryModel> bookingData = (HashMap<String, CategoryModel>) bundle.getSerializable("catDetails");
            Log.d("creation","bookingDataServices => "+bookingData.get("BookingData").getAll_services());
        }*/
        horizontalscroll = (HorizontalScrollView)findViewById(R.id.horizontalscroll);

        Intent customerResponse = getIntent();
        catToken = customerResponse.getStringExtra("catToken");
        try {
            categoryContent = new JSONArray(customerResponse.getStringExtra("catDetails"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Log.d("creation","Bundle => "+catToken);
        fragmentload(catToken);
        //catToken = "1";

        /*initVolleyCallback();
        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("catToken", catToken);
        RequestQueue mRequestQueueg = Volley.newRequestQueue(ItemListingActivity.this);
        mVolleyService = new VolleyService(mResultCallback,ItemListingActivity.this);
        mVolleyService.postStringDataVolley("PRODUCT_LISTING",PRODUCT_LISTING_URL,paramse,mRequestQueueg);

        recyclerView = (RecyclerView) findViewById(R.id.categoryRecycler);

        itemModelList = new ArrayList<>();
        adapter = new ItemListingAdapter(this, itemModelList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(adapter);

        listingProgress = (ProgressBar)findViewById(R.id.listingProgress);
        noItemFound = (TextView)findViewById(R.id.noItemFound);*/

        LinearLayout ll = (LinearLayout)findViewById(R.id.buttonlayout);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //int rat;
        JSONObject categoryEachRow = null;


        for (int r=0;r<categoryContent.length();r++) {
            final Button myButton = new Button(ItemListingActivity.this);
            try {
//                Log.d("creation","s"+categoryEachRow.getInt("catToken"));
                categoryEachRow = categoryContent.getJSONObject(r);
                if (categoryEachRow.getInt("catToken") != 888) {
                    myButton.setText(categoryEachRow.getString("catName"));
                    myButton.setTransformationMethod(null);
                    myButton.setId(categoryEachRow.getInt("catToken"));

                    myButton.setBackgroundColor(getResources().getColor(R.color.appBlackColor));
                    myButton.setTextColor(getResources().getColor(R.color.colorWhite));
                    myButton.setPadding(15, 5, 15, 5);


                    if (catToken.equalsIgnoreCase(categoryEachRow.getString("catToken"))) {
                        //myButton.setBackgroundColor(getResources().getColor(R.color.appThemegreencolor));
                        //myButton.setTextColor(getResources().getColor(R.color.white));

                        if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
                            myButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_bottom));
                        } else {
                            myButton.setBackground(getResources().getDrawable(R.drawable.border_bottom));
                        }
                    }
                    //myButton.setId(r);

                    ll.addView(myButton, lp);

                    myButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Button bt = (Button) findViewById(Integer.parseInt(catToken));
                            bt.setBackgroundColor(getResources().getColor(R.color.appBlackColor));
                            bt.setTextColor(getResources().getColor(R.color.colorWhite));
                            catToken = String.valueOf(myButton.getId());

                            if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
                                myButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_bottom));
                            } else {
                                myButton.setBackground(getResources().getDrawable(R.drawable.border_bottom));
                            }
//                            myButton.setBackgroundColor(getResources().getColor(R.color.appThemegreencolor));
//                            myButton.setTextColor(getResources().getColor(R.color.colorWhite));
                            fragmentload(String.valueOf(myButton.getId()));
                        }
                    });
                }
            } catch(JSONException e){
                e.printStackTrace();
            }

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            horizontalscroll.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    Log.d("creation","scrollX => "+scrollX);
                    Log.d("creation","scrollY => "+scrollY);
                    Log.d("creation","oldscrollX => "+oldScrollX);
                    Log.d("creation","oldscrollY => "+oldScrollY);

                }
            });
        }
        //horizontalscroll.scrollBy(1000,0);

        /*********/
        Action_menu_button n = new Action_menu_button(getApplicationContext(),ItemListingActivity.this);
        n.actionMenuButton(1);


//        itemlistfragment.setOnItemSelected(ItemListingActivity.this);

    }



    @Override
    protected void onResume() {
        super.onResume();
        horizontalscroll.fullScroll(5);
        horizontalscroll.scrollBy(485,0);
        horizontalscroll.scrollTo(485,0);
    }

    public void fragmentload(String catTo){
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentContainer = (LinearLayout)findViewById(R.id.fragment_container);

        if(fragmentContainer.getChildCount() > 0) {
            fragmentContainer.removeAllViews();
        }

        cartaddedQuantity = (TextView)findViewById(R.id.cartaddedQuantity);
        totalPriceCart = (TextView)findViewById(R.id.totalPriceButtom);

        if(db.getCartCountByUserID(session.get_customerToken())>0){
            proceedLayout.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,dptopixel(110),0,dptopixel(100));
            fragmentContainer.setLayoutParams(layoutParams);

            items = db.getAllCartItems(session.get_customerToken());
            double prce = 0;
            int qntity = 0;

            for (int i=0;i < items.size();i++)
            {
                ItemModel itemModel = items.get(i);
                //prce = prce + Double.parseDouble(itemModel.getMarketPrice());
                qntity = qntity + itemModel.getCartAddedQuantity();
            }
//            totalPriceCart.setText((int) prce);
            cartaddedQuantity.setText(""+qntity);

        }else{
            proceedLayout.setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );

            layoutParams.setMargins(0,dptopixel(110),0,dptopixel(50));
            fragmentContainer.setLayoutParams(layoutParams);
        }


        Bundle bundle = new Bundle();
        bundle.putString("catToken", catTo);


                    /*FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();*/
//        itemlistfragment = new ItemListFragment();
//        fragmentTransaction.add(R.id.fragment_container, itemlistfragment,"dd");
//        itemlistfragment.setArguments(bundle);
//        fragmentTransaction.commit();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation1", "Total Response GET : " + response);
                /**/

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation2", "Total Response POST: "+response);

                listingProgress.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if (status_code == 1) {
                        recyclerView.setVisibility(View.VISIBLE);
                        //JSONObject hh = jsonObject.getJSONObject("detail");
                        JSONArray productlistArray=jsonObject.getJSONArray("response");

                        for (int i=0;i<productlistArray.length();i++){
                            JSONObject productEachRow = productlistArray.getJSONObject(i);

                            ItemModel a = new ItemModel();
                            a.setIndex(i);
                            a.setProdDesc(productEachRow.getString("prodDesc"));
                            a.setProdName(productEachRow.getString("prodName"));
                            a.setPic(productEachRow.getString("pic"));
                            a.setMarketPrice(productEachRow.getString("marketPrice"));
                            a.setProdToken(productEachRow.getString("prodToken"));
                            a.setSellPrice(productEachRow.getString("sellPrice"));
                            a.setTomorrowDelivery(productEachRow.getString("tomorrowDelivery"));
                            a.setIsNew(productEachRow.getString("isNew"));
                            Log.e("Delivery ", productEachRow.getString("prodName"));
                            a.setUserId(session.get_customerToken());
                            /*Log.d("creation","ff1=> "+session.get_customerToken().toString());
                            Log.d("creation","ff2=> "+session.get_customerApiKey().toString());
                            Log.d("creation","ff3=> "+session.get_customerEmail().toString());
                            Log.d("creation","ff4=> "+session.get_customerName().toString());*/

                            itemModelList.add(a);

                        }
                        adapter.notifyDataSetChanged();
                    }else{
                        noItemFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Log.e("JSON Exception ", e.getMessage());
                    noItemFound.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    @Override
    public void setItemModelList(List<ItemModel> itemModelList,int pos) {
        ItemModel itemModel = itemModelList.get(pos);
        /*Log.d("creation","Product Name => "+itemModel.getProdName());
        Log.d("creation","Product Token => "+itemModel.getProdToken());*/


        Intent i = new Intent(ItemListingActivity.this, ProductDetailsActivity.class);

//        HashMap<String, ItemModel> itemlistsHashmap = new HashMap<String, ItemModel>();
//        itemlistsHashmap.put("itemsDataHash",itemModel);
//
//        HashMap<String, List<ItemModel>> itemTotalHashmap = new HashMap<String, List<ItemModel>>();
//
//        itemTotalHashmap.put("totalItems",itemModelList);
//
//        Bundle extras = new Bundle();
//        extras.putSerializable("itemsData", itemlistsHashmap);
//        extras.putSerializable("allItems", itemTotalHashmap);
//        extras.putInt("position", pos);
//
//        i.putExtras(extras);

        i.putExtra("catToken",String.valueOf(itemModel.getProdToken()));
        i.putExtra("netWeight", itemModel.getNetWeight().toString());
//        i.putExtra("\"₹\"+",holder.netWeight.getText());

        startActivityForResult(i,1000);
        //startActivity(i);
        /*Log.d("creation","Product Name => "+itemModel.getProdName());
        Log.d("creation","Addedd Cart => "+itemModel.isAddedCart());
        //i.putExtra("catToken",String.valueOf(itemModel.getProdToken()));
        Bundle extras = new Bundle();
        extras.putSerializable("itemsData", itemlistsHashmap);
        */
    }

    @Override
    public void onItemSelected(int k) {
        Log.d("creation","Activity value send"+k);

        if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
            userid = session.get_customerToken();
        }else{
            userid = "demouser";
        }
        Log.e("User Id", userid);
        if(db.getCartCountByUserID(userid)>0){
            proceedLayout.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,dptopixel(110),0,dptopixel(100));
            fragmentContainer.setLayoutParams(layoutParams);

            items = db.getAllCartItems(userid);
            double prce = 0;
            int qntity = 0;

            for (int i=0;i < items.size();i++)
            {
                ItemModel itemModel = items.get(i);
                //prce = prce + Double.parseDouble(itemModel.getMarketPrice());
                qntity = qntity + itemModel.getCartAddedQuantity();
            }
//            totalPriceCart.setText((int) prce);
            cartaddedQuantity.setText(""+qntity);

        }else{
            proceedLayout.setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );

            layoutParams.setMargins(0,dptopixel(110),0,dptopixel(50));
            fragmentContainer.setLayoutParams(layoutParams);

        }
    }

    public int dptopixel(int dpValue){
        float d = getApplicationContext().getResources().getDisplayMetrics().density;
        int pixl = (int)(dpValue * d); // margin in pixels
        return pixl;
    }



//    @Override
//    public void reloadItems(List<ItemModel> newItemList) {
//
//        itemModelList.clear();
//        adapter.notifyItemRangeRemoved(0,newItemList.size());
//        itemModelList = newItemList;
//        adapter.notifyDataSetChanged();
//
//        //itemModelList.remove(position);
//        //recycler.removeViewAt(position);
//    }
//
//    @Override
//    public void itemQuantitychange(List<ItemModel> itemModelList, int pos) {
//
//    }

}
