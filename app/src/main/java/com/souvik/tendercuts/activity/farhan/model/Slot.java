package com.souvik.tendercuts.activity.farhan.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Md Farhan Raja on 25-Feb-18.
 */

public class Slot implements Parcelable{
    @SerializedName("date")private String slotDate;
    @SerializedName("day")private String slotDayName;
    @SerializedName("time")private List<Time> slotTimeList;

    protected Slot(Parcel in) {
        slotDate = in.readString();
        slotDayName = in.readString();
        slotTimeList = in.createTypedArrayList(Time.CREATOR);
    }

    public static final Creator<Slot> CREATOR = new Creator<Slot>() {
        @Override
        public Slot createFromParcel(Parcel in) {
            return new Slot(in);
        }

        @Override
        public Slot[] newArray(int size) {
            return new Slot[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(slotDate);
        parcel.writeString(slotDayName);
        parcel.writeTypedList(slotTimeList);
    }

    public String getSlotDate() {
        return slotDate;
    }

    public String getSlotDayName() {
        return slotDayName;
    }

    public List<Time> getSlotTimeList() {
        return slotTimeList;
    }
}
