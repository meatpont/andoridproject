package com.souvik.tendercuts.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;
import com.souvik.tendercuts.adapter.CartListingAdapter;
import com.souvik.tendercuts.adapter.CartProdDemoListingAdapter;
import com.souvik.tendercuts.adapter.ProdListingOrdSumAdapter;
import com.souvik.tendercuts.common.PayMentGateWay;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.OnAddressChangeListener;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.AddressModel;
import com.souvik.tendercuts.model.GetTimeModel;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.model.PaymentConfirmModel;
import com.souvik.tendercuts.payyoumoney.AppEnvironment;
import com.souvik.tendercuts.payyoumoney.AppPreference;
import com.souvik.tendercuts.payyoumoney.BaseApplication;
import com.souvik.tendercuts.utils.MeatPointUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.ADD_ORDER;
import static com.souvik.tendercuts.config.AppConfig.CALCULATE_CART;
import static com.souvik.tendercuts.config.AppConfig.COUPON_VALIDATION;
import static com.souvik.tendercuts.config.AppConfig.GET_TIMESLOT;
import static com.souvik.tendercuts.config.AppConfig.UPDATE_TRANSACTOIN;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class PaymentConfirm extends AppCompatActivity {
    SessionManager session;
    public DatabaseHandler db;
    private boolean isDisableExitConfirmation = false;
    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
    private AppPreference mAppPreference;

    private ProgressDialog pDialog;
    private String orderId = "", payuResponse = "";
    LinearLayout fb_totLayout, fb_redemedPointsLayout, fb_deliveryOptionLayout,
            fb_gstOptionLayout, fb_discountOptionLayout, fb_earnPointsLayout,
            fb_couponlayout, fb_grandTotalLayout, couponappliedlayout, payableAmountLayout, ordersummaryLayout, orderClick;

    TextView fb_totalPrice, fb_redeemedPoint, fb_deliveryFee, fb_gstValue, fb_discountFee, fb_points, fb_couponpriceminus, fb_grandTotal, payableAmount, shippingname, shippingmobile, shippingaddress, totalPriceButtom;
    TextView cartaddedQuantity, extraInfoText, specialInstructionHeading;

    private String pm_custToken = "", pm_products = "", pm_deliveryMode = "", pm_deliveryDate = "", pm_deliveryTime = "", pm_deliveryDate2 = "", pm_deliveryTime2 = "", pm_deliveryAddres = "", pm_checkCouponCode = "no",
            pm_couponCode = "", pm_checkRewardPoint = "no", pm_paymentMethod = "", pm_place_order = "", pm_extraInfo = "";
    ArrayList<ItemModel> items, cartItems;
    private RecyclerView recyclerView;
    CartProdDemoListingAdapter adapter;
    TextView angledown;
    String date1 = "", date2 = "";
    View view1;
    View view2;
    double gAmount = 0.0;

    private boolean checkpaymentmethod = false;

    /*private LinearLayout deliverydttmLayout,deliveryOptionLayout,redemedPointsLayout,couponlayout,couponTextlayout;
    private Button chooseDeliveryDate,chooseDeliveryaddress,chooseDeliveryTime,confirmPayment,couponbtnapply;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private TextView addressDetails,totalPrice,grandTotal,deliveryFee,points,redeemedPoint,couponpriceminus;
    private String totalpricestrng="",grandtotalstrng="",products="",deliverySystem="",earn_points="",deliveryAddress="";
    private EditText coupontext;

    PaymentConfirmModel payment_confirm_modelobj = new PaymentConfirmModel();

    HashMap<String,String> payment_confirm_details = new HashMap<>();

    JSONArray dSystem;

    private String chdate="";
    int currentpoint=1000;


    private List<GetTimeModel> timeListsItems;
    RadioButton rbutton;
    GetTimeModel timemodel = new GetTimeModel();
    RadioGroup choose_time_slot;

    CheckBox meatwallet,havecoupon;

    JSONObject morningObj,eveningObj;

    ImageButton couponcancelbtn;*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_confirm);

        mAppPreference = new AppPreference();

        setFontAwesome((RelativeLayout) findViewById(R.id.paymentconfirmLayout), PaymentConfirm.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(PaymentConfirm.this);
        pDialog = new ProgressDialog(PaymentConfirm.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setVisibility(View.VISIBLE);
        titleToolbar.setText("Payment");

        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);
        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        ImageView imageView5 = (ImageView) findViewById(R.id.imageView5);
        imageView5.setVisibility(View.GONE);

        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        initVolleyCallback();

        fb_totLayout = (LinearLayout) findViewById(R.id.totLayout);
        fb_redemedPointsLayout = (LinearLayout) findViewById(R.id.redemedPointsLayout);
        fb_deliveryOptionLayout = (LinearLayout) findViewById(R.id.deliveryOptionLayout);
        fb_gstOptionLayout = (LinearLayout) findViewById(R.id.gstOptionLayout);
        fb_earnPointsLayout = (LinearLayout) findViewById(R.id.earnPointsLayout);
        fb_couponlayout = (LinearLayout) findViewById(R.id.couponlayout);
        fb_grandTotalLayout = (LinearLayout) findViewById(R.id.grandTotalLayout);
        payableAmountLayout = (LinearLayout) findViewById(R.id.payableAmountLayout);
        fb_discountOptionLayout = (LinearLayout) findViewById(R.id.discountOptionLayout);
        couponappliedlayout = (LinearLayout) findViewById(R.id.couponlayout);

        fb_totalPrice = (TextView) findViewById(R.id.totalPrice);
        fb_redeemedPoint = (TextView) findViewById(R.id.redeemedPoint);
        fb_deliveryFee = (TextView) findViewById(R.id.deliveryFee);
        fb_gstValue = (TextView) findViewById(R.id.gstValue);
        fb_points = (TextView) findViewById(R.id.points);
        fb_couponpriceminus = (TextView) findViewById(R.id.couponpriceminus);
        fb_grandTotal = (TextView) findViewById(R.id.grandTotal);
        payableAmount = (TextView) findViewById(R.id.payableAmount);
        fb_discountFee = (TextView) findViewById(R.id.discountFee);
        shippingname = (TextView) findViewById(R.id.shippingname);
        shippingmobile = (TextView) findViewById(R.id.shippingmobile);
        shippingaddress = (TextView) findViewById(R.id.shippingaddress);
        totalPriceButtom = (TextView) findViewById(R.id.totalPriceButtom);
        cartaddedQuantity = (TextView) findViewById(R.id.cartaddedQuantity);

        pm_custToken = session.get_customerToken();
        pm_products = getIntent().getStringExtra("products");
        pm_deliveryMode = getIntent().getStringExtra("deliveryMode");
        date1 = getIntent().getStringExtra("deliveryDate");
        pm_deliveryTime = getIntent().getStringExtra("deliveryTime");
        date2 = getIntent().getStringExtra("deliveryDate2");
        pm_deliveryTime2 = getIntent().getStringExtra("deliveryTime2");
        pm_deliveryDate = date1.substring(1, date1.length() - 1);
        pm_deliveryDate2 = date2.substring(1, date2.length() - 1);
       // Log.e("Date time ", pm_deliveryDate + " - " + pm_deliveryTime + " - " + pm_deliveryDate2 + " - " + pm_deliveryTime2);
        pm_deliveryAddres = getIntent().getStringExtra("deliveryAddress");
        pm_checkCouponCode = getIntent().getStringExtra("checkCouponCode");
        pm_couponCode = getIntent().getStringExtra("couponCode");
        pm_checkRewardPoint = getIntent().getStringExtra("checkRewardPoint");
        pm_extraInfo = getIntent().getStringExtra("extraInfo");
        pm_paymentMethod = "";


        items = new ArrayList<ItemModel>();
        cartItems = new ArrayList<ItemModel>();

        extraInfoText = (TextView) findViewById(R.id.extraInfoText);
        specialInstructionHeading = (TextView) findViewById(R.id.specialInstructionHeading);

        adapter = new CartProdDemoListingAdapter(PaymentConfirm.this, cartItems);
        recyclerView = (RecyclerView) findViewById(R.id.prodListsRecycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

        orderClick = (LinearLayout) findViewById(R.id.orderClick);
        ordersummaryLayout = (LinearLayout) findViewById(R.id.ordersummaryLayout);
        angledown = (TextView) findViewById(R.id.angledown);

        orderClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ordersummaryLayout.getVisibility() == View.GONE) {
                    angledown.setText(R.string.fa_angle_up);
                    ordersummaryLayout.setVisibility(View.VISIBLE);
                } else {
                    angledown.setText(R.string.fa_angle_down);
                    ordersummaryLayout.setVisibility(View.GONE);
                }
            }
        });
        pm_place_order = "no";
        paymentConfirmPageLoad(pm_custToken, pm_products, pm_deliveryMode, pm_deliveryDate, pm_deliveryTime, pm_deliveryDate2, pm_deliveryTime2, pm_deliveryAddres, pm_checkCouponCode, pm_couponCode, pm_checkRewardPoint, pm_place_order, pm_paymentMethod, pm_extraInfo);
        //setupCitrusConfigs();

        RadioGroup radioGroupPaymentOptions = (RadioGroup) findViewById(R.id.paymentOptions);
        radioGroupPaymentOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.usingPayUMoney) {
                    pm_paymentMethod = "2";
                    checkpaymentmethod = true;
                } else {
                    pm_paymentMethod = "1";
                    checkpaymentmethod = true;
                }
            }
        });

        Button proceedBtn = (Button) findViewById(R.id.proceedBtn);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkpaymentmethod) {
                    pm_place_order = "yes";
                    paymentConfirmPageLoad(pm_custToken, pm_products, pm_deliveryMode, pm_deliveryDate, pm_deliveryTime, pm_deliveryDate2, pm_deliveryTime2, pm_deliveryAddres, pm_checkCouponCode, pm_couponCode, pm_checkRewardPoint, pm_place_order, pm_paymentMethod, pm_extraInfo);
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Select payment method", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        TextView proceedText = (TextView) findViewById(R.id.proceedText);
        proceedText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkpaymentmethod) {
                    pm_place_order = "yes";
                    paymentConfirmPageLoad(pm_custToken, pm_products, pm_deliveryMode, pm_deliveryDate, pm_deliveryTime, pm_deliveryDate2, pm_deliveryTime2, pm_deliveryAddres, pm_checkCouponCode, pm_couponCode, pm_checkRewardPoint, pm_place_order, pm_paymentMethod, pm_extraInfo);
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Select payment method", Snackbar.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void checkOutFinal() {
        boolean chkstat = true;

        //pm_custToken,pm_products,pm_deliveryMode,pm_deliveryDate,pm_deliveryTime,pm_deliveryAddres,pm_checkCouponCode,pm_couponCode,pm_checkRewardPoint

        if (pm_deliveryMode.equalsIgnoreCase("2")) {
            if (pm_deliveryDate.isEmpty()) {
                MeatPointUtils.decisionAlertOnMainThread(PaymentConfirm.this,
                        android.R.drawable.ic_dialog_info,
                        R.string.error,
                        "No delivery Date Selected.",
                        "Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }, "", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                            }
                        }
                );
                chkstat = false;
            } else if (pm_deliveryTime.isEmpty()) {
                MeatPointUtils.decisionAlertOnMainThread(PaymentConfirm.this,
                        android.R.drawable.ic_dialog_info,
                        R.string.error,
                        "No delivery Time Selected.",
                        "Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }, "", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                            }
                        }
                );
                chkstat = false;
            }
        } else if (pm_deliveryAddres.isEmpty()) {
            MeatPointUtils.decisionAlertOnMainThread(PaymentConfirm.this,
                    android.R.drawable.ic_dialog_info,
                    R.string.error,
                    "No delivery Address",
                    "Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }, "", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                        }
                    }
            );
            chkstat = false;
        }
        if (chkstat) {

            //Log.d("creation","grandtotalstrng = .>" +grandtotalstrng);

            Map<String, String> paramse = new HashMap<String, String>();
            paramse.put("cusToken", pm_custToken);
            paramse.put("products", pm_products);
            paramse.put("addressToken", pm_deliveryAddres);
            paramse.put("ship_type", pm_deliveryMode);
            if (pm_checkCouponCode.equalsIgnoreCase("yes")) {
                paramse.put("coupon_code", pm_couponCode);
            } else {
                paramse.put("coupon_code", "");
            }
            if (pm_checkRewardPoint.equalsIgnoreCase("yes")) {
                paramse.put("point_redeemed", pm_checkRewardPoint);
            } else {
                paramse.put("point_redeemed", "");
            }

            RequestQueue mRequestQueueg = Volley.newRequestQueue(PaymentConfirm.this);
            mVolleyService = new VolleyService(mResultCallback, PaymentConfirm.this);
            mVolleyService.postStringDataVolley("CART_LISTING", ADD_ORDER, paramse, mRequestQueueg);
//            Log.d("Post Data", String.valueOf(paramse));
        }
    }

    private void paymentConfirmPageLoad(String custToken, String products, String deliveryMode, String deliveryDate, String deliveryTime, String deliveryDate2, String deliveryTime2, String deliveryAddres, String checkCouponCode, String couponCode, String checkRewardPoint, String pm_place_order, String pm_paymentMethod, String extraInfo) {
        initVolleyCallback();
        showDialog();
//        delivery_mode=1, [Not needed, can remove this]
//        extra_info=, [This is extra info which we add from cart summary]
//        payment_mode=, [Either 1 OR 2 (1=>COD, 2=>PayU Money)]
//        products=1:1,2:1,
//                cusToken=c4ca4238a0b923820dcc509a6f75849b,
//                first_delivery_time=05PM To 06PM,
//                second_delivery_time=01PM To 02PM,
//                place_order=no,
//                first_delivery_date=Today, [This should be in YYYY-MM-DD format]
//        check_reward_point=no,
//                coupon_code=, [Coupon code if applied any]
//        check_coupon_code=no,
//                delivery_address=9,
//                second_delivery_date=(10 Jun) [This should be in YYYY-MM-DD format]

        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", custToken);
        paramse.put("products", products);
//        paramse.put("delivery_mode", deliveryMode);
        paramse.put("first_delivery_date", deliveryDate);
        paramse.put("first_delivery_time", deliveryTime);
        paramse.put("second_delivery_date", deliveryDate2);
        paramse.put("second_delivery_time", deliveryTime2);
        paramse.put("delivery_address", deliveryAddres);
        paramse.put("check_coupon_code", checkCouponCode);
        paramse.put("coupon_code", couponCode);
        paramse.put("check_reward_point", checkRewardPoint);
        paramse.put("payment_mode", pm_paymentMethod);
        paramse.put("place_order", pm_place_order);
        paramse.put("extra_info", extraInfo);
        Log.e("Post data ", paramse.toString());
        RequestQueue mRequestQueueg = Volley.newRequestQueue(PaymentConfirm.this);
        mVolleyService = new VolleyService(mResultCallback, PaymentConfirm.this);
        mVolleyService.postStringDataVolley("CALCULATE_CART", CALCULATE_CART, paramse, mRequestQueueg);
    }

    double amount = 0.0;

    void initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

                if (requestType.equalsIgnoreCase("CALCULATE_CART")) {
                        hideDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status_code = jsonObject.getInt("status");
                            if (status_code == 1) {

                                JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
                                Log.e("Order id ", responseObject.getString("order_id"));
                                orderId = responseObject.getString("order_id");
                                if (checkpaymentmethod && pm_place_order == "yes") {
                                    if (pm_paymentMethod.equalsIgnoreCase("1")) {
                                        if (responseObject.getString("pay_status").equalsIgnoreCase("1")) {
                                            /*MeatPointUtils.decisionAlertOnMainThread(PaymentConfirm.this,
                                                    android.R.drawable.ic_dialog_info,
                                                    R.string.blank_msg,
                                                    "Order placed successfully",
                                                    "Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            //http://meatpoint.co.in/track/12
                                                            Intent i = new Intent(PaymentConfirm.this,OrderListingActivity.class);
                                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(i);
                                                            finish();
                                                            dialog.dismiss();
                                                        }
                                                    }, "", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                            );*/
                                            Intent i = new Intent(PaymentConfirm.this, OrderListingActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                            i.putExtra("order_id",responseObject.getString("order_id"));
//                                            i.putExtra("flag", 1);
//                                            Log.e("Order id ", responseObject.getString("order_id"));
                                            startActivity(i);
                                            finish();
                                        } else {
                                            /*MeatPointUtils.decisionAlertOnMainThread(PaymentConfirm.this,
                                                    android.R.drawable.ic_dialog_info,
                                                    R.string.blank_msg,
                                                    "Order not placed",
                                                    "Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Intent i = new Intent(PaymentConfirm.this,OrderListingActivity.class);
                                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(i);
                                                            finish();
                                                            dialog.dismiss();
                                                        }
                                                    }, "", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                            );*/
                                        }

                                    } else {
                                        launchPayUMoneyFlow();
                                    }
                                    /*"order_id": 87,
                                            "pay_status": 1,
                                            "success_url": "",
                                            "failure_url": "",
                                            "cancel_url": ""

                                    */

                                } else {
                                    fb_totalPrice.setText("₹" + responseObject.getString("sub_total"));
                                    if (!responseObject.getString("sub_total").isEmpty() && Double.parseDouble(responseObject.getString("sub_total")) > 0) {
                                        fb_totLayout.setVisibility(View.VISIBLE);
                                    }

                                    fb_discountFee.setText("(-)₹" + responseObject.getString("discount_amount"));
                                    if (!responseObject.getString("discount_amount").isEmpty() && Double.parseDouble(responseObject.getString("discount_amount")) > 0) {
                                        fb_discountOptionLayout.setVisibility(View.VISIBLE);
                                    }

                                    fb_deliveryFee.setText("₹" + responseObject.getString("delivery_charge"));
                                    if (!responseObject.getString("delivery_charge").isEmpty() && Double.parseDouble(responseObject.getString("delivery_charge")) > 0) {
                                        fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
                                    }

                                    fb_gstValue.setText("₹" + responseObject.getString("gst_value"));
                                    if (!responseObject.getString("gst_value").isEmpty() && Double.parseDouble(responseObject.getString("gst_value")) > 0) {
                                        fb_gstOptionLayout.setVisibility(View.VISIBLE);
                                    }

                                    fb_grandTotal.setText("₹" + responseObject.getString("grand_total"));

//                                    fb_grandTotal.setText("₹" + responseObject.getString("grand_total"));
                                    if (!responseObject.getString("grand_total").isEmpty() && Double.parseDouble(responseObject.getString("grand_total")) > 0) {
                                        fb_grandTotalLayout.setVisibility(View.VISIBLE);

                                    }

                                    fb_redeemedPoint.setText("(-)₹" + responseObject.getString("redeemed_point"));
                                    if (!responseObject.getString("redeemed_point").isEmpty() && Double.parseDouble(responseObject.getString("redeemed_point")) > 0) {
                                        fb_redemedPointsLayout.setVisibility(View.VISIBLE);
                                    }

                                    fb_points.setText(responseObject.getString("earn_points") + " Points");
//                                    if (!responseObject.getString("earn_points").isEmpty() && Double.parseDouble(responseObject.getString("earn_points")) > 0) {
                                        fb_earnPointsLayout.setVisibility(View.VISIBLE);
//                                    }

                                    totalPriceButtom.setText("₹" + responseObject.getString("payable_amount"));
                                    amount = Double.parseDouble(responseObject.getString("payable_amount"));
                                    gAmount = amount;
                                    payableAmount.setText("₹" + responseObject.getString("payable_amount"));
                                    if (!responseObject.getString("payable_amount").isEmpty() && Double.parseDouble(responseObject.getString("payable_amount")) > 0) {
                                        payableAmountLayout.setVisibility(View.VISIBLE);
                                    }

                                    int quantity = 0;
                                    JSONArray productlistArrayr = responseObject.getJSONArray("in_stock");
                                    for (int i = 0; i < productlistArrayr.length(); i++) {
                                        JSONObject productEachRow = productlistArrayr.getJSONObject(i);
                                        quantity = quantity + Integer.parseInt(productEachRow.getString("cartQuan"));
                                    }
                                    cartaddedQuantity.setText("" + quantity);


                                    if (!responseObject.getString("coupon_code").isEmpty()) {
                                        if (responseObject.getInt("coupon_applied")==1)
                                            couponappliedlayout.setVisibility(View.VISIBLE);
                                        else
                                            couponappliedlayout.setVisibility(View.GONE);
//                                    redemedPointsLayout.setVisibility(View.GONE);
//                                    couponTextlayout.setVisibility(View.GONE);
                                    }else {
                                        couponappliedlayout.setVisibility(View.GONE);
                                    }
                                    //responseObject.getString("coupon_code");
                                    //responseObject.getString("payable_amount");
                                    JSONObject jj = new JSONObject(responseObject.getString("default_address"));
                                    pm_deliveryAddres = jj.getString("shipToken");


//                                    String fullAdress = jj.getString("shipAddress").trim() + ",\n" +
//                                            jj.getString("shipCity").trim() + ",\n" +
//                                            jj.getString("shipPostcode").trim() + ",\n" +
//                                            jj.getString("shipEmail").trim();


                                    String fullAdress = jj.getString("shipName").trim() + "\n" +
                                            jj.getString("shipAddress").trim() + ",\n" +
                                            jj.getString("shipCity").trim() + ",\n" +
                                            jj.getString("shipPostcode").trim() + ",\n" +
                                            jj.getString("shipEmail").trim() + ",\n" +
                                            jj.getString("shipPhone").trim();

                                    shippingname.setVisibility(View.GONE);//.setText(jj.getString("shipName"));
                                    shippingmobile.setVisibility(View.GONE);//.setText("(" + jj.getString("shipPhone") + ")");
                                    shippingaddress.setText(fullAdress);


                                    /*LinearLayout prodLists = (LinearLayout) findViewById(R.id.prodLists);
                                    LinearLayout parent = null;

                                    JSONArray productlistArray = responseObject.getJSONArray("in_stock");
                                    for (int i = 0; i < productlistArray.length(); i++) {
                                        JSONObject productEachRow = productlistArray.getJSONObject(i);

                                        /*//**************************************************************************************************
                                     parent = new LinearLayout(PaymentConfirm.this);

                                     parent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                     parent.setPadding(30, 5, 0, 0);
                                     parent.setOrientation(LinearLayout.HORIZONTAL);
                                     parent.setBackground(getResources().getDrawable(R.drawable.layout_border_plain));
                                     //children of parent linearlayout

                                     ImageView iv = new ImageView(PaymentConfirm.this);
                                     LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 200);
                                     iv.setLayoutParams(layoutParams);
                                     //iv.setBackground(getResources().getDrawable(R.drawable.layout_border_plain));
                                     iv.setPadding(5, 5, 5, 5);
                                     iv.setScaleType(ImageView.ScaleType.FIT_XY);

                                     LinearLayout layout2 = new LinearLayout(PaymentConfirm.this);

                                     layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                     layout2.setPadding(25, 35, 0, 0);
                                     layout2.setOrientation(LinearLayout.VERTICAL);

                                     parent.addView(iv);
                                     parent.addView(layout2);

                                     //children of layout2 LinearLayout

                                     TextView tv1 = new TextView(PaymentConfirm.this);
                                     TextView tv2 = new TextView(PaymentConfirm.this);

                                     tv1.setTextColor(getResources().getColor(R.color.appThemegreencolor));
                                     layout2.addView(tv1);
                                     layout2.addView(tv2);

                                     tv1.setText(productEachRow.getString("prodName") + " (" + Integer.parseInt(productEachRow.getString("cartQuan")) + ") ");
                                     tv2.setText(productEachRow.getString("sellPrice"));

                                     Picasso
                                     .with(PaymentConfirm.this)
                                     .load(productEachRow.getString("pic"))
                                     .into(iv);

                                     prodLists.addView(parent);
                                     }*/

                                    JSONArray productlistArray = responseObject.getJSONArray("in_stock");
                                    for (int i = 0; i < productlistArray.length(); i++) {
                                        JSONObject productEachRow = productlistArray.getJSONObject(i);

                                        //ItemModel cartItemsList = items.get(i);

                                        ItemModel m = new ItemModel();
                                        m.setProdToken(productEachRow.getString("prodToken"));
                                        m.setProdName(productEachRow.getString("prodName"));
                                        m.setProdDesc(productEachRow.getString("prodDesc"));
//                                        m.setMarketPrice(productEachRow.getString("marketPrice"));
                                        m.setSellPrice(productEachRow.getString("sellPrice"));
                                        m.setPic(productEachRow.getString("pic"));
                                        m.setCartAddedQuantity(productEachRow.getInt("cartQuan"));
                                        m.setStockAddedQuantity(productEachRow.getInt("stockQuan"));
                                        m.setNetWeight(productEachRow.getString("netWeight"));
                                        cartItems.add(m);
//                                    Log.d("creation", " => " + productEachRow.getString("prodName"));
                                    }
                                    adapter.notifyDataSetChanged();

                                    extraInfoText.setText(responseObject.getString("extra_info"));
                                    if (!responseObject.getString("extra_info").isEmpty() && responseObject.getString("extra_info").length() > 0) {
                                        extraInfoText.setVisibility(View.VISIBLE);
                                        specialInstructionHeading.setVisibility(View.VISIBLE);
                                        view1.setVisibility(View.VISIBLE);
                                        view2.setVisibility(View.VISIBLE);
                                    }

                                }
                            }
                        } catch (JSONException e) {
                           Log.e("exception ",e.getMessage().toString());
                        }

                        Log.d("creation", "Total Response POST: " + response);

                }/*else {

                    Log.d("creation", "dTotal Response POST: "+response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status_code = jsonObject.getInt("status");
                        hideDialog();
                        if (status_code == 1) {

                            JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
                            if(responseObject.getString("pay_status").equalsIgnoreCase("2")) {
                                if(pm_paymentMethod.equalsIgnoreCase("no")) {
                                    launchPayUMoneyFlow();
                                }else{
                                    MeatPointUtils.decisionAlertOnMainThread(PaymentConfirm.this,
                                            android.R.drawable.ic_dialog_info,
                                            R.string.blank_msg,
                                            "Order placed successful",
                                            "Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent i = new Intent(PaymentConfirm.this,OrderListingActivity.class);
                                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(i);
                                                    finish();
                                                    dialog.dismiss();
                                                }
                                            }, "", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                    );

                                }
                            }else{
                                MeatPointUtils.decisionAlertOnMainThread(PaymentConfirm.this,
                                        android.R.drawable.ic_dialog_info,
                                        R.string.blank_msg,
                                        "Order placed successful",
                                        "Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent i = new Intent(PaymentConfirm.this,OrderListingActivity.class);
                                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(i);
                                                finish();
                                                dialog.dismiss();
                                            }
                                        }, "", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                );
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }*/
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
        db.deleteCart();
    }

    private void setupCitrusConfigs() {
        AppEnvironment appEnvironment = AppEnvironment.SANDBOX;
        if (appEnvironment == AppEnvironment.PRODUCTION) {
            Log.d("creation", "Environment Set to Production");
            //Toast.makeText(MainActivity.this, "Environment Set to Production", Toast.LENGTH_SHORT).show();
        } else {
            Log.d("creation", "Environment Set to SandBox");
            //Toast.makeText(MainActivity.this, "Environment Set to SandBox", Toast.LENGTH_SHORT).show();
        }
    }

    private void launchPayUMoneyFlow() {

        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();

        //Use this to set your custom text on result screen button
        payUmoneyConfig.setDoneButtonText("Ok");

        //Use this to set your custom title for the activity
        payUmoneyConfig.setPayUmoneyActivityTitle("MeatPoint");
        payUmoneyConfig.setAccentColor(String.valueOf(R.color.payumoney_white));
        payUmoneyConfig.setColorPrimaryDark(String.valueOf(R.color.cb_dark_grey));
        payUmoneyConfig.setColorPrimary(String.valueOf(R.color.cb_dark_grey));
        payUmoneyConfig.setTextColorPrimary(String.valueOf(R.color.payumoney_white));

        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();


        String txnId = System.currentTimeMillis() + "";
        String phone = session.get_customerPhone();
        String productName = "MeatPoint";
        String firstName = session.get_customerName();
        String email = session.get_customerEmail();
        String udf1 = "";
        String udf2 = "";
        String udf3 = "";
        String udf4 = "";
        String udf5 = "";
        String udf6 = "";
        String udf7 = "";
        String udf8 = "";
        String udf9 = "";
        String udf10 = "";

        AppEnvironment appEnvironment = AppEnvironment.PRODUCTION;
//        AppEnvironment appEnvironment = ((BaseApplication) getApplication()).getAppEnvironment();
        builder.setAmount(String.valueOf(gAmount))
                .setTxnId(txnId)
                .setPhone(phone)
                .setProductName(productName)
                .setFirstName(firstName)
                .setEmail(email)
                .setsUrl(appEnvironment.surl())
                .setfUrl(appEnvironment.furl())
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setUdf6(udf6)
                .setUdf7(udf7)
                .setUdf8(udf8)
                .setUdf9(udf9)
                .setUdf10(udf10)
                .setIsDebug(appEnvironment.debug())
                .setKey(appEnvironment.merchant_Key())
                .setMerchantId(appEnvironment.merchant_ID());

        try {
            mPaymentParams = builder.build();

            /*
            * Hash should always be generated from your server side.
            *
            */
            generateHashFromServer(mPaymentParams);

            /**
             * Do not use below code when going live
             * Below code is provided to generate hash from sdk.
             * It is recommended to generate hash from server side only.
             * */
            /*mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);

           if (AppPreference.selectedTheme != -1) {
                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,MainActivity.this, AppPreference.selectedTheme,mAppPreference.isOverrideResultScreen());
            } else {
                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,MainActivity.this, R.style.AppTheme_default, mAppPreference.isOverrideResultScreen());
            }*/

        } catch (Exception e) {
            // some exception occurred
            Toast.makeText(this, "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Thus function calculates the hash for transaction
     *
     * @param paymentParam payment params of transaction
     * @return payment params along with calculated merchant hash
     */
    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {

        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, String> params = paymentParam.getParams();
        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");

        AppEnvironment appEnvironment = AppEnvironment.PRODUCTION;
        stringBuilder.append(appEnvironment.salt());

        String hash = hashCal(stringBuilder.toString());
        paymentParam.setMerchantHash(hash);

        return paymentParam;
    }


    public void generateHashFromServer(PayUmoneySdkInitializer.PaymentParam paymentParam) {
        //nextButton.setEnabled(false); // lets not allow the user to click the button again and again.

        HashMap<String, String> params = paymentParam.getParams();

        // lets create the post params
        StringBuffer postParamsBuffer = new StringBuffer();
        postParamsBuffer.append(concatParams(PayUmoneyConstants.KEY, params.get(PayUmoneyConstants.KEY)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.AMOUNT, params.get(PayUmoneyConstants.AMOUNT)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.TXNID, params.get(PayUmoneyConstants.TXNID)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.EMAIL, params.get(PayUmoneyConstants.EMAIL)));
        postParamsBuffer.append(concatParams("productinfo", params.get(PayUmoneyConstants.PRODUCT_INFO)));
        postParamsBuffer.append(concatParams("firstname", params.get(PayUmoneyConstants.FIRSTNAME)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF1, params.get(PayUmoneyConstants.UDF1)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF2, params.get(PayUmoneyConstants.UDF2)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF3, params.get(PayUmoneyConstants.UDF3)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF4, params.get(PayUmoneyConstants.UDF4)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF5, params.get(PayUmoneyConstants.UDF5)));

        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();

        // lets make an api call
        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
        getHashesFromServerTask.execute(postParams);
    }

    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }

    private class GetHashesFromServerTask extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(PaymentConfirm.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... postParams) {

            String merchantHash = "";
            try {
                //TODO Below url is just for testing purpose, merchant needs to replace this with their server side hash generation url

                /*Map<String, String> paramse = new HashMap<String, String>();
                paramse.put("cusToken", "1");
                paramse.put("products", payment_confirm_details.get("products"));
                paramse.put("addressToken", payment_confirm_details.get("deliveryAddressToken"));
                paramse.put("ship_type", ""+deliveryType);

                RequestQueue mRequestQueueg = Volley.newRequestQueue(PaymentConfirm.this);
                mVolleyService = new VolleyService(mResultCallback,PaymentConfirm.this);*/
                //mVolleyService.postStringDataVolley("CART_LISTING","https://payu.herokuapp.com/get_hash",paramse,mRequestQueueg);

                URL url = new URL("https://meatpoint.co.in/mphashpayu.php");

                String postParam = postParams[0];

                byte[] postParamsByte = postParam.getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postParamsByte);

                InputStream responseInputStream = conn.getInputStream();
                StringBuffer responseStringBuffer = new StringBuffer();
                byte[] byteContainer = new byte[1024];
                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                    responseStringBuffer.append(new String(byteContainer, 0, i));
                }

                JSONObject response = new JSONObject(responseStringBuffer.toString());
                Log.e("pmt res ", response.toString());

                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    if (key.equals("payment_hash")) {
                        merchantHash = response.getString(key);
                    } else {
                    }
                    Log.e("Key ", key);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return merchantHash;
        }

        @Override
        protected void onPostExecute(String merchantHash) {
            super.onPostExecute(merchantHash);

            progressDialog.dismiss();

            if (merchantHash.isEmpty() || merchantHash.equals("")) {
                Toast.makeText(PaymentConfirm.this, "Could not generate hash", Toast.LENGTH_SHORT).show();
            } else {
                mPaymentParams.setMerchantHash(merchantHash);

                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, PaymentConfirm.this, R.style.paypa, mAppPreference.isOverrideResultScreen());

                /*Intent intent = new Intent(PaymentConfirm.this, OrderListingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();*/

            }
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result Code is -1 send from Payumoney activity
        Log.d("creation", "request code " + requestCode + " resultcode " + resultCode);

        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data != null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager.INTENT_EXTRA_TRANSACTION_RESPONSE);

            ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);

            // Check which object is non-null
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                String payuResponse = transactionResponse.getPayuResponse();
                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction
                    Log.d("creation", "Success");
                    updateTrancationFun(payuResponse);

                } else {
                    //Failure Transaction
                    Log.d("creation", "Failure");

                    //Intent i
                }
                // Response from Payumoney
                // Response from SURl and FURL
                String merchantResponse = transactionResponse.getTransactionDetails();


                Log.e("creation", "payuResponse = " + payuResponse + "merchantResponse = " + merchantResponse);
//                new AlertDialog.Builder(this)
//                        .setCancelable(false)
//                        .setMessage("Payu's Data : " + payuResponse + "\n\n\n Merchant's Data: " + merchantResponse)
//                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                dialog.dismiss();
//                            }
//                        }).show();
            } else if (resultModel != null && resultModel.getError() != null) {
                Log.d("Payment Conf", "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d("creation", "Both objects are null!");
            }
        }


    }

    private void updateTrancationFun(String payuResponse) {
        updateTrancation();
        showDialog();
        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("orderToke", orderId);
        paramse.put("payuResponse", payuResponse);

        RequestQueue mRequestQueueg = Volley.newRequestQueue(PaymentConfirm.this);
        mVolleyService = new VolleyService(mResultCallback, PaymentConfirm.this);
        mVolleyService.postStringDataVolley("UpdateTransaction", UPDATE_TRANSACTOIN, paramse, mRequestQueueg);

    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }

    void updateTrancation() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
                Toast.makeText(PaymentConfirm.this, "Response : " + response, Toast.LENGTH_LONG).show();
                Intent i = new Intent(PaymentConfirm.this, OrderListingActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: " + response);
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                finish();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
        hideDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(pm_paymentMethod.equals("2")){
            Intent i = new Intent(PaymentConfirm.this, OrderListingActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        }
    }
}
