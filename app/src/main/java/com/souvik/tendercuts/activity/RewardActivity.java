package com.souvik.tendercuts.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.adapter.OrderListingAdapter;
import com.souvik.tendercuts.adapter.RewardListingAdapter;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.OrderHistoryModel;
import com.souvik.tendercuts.model.RewardHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.GET_REWARDS;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class RewardActivity extends AppCompatActivity {

    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    private RewardListingAdapter adapter;
    private List<RewardHistoryModel> orderListsItems;
    private RecyclerView recyclerView;

    private WebView mywalletHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);
        Uri uri=null;

        setFontAwesome((LinearLayout)findViewById(R.id.rewardActivity),RewardActivity.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(RewardActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
//        if(getIntent().getStringExtra("title").equalsIgnoreCase("Rewards")) {
//            titleToolbar.setText("My Loyalty Wallet");
//            uri=Uri.parse("https://meatpoint.co.in/mywallet/"+session.get_customerToken());
//        }else
            if(getIntent().getStringExtra("title").equalsIgnoreCase("term")) {
            titleToolbar.setText("Terms & Condition");
            uri=Uri.parse("https://meatpoint.co.in/terms");
        }else if(getIntent().getStringExtra("title").equalsIgnoreCase("privacy")) {
                titleToolbar.setText("Terms & Condition");
                uri=Uri.parse("https://meatpoint.co.in/privacy");
        }
        else {
            titleToolbar.setText("My Loyalty Wallet");
            uri=Uri.parse("https://meatpoint.co.in/mywallet/"+session.get_customerToken());
        }

        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RewardActivity.this,HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        /*initVolleyCallback();
        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", session.get_customerToken());
        RequestQueue mRequestQueueg = Volley.newRequestQueue(RewardActivity.this);
        mVolleyService = new VolleyService(mResultCallback,RewardActivity.this);
        mVolleyService.postStringDataVolley("GETREWARDS",GET_REWARDS,paramse,mRequestQueueg);

        orderListsItems = new ArrayList<>();
        adapter = new RewardListingAdapter(this, orderListsItems);

        recyclerView = (RecyclerView)findViewById(R.id.rewardRecycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RewardActivity.this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);*/
        if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
            mywalletHistory = (WebView)findViewById(R.id.mywalletHistory);
//            String udl = "https://meatpoint.co.in/mywallet/"+session.get_customerToken();
            mywalletHistory.loadUrl(String.valueOf(uri));

        }else{
            Intent i = new Intent(RewardActivity.this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if(status_code==1) {
                        //orderTrackScroll.setVisibility(View.VISIBLE);
                        //orderTrackProgress.setVisibility(View.GONE);

                        JSONArray responsearr = jsonObject.getJSONArray("response");
                        for(int i=0;i<responsearr.length();i++) {
                            JSONObject responsearrEachRow = responsearr.getJSONObject(i);

                            RewardHistoryModel a = new RewardHistoryModel();

                            a.setPointType(responsearrEachRow.getString("poinType"));
                            a.setPoints(responsearrEachRow.getString("points"));
                            a.setPointdate(responsearrEachRow.getString("date"));

                            orderListsItems.add(a);
                        }
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(RewardActivity.this,HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

}
