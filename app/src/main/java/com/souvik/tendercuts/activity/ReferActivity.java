package com.souvik.tendercuts.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.config.AppConfig;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.RewardHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class ReferActivity extends AppCompatActivity {

    private Button referBtn;
    DatabaseHandler db;
    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    SessionManager session;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer);

        session = SessionManager.getInstance(getApplicationContext());

        setFontAwesome((RelativeLayout)findViewById(R.id.referLayout),ReferActivity.this);
        pDialog = new ProgressDialog(ReferActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setText("Refer & Earn");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ReferActivity.this,HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        referBtn = (Button)findViewById(R.id.referBtn);
        referBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appforMembership();
            }
        });

        String ul = "https://meatpoint.co.in/referearn";
        Uri url = Uri.parse(ul);
        WebView ob = (WebView)findViewById(R.id.referWeb);
        ob.loadUrl(String.valueOf(url));

    }

    private void appforMembership() {
        initVolleyCallback();
        showDialog();
        mVolleyService = new VolleyService(mResultCallback, ReferActivity.this);

        HashMap<String, String> DataPost = new HashMap<String, String>();
        DataPost.put("cusToken", session.get_customerToken());


        RequestQueue requestQueue = Volley.newRequestQueue(ReferActivity.this);
        mVolleyService.postStringDataVolley("CustomerAuthenticate", AppConfig.REFER_FRIEND, DataPost, requestQueue);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ReferActivity.this,HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                hideDialog();
                Log.d("creation", "Total Response POST: "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if(status_code==1) {
                        Intent share = new Intent(android.content.Intent.ACTION_SEND);
                        share.setType("text/plain");
                        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                        // Add data to the intent, the receiving app will decide
                        // what to do with it.
                        share.putExtra(Intent.EXTRA_SUBJECT, jsonObject.getString("title"));
//        share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.flavoursclub.fcc&hl=en");
                        share.putExtra(Intent.EXTRA_TEXT, jsonObject.getString("message"));//"Use my referal code: 112233 and get exclusive offer on your membership card. You can download app from:");
                        startActivity(Intent.createChooser(share, "Share App!"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                hideDialog();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };

    }
    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
