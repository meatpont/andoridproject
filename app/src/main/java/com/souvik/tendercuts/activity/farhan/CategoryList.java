package com.souvik.tendercuts.activity.farhan;

/**
 * Created by Sushil on 3/18/2018.
 */

public class CategoryList {
    String name, token;
    int defaultTime=0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getDefaultTime() {
        return defaultTime;
    }

    public void setDefaultTime(int defaultTime) {
        this.defaultTime = defaultTime;
    }
}
