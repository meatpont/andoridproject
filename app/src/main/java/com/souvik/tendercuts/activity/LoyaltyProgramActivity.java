package com.souvik.tendercuts.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.souvik.tendercuts.database.DatabaseHandler;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class LoyaltyProgramActivity extends AppCompatActivity {
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loyalty_program);

        setFontAwesome((LinearLayout) findViewById(R.id.loyaltyProgramLayout), LoyaltyProgramActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setText("Loyalty Program");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoyaltyProgramActivity.this, HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        String ul = "https://meatpoint.co.in/loyaltyprogram";
        Uri url = Uri.parse(ul);
        WebView ob = (WebView) findViewById(R.id.loyaltyWeb);
        ob.loadUrl(String.valueOf(url));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(LoyaltyProgramActivity.this, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }


}
