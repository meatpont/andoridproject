package com.souvik.tendercuts.activity;

import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.souvik.tendercuts.adapter.TimeGridViewAdapter;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.model.TimeItemsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.co.meatpoint.R;


public class DeliveryDateTimeActivity extends AppCompatActivity {

    JSONArray deliverySlotArray;
    private GridView gridView;
    private TimeGridViewAdapter gridAdapter;
    ArrayList<TimeItemsModel> imageItems;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_date_time);

        Bundle bundle = this.getIntent().getExtras();
        ViewGroup hourButtonLayout = (ViewGroup) findViewById(R.id.hour_radio_group);
        gridView = (GridView)findViewById(R.id.timegridView);
        imageItems = new ArrayList<TimeItemsModel>();

        if(bundle != null) {
            Log.d("creation","bookingDataServices deliveryType => "+bundle.getString("deliveryType"));
            Log.d("creation","bookingDataServices deliverySlot=> "+bundle.getString("deliverySlot"));
            try {
                deliverySlotArray = new JSONArray(bundle.getString("deliverySlot"));
                for (int k=0; k<deliverySlotArray.length();k++){
                    JSONObject eachSlot = deliverySlotArray.getJSONObject(k);
                    //Log.d("creation",eachSlot.getString("date"));
                    RadioButton button = new RadioButton(this);
                    button.setId(k);
                    button.setText(eachSlot.getString("day"));
                    button.setPadding(28, 48, 28, 48);   //left, top, right, bottom
                    button.setTextSize(18);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        button.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    }
                    button.setTextColor(Color.parseColor("#424242"));
                    button.setBackgroundResource(R.drawable.tags_rounded_corners_white);
                    button.setWidth(250);

                    RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(
                            RadioGroup.LayoutParams.WRAP_CONTENT,
                            RadioGroup.LayoutParams.WRAP_CONTENT
                    );
                    params.setMargins(0, 0, 2, 0);
                    button.setLayoutParams(params);
                    button.setButtonDrawable(android.R.color.transparent);
                    hourButtonLayout.addView(button);

                    JSONArray aj = eachSlot.getJSONArray("time");
                    for (int j = 0; j < aj.length(); j++) {
                        JSONObject eachSlotTime = aj.getJSONObject(j);
                        imageItems.add(new TimeItemsModel("", eachSlotTime.getString("time")));
                    }
                }
                RadioGroup radioGroup = (RadioGroup) findViewById(R.id.hour_radio_group);

                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        for (int i = 0; i<deliverySlotArray.length(); i++) {
                            RadioButton radioButton = (RadioButton) group.findViewById(i);
                            if (i == checkedId) {
                                Log.d("selected=> ", String.valueOf(radioButton.getText()));
                                radioButton.setBackgroundResource(R.drawable.tags_rounded_corners_selected);
                                radioButton.setTextColor(Color.WHITE);
                            } else {
                                radioButton.setBackgroundResource(R.drawable.tags_rounded_corners_white);
                                radioButton.setTextColor(0xFF8B8B8B);
                            }
                        }
                    }
                });

                gridAdapter = new TimeGridViewAdapter(DeliveryDateTimeActivity.this, R.layout.time_grid_item_layout, imageItems);
                gridView.setAdapter(gridAdapter);

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    public interface dateChoosenInterface{
        public void setDateTime();
    }

}
