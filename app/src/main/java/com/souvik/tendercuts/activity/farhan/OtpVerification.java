package com.souvik.tendercuts.activity.farhan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.CartActivity;
import com.souvik.tendercuts.activity.ForgetPasswordActivity;
import com.souvik.tendercuts.activity.HomeActivity;
import com.souvik.tendercuts.activity.LoginActivity;
import com.souvik.tendercuts.activity.RegistrationActivity;
import com.souvik.tendercuts.config.AppConfig;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.*;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.SEND_OTP;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class OtpVerification extends AppCompatActivity {

    EditText et1, et2, et3, et4;
    TextView tv_mobile;
    String otp, otp_string, mobile;
    private ProgressDialog pDialog;
    FccResult mResultCallback = null;
    VolleyService mVolleyService;

    SessionManager session;
    public DatabaseHandler db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        setFontAwesome((LinearLayout) findViewById(R.id.otp_varifiacation_layout), OtpVerification.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setText("OTP Verification");
        titleToolbar.setVisibility(View.VISIBLE);

        session = com.souvik.tendercuts.helper.SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(OtpVerification.this);

        pDialog = new ProgressDialog(OtpVerification.this);
        mobile = getIntent().getStringExtra("mobile");
        otp= getIntent().getStringExtra("otp");

        et1 = findViewById(R.id.et_otp1);
        et2 = findViewById(R.id.et_otp2);
        et3 = findViewById(R.id.et_otp3);
        et4 = findViewById(R.id.et_otp4);

        tv_mobile = findViewById(R.id.tv_mobile);
        tv_mobile.setText(mobile);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.btn_resend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initVolleyCallback();
                showDialog();
                mVolleyService = new VolleyService(mResultCallback, OtpVerification.this);
                otp="";

                HashMap<String, String> loginDataPost = new HashMap<String, String>();
                loginDataPost.put("mobile", getIntent().getStringExtra("mobile"));

                RequestQueue requestQueue = Volley.newRequestQueue(OtpVerification.this);
                mVolleyService.postStringDataVolley("FORGET_PASSWORD", SEND_OTP, loginDataPost, requestQueue);
            }
        });
        findViewById(R.id.btn_verify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (otp.equalsIgnoreCase(otp_string)) {
                    verify_initVolleyCallback();
                    showDialog();
                    mVolleyService = new VolleyService(mResultCallback, OtpVerification.this);

                    HashMap<String, String> loginDataPost = new HashMap<String, String>();
                    loginDataPost.put("mobile", getIntent().getStringExtra("mobile"));
                    loginDataPost.put("otp", otp_string);
                    loginDataPost.put("type", getIntent().getStringExtra("type"));

                    RequestQueue requestQueue = Volley.newRequestQueue(OtpVerification.this);
                    mVolleyService.postStringDataVolley("CustomerAuthenticate", AppConfig.VERIFY_OTP, loginDataPost, requestQueue);
                } else {
                    MeatPointUtils.decisionAlertOnMainThread(OtpVerification.this,
                            android.R.drawable.ic_dialog_info,
                            R.string.error,
                            "Enter valid OTP.",
                            "Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                    dialog.dismiss();
                                    //finish();
                                }
                            }, "", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                }
                            }
                    );

                }
            }
        });

        et1.addTextChangedListener(new GenericTextWatcher(et1));
        et2.addTextChangedListener(new GenericTextWatcher(et2));
        et3.addTextChangedListener(new GenericTextWatcher(et3));
        et4.addTextChangedListener(new GenericTextWatcher(et4));
    }


    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.et_otp1:
                    if (text.length() == 1)
                        et2.requestFocus();
                    if(text.length()==0)
                        et1.requestFocus();
                    break;
                case R.id.et_otp2:
                    if (text.length() == 1)
                        et3.requestFocus();
                    if(text.length()==0)
                        et1.requestFocus();
                    break;
                case R.id.et_otp3:
                    if (text.length() == 1)
                        et4.requestFocus();
                    if(text.length()==0)
                        et2.requestFocus();
                    break;
                case R.id.et_otp4:
                    if(text.length()==0)
                        et3.requestFocus();
                    break;

            }
            otp_string = et1.getText().toString().concat(et2.getText().toString()).concat(et3.getText().toString()).concat(et4.getText().toString());

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub

        }
    }

    void initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response : " + response);
                hideDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        HashMap<String, String> detailsMap = new HashMap<String, String>();

                        JSONObject loginDetails = jsonObject.getJSONObject("response");
//                        Intent intent = new Intent(O.this, OtpVerification.class);
//                        intent.putExtra("mobile", loginDetails.getString("mobile"));
//                        intent.putExtra("otp", loginDetails.getString("otp"));
//                        startActivity(intent);
                        Log.d("creation", loginDetails.getString("cusToken"));
                        Log.d("creation", loginDetails.getString("otp"));
                        otp=loginDetails.getString("otp");

                    } else if (status == 0) {
                        Snackbar.make(findViewById(android.R.id.content), "Enter register mobile number.", Snackbar.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                hideDialog();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    void verify_initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation2", "Total Response : " + response);
                hideDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        HashMap<String, String> detailsMap = new HashMap<String, String>();

                        JSONObject loginDetails = jsonObject.getJSONObject("response");

                        detailsMap.put("apiKey", loginDetails.getString("apiKey"));
                        detailsMap.put("customerToken", loginDetails.getString("customerToken"));
                        detailsMap.put("customerEmail", loginDetails.getString("customerEmail"));
                        detailsMap.put("customerName", loginDetails.getString("customerName"));
                        detailsMap.put("customerPhone", loginDetails.getString("customerPhone"));
                        detailsMap.put("customerPhoto", loginDetails.getString("customerPhoto"));

                        db.updateCartProductToAnotherUser(loginDetails.getString("customerToken"));
                        if (session.isCustomerLoggedIn() == null) {
                            detailsMap.put("isCustomerLoggedIn", "1");
                            session.set_UserDetails(detailsMap);
                            Intent i = new Intent(OtpVerification.this, HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        } else if (session.isCustomerLoggedIn() == "4") {
                            detailsMap.put("isCustomerLoggedIn", "1");
                            session.set_UserDetails(detailsMap);
                            Intent i = new Intent(OtpVerification.this, CartActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        } else {
                            detailsMap.put("isCustomerLoggedIn", "1");
                            session.set_UserDetails(detailsMap);
                            Intent i = new Intent(OtpVerification.this, HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        }

                    } else if (status == 0) {

                        MeatPointUtils.decisionAlertOnMainThread(OtpVerification.this,
                                android.R.drawable.ic_dialog_info,
                                R.string.error,
                                "Enter register mobile number.",
                                "Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                        dialog.dismiss();
                                        //finish();
                                    }
                                }, "", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        );
                       // Snackbar.make(findViewById(android.R.id.content), "Enter register mobile number.", Snackbar.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                hideDialog();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getIntent().getStringExtra("type").equalsIgnoreCase("registration")){
            startActivity(new Intent(OtpVerification.this, RegistrationActivity.class));
            finish();
        }else {
            startActivity(new Intent(OtpVerification.this, ForgetPasswordActivity.class));
            finish();
        }
    }
}
