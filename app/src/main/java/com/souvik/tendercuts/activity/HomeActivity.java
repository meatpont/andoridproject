package com.souvik.tendercuts.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.souvik.tendercuts.activity.farhan.CatagoryHolder;
import com.souvik.tendercuts.adapter.CategoryAdapter;
import com.souvik.tendercuts.common.Action_menu_button;
import com.souvik.tendercuts.config.AppConfig;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.CategoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.co.meatpoint.R;

import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class HomeActivity extends AppCompatActivity {
    private static long back_pressed;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    private String homecontent;

    public RecyclerView recyclerView;
    private CategoryAdapter adapter;
    private List<CategoryModel> categoryModelList;

    private SessionManager session;
    public DatabaseHandler db;
//    private LinearLayout main_content;

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ScrollView contentFullPanel;
    private ProgressBar cartProgress;

    boolean categoryload = false;
    JSONArray catFullArray;
    boolean isOpen=false;

    private TextView navigationUserEmail,navigationImageViewUserProfile, navigationRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(HomeActivity.this);

        //Log.d("creation","hello =>"+session.get_customerApiKey());

        setFontAwesome((LinearLayout)findViewById(R.id.main_content),HomeActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        contentFullPanel = (ScrollView)findViewById(R.id.contentFullPanel);
        cartProgress = (ProgressBar)findViewById(R.id.cartProgress);

        Button searchBtn = (Button)findViewById(R.id.searchBtn);
        searchBtn.setVisibility(View.GONE);
////Delete cart item
      //db.deleteCart();
        //com.souvik.tendercuts.activity.farhan.SessionManager.clearSession(this);

       /*
        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this,CartActivity.class);
                startActivity(i);
            }
        });*/
        //cartBtn.setVisibility(View.GONE);

        /*TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        View headerView = navigationView.getHeaderView(0);

        setFontAwesome((TextView)headerView.findViewById(R.id.imageView_user_profile),HomeActivity.this);
        navigationUserEmail = (TextView)headerView.findViewById(R.id.navigationUserEmail);
        navigationUserEmail.setText(session.get_customerName());
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                //Checking if the item is in checked state or not, if not make it in checked state
                //menuItem.setChecked(false);
                if(menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);
                //Closing drawer on item click
                drawerLayout.closeDrawers();

                Intent intentobj = null;
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_dashboard:
                        return true;
                    case R.id.nav_profile:
                        intentobj = new Intent(HomeActivity.this, ProfileActivity.class);
                        startActivity(intentobj);
                        return true;
                    case R.id.loyalty_program:
                        intentobj = new Intent(HomeActivity.this, LoyaltyProgramActivity.class);
                        startActivity(intentobj);
                        return true;
                    case R.id.refer_friend:
                        intentobj = new Intent(HomeActivity.this, ReferActivity.class);
                        startActivity(intentobj);
                        return true;
                    case R.id.nav_order_history:
                        intentobj = new Intent(HomeActivity.this, OrderListingActivity.class);
                        startActivity(intentobj);
                        return true;
                    case R.id.nav_bulk_order:
                        if (categoryload) {
                            intentobj = new Intent(HomeActivity.this, BulkOrderActivity.class);
                            intentobj.putExtra("catDetails", String.valueOf(catFullArray));
                            startActivity(intentobj);
                        }
                        return true;
                    case R.id.nav_contact:
                        intentobj = new Intent(HomeActivity.this, ContactActivity.class);
                        startActivity(intentobj);
                        return true;
                    case R.id.nav_faq:
                        intentobj = new Intent(HomeActivity.this, FaqActivity.class);
                        startActivity(intentobj);
                        return true;
                    case R.id.nav_login:
                        intentobj = new Intent(HomeActivity.this, LoginActivity.class);
                        intentobj.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentobj);
                        return true;
                    case R.id.nav_signup:
                        intentobj = new Intent(HomeActivity.this, RegistrationActivity.class);
                        intentobj.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentobj);
                        return true;
                    case R.id.nav_redeem:
                        intentobj = new Intent(HomeActivity.this, RewardActivity.class);
                        intentobj.putExtra("title","Rewards");
                        startActivity(intentobj);
                        return true;

                    case R.id.nav_tandc:
                        String url = "https://meatpoint.co.in/terms";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);

////                        intentobj = new Intent(Intent.ACTION_VIEW, Uri.parse("https://meatpoint.co.in/terms"));
//                        intentobj = new Intent(HomeActivity.this, RewardActivity.class);
//                        intentobj.putExtra("title","term");
//                        startActivity(intentobj);
                        return true;
                    case R.id.nav_logout:
                        if (db.getCartCountByUserID(session.get_customerToken()) > 0) {
                            db.deleteCartUsingUserId(session.get_customerToken());
                        }
                        HashMap<String, String> detailsMap = new HashMap<String, String>();
                        detailsMap.put("isCustomerLoggedIn", "0");
                        session.set_UserDetails(detailsMap);
                        Intent intnt = new Intent(HomeActivity.this, LoginActivity.class);
                        intnt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intnt);
//                        /*final AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this, R.style.MyAlertDialogStyle);
//                        builder.setTitle("Are you Sure? ");
//                        // Set up the buttons
//                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                HashMap<String,String> detailsMap = new HashMap<>();
//                                detailsMap.put("isCustomerLoggedIn","0");
//                                session.set_UserDetails(detailsMap);
//                                Intent intnt = new Intent(HomeActivity.this, LoginActivity.class);
//                                intnt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intnt);
//                            }
//
//                        });
//                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                //Toast.makeText(getApplicationContext(),"You clicked no button",Toast.LENGTH_LONG).show();
//                            }
//
//                        });
//                        builder.show();*/
                        return true;
                    default:
                        return true;
                }

            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
//        final ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
//            @SuppressLint("WrongConstant")
//            @Override
//            public void onDrawerStateChanged(int newState) {
//                super.onDrawerStateChanged(newState);
//                if (newState == DrawerLayout.STATE_SETTLING) {
//                    if (!actionBarDrawerToggle.) {
//                        // starts opening
//                        getActionBar()
//                                .setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
//                    } else {
//                        // closing drawer
//                        getActionBar()
//                                .setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//                    }
//                    invalidateOptionsMenu();
//                }
//            }
//        };

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
//                Toast.makeText(HomeActivity.this, "Close", Toast.LENGTH_SHORT).show();
                isOpen=false;
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
//                Toast.makeText(HomeActivity.this, "Open", Toast.LENGTH_SHORT).show();
                isOpen=true;

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                if(session!=null) {
                    if (session.isCustomerLoggedIn().equalsIgnoreCase("0") || session.isCustomerLoggedIn().equalsIgnoreCase("4")) {
                        hideItem();
                    }
                }else {
                    navigationUserEmail.setText(session.get_customerName());
                }
//                if(newState==DrawerLayout.STATE_SETTLING){
//                    if(!isOpen) {
//                        Toast.makeText(HomeActivity.this, "State", Toast.LENGTH_SHORT).show();
//                        session = SessionManager.getInstance(getApplicationContext());
//                    }
//                }
            }
        };
        //Setting the actionbarToggle to drawer layout
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        navigationImageViewUserProfile = (TextView)headerView.findViewById(R.id.imageView_user_profile);
        navigationImageViewUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
        });
//
//
//        /*
//        Button Buttonback = (Button)findViewById(R.id.Buttonback);
//        Buttonback.setText("");*/
//
//        /*Button collectFeedback = (Button)findViewById(R.id.collectFeedback);
//        Button feedbackAnalytics = (Button)findViewById(R.id.feedbackAnalytics);
//
//        collectFeedback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent(HomeActivity.this, FeedbackActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(i);
//            }
//        });
//
//        feedbackAnalytics.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent(HomeActivity.this, FeedbackAnalyticsActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(i);
//            }
//        });*/

        initVolleyCallback();
        mVolleyService = new VolleyService(mResultCallback,HomeActivity.this);

        RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);

        HashMap<String, String> loginDataPost = new HashMap<String, String>();
        //mVolleyService.getDataVolley("HomeActivity", AppConfig.HOMEPAGE);
        mVolleyService.postStringDataVolley("HomeActivity", AppConfig.HOMEPAGE,loginDataPost,requestQueue);


        //New Code
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        //initCollapsingToolbar();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        categoryModelList = new ArrayList<CategoryModel>();
        adapter = new CategoryAdapter(this, categoryModelList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(2), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        //prepareAlbums();

        /*try {
            Glide.with(this).load(R.drawable.cover).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        //New Code End
        /*********/
        Action_menu_button n = new Action_menu_button(getApplicationContext(),HomeActivity.this);
        n.actionMenuButton(1);

        try {
            if(session!=null) {
                if (session.isCustomerLoggedIn().equalsIgnoreCase("0")) {
                    hideItem();
                }
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void hideItem() {
        Menu nav_Menu = navigationView.getMenu();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        View headerView = navigationView.getHeaderView(0);
        navigationUserEmail = (TextView)headerView.findViewById(R.id.navigationUserEmail);
        navigationUserEmail.setText("Guest");

        nav_Menu.findItem(R.id.nav_login).setVisible(true);
        nav_Menu.findItem(R.id.nav_signup).setVisible(true);

        nav_Menu.findItem(R.id.nav_profile).setVisible(false);
        nav_Menu.findItem(R.id.refer_friend).setVisible(false);
        nav_Menu.findItem(R.id.nav_order_history).setVisible(false);
        nav_Menu.findItem(R.id.nav_redeem).setVisible(false);
        nav_Menu.findItem(R.id.nav_logout).setVisible(false);

    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) super.onBackPressed();
        else Toast.makeText(HomeActivity.this, "Press once again to exit!", Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();

        db = new DatabaseHandler(this);
        Log.e("deleted items " ,""+db.deleteCart());
        com.souvik.tendercuts.activity.farhan.SessionManager.clearSession(this);
    }

    private void loadBanner(JSONArray bannerContent) {
        if(bannerContent.length()>0){
            try {
                final SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);

                sliderShow.stopAutoCycle();

                //JSONObject jsonObject = new JSONObject(bannerContent);
                JSONArray dealsChildArray = bannerContent;

                //HashMap<String,String> url_maps = new HashMap<String, String>();

                for(int j=0;j<dealsChildArray.length();j++){

                    JSONObject dealsChildJsonEachRow=dealsChildArray.getJSONObject(j);
                    final int finalI = j;
                    //url_maps.put("img"+j,dealsChildJsonEachRow.getString("img"));
                    DefaultSliderView defaultSliderView = new DefaultSliderView(HomeActivity.this);

//                    /*typeArray.add(j,dealsChildJsonEachRow.getString("type"));
//                    membershipTokenArray.add(j,dealsChildJsonEachRow.getString("membershipToken"));
//                    industrySlugArray.add(j,dealsChildJsonEachRow.getString("industrySlug"));
//                    bizTokenArray.add(j,dealsChildJsonEachRow.getString("bizToken"));
//                    serviceTokenArray.add(j,dealsChildJsonEachRow.getString("serviceToken"));*//*
//
//                    *//*.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
//                                @Override
//                                public void onSliderClick(BaseSliderView slider) {
////                                    Log.d("creation", "Click Image => " + urls.get(finalI));
//                                    //membership,listing,specialoffer,health,details,event
//                                    *//*if(typeArray.get(finalI).equalsIgnoreCase("membership")){
//                                        intnt = new Intent(HomeActivity.this, Membership.class);
//                                        startActivity(intnt);
//                                    }else if(typeArray.get(finalI).equalsIgnoreCase("listing")){
//                                        intnt = new Intent(HomeActivity.this, BusinessListingActivity.class);
//                                        intnt.putExtra("industry_slug", industrySlugArray.get(finalI));
//                                        intnt.putExtra("selector","homeactivity");
//                                        startActivity(intnt);
//                                    }else if(typeArray.get(finalI).equalsIgnoreCase("specialoffer")){
//                                        intnt = new Intent(HomeActivity.this, BusinessListingActivity.class);
//                                        // ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        intnt.putExtra("industry_slug", industrySlugArray.get(finalI));
//                                        intnt.putExtra("selector", "specialactivity");
//                                        startActivity(intnt);
//                                    }else if(typeArray.get(finalI).equalsIgnoreCase("health")){
//                                        intnt = new Intent(HomeActivity.this, HealthActivity.class);
//                                        startActivity(intnt);
//                                    }else if(typeArray.get(finalI).equalsIgnoreCase("details")){
//                                        intnt = new Intent(HomeActivity.this, DetailsBusinessActivity.class);
//                                        //intnt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        intnt.putExtra("business_master_id",  Integer.parseInt(bizTokenArray.get(finalI)));
//                                        intnt.putExtra("IndustrySlug",  industrySlugArray.get(finalI));
//                                        startActivity(intnt);
//                                    }else if(typeArray.get(finalI).equalsIgnoreCase("healthpackage")){
//                                        intnt = new Intent(HomeActivity.this, HealthListingActivity.class);
//                                        intnt.putExtra("industry_slug", industrySlugArray.get(finalI));
//                                        intnt.putExtra("id", serviceTokenArray.get(finalI) );
//                                        intnt.putExtra("isServiceTokenBased", "null");
//                                        intnt.putExtra("selector", "specialactivity");
//                                        startActivity(intnt);
//                                    }else{
//
//                                    }
//                                    Log.d("creation", "Click Image => " + typeArray.get(finalI));*
//                                }
//                            })
//                    //.empty(R.mipmap.no_image)
//                    Log.d("creation","dd => "+dealsChildJsonEachRow.getString("pic"));*/
                    defaultSliderView
                            .description("img"+j)
                            .image(dealsChildJsonEachRow.getString("pic"))
                            .setScaleType(BaseSliderView.ScaleType.Fit);

                    sliderShow.addSlider(defaultSliderView);
                }

                sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

                sliderShow.setCurrentPosition(0, true);

                // play from first image, when all images loaded
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sliderShow.startAutoCycle();
                    }
                }, 5000);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);


            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);
                contentFullPanel.setVisibility(View.VISIBLE);
                cartProgress.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if (status_code == 1) {
                        JSONObject hh = jsonObject.getJSONObject("detail");
                        JSONArray bannerLists=hh.getJSONArray("banners");
                        if(bannerLists.length()>0){
                            loadBanner(bannerLists);
                        }
                        JSONArray categoryLists=hh.getJSONArray("categories");

                        catFullArray = categoryLists;
                        Log.e("Category ", String.valueOf(catFullArray.getString(2)));
                        Log.e("Size ", String.valueOf(categoryLists.length()));
                        categoryload = true;
//                        ArrayList<String> catagory = new ArrayList<String>();
//                        catagory.clear();
//                        for(int i =1;i<catFullArray.length();i++){
//                            JSONObject object = catFullArray.getJSONObject(i);
//                            catagory.add(object.getString("catName"));
//                        }
//                        new CatagoryHolder().setCatagory(catagory);
                        if(categoryLists.length()>0){
                            loadCategory(categoryLists);
                        }
                        //Log.d("creation","dd"+);
                    }
                } catch (JSONException e) {
                    Log.e("Exp ", e.getMessage());
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Toast.makeText(HomeActivity.this, "Check your internet connection.", Toast.LENGTH_SHORT).show();
                finish();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

//    // New Functions For Menu
//    /**
//     * Initializing collapsing toolbar
//     * Will show and hide the toolbar title on scroll
//     */
//    /*private void initCollapsingToolbar() {
//        final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//        collapsingToolbar.setTitle(" ");
//        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
//        appBarLayout.setExpanded(true);
//
//        // hiding & showing the title when toolbar expanded & collapsed
//        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
//            boolean isShow = false;
//            int scrollRange = -1;
//
//            @Override
//            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                if (scrollRange == -1) {
//                    scrollRange = appBarLayout.getTotalScrollRange();
//                }
//                if (scrollRange + verticalOffset == 0) {
//                    collapsingToolbar.setTitle(getString(R.string.app_name));
//                    isShow = true;
//                } else if (isShow) {
//                    collapsingToolbar.setTitle(" ");
//                    isShow = false;
//                }
//            }
//        });
//    }*/
//
//    /**
//     * Adding few albums for testing
//     */

    private void loadCategory(JSONArray categoryContent) {

        try {
            //Log.d("creation", "Total Response POST: "+categoryContent);
            /*ViewGroup.LayoutParams params=homeBlockRecycler.getLayoutParams();
            params.height = params.height * homeBlockJsonArray.length();
            //Toast.makeText(getApplicationContext(),"params.height"+params.height,Toast.LENGTH_LONG).show();
            homeBlockRecycler.setLayoutParams(params);*/
            int rowCountList = 0;
            if (categoryContent.length()%2==0){
                rowCountList = categoryContent.length() / 2;
            }else{
                rowCountList = categoryContent.length() / 2;
                rowCountList = rowCountList + 1;
            }

            ViewGroup.LayoutParams params=recyclerView.getLayoutParams();
            params.height = params.height * rowCountList;
            recyclerView.setLayoutParams(params);

            for (int j = 0; j < categoryContent.length(); j++) {
                JSONObject categoryEachRow = categoryContent.getJSONObject(j);

                CategoryModel a = new CategoryModel(categoryEachRow.getString("catName"), Integer.parseInt(categoryEachRow.getString("catToken")), categoryEachRow.getString("pic"),categoryContent,categoryEachRow.getString("is_bulk"));
                categoryModelList.add(a);
            }
            adapter.notifyDataSetChanged();
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    int rowcount = 0;

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if(column==0){
                rowcount++;
            }

            //Log.d("creation","position "+position);
            //Log.d("creation","column "+column);
            //Log.d("creation","rowcount "+rowcount);

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


}
