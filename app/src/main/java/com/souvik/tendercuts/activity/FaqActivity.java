package com.souvik.tendercuts.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.adapter.ExpandableListAdapter;
import com.souvik.tendercuts.adapter.FaqListingAdapter;
import com.souvik.tendercuts.adapter.ItemListingAdapter;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.FaqModel;
import com.souvik.tendercuts.model.ItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.FAQ;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class FaqActivity extends AppCompatActivity {

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;
    DatabaseHandler db;

    RecyclerView recyclerView;
    private List<FaqModel> itemModelList;
    private FaqListingAdapter adapter;
    private ProgressBar faqProgress;
    private TextView nofaqlistfound;

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        setFontAwesome((LinearLayout)findViewById(R.id.faq_linearlayout),FaqActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setText("FAQ");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FaqActivity.this,HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        /*String ul = "http://meatpoint.co.in/faqs";
        WebView ob = (WebView)findViewById(R.id.faqWeb);
        ob.loadUrl(ul);*/

        initVolleyCallback();
        Map<String, String> paramse = new HashMap<String, String>();
        RequestQueue mRequestQueueg = Volley.newRequestQueue(FaqActivity.this);
        mVolleyService = new VolleyService(mResultCallback,FaqActivity.this);
        mVolleyService.postStringDataVolley("FAQ",FAQ,paramse,mRequestQueueg);

        recyclerView = (RecyclerView)findViewById(R.id.faqRecycler);

        itemModelList = new ArrayList<FaqModel>();
        adapter = new FaqListingAdapter(FaqActivity.this, itemModelList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(FaqActivity.this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(adapter);


        nofaqlistfound = (TextView)findViewById(R.id.nofaqlistfound);
        faqProgress = (ProgressBar)findViewById(R.id.faqProgress);
        listView = (ExpandableListView)findViewById(R.id.lvExp);

        listDataHeader = new ArrayList<String>();
        listHash = new HashMap<String, List<String>>();

    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if (status_code == 1) {
                        listView.setVisibility(View.VISIBLE);
                        faqProgress.setVisibility(View.GONE);
                        //JSONObject hh = jsonObject.getJSONObject("detail");
                        JSONArray productlistArray=jsonObject.getJSONArray("response");
                        for (int i=0;i<productlistArray.length();i++){
                            JSONObject productEachRow = productlistArray.getJSONObject(i);

                            listDataHeader.add(productEachRow.getString("question"));
                            List<String> edmtdev = new ArrayList<String>();
                            edmtdev.add(productEachRow.getString("answer"));
                            listHash.put(listDataHeader.get(i),edmtdev);

                            FaqModel a = new FaqModel();
                            a.setQuestion(productEachRow.getString("question"));
                            a.setAnswer(productEachRow.getString("answer"));

                            itemModelList.add(a);

                        }
                        listAdapter = new ExpandableListAdapter(FaqActivity.this,listDataHeader,listHash);
                        listView.setAdapter(listAdapter);
                        //adapter.notifyDataSetChanged();
                    }else{
                        nofaqlistfound.setVisibility(View.VISIBLE);
                        faqProgress.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    nofaqlistfound.setVisibility(View.VISIBLE);
                    faqProgress.setVisibility(View.GONE);
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(FaqActivity.this,HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

}
