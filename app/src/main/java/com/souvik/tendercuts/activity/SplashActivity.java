package com.souvik.tendercuts.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.SessionManager;

import org.json.JSONObject;

import java.util.HashMap;

import in.co.meatpoint.R;


public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;

    private SessionManager session;
    public DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(SplashActivity.this);
        db.deleteCart();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                /*Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);*/
                try {
                    if(session!=null){
                        if (session.isCustomerLoggedIn()==null) {

                            HashMap<String, String> detailsMap = new HashMap<String, String>();

                            detailsMap.put("apiKey", "");
                            detailsMap.put("customerToken", "demouser");
                            detailsMap.put("customerEmail", "");
                            detailsMap.put("customerName", "");
                            detailsMap.put("customerPhone", "");
                            detailsMap.put("customerPhoto", "");
                            detailsMap.put("isCustomerLoggedIn", "0");

                            session.set_UserDetails(detailsMap);
                        }
                    }

                }catch (Exception e){
                    Log.e("Exp ", e.getMessage());
                }

                Intent i = new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(i);
                finish();

            }
        }, SPLASH_TIME_OUT);
    }
}
