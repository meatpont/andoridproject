package com.souvik.tendercuts.activity;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.adapter.CartListingAdapter;
import com.souvik.tendercuts.common.Action_menu_button;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.OnRefreshCartViewListner;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.AddressModel;
import com.souvik.tendercuts.model.CartModel;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.CALCULATE_CART;
import static com.souvik.tendercuts.config.AppConfig.CART_CHECK_URL;
import static com.souvik.tendercuts.config.AppConfig.COUPON_VALIDATION;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class CartActivity extends AppCompatActivity implements OnRefreshCartViewListner{

    SessionManager session;
    public DatabaseHandler db;

    private RecyclerView recyclerView;
    List<ItemModel> items,cartItems;
    CartListingAdapter adapter;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    TextView totalPrice,grandTotal,points,redeemedPoint,couponpriceminus,totalPriceButtom,payableAmount,discountFee,couponmsgtext,fb_gstValue,
    details;
    Button Confirm,couponbtnapply;
    EditText coupontext;

    String single_item = "";
    ProgressBar cartProgress;
    LinearLayout fb_grandTotalLayout,fb_totLayout,cartlayout,redemedPointsLayout,couponlayout,couponTextlayout,couponappliedlayout,payableAmountLayout,fb_gstOptionLayout,fb_discountOptionLayout,fb_earnPointsLayout;

    String earn_points,deliveryAddress;
    double totalprice,prevGrandTotal,updatedGrandTotal;

    private ProgressDialog pDialog;
    CheckBox meatwallet,havecoupon;
    TextView cartaddedQuantity;
    RelativeLayout bottomProceedButton;

    String f_checkCouponCode="no",f_couponCode="",f_checkRewardPoint="no",f_extraInfo="";
    int available_points=0;
    EditText extraInfoEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        setFontAwesome((RelativeLayout)findViewById(R.id.cartListingLinearLayout),CartActivity.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(CartActivity.this);
        pDialog = new ProgressDialog(CartActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(CartActivity.this, ListActivity.class));
                finish();
            }
        });

        ImageView imageView5 = (ImageView)findViewById(R.id.imageView5);
        imageView5.setVisibility(View.GONE);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setVisibility(View.VISIBLE);
        titleToolbar.setText("Cart Summary");

        items = new ArrayList<ItemModel>();
        cartItems = new ArrayList<ItemModel>();
        recyclerView = (RecyclerView) findViewById(R.id.cartRecycler);
        cartProgress = (ProgressBar)findViewById(R.id.cartProgress);
        cartlayout = (LinearLayout)findViewById(R.id.cartlayout);
        bottomProceedButton = (RelativeLayout)findViewById(R.id.bottomProceedButton);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.setAdapter(adapter);
        totalPrice = (TextView)findViewById(R.id.totalPrice);
        grandTotal = (TextView)findViewById(R.id.grandTotal);
        Confirm = (Button)findViewById(R.id.Confirm);
        points = (TextView)findViewById(R.id.points);
        redemedPointsLayout = (LinearLayout)findViewById(R.id.redemedPointsLayout);
        redeemedPoint = (TextView)findViewById(R.id.redeemedPoint);
        couponbtnapply = (Button)findViewById(R.id.couponbtnapply);
        coupontext = (EditText)findViewById(R.id.coupontext);
        couponpriceminus = (TextView)findViewById(R.id.couponpriceminus);
        couponlayout = (LinearLayout)findViewById(R.id.couponlayout);
        totalPriceButtom = (TextView)findViewById(R.id.totalPriceButtom);
        meatwallet =(CheckBox)findViewById(R.id.meatwallet);
        havecoupon = (CheckBox)findViewById(R.id.havecoupon);
        couponappliedlayout = (LinearLayout)findViewById(R.id.couponappliedlayout);
        payableAmount = (TextView)findViewById(R.id.payableAmount);
        payableAmountLayout = (LinearLayout)findViewById(R.id.payableAmountLayout);
        couponTextlayout = (LinearLayout)findViewById(R.id.couponTextlayout);
        discountFee = (TextView)findViewById(R.id.discountFee);
        //Log.d("creative","dd => "+db.getCartCountByUserID(session.get_customerToken()));
        fb_discountOptionLayout = (LinearLayout)findViewById(R.id.discountOptionLayout);
        couponmsgtext = (TextView)findViewById(R.id.couponmsgtext);
        cartaddedQuantity = (TextView)findViewById(R.id.cartaddedQuantity);
        fb_gstValue = (TextView)findViewById(R.id.gstValue);
        fb_gstOptionLayout = (LinearLayout)findViewById(R.id.gstOptionLayout);
        fb_totLayout = (LinearLayout)findViewById(R.id.totLayout);
        fb_grandTotalLayout = (LinearLayout)findViewById(R.id.grandTotalLayout);
        fb_earnPointsLayout = (LinearLayout)findViewById(R.id.earnPointsLayout);

        details =findViewById(R.id.tv_details);

        loadCartPage();

        extraInfoEditText = (EditText) findViewById(R.id.extra_info);


        meatwallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(available_points>0) {
                    if (isChecked) {
                        havecoupon.setChecked(false);
                        clear();
                        //********************************************************************************************************
                        f_checkCouponCode = "";
                        f_couponCode = "no";
                        f_checkRewardPoint = "yes";
                        paymentConfirmPageLoad(session.get_customerToken(), getProductLists(), f_checkCouponCode, f_couponCode, f_checkRewardPoint, f_extraInfo);
                        //********************************************************************************************************
                        couponTextlayout.setVisibility(View.GONE);
                        couponappliedlayout.setVisibility(View.GONE);
                    } else {
                        clear();
                        //********************************************************************************************************
                        f_checkCouponCode = "";
                        f_couponCode = "no";
                        f_checkRewardPoint = "no";
                        paymentConfirmPageLoad(session.get_customerToken(), getProductLists(), f_checkCouponCode, f_couponCode, f_checkRewardPoint, f_extraInfo);
                        //********************************************************************************************************
                        redemedPointsLayout.setVisibility(View.GONE);
                    }
                }else {
                    Toast.makeText(CartActivity.this, "No points available in wallet.", Toast.LENGTH_LONG).show();
                    meatwallet.setChecked(false);
                }
            }
        });

        havecoupon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    meatwallet.setChecked(false);

                    redemedPointsLayout.setVisibility(View.GONE);
                    couponTextlayout.setVisibility(View.VISIBLE);
                    couponappliedlayout.setVisibility(View.GONE);

                    couponbtnapply = (Button)findViewById(R.id.couponbtnapply);
                    coupontext = (EditText)findViewById(R.id.coupontext);
                }else{
                    couponTextlayout.setVisibility(View.GONE);
                    couponappliedlayout.setVisibility(View.GONE);
                    f_checkCouponCode="no";f_couponCode="";
                    paymentConfirmPageLoad(session.get_customerToken(), getProductLists(), "No","2", f_checkRewardPoint,f_extraInfo);
                }
            }
        });

        coupontext.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }
            @Override
            public void afterTextChanged(Editable et) {
                String s=et.toString();
                if(!s.equals(s.toUpperCase()))
                {
                    s=s.toUpperCase();
                    coupontext.setText(s);
                }
                coupontext.setSelection(coupontext.getText().length());
            }
        });

        couponbtnapply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!coupontext.getText().toString().isEmpty()) {
                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {
                    }
                    //************************************************************************************************
                    clear();
                    f_checkCouponCode = "yes";
                    f_couponCode = coupontext.getText().toString();
                    f_checkRewardPoint = "no";
                    paymentConfirmPageLoad(session.get_customerToken(), getProductLists(), f_checkCouponCode,f_couponCode, f_checkRewardPoint,f_extraInfo);
                    //************************************************************************************************
                } else{

                }
            }
        });
        //LinearLayout couponappliedlayout = (LinearLayout)findViewById(R.id.couponappliedlayout);
        ImageButton couponcancelbftn = (ImageButton)findViewById(R.id.couponcancelbftn);
        couponcancelbftn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
                //************************************************************************************************
                f_checkCouponCode = "no";
                f_couponCode = "";
                f_checkRewardPoint = "no";
                //************************************************************************************************
                redemedPointsLayout.setVisibility(View.GONE);
                couponTextlayout.setVisibility(View.VISIBLE);
                couponappliedlayout.setVisibility(View.GONE);
                couponlayout.setVisibility(View.GONE);
                fb_discountOptionLayout.setVisibility(View.GONE);
                paymentConfirmPageLoad(session.get_customerToken(), getProductLists(), f_checkCouponCode,f_couponCode, f_checkRewardPoint,f_extraInfo);
            }
        });


        TextView proceedText = (TextView)findViewById(R.id.proceedText);
        proceedText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddressModel adrs = new AddressModel();
                adrs.setAddressPresent(false);

                Intent i = new Intent(CartActivity.this,DeliverySummaryActivity.class);
                i.putExtra("products",getProductLists());
                i.putExtra("checkCouponCode",f_checkCouponCode);
                i.putExtra("couponCode",f_couponCode);
                i.putExtra("checkRewardPoint",f_checkRewardPoint);
                i.putExtra("extraInfo",extraInfoEditText.getText().toString());


                /*i.putExtra("totalprice",totalPrice.getText().toString());
                i.putExtra("grandtotalprice",grandTotal.getText().toString());
                i.putExtra("addressDetails",adrs);
                i.putExtra("products",single_item);
                i.putExtra("earn_points",earn_points);
                i.putExtra("deliveryAddress",deliveryAddress);
                i.putExtra("deliverySystem",delivery_option_string);*/

                startActivity(i);
            }
        });

        Button proceedBtn = (Button)findViewById(R.id.proceedBtn);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddressModel adrs = new AddressModel();
                adrs.setAddressPresent(false);

                Intent i = new Intent(CartActivity.this,DeliverySummaryActivity.class);
                i.putExtra("products",getProductLists());
                i.putExtra("checkCouponCode",f_checkCouponCode);
                i.putExtra("couponCode",f_couponCode);
                i.putExtra("checkRewardPoint",f_checkRewardPoint);
                i.putExtra("extraInfo",extraInfoEditText.getText().toString());

                startActivity(i);
            }
        });
    }

    private String getProductLists(){
        String Pitem = "";
        if(db.getCartCountByUserID(session.get_customerToken())>0) {
            items = db.getAllCartItems(session.get_customerToken());
            int quantity=0;

            for (int i = 0; i < items.size(); i++) {
                ItemModel itemModel = items.get(i);
                Pitem = Pitem + itemModel.getProdToken() + ":" + itemModel.getCartAddedQuantity() + ",";
                quantity = quantity + itemModel.getCartAddedQuantity();
            }
            cartaddedQuantity.setText(""+quantity);

            Pitem = removeLastChar(Pitem);
        }
        Log.e("Product List ", Pitem);
        return Pitem;
    }

    private void loadCartPage(){
        Log.e("cart item ", String.valueOf(cartItems.size()));
        if(db.getCartCountByUserID(session.get_customerToken())>0){
            //showDialog();
            adapter = new CartListingAdapter(CartActivity.this,cartItems);
            recyclerView.setAdapter(adapter);
            paymentConfirmPageLoad(session.get_customerToken(), getProductLists(), f_checkCouponCode,f_couponCode, f_checkRewardPoint,f_extraInfo);

        }else{
            finish();
        }
    }

    private static String removeLastChar(String str) {
        return str.substring(0, str.length() - 1);
    }

    private void paymentConfirmPageLoad(String custToken, String products, String checkCouponCode,String couponCode, String checkRewardPoint,String extraInfo){
        initVolleyCallback();

//        showDialog();
        cartlayout.setVisibility(View.GONE);
        bottomProceedButton.setVisibility(View.GONE);
        cartProgress.setVisibility(View.VISIBLE);

        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", custToken);
        paramse.put("products", products);
        paramse.put("delivery_mode", "");
        paramse.put("delivery_date", "");
        paramse.put("delivery_time", "");
        paramse.put("delivery_address", "");
        paramse.put("check_coupon_code", checkCouponCode);
        paramse.put("coupon_code", couponCode);
        paramse.put("check_reward_point", checkRewardPoint);
        paramse.put("cod", "no");
        paramse.put("extra_info",extraInfo);

        RequestQueue mRequestQueueg = Volley.newRequestQueue(CartActivity.this);
        mVolleyService = new VolleyService(mResultCallback,CartActivity.this);
        mVolleyService.postStringDataVolley("CALCULATE_CART",CALCULATE_CART,paramse,mRequestQueueg);
    }

    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: "+response);
                if(requestType.equalsIgnoreCase("CALCULATE_CART")){
                    //hideDialog();
                    try {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status_code = jsonObject.getInt("status");
                            if (status_code == 1) {

                                cartProgress.setVisibility(View.GONE);
                                cartlayout.setVisibility(View.VISIBLE);
                                bottomProceedButton.setVisibility(View.VISIBLE);

                                JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
                                if(responseObject.getString("available_points").equals("")){
                                    available_points=0;
                                }else {
                                    available_points = Integer.parseInt(responseObject.getString("available_points"));
                                }

                                totalPrice.setText("₹"+responseObject.getString("sub_total"));
                                if(!responseObject.getString("sub_total").isEmpty() && Double.parseDouble(responseObject.getString("sub_total"))>0) {
                                    fb_totLayout.setVisibility(View.VISIBLE);
                                }

                                discountFee.setText("(-)₹"+responseObject.getString("discount_amount"));
                                if(!responseObject.getString("discount_amount").isEmpty() && Double.parseDouble(responseObject.getString("discount_amount"))>0) {
                                    fb_discountOptionLayout.setVisibility(View.VISIBLE);
                                }

                                /*fb_deliveryFee.setText("₹"+responseObject.getString("delivery_charge"));
                                if(!responseObject.getString("delivery_charge").isEmpty() && Double.parseDouble(responseObject.getString("delivery_charge"))>0) {
                                    fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
                                }

                                */
                                fb_gstValue.setText("₹"+responseObject.getString("gst_value"));
                                if(!responseObject.getString("gst_value").isEmpty() && Double.parseDouble(responseObject.getString("gst_value"))>0) {
                                    fb_gstOptionLayout.setVisibility(View.VISIBLE);
                                }

                                //grandTotal.setText("₹"+responseObject.getString("grand_total"));
                                grandTotal.setText("₹"+responseObject.getString("grand_total"));
                                if(!responseObject.getString("grand_total").isEmpty() && Double.parseDouble(responseObject.getString("grand_total"))>0) {
                                    fb_grandTotalLayout.setVisibility(View.VISIBLE);
                                }

                                redeemedPoint.setText("(-)₹"+responseObject.getString("redeemed_point"));
                                if(!responseObject.getString("redeemed_point").isEmpty() && Double.parseDouble(responseObject.getString("redeemed_point"))>0) {
                                    redemedPointsLayout.setVisibility(View.VISIBLE);
                                }

                                points.setText(responseObject.getString("earn_points")+" Points");
//                                if(!responseObject.getString("earn_points").isEmpty() && Double.parseDouble(responseObject.getString("earn_points"))>0) {
                                    fb_earnPointsLayout.setVisibility(View.VISIBLE);
//                                }

                                payableAmount.setText("₹"+responseObject.getString("payable_amount"));
                                totalPriceButtom.setText("₹" + responseObject.getString("payable_amount"));
                                if(!responseObject.getString("payable_amount").isEmpty() && Double.parseDouble(responseObject.getString("payable_amount"))>0) {
                                    payableAmountLayout.setVisibility(View.VISIBLE);
                                }


                                if(!responseObject.getString("coupon_code").isEmpty()) {
                                    if (responseObject.getInt("coupon_applied")==1) {
                                        couponappliedlayout.setVisibility(View.VISIBLE);
                                        redemedPointsLayout.setVisibility(View.GONE);
                                        couponTextlayout.setVisibility(View.GONE);
                                        couponmsgtext.setTextColor(getResources().getColor(R.color.appThemegreencolor));
                                        couponmsgtext.setText(responseObject.getString("coupon_msg"));
                                        couponlayout.setVisibility(View.VISIBLE);
                                    } else {
                                        couponlayout.setVisibility(View.GONE);
                                        couponappliedlayout.setVisibility(View.VISIBLE);
                                        redemedPointsLayout.setVisibility(View.GONE);
                                        couponTextlayout.setVisibility(View.GONE);
                                        couponmsgtext.setText(responseObject.getString("coupon_msg"));
                                        couponmsgtext.setTextColor(getResources().getColor(R.color.color_error));
                                    }
                                }else {
                                    couponlayout.setVisibility(View.GONE);
                                }

                                /*if(!responseObject.getString("coupon_code").isEmpty()) {
                                    couponappliedlayout.setVisibility(View.VISIBLE);
                                    redemedPointsLayout.setVisibility(View.GONE);
                                    couponTextlayout.setVisibility(View.GONE);
                                }*/
                                //responseObject.getString("coupon_code");
                                //responseObject.getString("payable_amount");
                                cartItems.clear();
                                JSONArray productlistArray = responseObject.getJSONArray("in_stock");
                                for (int i = 0; i < productlistArray.length(); i++) {
                                    JSONObject productEachRow = productlistArray.getJSONObject(i);

                                    ItemModel cartItemsList = items.get(i);

                                    ItemModel m = new ItemModel();
                                    m.setProdToken(productEachRow.getString("prodToken"));
                                    m.setProdName(productEachRow.getString("prodName"));
                                    m.setProdDesc(productEachRow.getString("prodDesc"));
                                   // m.setMarketPrice(productEachRow.getString("marketPrice"));
                                    m.setSellPrice(productEachRow.getString("sellPrice"));
                                    m.setPic(productEachRow.getString("pic"));
                                    m.setCartAddedQuantity(productEachRow.getInt("cartQuan"));
//                                    m.setCartAddedQuantity(productEachRow.getInt("stockQuan"));
                                    m.setStockAddedQuantity(productEachRow.getInt("stockQuan"));
                                    m.setNetWeight(productEachRow.getString("netWeight"));
                                    cartItems.add(m);
                                    Log.d("creation", " => " + productEachRow.getString("prodName"));
                                }
                                adapter.notifyDataSetChanged();
                            }else {
                                finish();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        Log.d("creation", "Total Response POST: "+response);

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
//                    hideDialog();
                    try {
                        cartProgress.setVisibility(View.GONE);
                        cartlayout.setVisibility(View.VISIBLE);
                        JSONObject jsonObject = new JSONObject(response);

                        Log.d("creation", "response =>>> " + response);

                        int status_code = jsonObject.getInt("status");
                        double totalPriceCount = 0.0;
                        if (status_code == 1) {
                            JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
                            JSONArray productlistArray = responseObject.getJSONArray("in_stock");
                            cartItems.clear();
                            for (int i = 0; i < productlistArray.length(); i++) {
                                JSONObject productEachRow = productlistArray.getJSONObject(i);

                                ItemModel cartItemsList = items.get(i);

                                ItemModel m = new ItemModel();
                                m.setProdToken(productEachRow.getString("prodToken"));
                                m.setProdName(productEachRow.getString("prodName"));
                                m.setProdDesc(productEachRow.getString("prodDesc"));
                                m.setMarketPrice(productEachRow.getString("marketPrice"));
                                m.setSellPrice(productEachRow.getString("sellPrice"));
                                m.setPic(productEachRow.getString("pic"));
                                m.setCartAddedQuantity(productEachRow.getInt("cartQuan"));
                                m.setStockAddedQuantity(productEachRow.getInt("stockQuan"));
                                cartItems.add(m);

                            /*totalPriceCount = totalPriceCount + Double.parseDouble(productEachRow.getString("sellPrice"));

                            totalPriceCount = cartItemsList.getCartAddedQuantity() * totalPriceCount;*/

                                Log.d("creation", " => " + productEachRow.getString("prodName"));
                            }
                            //delivery_option_string = responseObject.getString("delivery_option");

                            totalprice = Double.parseDouble(responseObject.getString("cart_subtotal").replaceAll(",", ""));
                            totalPrice.setText(responseObject.getString("cart_subtotal"));
                            points.setText(responseObject.getString("earn_points") + " Points");

                            earn_points = responseObject.getString("earn_points");
                            deliveryAddress = responseObject.getString("default_address");


                            //int grandtotal = responseObject.getInt("cart_subtotal") - responseObject.getInt("earn_points");
                            prevGrandTotal = Double.parseDouble(responseObject.getString("cart_subtotal").replaceAll(",", ""));
                            updatedGrandTotal = prevGrandTotal;
                            grandTotal.setText("" + prevGrandTotal);
                            totalPriceButtom.setText("₹" + prevGrandTotal);

                            adapter.notifyDataSetChanged();
                        } else {
                            finish();
                        }
                    } catch (JSONException e) {
                       Log.e("Exception ", e.getMessage().toString());
                    }
                }

                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                finish();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void refreshView() {
        clear();
        loadCartPage();
    }

    public void clear() {
        items.clear();
        cartItems.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}


