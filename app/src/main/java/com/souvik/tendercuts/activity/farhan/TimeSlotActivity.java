package com.souvik.tendercuts.activity.farhan;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.souvik.tendercuts.activity.DeliverySummaryActivity;
import com.souvik.tendercuts.activity.farhan.api.ApiEndpoints;
import com.souvik.tendercuts.activity.farhan.api.AppUtils;
import com.souvik.tendercuts.activity.farhan.api.AppVolleySingleton;
import com.souvik.tendercuts.activity.farhan.api.VolleyApiRequest;
import com.souvik.tendercuts.activity.farhan.model.Time;
import com.souvik.tendercuts.activity.farhan.model.TimeHolder;
import com.souvik.tendercuts.activity.farhan.model.TimeSlot;
import com.souvik.tendercuts.database.DatabaseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.co.meatpoint.R;
import in.galaxyofandroid.widgets.AdvanceRippleView;

public class TimeSlotActivity extends AppCompatActivity {
    private static final String TAG=TimeSlotActivity.class.getSimpleName();
    public static final String DAY_TYPE="dayType";
    public static final String SELECTED_TIME_SLOT="timeSlot";
    public static final String SELECTED_DATE="date";
    public static final String SELECTED_DATE2="date2";
    public static final int SUCCESS = 100;
    private Context context;
    @BindView(R.id.toolbar)Toolbar toolbar;
    @BindView(R.id.firstDate)TextView firstDate;
    @BindView(R.id.secondDate)TextView secondDate;
    @BindView(R.id.firtRippleView)AdvanceRippleView firtRippleView;
    @BindView(R.id.secondRippleView)AdvanceRippleView secondRippleView;
    @BindView(R.id.vw)View vw;
    private String dayType=null;
    public DatabaseHandler db;

    @BindView(R.id.timeSlots)RecyclerView timeSlots;
    private List<Time> timeList=new ArrayList<Time>();
    ArrayList<TimeHolder> deliveryTime = new ArrayList<TimeHolder>();

    private TimeSlotAdapter adapter;
    private String selectedTime="";
    String slotFirst, slotSecond, date1="",date2="", selecteddate="", selecteddate2="", stdt="", stdt2="";

    int flag=0, selected_pos=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slot);
        ButterKnife.bind(this);
        this.context=this;

        setToolBar();
        setRippleClickListener();
        date1 = firstDate.getText().toString();
    }



    private void setRippleClickListener() {
        firtRippleView.setOnRippleCompleteListener(new AdvanceRippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(AdvanceRippleView advanceRippleView) {
                firstDate.setTextColor(getResources().getColor(R.color.cb_errorRed));
                secondDate.setTextColor(getResources().getColor(R.color.colorGrey));
                flag=0;
                setSlotRecycler();
                date1 = firstDate.getText().toString();
                stdt=selecteddate;
            }
        });

        secondRippleView.setOnRippleCompleteListener(new AdvanceRippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(AdvanceRippleView advanceRippleView) {
                secondDate.setTextColor(getResources().getColor(R.color.cb_errorRed));
                firstDate.setTextColor(getResources().getColor(R.color.colorGrey));
                flag=1;
                setSlotRecycler();
                date1= secondDate.getText().toString();
                stdt=selecteddate2;
            }
        });

    }

    private void setToolBar() {
        setSupportActionBar(toolbar);
         dayType= getIntent().getStringExtra("deliveryType");
         Log.e("Day Type ", dayType);
        if(dayType.equalsIgnoreCase("first")) {
            getSupportActionBar().setTitle("Slots for Delivery");
            firstDate.setTextColor(getResources().getColor(R.color.cb_errorRed));
            secondDate.setTextColor(getResources().getColor(R.color.colorGrey));

            //time slot
            slotFirst = getIntent().getStringExtra("deliverySlot");
            secondRippleView.setVisibility(View.VISIBLE);
            vw.setVisibility(View.VISIBLE);
            firtRippleView.setVisibility(View.VISIBLE);
        }
        else if(dayType.equalsIgnoreCase("second")) {
            getSupportActionBar().setTitle("Slots for Delivery");
            firstDate.setTextColor(getResources().getColor(R.color.cb_errorRed));
            secondDate.setTextColor(getResources().getColor(R.color.colorGrey));
            //time slot
            slotSecond=getIntent().getStringExtra("deliverySlot");
            vw.setVisibility(View.VISIBLE);
            firtRippleView.setVisibility(View.VISIBLE);
            secondRippleView.setVisibility(View.VISIBLE);
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getDate();
        stdt=selecteddate;

    }
    private void getDate() {
        JSONArray timeSlot=null;
        JSONObject object=null;
        String [] dt, dt2;

        if(slotFirst!=null || slotSecond!=null) {
            try {
             //   Log.e("Slot ", slotFirst.length()+" "+slotSecond.length());
                if (dayType.equalsIgnoreCase("first")) {
                    timeSlot = new JSONArray(slotFirst);
                        object = timeSlot.getJSONObject(0);
                        stdt=object.getString("date");
                        dt = object.getString("day").split(" ");
                        firstDate.setText(dt[0]+"\n("+dt[1]+" "+dt[2]+")");
                        if(timeSlot.length()>0) {
                            object = timeSlot.getJSONObject(1);
                            stdt = object.getString("date");
                            dt2 = object.getString("day").split(" ");
                            secondDate.setText(dt2[0] + "\n(" + dt2[1] + " " + dt2[2] + ")");
                        secondRippleView.setVisibility(View.VISIBLE);
                        vw.setVisibility(View.VISIBLE);
                        }else {
                            secondRippleView.setVisibility(View.GONE);
                            vw.setVisibility(View.GONE);
                        }

                } else if (dayType.equalsIgnoreCase("second")) {
                    timeSlot = new JSONArray(slotSecond);
                    object = timeSlot.getJSONObject(0);
                    stdt= object.getString("date");
                    dt = object.getString("day").split(" ");
                    firstDate.setText(dt[0]+"\n("+dt[1]+" "+dt[2]+")");
                    if(timeSlot.length()>0){
                    object = timeSlot.getJSONObject(1);
                    stdt=object.getString("date");
                    dt2=object.getString("day").split(" ");
                    secondDate.setText( dt2[0]+"\n("+dt2[1]+" "+dt2[2]+")");
                        secondRippleView.setVisibility(View.VISIBLE);
                        vw.setVisibility(View.VISIBLE);
                    }else {
                        secondRippleView.setVisibility(View.GONE);
                        vw.setVisibility(View.GONE);
                    }

                }
            } catch (JSONException e) {
                Log.e("Exp ", e.getMessage().toString());
            }
        }
        setSlotRecycler();
    }

    private void setDates(){
//        Calendar calendar=Calendar.getInstance();
//        SimpleDateFormat formater=new SimpleDateFormat("dd MMM");
        Log.e("Date ", date1+" "+date2);
        String firstdate=date1;//formater.format(calendar.getTime());
        firstDate.setText(firstdate);
//        calendar.add(Calendar.DAY_OF_MONTH,1);
        String seconddate=date2;//formater.format(calendar.getTime());
        secondDate.setText(seconddate);
        setSlotRecycler();
    }

    private void setSlotRecycler() {
        timeSlots.setLayoutManager(new GridLayoutManager(context,2));
        adapter=new TimeSlotAdapter(context,deliveryTime);
        adapter.setItemClickListener(new TimeSlotAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                setSlotSelected(position);
            }
        });
        timeSlots.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        getTimeSlots();
    }

    private void setSlotSelected(int position) {
        selected_pos =position;
        for(int i=0;i<deliveryTime.size();i++){
            deliveryTime.get(i).setDefault_selected(0);
        }
       deliveryTime.get(position).setDefault_selected(1);
        adapter.notifyDataSetChanged();
        selectedTime=deliveryTime.get(position).getTime();
        Log.e("Selected time ", String.valueOf(position));
        deliveryTime.get(position).setTime(selectedTime);
       // new TimeHolder().setTime(selectedTime);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void getTimeSlots() {
//        AppUtils.showProgressDialog(context);
//        Response.Listener<String> stringListener = new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AppUtils.hideProgressDialog();
//                try {
//                    JSONObject object = new JSONObject(response);
//                    int code = object.getInt("status");
//                    String msg = object.getString("message");
//                    if(code==1){
//                        TimeSlot timeSlot=VolleyApiRequest.parseTimeSlots(object.getJSONObject("response").getJSONObject("first"));
//                        if(timeSlot!=null && timeSlot.getSlotList().size()>0){
//                            timeList.addAll(timeSlot.getSlotList().get(0).getSlotTimeList());
//                            adapter.notifyDataSetChanged();
//                        }
//                    }
//                } catch (Exception e) {
//
//                }
//            }
//        };
//        Response.ErrorListener errorListener = new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Time Slot  Error " + error.getMessage());
//                AppUtils.hideProgressDialog();
//            }
//        };
//
//        Map<String, String> bodyParams = new HashMap<String,String>();
//        bodyParams.put("cusToken", "c4ca4238a0b923820dcc509a6f75849b");
//        bodyParams.put("products", "2:1,7:1,3:2");
//        VolleyApiRequest request = new VolleyApiRequest(ApiEndpoints.GET_TIME_SLOTS, bodyParams, stringListener, errorListener);
//        AppVolleySingleton.getInstance(context).addToRequestQueue(request, TAG);
        JSONArray timeSlot=null;
        JSONObject object=null;
        if(slotFirst!=null || slotSecond!=null){
            try {
                if(dayType.equalsIgnoreCase("first")) {
                    timeSlot = new JSONArray(slotFirst);
                    if(flag==0) {
                        object = timeSlot.getJSONObject(0);

//                        firstDate.setText("TODAY\n"+object.getString("date"));
                    }else {
                        object = timeSlot.getJSONObject(1);
//                        secondDate.setText("TOMORROW\n"+object.getString("date"));
                    }
                }
                else if(dayType.equalsIgnoreCase("second")) {
                    timeSlot = new JSONArray(slotSecond);
                    if(flag==0) {
                        object = timeSlot.getJSONObject(0);
//                        firstDate.setText("TOMORROW\n"+object.getString("date"));
                    }else {
                        object = timeSlot.getJSONObject(1);
//                        secondDate.setText(object.getString("date"));
                    }
                }

                //for slot
//                "slot_id":1,
//                 "available":1,
//                 "time":"Delivered in 90 min",
//                 "default_selected":0
//                for(int i =0;i<timeSlot.length();i++){
                    JSONArray aryTime = object.getJSONArray("time");
                    Log.e("Time ", aryTime.toString());
                    deliveryTime.clear();
                    for(int a =0;a<aryTime.length();a++) {
                        JSONObject object1 = aryTime.getJSONObject(a);
                        if(object1.getString("available").equalsIgnoreCase("1")) {
                            TimeHolder th = new TimeHolder();
                            th.setSlot_id(object1.getString("slot_id"));
                            th.setTime(object1.getString("time"));
                            th.setAvailable(Integer.parseInt(object1.getString("available")));
                            th.setDefault_selected(object1.getInt("default_selected"));

                            deliveryTime.add(th);
                        }
                    }

//                }

            } catch (JSONException e) {
                Log.e("JSON Exp ", e.getMessage());
            }
        }
    }

    @OnClick(R.id.done)public void done(){
        if(selectedTime.length()>0){
            Intent intent=new Intent();
            intent.putExtra(SELECTED_TIME_SLOT,selectedTime);
            intent.putExtra(SELECTED_DATE,date1);
            intent.putExtra(SELECTED_DATE2,stdt);
            setResult(SUCCESS,intent);
            finish();
        }else {
            Toast.makeText(context, "Kindly select any time slot.", Toast.LENGTH_SHORT).show();
        }
    }

}
