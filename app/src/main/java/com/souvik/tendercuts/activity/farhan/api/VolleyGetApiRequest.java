package com.souvik.tendercuts.activity.farhan.api;

import android.util.Log;

import com.android.volley.Response;

/**
 * Created by Farhan Raja on 18/12/2017.
 */

public class VolleyGetApiRequest extends BaseStringRequest {

    public VolleyGetApiRequest(String Url,Response.Listener<String> listener,
                               Response.ErrorListener errorListener) {
        super(Method.GET, Url, listener, errorListener);
        Log.e("API: ",Url);
    }

}
