package com.souvik.tendercuts.activity.farhan.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Md Farhan Raja on 25-Feb-18.
 */

public class TimeSlot implements Parcelable{
    @SerializedName("slot")private List<Slot> slotList;

    protected TimeSlot(Parcel in) {
        slotList = in.createTypedArrayList(Slot.CREATOR);
    }

    public static final Creator<TimeSlot> CREATOR = new Creator<TimeSlot>() {
        @Override
        public TimeSlot createFromParcel(Parcel in) {
            return new TimeSlot(in);
        }

        @Override
        public TimeSlot[] newArray(int size) {
            return new TimeSlot[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(slotList);
    }

    public List<Slot> getSlotList() {
        return slotList;
    }
}
