package com.souvik.tendercuts.activity.farhan;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.souvik.tendercuts.activity.ItemListingActivity;
import com.souvik.tendercuts.activity.farhan.model.CategoryHolder;
import com.souvik.tendercuts.activity.farhan.model.FragmentChild;
import com.souvik.tendercuts.fragment.ItemListFragment;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Sushil on 3/11/2018.
 */

class ViewPagerAdapter extends FragmentStatePagerAdapter {
//    ArrayList<CategoryHolder> list;
//    Fragment fragment = null;
    private final List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private final ArrayList<String> mFragmentTitleList = new ArrayList<String>();


    public ViewPagerAdapter(FragmentManager fragmentManager){//}, ArrayList<CategoryHolder> list) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {

//        for (int i = 0; i < list.size(); i++) {
//
//            if (i == position) {
//                Log.e("Item ", "Name "+list.get(i).getCatName()+" id "+list.get(i).getCatToken()+" Position "+position);
//                fragment = new ItemListFragment(list.get(i).getCatToken());//.newInstance("Chicken");
//                break;
//            }
//        }
        return mFragmentList.get(position);
    }
    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public int getCount() {
        return mFragmentTitleList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
