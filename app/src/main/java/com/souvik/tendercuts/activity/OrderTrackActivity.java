package com.souvik.tendercuts.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.adapter.OrderItemListingAdapter;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.OrderHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.GET_ORDER_DETAILS;
import static com.souvik.tendercuts.config.AppConfig.ORDER_TRACK;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class OrderTrackActivity extends AppCompatActivity {

    SessionManager session;
    public DatabaseHandler db;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    TextView orderidtextview,customerstatus,enquirymsg;
    TextView trackstep1,trackstep1desc,trackstep1date,trackstep2,trackstep2desc,trackstep2date,trackstep3,trackstep3desc,trackstep3date,trackstep4,trackstep4desc,trackstep4date;
    ScrollView orderTrackScroll;
    ProgressBar orderTrackProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_track);

        setFontAwesome((LinearLayout)findViewById(R.id.ordertracklayout),OrderTrackActivity.this);

        session = SessionManager.getInstance(getApplicationContext());
        db = new DatabaseHandler(OrderTrackActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView)findViewById(R.id.titleToolbar);
        titleToolbar.setText("Order Tracking");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button)findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView)findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(OrderTrackActivity.this,OrderDetailsActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(i);
                finish();
            }
        });

        initVolleyCallback();
        /*Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", session.get_customerToken());
        paramse.put("ordToken", getIntent().getStringExtra("orderToken"));
        RequestQueue mRequestQueueg = Volley.newRequestQueue(OrderTrackActivity.this);
        mVolleyService = new VolleyService(mResultCallback,OrderTrackActivity.this);
        mVolleyService.postStringDataVolley("OrderDetails",ORDER_TRACK,paramse,mRequestQueueg);
*/
        // Declaration
        orderidtextview = (TextView)findViewById(R.id.orderidtextview);
        customerstatus  = (TextView)findViewById(R.id.customerstatus);
        enquirymsg = (TextView)findViewById(R.id.enquirymsg);
        trackstep1 = (TextView)findViewById(R.id.trackstep1);
        trackstep1desc = (TextView)findViewById(R.id.trackstep1desc);
        trackstep1date = (TextView)findViewById(R.id.trackstep1date);
        trackstep2 = (TextView)findViewById(R.id.trackstep2);
        trackstep2desc = (TextView)findViewById(R.id.trackstep2desc);
        trackstep2date = (TextView)findViewById(R.id.trackstep2date);
        trackstep3 = (TextView)findViewById(R.id.trackstep3);
        trackstep3desc = (TextView)findViewById(R.id.trackstep3desc);
        trackstep3date = (TextView)findViewById(R.id.trackstep3date);
        trackstep4 = (TextView)findViewById(R.id.trackstep4);
        trackstep4desc = (TextView)findViewById(R.id.trackstep4desc);
        trackstep4date = (TextView)findViewById(R.id.trackstep4date);

        orderTrackScroll = (ScrollView)findViewById(R.id.orderTrackScroll);
        orderTrackProgress = (ProgressBar)findViewById(R.id.orderTrackProgress);

        if(db.getCartCountByUserID(session.get_customerToken())>0){
           // db.deleteCartUsingUserId(session.get_customerToken());
        }

        WebView ordertrackwebview = (WebView)findViewById(R.id.ordertrackwebview);
        Log.e("url ", getIntent().getStringExtra("url"));

        WebViewClientImpl webViewClient = new WebViewClientImpl(OrderTrackActivity.this);
        ordertrackwebview.setWebViewClient(webViewClient);

        ordertrackwebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        String weburl = getIntent().getStringExtra("url");
        ordertrackwebview.loadUrl(weburl);

    }


    void initVolleyCallback(){
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.e("response ", "Total Response POST: "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if(status_code==1) {
                        orderTrackScroll.setVisibility(View.VISIBLE);
                        orderTrackProgress.setVisibility(View.GONE);
                        //JSONObject responseArray=new JSONObject(jsonObject.getString("response"));

                        JSONObject responseObject = jsonObject.getJSONObject("response");

                        Log.d("response ",response.toString());

                        orderidtextview.setText("Id: "+responseObject.getString("order_id"));
                        customerstatus.setText("Customer Status :"+responseObject.getString("current_status"));
                        enquirymsg.setText(responseObject.getString("enquiry_msg"));

                        JSONArray trackingarr = responseObject.getJSONArray("tracking");
                        for(int i=0;i<trackingarr.length();i++) {
                            JSONObject trackingEachRow = trackingarr.getJSONObject(i);

                            if(trackingEachRow.getString("statuStep").equalsIgnoreCase("track_step_1")){

                                trackstep1.setText(trackingEachRow.getString("statusName"));
                                trackstep1desc.setText(trackingEachRow.getString("statusMsg"));
                                trackstep1date.setText(trackingEachRow.getString("statusDate"));

                            }else if(trackingEachRow.getString("statuStep").equalsIgnoreCase("track_step_2")){
                                trackstep2.setText(trackingEachRow.getString("statusName"));
                                trackstep2desc.setText(trackingEachRow.getString("statusMsg"));
                                trackstep2date.setText(trackingEachRow.getString("statusDate"));
                            }else if(trackingEachRow.getString("statuStep").equalsIgnoreCase("track_step_3")){
                                trackstep3.setText(trackingEachRow.getString("statusName"));
                                trackstep3desc.setText(trackingEachRow.getString("statusMsg"));
                                trackstep3date.setText(trackingEachRow.getString("statusDate"));
                            }else if(trackingEachRow.getString("statuStep").equalsIgnoreCase("track_step_4")){
                                trackstep4.setText(trackingEachRow.getString("statusName"));
                                trackstep4desc.setText(trackingEachRow.getString("statusMsg"));
                                trackstep4date.setText(trackingEachRow.getString("statusDate"));
                            }else{

                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //CART_LISTING
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }
//    @Override
    public void onBackPressed() {
        if(getIntent().getIntExtra("flag",0)==1) {
            Intent i = new Intent(OrderTrackActivity.this, HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
        else {
            finish();
        }
    }

    private class WebViewClientImpl extends WebViewClient {

        private Activity activity = null;

        public WebViewClientImpl(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if(url.indexOf("journaldev.com") > -1 ) return false;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
            return true;
        }
    }


//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
//    }
}
