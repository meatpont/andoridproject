package com.souvik.tendercuts.activity.farhan.model;

import java.util.ArrayList;

/**
 * Created by Sushil on 3/9/2018.
 */

public class TimeHolder {
//    "slot_id":1,
//            "available":1,
//            "time":"Delivered in 90 min",
//            "default_selected":0
   String time, date, slot_id;
   int available, default_selected ;
     String catToken;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public  String getCatToken() {
        return catToken;
    }

    public  void setCatToken(String catToken) {
        this.catToken = catToken;
    }

    public String getSlot_id() {
        return slot_id;
    }

    public void setSlot_id(String slot_id) {
        this.slot_id = slot_id;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getDefault_selected() {
        return default_selected;
    }

    public void setDefault_selected(int default_selected) {
        this.default_selected = default_selected;
    }
}
