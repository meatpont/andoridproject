package com.souvik.tendercuts.activity;

import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.common.AlertDialogRadio.AlertPositiveListener;
import com.souvik.tendercuts.common.AlertDialogRadio;
import com.souvik.tendercuts.config.AppConfig;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.BULKORDER_INFO;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class BulkOrderActivity extends AppCompatActivity implements AlertPositiveListener {

    JSONArray categoryContent;
    JSONObject categoryEachRow;

    ArrayList<String> itemCatid = new ArrayList<String>();
    ArrayList<String> itemCatQuantity = new ArrayList<String>();
    ArrayList<String> itemCatText = new ArrayList<String>();

    private Button bulkOrderSubmit, delivery_date, delivery_time;
    private EditText fullname, email, phone, deliveryaddress, extra_info;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;
    RequestQueue requestQueue;
    LinearLayout ll, noteLayout;
    LinearLayout.LayoutParams lp;

    private ProgressDialog pDialog;

    private int mYear, mMonth, mDay, mHour, mMinute;
    int position = 0, deiveryTime = 0;

    String[] code;
    ArrayList<String> cfe;

    Boolean dateset = false;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk_order);

        setFontAwesome((LinearLayout) findViewById(R.id.bulk_order_linerlayout), BulkOrderActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleToolbar = (TextView) findViewById(R.id.titleToolbar);
        titleToolbar.setText("Bulk Order");
        titleToolbar.setVisibility(View.VISIBLE);

        Button cartBtn = (Button) findViewById(R.id.cartBtn);
        cartBtn.setVisibility(View.GONE);

        TextView buttonback = (TextView) findViewById(R.id.Buttonback);
        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnHomeActivity();
            }
        });
        pDialog = new ProgressDialog(BulkOrderActivity.this);

        ll = (LinearLayout) findViewById(R.id.buttonlayout);
        lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        initVolleyCallback();
        mVolleyService = new VolleyService(mResultCallback, BulkOrderActivity.this);
        requestQueue = Volley.newRequestQueue(BulkOrderActivity.this);

        noteLayout = (LinearLayout) findViewById(R.id.noteLayout);
        //code = new String[]{};

        /*Intent customerResponse = getIntent();
        try {
            categoryContent = new JSONArray(customerResponse.getStringExtra("catDetails"));
            for (int r=0;r<categoryContent.length();r++) {
                categoryEachRow = categoryContent.getJSONObject(r);
                if(categoryEachRow.getInt("catToken")!=999 && categoryEachRow.getInt("catToken")!= 888 && (categoryEachRow.getInt("catToken")== 1  || categoryEachRow.getInt("catToken")== 2  || categoryEachRow.getInt("catToken")== 3)) {
                    CheckBox mychkbox = new CheckBox(BulkOrderActivity.this);
                    mychkbox.setText(categoryEachRow.getString("catName"));
                    mychkbox.setId(categoryEachRow.getInt("catToken"));
                    ll.addView(mychkbox, lp);

                    mychkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(final CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                LayoutInflater li = LayoutInflater.from(BulkOrderActivity.this);
                                View confirmDialog = li.inflate(R.layout.dialog_item_quantity, null);
                                AppCompatButton buttonUpdate = (AppCompatButton) confirmDialog.findViewById(R.id.buttonUpdate);
                                final EditText quantityItem = (EditText) confirmDialog.findViewById(R.id.quantityItem);
                                AlertDialog.Builder alert = new AlertDialog.Builder(BulkOrderActivity.this);
                                alert.setView(confirmDialog);
                                final AlertDialog alertDialog = alert.create();
                                alertDialog.show();
                                buttonUpdate.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (quantityItem.getText().length() > 0) {
                                            alertDialog.dismiss();
                                            //Log.d("creation","Button View => "+buttonView.getId());
                                            //Log.d("creation","quantityItem View => "+quantityItem.getText().toString());
                                            itemCatid.add("" + buttonView.getId());
                                            itemCatText.add("" + buttonView.getText());
                                            itemCatQuantity.add(quantityItem.getText().toString());
                                            buttonView.setText(buttonView.getText() + " (" + quantityItem.getText().toString() + "kg) ");

                                        }

                                    }
                                });
                            } else {
                                int cartIndx = getCategoryPos("" + buttonView.getId(), itemCatid);
                                itemCatid.remove(cartIndx);
                                itemCatQuantity.remove(cartIndx);

                                buttonView.setText(itemCatText.get(cartIndx));
                                itemCatText.remove(cartIndx);


                                //Log.d("creation","Position=> "+getCategoryPos(""+buttonView.getId(),itemCatid));
                            }
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        fullname = (EditText) findViewById(R.id.fullname);
        email = (EditText) findViewById(R.id.email);
        phone = (EditText) findViewById(R.id.phone);
        deliveryaddress = (EditText) findViewById(R.id.deliveryaddress);
        delivery_date = (Button) findViewById(R.id.delivery_date);
        delivery_time = (Button) findViewById(R.id.delivery_time);
        extra_info = (EditText) findViewById(R.id.extra_info);


        cfe = new ArrayList<String>();

        delivery_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(BulkOrderActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                dateset = true;
                                //chdate = year+"-"+(monthOfYear + 1)+"-"+dayOfMonth;
                                delivery_date.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                delivery_date.setTextColor(getResources().getColor(R.color.payumoney_black));
                                //pm_deliveryDate = year+"-"+(monthOfYear + 1)+"-"+dayOfMonth;
                                /*
                                payment_confirm_details.put("deliveryDate",dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                chooseDeliveryDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                //payment_confirm_modelobj.setDeliveryDateAdd(true);

                                /*//****************************************************************************************
                                 pm_deliveryDate = chdate;
                                 /*//*****************************************************************************************/

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            }
        });
        code = new String[]{"09:00am To 11:00am", "03:00pm To 05:00pm"};

        delivery_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Getting the fragment manager */
                FragmentManager manager = getFragmentManager();

                /** Instantiating the DialogFragment class */
                AlertDialogRadio alert = new AlertDialogRadio(code);

//                AlertDialogRadio alert = new AlertDialogRadio(cfe);

                /** Creating a bundle object to store the selected item's index */
                Bundle b = new Bundle();

                /** Storing the selected item's index in the bundle object */
                b.putInt("position", position);

                /** Setting the bundle object to the dialog fragment object */
                alert.setArguments(b);

                /** Creating the dialog fragment object, which will in turn open the alert dialog window */
                alert.show(manager, "alert_dialog_radio");
            }
        });


        bulkOrderSubmit = (Button) findViewById(R.id.bulkOrderSubmit);
        bulkOrderSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fullname.length() > 0) {
                    if (email.length() > 0 && isValidEmail(email.getText().toString())) {
                        if(phone.length()>0 && phone.length()==10) {
                            if(deliveryaddress.length()>0) {
                                if(dateset) {
                                    if (deiveryTime > 0) {

                                        showDialog();

                                        String f = "";
                                        for (int i = 0; i < itemCatid.size(); i++) {
                                            f = f + (itemCatid.get(i) + ":" + itemCatQuantity.get(i) + "kg");
                                            if (i != (itemCatid.size() - 1)) {
                                                f = f + ",";
                                            }
                                        }
                                        HashMap<String, String> regDataPost = new HashMap<String, String>();
                                        regDataPost.put("full_name", fullname.getText().toString());
                                        regDataPost.put("email", email.getText().toString());
                                        regDataPost.put("phone", phone.getText().toString());
                                        regDataPost.put("delivery_address", deliveryaddress.getText().toString());
                                        regDataPost.put("delivery_date", delivery_date.getText().toString());
                                        regDataPost.put("delivery_time", "" + deiveryTime);
                                        regDataPost.put("extra_info", extra_info.getText().toString());
                                        regDataPost.put("items", f);
                                        mVolleyService.postStringDataVolley("ADD_MASSORDER", AppConfig.ADD_MASSORDER, regDataPost, requestQueue);
                                    } else {
                                        Toast.makeText(BulkOrderActivity.this, "Select delivery time", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else {
                                    Toast.makeText(BulkOrderActivity.this, "Select date", Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(BulkOrderActivity.this, "Enter delivery address", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(BulkOrderActivity.this, "Enter mobile number", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(BulkOrderActivity.this, "Enter email", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(BulkOrderActivity.this, "Enter name", Toast.LENGTH_SHORT).show();

                }


                //Log.d("creation","itemCatid => "+f);
//            }else
//
//            {
//                Toast.makeText(BulkOrderActivity.this, "Please fill all the fields", Toast.LENGTH_LONG).show();
//            }
        }
    });

    showDialog();

    HashMap<String, String> bulk = new HashMap<String, String>();
        mVolleyService.postStringDataVolley("BULK_ORDER_FORM",BULKORDER_INFO,bulk,requestQueue);


}

    private int getCategoryPos(String category, ArrayList<String> _categoryArry) {
        return _categoryArry.indexOf(category);
    }


    void initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                //Log.d("creation", "Total Response : "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    hideDialog();
                    if (status == 1) {
                        if (requestType == "BULK_ORDER_FORM") {

                            JSONObject detailsObject = jsonObject.getJSONObject("response");

                            JSONArray categoriesArray = detailsObject.getJSONArray("categories");
                            for (int i = 0; i < categoriesArray.length(); i++) {
                                JSONObject catEachRow = categoriesArray.getJSONObject(i);
                                Log.d("creation", catEachRow.getString("catName"));


                                CheckBox mychkbox = new CheckBox(BulkOrderActivity.this);
                                mychkbox.setText(catEachRow.getString("catName"));
                                mychkbox.setId(catEachRow.getInt("catToken"));
                                ll.addView(mychkbox, lp);

                                mychkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(final CompoundButton buttonView, boolean isChecked) {
                                        if (isChecked) {
                                            LayoutInflater li = LayoutInflater.from(BulkOrderActivity.this);
                                            View confirmDialog = li.inflate(R.layout.dialog_item_quantity, null);
                                            AppCompatButton buttonUpdate = (AppCompatButton) confirmDialog.findViewById(R.id.buttonUpdate);
                                            final EditText quantityItem = (EditText) confirmDialog.findViewById(R.id.quantityItem);
                                            AlertDialog.Builder alert = new AlertDialog.Builder(BulkOrderActivity.this);
                                            alert.setView(confirmDialog);
                                            final AlertDialog alertDialog = alert.create();
                                            alertDialog.show();
                                            alertDialog.setCancelable(true);
                                            buttonUpdate.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (quantityItem.getText().length() > 0) {
                                                        alertDialog.dismiss();
                                                        //Log.d("creation","Button View => "+buttonView.getId());
                                                        //Log.d("creation","quantityItem View => "+quantityItem.getText().toString());
                                                        itemCatid.add("" + buttonView.getId());
                                                        itemCatText.add("" + buttonView.getText());
                                                        itemCatQuantity.add(quantityItem.getText().toString());
                                                        buttonView.setText(buttonView.getText() + " (" + quantityItem.getText().toString() + "kg) ");

                                                    }

                                                }
                                            });
                                        } else {
                                            //int cartIndx = getCategoryPos("" + buttonView.getId(), itemCatid);
                                            //Toast.makeText(BulkOrderActivity.this, ""+cartIndx, Toast.LENGTH_SHORT).show();
//                                            itemCatid.remove(cartIndx);
//                                            itemCatQuantity.remove(cartIndx);

//                                            buttonView.setText(itemCatText.get(cartIndx));
//                                            itemCatText.remove(cartIndx);


                                            //Log.d("creation","Position=> "+getCategoryPos(""+buttonView.getId(),itemCatid));
                                        }
                                    }
                                });
                            }

                            JSONArray notesArray = detailsObject.getJSONArray("notes");
                            for (int i = 0; i < notesArray.length(); i++) {
                                TextView t = new TextView(BulkOrderActivity.this);
                                t.setTextSize(12);
                                t.setText("\u2022  " + notesArray.getString(i));
                                t.setPadding(0, 5, 0, 5);
                                t.setTextColor(Color.parseColor("#424242"));
                                noteLayout.addView(t);
                            }

                            int t = detailsObject.getInt("total_slot");
                            JSONObject tslot = detailsObject.getJSONObject("time_slot");

                            int k = t;
                            for (int j = 0; j < t; j++) {
                                Log.d("creation", tslot.getString("" + k));
//                                cfe.add(tslot.getString(""+k));
//                                code[j] = tslot.getString(""+k);
//                                k--;
                            }

                            /*JSONArray notesArray = detailsObject.getJSONArray("notes");
                            for (int i=0;i<notesArray.length();i++) {
                                TextView t = new TextView(BulkOrderActivity.this);
                                t.setText("\u2022  "+ notesArray.getString(i));
                                t.setPadding(0,5,0,5);
                                t.setTextColor(Color.parseColor("#424242"));
                                noteLayout.addView(t);
                            }*/

                           /* categories
                                    time_slot
                            notes
                           */

                        } else {
                            MeatPointUtils.decisionAlertOnMainThread(BulkOrderActivity.this,
                                    android.R.drawable.ic_dialog_info,
                                    R.string.blank_msg,
                                    "Your query is successfully submitted.",
                                    "Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Toast.makeText(SplashActivity.this,"Yes Clicked", Toast.LENGTH_SHORT ).show();
                                            dialog.dismiss();
                                            returnHomeActivity();
                                        }
                                    }, "", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Toast.makeText(SplashActivity.this, "NO Clicked", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                            );
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                hideDialog();
                Log.d("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        returnHomeActivity();
    }

    public void returnHomeActivity() {
        Intent i = new Intent(BulkOrderActivity.this, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    @Override
    public void onPositiveClick(int position) {
        this.position = position;
        deiveryTime = position + 1;
        /** Setting the selected android version in the textview */
        delivery_time.setText(code[this.position]);
        delivery_time.setTextColor(getResources().getColor(R.color.payumoney_black));
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

}
