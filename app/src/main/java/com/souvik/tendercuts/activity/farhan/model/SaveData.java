package com.souvik.tendercuts.activity.farhan.model;

/**
 * Created by Sushil on 6/24/2018.
 */

public class SaveData {
    static String slot1, slot2, date1, date2;
    static String dcharge1, dcharge2;

    public static String getDate1() {
        return date1;
    }

    public static void setDate1(String date1) {
        SaveData.date1 = date1;
    }

    public static String getDate2() {
        return date2;
    }

    public static void setDate2(String date2) {
        SaveData.date2 = date2;
    }

    public static String getSlot1() {
        return slot1;
    }

    public static void setSlot1(String slot1) {
        SaveData.slot1 = slot1;
    }

    public static String getSlot2() {
        return slot2;
    }

    public static void setSlot2(String slot2) {
        SaveData.slot2 = slot2;
    }

    public static String getDcharge1() {
        return dcharge1;
    }

    public static void setDcharge1(String dcharge1) {
        SaveData.dcharge1 = dcharge1;
    }

    public static String getDcharge2() {
        return dcharge2;
    }

    public static void setDcharge2(String dcharge2) {
        SaveData.dcharge2 = dcharge2;
    }
}
