package com.souvik.tendercuts.model;

/**
 * Created on 25/09/17.
 */
public class GetTimeModel implements java.io.Serializable{

    private String timeSlot;
    private int timeSlotToken;
    private Boolean timeSlotPresent;

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public int getTimeSlotToken() {
        return timeSlotToken;
    }

    public void setTimeSlotToken(int timeSlotToken) {
        this.timeSlotToken = timeSlotToken;
    }

    public Boolean getTimeSlotPresent() {
        return timeSlotPresent;
    }

    public void setTimeSlotPresent(Boolean timeSlotPresent) {
        this.timeSlotPresent = timeSlotPresent;
    }
}
