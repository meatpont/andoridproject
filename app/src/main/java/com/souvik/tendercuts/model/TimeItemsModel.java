package com.souvik.tendercuts.model;

/**
 * Created by Souvik on 4/3/2017.
 */

public class TimeItemsModel {
    private String image;
    private String title;

    public TimeItemsModel(){

    }
    public TimeItemsModel(String image, String title) {
        super();
        this.image = image;
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}