package com.souvik.tendercuts.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class RewardHistoryModel implements java.io.Serializable{

    private String pointType,points,pointdate;

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPointdate() {
        return pointdate;
    }

    public void setPointdate(String pointdate) {
        this.pointdate = pointdate;
    }
}
