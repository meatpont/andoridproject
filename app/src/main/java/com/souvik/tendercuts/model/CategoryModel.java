package com.souvik.tendercuts.model;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Lincoln on 18/05/16.
 */
public class CategoryModel implements Serializable{
    private String name;
    private int catToken;
    private String thumbnail,is_bulk;
    JSONArray categoryContent;


    public CategoryModel(String name, int catToken, String thumbnail,JSONArray categoryContnt,String is_bulk) {
        this.name = name;
        this.catToken = catToken;
        this.thumbnail = thumbnail;
        this.categoryContent = categoryContnt;
        this.is_bulk = is_bulk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCatToken() {
        return catToken;
    }

    public void setCatToken(int catToken) {
        this.catToken = catToken;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public JSONArray getCategoryContent() {
        return categoryContent;
    }

    public void setCategoryContent(JSONArray categoryContent) {
        this.categoryContent = categoryContent;
    }

    public String getIs_bulk() {
        return is_bulk;
    }

    public void setIs_bulk(String is_bulk) {
        this.is_bulk = is_bulk;
    }
}
