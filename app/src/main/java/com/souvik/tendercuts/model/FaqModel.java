package com.souvik.tendercuts.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class FaqModel implements java.io.Serializable{

    private String question,answer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
