package com.souvik.tendercuts.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class ItemModel implements java.io.Serializable{

    private String prodToken,prodName,prodDesc, marketPrice, sellPrice,pic,userId,netWeight, tomorrowDelivery, isNew, deal_price, outStock;
    private boolean isAddedCart;
    private int index,cartAddedQuantity,cartID,stockAddedQuantity;

    public ItemModel(String prodToken, int cartAddedQuantity, String userId) {
        this.prodToken = prodToken;
        this.cartAddedQuantity = cartAddedQuantity;
        this.userId = userId;
    }

    public ItemModel(String prodToken, String userId) {
        this.prodToken = prodToken;
        this.userId = userId;
    }

    public ItemModel(int cartAddedQuantity,int cartID) {
        this.cartAddedQuantity = cartAddedQuantity;
        this.cartID = cartID;
    }

    public ItemModel() {
    }

    public int getCartID() {
        return cartID;
    }

    public void setCartID(int cartID) {
        this.cartID = cartID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getProdToken() {
        return prodToken;
    }

    public void setProdToken(String prodToken) {
        this.prodToken = prodToken;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(String sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getCartAddedQuantity() {
        return cartAddedQuantity;
    }

    public void setCartAddedQuantity(int cartAddedQuantity) {
        this.cartAddedQuantity = cartAddedQuantity;
    }

    public int getStockAddedQuantity() {
        return stockAddedQuantity;
    }

    public void setStockAddedQuantity(int stockAddedQuantity) {
        this.stockAddedQuantity = stockAddedQuantity;
    }

    public boolean isAddedCart() {
        return isAddedCart;
    }

    public void setAddedCart(boolean addedCart) {
        isAddedCart = addedCart;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getTomorrowDelivery() {
        return tomorrowDelivery;
    }

    public void setTomorrowDelivery(String tomorrowDelivery) {
        this.tomorrowDelivery = tomorrowDelivery;
    }

    public String getIsNew() {
        return isNew;
    }

    public void setIsNew(String isNew) {
        this.isNew = isNew;
    }

    public String getDeal_price() {
        return deal_price;
    }

    public void setDeal_price(String deal_price) {
        this.deal_price = deal_price;
    }

    public String getOutStock() {
        return outStock;
    }

    public void setOutStock(String outStock) {
        this.outStock = outStock;
    }
}
