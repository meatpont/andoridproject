package com.souvik.tendercuts.model;

import android.util.Log;

import com.souvik.tendercuts.common.CalculateCart;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created on 25/09/17.
 */
public class CalculateCartModel implements java.io.Serializable{

    int status;
    JSONObject response;
    String message, first_delivery_date,
            first_delivery_time,second_delivery_date,
            second_delivery_time, available_points,
            sub_total,extra_info,coupon_code,
            discount_amount,delivery_charge,gst_value,grand_total,
            redeemed_point,payable_amount,earn_points, coupon_msg;
    int coupon_applied;


    String first, second;
    JSONArray in_stock, out_stock,free_product,delivery_option,payment_mode;
    String default_address;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public JSONObject getResponse() {
        return response;
    }

    public void setResponse(JSONObject response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFirst_delivery_date() {
        return first_delivery_date;
    }

    public void setFirst_delivery_date(String first_delivery_date) {
        this.first_delivery_date = first_delivery_date;
    }

    public String getFirst_delivery_time() {
        return first_delivery_time;
    }

    public void setFirst_delivery_time(String first_delivery_time) {
        this.first_delivery_time = first_delivery_time;
    }

    public String getSecond_delivery_date() {
        return second_delivery_date;
    }

    public void setSecond_delivery_date(String second_delivery_date) {
        this.second_delivery_date = second_delivery_date;
    }

    public String getSecond_delivery_time() {
        return second_delivery_time;
    }

    public void setSecond_delivery_time(String second_delivery_time) {
        this.second_delivery_time = second_delivery_time;
    }

    public String getAvailable_points() {
        return available_points;
    }

    public void setAvailable_points(String available_points) {
        this.available_points = available_points;
    }

    public String getSub_total() {
        return sub_total;
    }

    public void setSub_total(String sub_total) {
        this.sub_total = sub_total;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public void setExtra_info(String extra_info) {
        this.extra_info = extra_info;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public int getCoupon_applied() {
        return coupon_applied;
    }

    public void setCoupon_applied(int coupon_applied) {
        this.coupon_applied = coupon_applied;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }

    public String getGst_value() {
        return gst_value;
    }

    public void setGst_value(String gst_value) {
        this.gst_value = gst_value;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public String getRedeemed_point() {
        return redeemed_point;
    }

    public void setRedeemed_point(String redeemed_point) {
        this.redeemed_point = redeemed_point;
    }

    public String getPayable_amount() {
        return payable_amount;
    }

    public void setPayable_amount(String payable_amount) {
        this.payable_amount = payable_amount;
    }

    public String getEarn_points() {
        return earn_points;
    }

    public void setEarn_points(String earn_points) {
        this.earn_points = earn_points;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public JSONArray getIn_stock() {
        return in_stock;
    }

    public void setIn_stock(JSONArray in_stock) {
        this.in_stock = in_stock;
    }

    public JSONArray getOut_stock() {
        return out_stock;
    }

    public void setOut_stock(JSONArray out_stock) {
        this.out_stock = out_stock;
    }

    public JSONArray getFree_product() {
        return free_product;
    }

    public void setFree_product(JSONArray free_product) {
        this.free_product = free_product;
    }

    public JSONArray getDelivery_option() {
        return delivery_option;
    }

    public void setDelivery_option(JSONArray delivery_option) {
        this.delivery_option = delivery_option;
    }

    public JSONArray getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(JSONArray payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getDefault_address() {
        return default_address;
    }

    public void setDefault_address(String default_address) {
        this.default_address = default_address;
    }

    public String getCoupon_msg() {
        return coupon_msg;
    }

    public void setCoupon_msg(String coupon_msg) {
        this.coupon_msg = coupon_msg;
    }

    public static CalculateCartModel setCalculateCart(JSONObject jsonObject) throws Exception {

        CalculateCartModel c = new CalculateCartModel();

        c.setStatus(jsonObject.getInt("status"));
        c.setMessage(jsonObject.getString("message"));

        JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
        c.setResponse(responseObject);

        c.setFirst_delivery_date(responseObject.getString("first_delivery_date"));
        c.setFirst_delivery_time(responseObject.getString("first_delivery_time"));
        c.setSecond_delivery_date(responseObject.getString("second_delivery_date"));
        c.setSecond_delivery_time(responseObject.getString("second_delivery_time"));
        c.setAvailable_points(responseObject.getString("available_points"));
        c.setSub_total(responseObject.getString("sub_total"));
        c.setExtra_info(responseObject.getString("extra_info"));
        c.setCoupon_code(responseObject.getString("coupon_code"));
        if(!c.getCoupon_code().isEmpty()) {
            c.setCoupon_applied(responseObject.getInt("coupon_applied"));
        }
        c.setDiscount_amount(responseObject.getString("discount_amount"));
        c.setDelivery_charge(responseObject.getString("delivery_charge"));
        c.setGst_value(responseObject.getString("gst_value"));
        c.setGrand_total(responseObject.getString("grand_total"));
        c.setRedeemed_point(responseObject.getString("redeemed_point"));
        c.setPayable_amount(responseObject.getString("payable_amount"));
        c.setEarn_points(responseObject.getString("earn_points"));
        c.setFirst(responseObject.getString("first"));
        c.setSecond(responseObject.getString("second"));
        c.setDefault_address(responseObject.getString("default_address"));

        c.setIn_stock(responseObject.getJSONArray("in_stock"));
        c.setOut_stock(responseObject.getJSONArray("out_stock"));
        //Log.e("ARRAY ", )
        c.setFree_product(responseObject.getJSONArray("free_product"));
        c.setDelivery_option(responseObject.getJSONArray("delivery_option"));
        c.setPayment_mode(responseObject.getJSONArray("payment_mode"));



        return c;

    }

}
