package com.souvik.tendercuts.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class OrderHistoryModel implements java.io.Serializable{

    private String orderToken,orderNumber,totProd,totQuan,paidAmount,deliveryDate,orderDate,orderStatus,paymentStatus;
    private String productName,productQuantity,productPrice,productTotalPrice,productPic, is_cancel_msg, can_cancel_msg, parentOrderToken, trackUrl;
    private int index, is_cancel, can_cancel;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getOrderToken() {
        return orderToken;
    }

    public void setOrderToken(String orderToken) {
        this.orderToken = orderToken;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTotProd() {
        return totProd;
    }

    public void setTotProd(String totProd) {
        this.totProd = totProd;
    }

    public String getTotQuan() {
        return totQuan;
    }

    public void setTotQuan(String totQuan) {
        this.totQuan = totQuan;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductTotalPrice() {
        return productTotalPrice;
    }

    public void setProductTotalPrice(String productTotalPrice) {
        this.productTotalPrice = productTotalPrice;
    }

    public String getProductPic() {
        return productPic;
    }

    public void setProductPic(String productPic) {
        this.productPic = productPic;
    }

    public int getIs_cancel() {
        return is_cancel;
    }

    public void setIs_cancel(int is_cancel) {
        this.is_cancel = is_cancel;
    }

    public String getIs_cancel_msg() {
        return is_cancel_msg;
    }

    public void setIs_cancel_msg(String is_cancel_msg) {
        this.is_cancel_msg = is_cancel_msg;
    }

    public int getCan_cancel() {
        return can_cancel;
    }

    public void setCan_cancel(int can_cancel) {
        this.can_cancel = can_cancel;
    }

    public String getCan_cancel_msg() {
        return can_cancel_msg;
    }

    public void setCan_cancel_msg(String can_cancel_msg) {
        this.can_cancel_msg = can_cancel_msg;
    }

    public String getParentOrderToken() {
        return parentOrderToken;
    }

    public void setParentOrderToken(String parentOrderToken) {
        this.parentOrderToken = parentOrderToken;
    }

    public String getTrackUrl() {
        return trackUrl;
    }

    public void setTrackUrl(String trackUrl) {
        this.trackUrl = trackUrl;
    }
}


