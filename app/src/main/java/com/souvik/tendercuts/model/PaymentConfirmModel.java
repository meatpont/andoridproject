package com.souvik.tendercuts.model;

/**
 * Created on 25/09/17.
 */
public class PaymentConfirmModel implements java.io.Serializable{

    private String deliveryMode, deliveryDate, deliveryTime, deliveryAddress, totalprice, grandTotal;
    private int deliveryAddressToken;
    private boolean deliverymodeAdd = false,deliveryDateAdd = false, deliveryTimeAdd = false;


    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getDeliveryAddressToken() {
        return deliveryAddressToken;
    }

    public void setDeliveryAddressToken(int deliveryAddressToken) {
        this.deliveryAddressToken = deliveryAddressToken;
    }

    public boolean isDeliverymodeAdd() {
        return deliverymodeAdd;
    }

    public void setDeliverymodeAdd(boolean deliverymodeAdd) {
        this.deliverymodeAdd = deliverymodeAdd;
    }

    public boolean isDeliveryDateAdd() {
        return deliveryDateAdd;
    }

    public void setDeliveryDateAdd(boolean deliveryDateAdd) {
        this.deliveryDateAdd = deliveryDateAdd;
    }

    public boolean isDeliveryTimeAdd() {
        return deliveryTimeAdd;
    }

    public void setDeliveryTimeAdd(boolean deliveryTimeAdd) {
        this.deliveryTimeAdd = deliveryTimeAdd;
    }
}
