package com.souvik.tendercuts.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class CartModel implements java.io.Serializable{

    private String prodToken,prodName,prodDesc, marketPrice, sellPrice,pic;
    private boolean isAddedCart;
    private int index,cartAddedQuantity,userId,cartID;

    public CartModel(){

    }
    public CartModel(String prodToken, int cartAddedQuantity, int userId) {
        this.prodToken = prodToken;
        this.cartAddedQuantity = cartAddedQuantity;
        this.userId = userId;
    }

    public int getCartID() {
        return cartID;
    }

    public void setCartID(int cartID) {
        this.cartID = cartID;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getProdToken() {
        return prodToken;
    }

    public void setProdToken(String prodToken) {
        this.prodToken = prodToken;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(String sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getCartAddedQuantity() {
        return cartAddedQuantity;
    }

    public void setCartAddedQuantity(int cartAddedQuantity) {
        this.cartAddedQuantity = cartAddedQuantity;
    }

    public boolean isAddedCart() {
        return isAddedCart;
    }

    public void setAddedCart(boolean addedCart) {
        isAddedCart = addedCart;
    }
}
