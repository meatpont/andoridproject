package com.souvik.tendercuts.model;

/**
 * Created on 25/09/17.
 */
public class AddressModel implements java.io.Serializable{

    private String shipToken, shipName, shipEmail, shipPhone, shipAddress, shipCity, shipPostcode, shipDefault;
    private Boolean addressPresent;

    public Boolean getAddressPresent() {
        return addressPresent;
    }

    public void setAddressPresent(Boolean addressPresent) {
        this.addressPresent = addressPresent;
    }

    public String getShipToken() {
        return shipToken;
    }

    public void setShipToken(String shipToken) {
        this.shipToken = shipToken;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipEmail() {
        return shipEmail;
    }

    public void setShipEmail(String shipEmail) {
        this.shipEmail = shipEmail;
    }

    public String getShipPhone() {
        return shipPhone;
    }

    public void setShipPhone(String shipPhone) {
        this.shipPhone = shipPhone;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipPostcode() {
        return shipPostcode;
    }

    public void setShipPostcode(String shipPostcode) {
        this.shipPostcode = shipPostcode;
    }

    public String getShipDefault() {
        return shipDefault;
    }

    public void setShipDefault(String shipDefault) {
        this.shipDefault = shipDefault;
    }
}
