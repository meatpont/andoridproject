package com.souvik.tendercuts.common;

public interface DialogToolbarInterface {
	
	void onBackPressed();
	void onDonePressed();

}
