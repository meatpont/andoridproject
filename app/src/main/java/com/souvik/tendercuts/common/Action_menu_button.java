package com.souvik.tendercuts.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.souvik.tendercuts.activity.CartActivity;
import com.souvik.tendercuts.activity.HomeActivity;
import com.souvik.tendercuts.activity.LoginActivity;
import com.souvik.tendercuts.activity.OrderListingActivity;
import com.souvik.tendercuts.activity.ProfileActivity;
import com.souvik.tendercuts.activity.ReferActivity;
import com.souvik.tendercuts.activity.RewardActivity;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.SessionManager;

import in.co.meatpoint.R;

/**
 * Created by Souvik Chaudhury on 03-12-2017.
 */

public class Action_menu_button  extends AppCompatActivity {

    Context mcontext;
    Activity mactivity;

    SessionManager session;
    DatabaseHandler db;

    public Action_menu_button(Context mcontext,Activity mactivity) {
        this.mcontext = mcontext;
        this.mactivity = mactivity;

        this.session = SessionManager.getInstance(this.mcontext);
        this.db = new DatabaseHandler(this.mcontext);
    }

    public void actionMenuButton(int pageid){

        LinearLayout homeicon = (LinearLayout) mactivity.findViewById(R.id.layoutHome);
        LinearLayout ordericon = (LinearLayout)mactivity.findViewById(R.id.layoutOrder);
        LinearLayout profileicon = (LinearLayout)mactivity.findViewById(R.id.layoutProfile);
        LinearLayout chaticon = (LinearLayout)mactivity.findViewById(R.id.layoutWallet);

        if(pageid==1){
            homeicon.setBackgroundColor(mcontext.getResources().getColor(R.color.appThemegreencolor));
        }else if(pageid == 2){
            ordericon.setBackgroundColor(mcontext.getResources().getColor(R.color.appThemegreencolor));
        }else if(pageid == 3){
            profileicon.setBackgroundColor(mcontext.getResources().getColor(R.color.appThemegreencolor));
        }else if(pageid == 4){
            chaticon.setBackgroundColor(mcontext.getResources().getColor(R.color.appThemegreencolor));
        }else{
            homeicon.setBackgroundColor(mcontext.getResources().getColor(R.color.appThemegreencolor));
        }


        homeicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mactivity, HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mactivity.getApplicationContext().startActivity(i);
            }
        });

        ordericon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(session!=null) {
                    try {
                        if (!session.isCustomerLoggedIn().equalsIgnoreCase("1") && session.get_customerToken().isEmpty()) {
                            Intent i = new Intent(mactivity, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            mactivity.getApplicationContext().startActivity(i);
                        } else {
                            Intent i = new Intent(mactivity, OrderListingActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            mactivity.getApplicationContext().startActivity(i);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Intent i = new Intent(mactivity, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mactivity.getApplicationContext().startActivity(i);
                }
            }
        });

        profileicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(session!=null) {
                    try {
                        if (!session.isCustomerLoggedIn().equalsIgnoreCase("1") && session.get_customerToken().isEmpty()) {
                            Intent i = new Intent(mactivity, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            mactivity.getApplicationContext().startActivity(i);
                        } else {
                            Intent i = new Intent(mactivity, ProfileActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            mactivity.getApplicationContext().startActivity(i);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Intent i = new Intent(mactivity, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mactivity.getApplicationContext().startActivity(i);
                }
            }
        });

        chaticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(session!=null) {
                    try {
                        if (!session.isCustomerLoggedIn().equalsIgnoreCase("1") && session.get_customerToken().isEmpty()) {
                            Intent i = new Intent(mactivity, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            mactivity.getApplicationContext().startActivity(i);
                        } else {
                            Intent i = new Intent(mcontext, RewardActivity.class);
                            i.putExtra("title","");
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            mactivity.getApplicationContext().startActivity(i);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Intent i = new Intent(mactivity, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mactivity.getApplicationContext().startActivity(i);
                }
            }
        });
    }
}
