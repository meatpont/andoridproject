package com.souvik.tendercuts.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.DeliverySummaryActivity;
import com.souvik.tendercuts.activity.OrderListingActivity;
import com.souvik.tendercuts.adapter.CartProdDemoListingAdapter;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.CalculateCartModel;
import com.souvik.tendercuts.model.GetTimeModel;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.utils.MeatPointUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.souvik.tendercuts.config.AppConfig.CALCULATE_CART;
import static com.souvik.tendercuts.model.CalculateCartModel.setCalculateCart;

/**
 * Created by RAD365 on 30-01-2018.
 */

public class CalculateCart extends AppCompatActivity {

    Context activityName;
    ProgressDialog pDialog;

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    View v;

    ArrayList<ItemModel> items,cartItems;
    CartProdDemoListingAdapter adapter;

    public CalculateCart() {

    }

    public void initialization(Context c){
        this.activityName = c;
        pDialog = new ProgressDialog(activityName);
    }

    public void paymentConfirmPageLoad(String custToken, String products, String deliveryMode, String deliveryDate, String deliveryTime, String deliveryDate2, String deliveryTime2, String deliveryAddres, String checkCouponCode, String couponCode, String checkRewardPoint, String extraInfo, String pm_paymentMethod,String pm_place_order){
        initVolleyCallback();
        showDialog();

        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("cusToken", custToken);
        paramse.put("products", products);
        paramse.put("delivery_mode", deliveryMode);
        paramse.put("first_delivery_date", deliveryDate);
        paramse.put("first_delivery_time", deliveryTime);
        paramse.put("second_delivery_date", deliveryDate2);
        paramse.put("second_delivery_time", deliveryTime2);
        paramse.put("delivery_address", deliveryAddres);
        paramse.put("check_coupon_code", checkCouponCode);
        paramse.put("coupon_code", couponCode);
        paramse.put("check_reward_point", checkRewardPoint);
        paramse.put("cod", pm_paymentMethod);
        paramse.put("place_order", pm_place_order);
        paramse.put("extra_info",extraInfo);

        RequestQueue mRequestQueueg = Volley.newRequestQueue(activityName);
        mVolleyService = new VolleyService(mResultCallback,activityName);
        Log.e("cart post pmtr2", String.valueOf(paramse));
        mVolleyService.postStringDataVolley("CALCULATE_CART",CALCULATE_CART,paramse,mRequestQueueg);


    }

    void initVolleyCallback(){
        Log.e("Test","Test");
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType,JSONObject response) {
                Log.e("Response ", "Total Response GET : " + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.e("Request type ","" + requestType);
                if(requestType.equalsIgnoreCase("CALCULATE_CART")){
                    try {
                        hideDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e("Cart response ", response);
                            CalculateCartModel c = setCalculateCart(jsonObject);

                            ((DeliverySummaryActivity)activityName).outputCalculateResponse(c);

//                            //outputResponse(c);
//
//                            /*int status_code = jsonObject.getInt("status");
//                            if (status_code == 1) {
//
//                                JSONObject responseObject = new JSONObject(jsonObject.getString("response"));
//
//                                fb_totalPrice.setText("₹"+responseObject.getString("sub_total"));
//                                if(!responseObject.getString("sub_total").isEmpty() && Double.parseDouble(responseObject.getString("sub_total"))>0) {
//                                    fb_totLayout.setVisibility(View.VISIBLE);
//                                }
//
//                                fb_discountFee.setText("₹"+responseObject.getString("discount_amount"));
//                                if(!responseObject.getString("discount_amount").isEmpty() && Double.parseDouble(responseObject.getString("discount_amount"))>0) {
//                                    fb_discountOptionLayout.setVisibility(View.VISIBLE);
//                                }
//
//                                fb_deliveryFee.setText("₹"+responseObject.getString("delivery_charge"));
//                                if(!responseObject.getString("delivery_charge").isEmpty() && Double.parseDouble(responseObject.getString("delivery_charge"))>0) {
//                                    fb_deliveryOptionLayout.setVisibility(View.VISIBLE);
//                                }
//
//                                fb_gstValue.setText("₹"+responseObject.getString("gst_value"));
//                                if(!responseObject.getString("gst_value").isEmpty() && Double.parseDouble(responseObject.getString("gst_value"))>0) {
//                                    fb_gstOptionLayout.setVisibility(View.VISIBLE);
//                                }
//
////                                fb_grandTotal.setText("₹"+responseObject.getString("grand_total"));
//                                fb_grandTotal.setText("₹"+responseObject.getString("payable_amount"));
//                                if(!responseObject.getString("grand_total").isEmpty() && Double.parseDouble(responseObject.getString("grand_total"))>0) {
//                                    fb_grandTotalLayout.setVisibility(View.VISIBLE);
//                                }
//
//                                fb_redeemedPoint.setText("(-)₹"+responseObject.getString("redeemed_point"));
//                                if(!responseObject.getString("redeemed_point").isEmpty() && Double.parseDouble(responseObject.getString("redeemed_point"))>0) {
//                                    fb_redemedPointsLayout.setVisibility(View.VISIBLE);
//                                }
//
//                                fb_points.setText(responseObject.getString("earn_points")+" Points");
//                                if(!responseObject.getString("earn_points").isEmpty() && Double.parseDouble(responseObject.getString("earn_points"))>0) {
//                                    fb_earnPointsLayout.setVisibility(View.VISIBLE);
//                                }
//
////                                payableAmount.setText("₹"+responseObject.getString("payable_amount"));
//                                totalPriceButtom.setText("₹" + responseObject.getString("payable_amount"));
//
//                                //responseObject.getString("coupon_code");
//                                //responseObject.getString("payable_amount");
//                                JSONObject jj = new JSONObject(responseObject.getString("default_address"));
//                                pm_deliveryAddres = jj.getString("shipToken");
//                                String fullAdress = jj.getString("shipName") + ", " +
//                                        jj.getString("shipEmail") + ", " +
//                                        jj.getString("shipPhone") + ", " +
//                                        jj.getString("shipAddress") + ", " +
//                                        jj.getString("shipCity") + ", " +
//                                        jj.getString("shipPostcode");
//                                addressDetails.setText(fullAdress);
//
//                                //Todo: For First Item Delivery
//                                Log.d("creation",responseObject.getString("first"));
//                                *//*if (responseObject.getJSONObject("first") instanceof JSONObject){
//                                    Log.d("creation","First -> jsonobject");
//                                }*//*
//
//                                if (responseObject.getString("first").equalsIgnoreCase("null")){
//                                    Log.d("creation","First -> jsonarray");
//                                }else{
//                                    JSONObject firstProductlistdArray = new JSONObject(responseObject.getString("first"));
//                                    Log.d("creation","msg 1 => "+firstProductlistdArray.getString("msg_1"));
//                                }
//
//                                if (responseObject.getString("second").equalsIgnoreCase("null")){
//                                    Log.d("creation","Second -> jsonarray");
//                                }else{
//                                    JSONObject secondProductlistdArray = new JSONObject(responseObject.getString("second"));
//                                    seconditemCountHeading.setText(secondProductlistdArray.getString("msg_1"));
//                                    secondTitle.setText(secondProductlistdArray.getString("msg_3"));
//                                    secondDate.setText(secondProductlistdArray.getString("msg_4"));
//
//                                    JSONArray slotArray = secondProductlistdArray.getJSONArray("slot");
//                                    secondDeliveryDateTime.setText(slotArray.getJSONObject(0).getString("day"));
//
//                                    secondDeliveryValue.setText(" ₹"+secondProductlistdArray.getString("delivery_charge"));
//                                    secondDeliveryCharge.setText(" ₹"+secondProductlistdArray.getString("delivery_charge"));
//                                }
//                            }*/
                        }catch (Exception e){
                            Log.e("Exception", e.getMessage());
                        }

                        Log.d("creation", "Total Response POST: "+response);

                    }catch (Exception e){
                        Log.e("Exception -- ", e.getMessage());
                    }
                }

            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                Log.e("---", "Volley requester error==" + error);
                Log.d("---", "Volley JSON post" + "That didn't work!");
            }
        };
    }

    private void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            //pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public interface outputResponseInteface{
         void outputCalculateResponse(CalculateCartModel c);
    }
}
