package com.souvik.tendercuts.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.souvik.tendercuts.activity.CartActivity;
import com.souvik.tendercuts.activity.ItemListingActivity;
import com.souvik.tendercuts.activity.LoginActivity;
import com.souvik.tendercuts.activity.ProductDetailsActivity;
import com.souvik.tendercuts.activity.farhan.ListActivity;
import com.souvik.tendercuts.activity.farhan.model.TimeHolder;
import com.souvik.tendercuts.adapter.CartListingAdapter;
import com.souvik.tendercuts.adapter.ItemListingAdapter;
import com.souvik.tendercuts.database.DatabaseHandler;
import com.souvik.tendercuts.helper.FccResult;
import com.souvik.tendercuts.helper.OnRefreshCartViewListner;
import com.souvik.tendercuts.helper.SessionManager;
import com.souvik.tendercuts.helper.VolleyService;
import com.souvik.tendercuts.model.ItemModel;
import com.souvik.tendercuts.utils.CartAddNotify;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.co.meatpoint.R;

import static com.souvik.tendercuts.config.AppConfig.PRODUCT_LISTING_URL;
import static com.souvik.tendercuts.utils.MeatPointUtils.setFontAwesome;

public class ItemListFragment extends Fragment implements CartAddNotify, ItemListingAdapter.ItemAdapterCallback {

    FccResult mResultCallback = null;
    VolleyService mVolleyService;
    boolean allowRefresh = true;
    View v;

    private RecyclerView recyclerView;
    private ItemListingAdapter adapter;
    private List<ItemModel> itemModelList;
    private ProgressBar listingProgress;
    private TextView noItemFound, cartaddedQuantity, totalPriceCart;

    private String catToken, userid = "";
    SessionManager session;
    public DatabaseHandler db;
    List<ItemModel> items = new ArrayList<ItemModel>();

    private Intent listingItemIntent;
    String strtext;
    private CartAddNotify yourIn;
    LinearLayout proceedLayout;
    View vw;

    @SuppressLint("ValidFragment")
    public ItemListFragment(String catToken) {
        this.catToken = catToken;
        Log.e("Token ", this.catToken);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listingItemIntent = getActivity().getIntent();
        db = new DatabaseHandler(getActivity());
//        catToken =  getArguments().getString("catToken");
//        Log.d("creation","catToken => ");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vw = inflater.inflate(R.layout.fragment_item_list, container, false);
        setFontAwesome((RelativeLayout) vw.findViewById(R.id.layout), getActivity());
        return vw;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        catToken = listingItemIntent.getStringExtra("catToken");
        session = SessionManager.getInstance(getContext());


        initVolleyCallback();
        Map<String, String> paramse = new HashMap<String, String>();
        paramse.put("catToken", catToken);
        RequestQueue mRequestQueueg = Volley.newRequestQueue(getContext());
        mVolleyService = new VolleyService(mResultCallback, getContext());
        mVolleyService.postStringDataVolley("PRODUCT_LISTING", PRODUCT_LISTING_URL, paramse, mRequestQueueg);

        recyclerView = (RecyclerView) getView().findViewById(R.id.categoryRecycler);
        proceedLayout = getView().findViewById(R.id.proceedLayout);

//        if(db.getCartCountByUserID(userid)>0)
//            proceedLayout.setVisibility(View.VISIBLE);
//        else
//            proceedLayout.setVisibility(View.GONE);

        itemModelList = new ArrayList<ItemModel>();
        adapter = new ItemListingAdapter(getContext(), itemModelList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        listingProgress = (ProgressBar) getView().findViewById(R.id.listingProgress);
        noItemFound = (TextView) getView().findViewById(R.id.noItemFound);
        cartaddedQuantity = (TextView) getView().findViewById(R.id.cartaddedQuantity);

        int count = 0;
        if (db.getCartCountByUserID(session.get_customerToken()) >= 1) {
            proceedLayout.setVisibility(View.VISIBLE);
            count = 0;
            count = com.souvik.tendercuts.activity.farhan.SessionManager.getQuantity(getActivity());
            cartaddedQuantity.setText("" + count);
        } else {
            proceedLayout.setVisibility(View.GONE);
        }

        adapter.setOnItemSelected(this);

        TextView proceedText = (TextView) getView().findViewById(R.id.proceedText);
        proceedText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                    Intent i = new Intent(getActivity(), CartActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(getActivity(), LoginActivity.class);

                    HashMap<String, String> detailsMap = new HashMap<String, String>();
                    detailsMap.put("isCustomerLoggedIn", "4");
                    session.set_UserDetails(detailsMap);

                    startActivity(i);
                }

            }
        });

        Button proceedBtn = (Button) getView().findViewById(R.id.proceedBtn);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
                    Intent i = new Intent(getActivity(), CartActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(getActivity(), LoginActivity.class);

                    HashMap<String, String> detailsMap = new HashMap<String, String>();
                    detailsMap.put("isCustomerLoggedIn", "4");
                    session.set_UserDetails(detailsMap);

                    startActivity(i);
                }
            }
        });

    }

    void initVolleyCallback() {
        mResultCallback = new FccResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.d("creation", "Total Response GET : " + response);
                /**/

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.d("creation", "Total Response POST: " + response);

                listingProgress.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status_code = jsonObject.getInt("status");
                    if (status_code == 1) {
                        recyclerView.setVisibility(View.VISIBLE);
                        itemModelList.clear();
                        //JSONObject hh = jsonObject.getJSONObject("detail");
                        JSONArray productlistArray = jsonObject.getJSONArray("response");
                        for (int i = 0; i < productlistArray.length(); i++) {
                            JSONObject productEachRow = productlistArray.getJSONObject(i);

                            ItemModel a = new ItemModel();
                            a.setIndex(i);
                            a.setProdDesc(productEachRow.getString("prodDesc"));
                            a.setProdName(productEachRow.getString("prodName"));
                            a.setTomorrowDelivery(productEachRow.getString("tomorrowDelivery"));
                            a.setIsNew(productEachRow.getString("isNew"));
                            a.setPic(productEachRow.getString("pic"));
//                            a.setMarketPrice(productEachRow.getString("marketPrice"));
                            a.setProdToken(productEachRow.getString("prodToken"));
                            a.setSellPrice(productEachRow.getString("sellPrice"));
                            a.setNetWeight(productEachRow.getString("netWeight"));
                            a.setUserId(session.get_customerToken());
                            a.setStockAddedQuantity(productEachRow.getInt("stockQuan"));
                            a.setDeal_price(productEachRow.getString("dealPrice"));
                            a.setOutStock(productEachRow.getString("outStock"));
                            /*Log.d("creation","ff1=> "+session.get_customerToken().toString());
                            Log.d("creation","ff2=> "+session.get_customerApiKey().toString());
                            Log.d("creation","ff3=> "+session.get_customerEmail().toString());
                            Log.d("creation","ff4=> "+session.get_customerName().toString());*/
                            itemModelList.add(a);

                        }
                    } else {
                        noItemFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    noItemFound.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
//                Toast.makeText(getActivity(), "Request time out", Toast.LENGTH_SHORT).show();

                Log.d("---", "Volley requester error==" + error.getMessage());
                Log.d("---", "Volley JSON post" + "That didn't work!");
                // listingProgress.setVisibility(View.GONE);
            }
        };
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onItemSelected(int k) {

        if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
            userid = session.get_customerToken();
        } else {
            userid = "demouser";
        }
        if (db.getCartCountByUserID(userid) > 0) {
            proceedLayout.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, dptopixel(110), 0, dptopixel(100));
//            fragmentContainer.setLayoutParams(layoutParams);
            items.clear();
            items = db.getAllCartItems(userid);
            double prce = 0;
            int qntity = 0;
            for (int i = 0; i < items.size(); i++) {
                ItemModel itemModel = items.get(i);
                //  prce = prce + Double.parseDouble(itemModel.getMarketPrice());
                qntity = qntity + itemModel.getCartAddedQuantity();
            }
            Log.e("quantity ", String.valueOf(qntity));
            com.souvik.tendercuts.activity.farhan.SessionManager.createSession(getActivity(), qntity);

            //  totalPriceCart.setText((int) prce);
            cartaddedQuantity.setText("" + qntity);
//
        } else {
            proceedLayout.setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );

            layoutParams.setMargins(0, dptopixel(110), 0, dptopixel(50));
//            fragmentContainer.setLayoutParams(layoutParams);

        }
//        adapter.notifyDataSetChanged();
    }

    public int dptopixel(int dpValue) {
        float d = getContext().getResources().getDisplayMetrics().density;
        int pixl = (int) (dpValue * d); // margin in pixels
        return pixl;
    }

    int count = 00;

    @Override
    public void onResume() {
        super.onResume();

//        if(db.getCartCountByUserID(session.get_customerToken())>=1) {
//            proceedLayout.setVisibility(View.VISIBLE);
//            int count = db.getAllCartItems(session.get_customerToken());
//
//        }
//        else{
//            proceedLayout.setVisibility(View.GONE);
//        }

        if (session.isCustomerLoggedIn().equalsIgnoreCase("1")) {
            userid = session.get_customerToken();
        } else {
            userid = "demouser";
        }
        if (db.getCartCountByUserID(userid) > 0) {
            proceedLayout.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, dptopixel(110), 0, dptopixel(100));
//            fragmentContainer.setLayoutParams(layoutParams);
            items = db.getAllCartItems(userid);
            double prce = 0;
            int qntity = 0;
            for (int i = 0; i < items.size(); i++) {
                ItemModel itemModel = items.get(i);
                //  prce = prce + Double.parseDouble(itemModel.getMarketPrice());
                qntity = qntity + itemModel.getCartAddedQuantity();
            }
            Log.e("quantity ", String.valueOf(qntity));
            com.souvik.tendercuts.activity.farhan.SessionManager.createSession(getActivity(), qntity);

            //  totalPriceCart.setText((int) prce);
            cartaddedQuantity.setText("" + qntity);
//
        } else {
            proceedLayout.setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );

            layoutParams.setMargins(0, dptopixel(110), 0, dptopixel(50));
//            fragmentContainer.setLayoutParams(layoutParams);

        }
//        adapter.notifyDataSetChanged();
    }

    @Override
    public void setItemModelList(List<ItemModel> itemModelList, int pos) {
//        adapter.notifyDataSetChanged();

//        ItemModel itemModel = itemModelList.get(pos);
//        /*Log.d("creation","Product Name => "+itemModel.getProdName());
//        Log.d("creation","Product Token => "+itemModel.getProdToken());*/
//
//
//        Intent i = new Intent(getActivity(), ProductDetailsActivity.class);
//
//        HashMap<String, ItemModel> itemlistsHashmap = new HashMap<String, ItemModel>();
//        itemlistsHashmap.put("itemsDataHash",itemModel);
//
//        HashMap<String, List<ItemModel>> itemTotalHashmap = new HashMap<String, List<ItemModel>>();
//
//        itemTotalHashmap.put("totalItems",itemModelList);
//
//        Bundle extras = new Bundle();
//        extras.putSerializable("itemsData", itemlistsHashmap);
//        extras.putSerializable("allItems", itemTotalHashmap);
//        extras.putInt("position", pos);
//
//        i.putExtras(extras);
//        startActivityForResult(i,1000);
        //startActivity(i);
        /*Log.d("creation","Product Name => "+itemModel.getProdName());
        Log.d("creation","Addedd Cart => "+itemModel.isAddedCart());
        //i.putExtra("catToken",String.valueOf(itemModel.getProdToken()));
        Bundle extras = new Bundle();
        extras.putSerializable("itemsData", itemlistsHashmap);
        */
    }


    //    @Override
//    public void refreshView() {
//        clear();
//        loadCartPage();
//    }
//
//    private void loadCartPage() {
//        Log.e("cart item ", String.valueOf(items.size()));
//        if(db.getCartCountByUserID(session.get_customerToken())>0){
//            //showDialog();
//            adapter = new ItemListingAdapter(getContext(),itemModelList);
//            recyclerView.setAdapter(adapter);
////            paymentConfirmPageLoad(session.get_customerToken(), getProductLists(), f_checkCouponCode,f_couponCode, f_checkRewardPoint,f_extraInfo);
//
//        }else{
//            getActivity().finish();
//        }
//    }
//
//    public void clear() {
//        items.clear();
//        itemModelList.clear();
//        adapter.notifyDataSetChanged();
//    }
}
