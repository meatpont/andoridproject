package com.souvik.tendercuts.config;

/**
 * Created by Souvik on 3/30/2017.
 */

public class AppConfig {

    public static String SITE_URL = "https://meatpoint.co.in/api/v1/";  // Production

    public static String CUSTOMER_AUTHENTICATE = SITE_URL+"Authenticate/authenticateCustomer";

    public static String REGISTRATION_URL = SITE_URL+"Account/signup";

    public static String HOMEPAGE = SITE_URL+"Home/category";

    public static String PRODUCT_LISTING_URL = SITE_URL+"Home/products";

    public static String PRODUCT_DETAILS_URL = SITE_URL+"Home/productdetail";

    public static String CART_CHECK_URL = SITE_URL+"Cart/validate_cart";

    public static String GET_DELIVERY_ADDRESS = SITE_URL+"Cart/delivery_address";

    public static String ADD_DELIVERY_ADDRESS = SITE_URL+"Cart/delivery_address_add";

    public static String DELETE_DELIVERY_ADDRESS = SITE_URL+"Cart/delivery_address_delete";

    public static String EDIT_DELIVERY_ADDRESS = SITE_URL+"Cart/delivery_address_edit";

    public static String ADD_ORDER = SITE_URL+"Cart/add_order";

    public static String COUPON_VALIDATION = SITE_URL+"Cart/validate_coupon";

    public static String GET_TIMESLOT = SITE_URL+"Cart/get_timeslot";

    public static String GET_ORDER = SITE_URL+"Account/get_orders";

    public static String GET_ORDER_DETAILS = SITE_URL+"Account/order_detail";

    public static String UPDATE_PROFILE = SITE_URL+"Account/update_profile";

    public static String GET_PROFILE = SITE_URL+"Account/profile";

    public static String ADD_MASSORDER = SITE_URL+"Cart/massorder_add";

    public static String FAQ = SITE_URL+"Home/faq";

    public static String CONTACT_US = SITE_URL+"Home/contactus";

    public static String CALCULATE_CART = SITE_URL+"Cart/calculate_cart";

    public static String ORDER_TRACK = SITE_URL+"Account/order_track";

    public static String GET_REWARDS = SITE_URL+"Account/get_rewards";

    public static String MOBILE_VERIFY = SITE_URL+"Account/mobile_verification";

    public static String BULKORDER_INFO = SITE_URL+"Home/bulkorder_info";

    public static String CANCEL_ORDER = SITE_URL+"Account/cancel_order";

    public static String SEND_OTP = SITE_URL+"Authenticate/send_otp";

    public static String VERIFY_OTP = SITE_URL+"Authenticate/verify_otp";

    public static String REFER_FRIEND = SITE_URL+"Account/refer_friend";

    public static String UPDATE_TRANSACTOIN = SITE_URL+"Cart/update_transaction";

   // public static String cusToken ="c4ca4238a0b923820dcc509a6f75849b";
}
