package com.souvik.tendercuts.payyoumoney;

public enum AppEnvironment {

//    Success URL: https://meatpoint.co.in/Cart/payu_response
//    Failure URL : https://meatpoint.co.in/Cart/payu_response
//    Cancel URL : https://meatpoint.co.in/Cart/payu_response

    SANDBOX {
        @Override
        public String merchant_Key() {
            return "gtKFFx";
        }
       /* public String merchant_Key() {
            return "JBZaLc";
        }*/

        @Override
        public String merchant_ID() {
            return "393463";
        }

        @Override
        public String furl() {
            return "https://meatpoint.co.in/Cart/payu_response";
        }

        @Override
        public String surl() {
            return "https://meatpoint.co.in/Cart/payu_response";
        }

        @Override
        public String salt() {
            return "qauKbEAJ";
            //return "qauKbEAJ";
            //return "GQs7yium";
        }

        @Override
        public boolean debug() {
            return true;
        }
    },
    PRODUCTION {
        @Override
        public String merchant_Key() {
            return "xiM0V142";
        }

        @Override
        public String merchant_ID() {
            return "5992468";
        }

        @Override
        public String furl() {
            return "https://meatpoint.co.in/Cart/payu_response";
        }

        @Override
        public String surl() {
            return "https://meatpoint.co.in/Cart/payu_response";
        }

        @Override
        public String salt() {
            return "nPuXoWw2QQ";
        }

        @Override
        public boolean debug() {
            return false;
        }
    };

    public abstract String merchant_Key();

    public abstract String merchant_ID();

    public abstract String furl();

    public abstract String surl();

    public abstract String salt();

    public abstract boolean debug();


}
