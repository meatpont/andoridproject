
package com.souvik.tendercuts.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.souvik.tendercuts.model.CartModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SessionManager {

    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;
    private static SessionManager sessionManager;

    // Shared preferences file name
    private static final String PREF_NAME = "FlavourClub";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public static SessionManager getInstance(Context ctx)
    {
        if(sessionManager == null)
        {
            sessionManager =  new SessionManager(ctx);
        }
        return  sessionManager;
    }

    //KEY Declared


    public static final String KEY_API_KEY ="apiKey";
    public static final String KEY_customerToken ="customerToken";
    public static final String KEY_customerEmail ="customerEmail";
    public static final String KEY_customerName ="customerName";
    public static final String KEY_customerPhone ="customerPhone";
    public static final String KEY_customerPhoto ="customerPhoto";
    public static final String KEY_isCustomerLoggedIn ="isCustomerLoggedIn";
    // For Login
    public String get_customerApiKey(){
        HashMap<String, String> userdetailsblock = new HashMap<String, String>();
        userdetailsblock.put(KEY_API_KEY, pref.getString(KEY_API_KEY, null));
        return userdetailsblock.get(SessionManager.KEY_API_KEY);
    }
    public String get_customerToken(){
        HashMap<String, String> userdetailsblock = new HashMap<String, String>();
        userdetailsblock.put(KEY_customerToken, pref.getString(KEY_customerToken, null));
        return userdetailsblock.get(SessionManager.KEY_customerToken);
    }
    public String get_customerEmail(){
        HashMap<String, String> userdetailsblock = new HashMap<String, String>();
        userdetailsblock.put(KEY_customerEmail, pref.getString(KEY_customerEmail, null));
        return userdetailsblock.get(SessionManager.KEY_customerEmail);
    }
    public String get_customerName(){
        HashMap<String, String> userdetailsblock = new HashMap<String, String>();
        userdetailsblock.put(KEY_customerName, pref.getString(KEY_customerName, null));
        return userdetailsblock.get(SessionManager.KEY_customerName);
    }
    public String get_customerPhone(){
        HashMap<String, String> userdetailsblock = new HashMap<String, String>();
        userdetailsblock.put(KEY_customerPhone, pref.getString(KEY_customerPhone, null));
        return userdetailsblock.get(SessionManager.KEY_customerPhone);
    }
    public String get_customerPhoto(){
        HashMap<String, String> userdetailsblock = new HashMap<String, String>();
        userdetailsblock.put(KEY_customerPhoto, pref.getString(KEY_customerPhoto, null));
        return userdetailsblock.get(SessionManager.KEY_customerPhoto);
    }
    public String isCustomerLoggedIn(){
        HashMap<String, String> userdetailsblock = new HashMap<String, String>();
        userdetailsblock.put(KEY_isCustomerLoggedIn, pref.getString(KEY_isCustomerLoggedIn, null));
        return userdetailsblock.get(SessionManager.KEY_isCustomerLoggedIn);
    }

    public void set_UserDetails(HashMap<String,String> userdetails){
        for( Map.Entry entry : userdetails.entrySet() ) {
            Log.d("creation",""+entry.getKey());
            editor.putString(entry.getKey().toString(), entry.getValue().toString());
        }
        editor.commit();
    }

    public void setCartDetails(List<CartModel> cartItems){

    }

}

