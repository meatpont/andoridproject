package com.souvik.tendercuts.helper;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by Souvik Chaudhury on 15-06-2017.
 */

public interface FccResult {
    public void notifySuccess(String requestType, JSONObject response);
    public void notifySuccessString(String requestType, String response);
    public void notifyError(String requestType, VolleyError error);
}
