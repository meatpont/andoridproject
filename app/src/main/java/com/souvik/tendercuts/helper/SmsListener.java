package com.souvik.tendercuts.helper;

/**
 * Created by Souvik Chaudhury on 20-07-2017.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}
