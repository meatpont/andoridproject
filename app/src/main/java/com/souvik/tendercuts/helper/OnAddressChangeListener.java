package com.souvik.tendercuts.helper;

import com.souvik.tendercuts.model.AddressModel;

/**
 * Created by HP on 20-10-2017.
 */

public interface OnAddressChangeListener {
    public void addressChange(AddressModel adrs);
}
