package com.souvik.tendercuts.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.souvik.tendercuts.model.CartModel;
import com.souvik.tendercuts.model.ItemModel;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 2;

	// Database Name
	private static final String DATABASE_NAME = "meetpoint";

	// table name
	private static final String TABLE_CARTS = "cart";

	// Cart Table Columns names
	private static final String KEY_ID = "id";
	//private static final String KEY_NAME = "product_id";
	private static final String KEY_PRODUCT_ID = "product_id";
	private static final String KEY_PRODUCT_QUANTITY = "product_quantity";
	private static final String KEY_USER_ID = "user_id";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARTS);
		String CREATE_CART_TABLE = "CREATE TABLE " + TABLE_CARTS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_PRODUCT_ID + " INTEGER," + KEY_PRODUCT_QUANTITY + " INTEGER," + KEY_USER_ID + " TEXT" + ")";
		db.execSQL(CREATE_CART_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARTS);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding data in cart
	public void addProductInCart(ItemModel cartitem) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_PRODUCT_ID, cartitem.getProdToken()); // Product Id
		values.put(KEY_PRODUCT_QUANTITY, cartitem.getCartAddedQuantity()); // Product Quantity
		values.put(KEY_USER_ID, cartitem.getUserId()); // User ID

		// Inserting Row
		db.insert(TABLE_CARTS, null, values);
		db.close(); // Closing database connection
	}

	// Getting single contact
	/*Contact getCartDetailsByUserID(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_CARTS, new String[] { KEY_ID, KEY_PRODUCT_ID, KEY_PRODUCT_QUANTITY }, KEY_USER_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Contact contact = new Contact(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
		// return contact
		return contact;
	}*/
	
	// Getting All Contacts
	public List<ItemModel> getAllCartItems(String id) {
		List<ItemModel> cartList = new ArrayList<ItemModel>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CARTS +" WHERE "+ KEY_USER_ID + "='"+id+"'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				ItemModel cart = new ItemModel();
				cart.setCartID(Integer.parseInt(cursor.getString(0)));
				cart.setProdToken(cursor.getString(1));
				cart.setCartAddedQuantity(Integer.parseInt(cursor.getString(2)));
				cart.setUserId(cursor.getString(3));

				// Adding cart to list
				cartList.add(cart);

			} while (cursor.moveToNext());
		}

		// return cart list
		return cartList;
	}
	public int deleteCart(){
		String countQuery = "DELETE  FROM " + TABLE_CARTS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);

		// return count
		return cursor.getCount();
	}

	public int getCartCountByUserID(String userId) {
		String countQuery = "SELECT * FROM " + TABLE_CARTS + " WHERE "+KEY_USER_ID +"='"+userId +"'";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);

		// return count
		return cursor.getCount();
	}

	// Updating single cart quantity
	public int updateCartProductQuantity(ItemModel cart) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_PRODUCT_QUANTITY, cart.getCartAddedQuantity());

		// updating row
		return db.update(TABLE_CARTS, values, KEY_ID + " = ?",
				new String[] { String.valueOf(cart.getCartID()) });
	}

	//Update all guestlogin user cart items
	public int updateCartProductToAnotherUser(String USER_ID) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_USER_ID, USER_ID);

		// updating row
		return db.update(TABLE_CARTS, values, KEY_USER_ID + " = ?",
				new String[] { "demouser" });
	}

	// Deleting single contact
	public void deleteItemFromCart(int cartid) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CARTS, KEY_ID + " = ?", new String[] { String.valueOf(cartid) });
		db.close();
	}

	public void deleteCartUsingUserId(String userid) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CARTS, KEY_USER_ID + " = ?", new String[] { userid });
		db.close();
	}


	// Getting contacts Count
	public int getCartCount(ItemModel cart) {
		String countQuery = "SELECT * FROM " + TABLE_CARTS + " WHERE "+KEY_USER_ID +"='"+cart.getUserId()+"' AND "+ KEY_PRODUCT_ID+"="+cart.getProdToken();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		//cursor.close();

		// return count
		return cursor.getCount();
	}

	public int getCartID(ItemModel cart) {
		String countQuery = "SELECT "+KEY_ID+" FROM " + TABLE_CARTS + " WHERE "+KEY_USER_ID +"='"+cart.getUserId()+"' AND "+ KEY_PRODUCT_ID+"="+cart.getProdToken();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		//cursor.close();
		String CartID="0";
		if (cursor.moveToFirst()) {
			do {
				CartID = cursor.getString(0);

			} while (cursor.moveToNext());
		}
		// return count43
		return Integer.parseInt(CartID);
	}

	public int getCartAddedQuantity(ItemModel cart) {
		String countQuery = "SELECT "+KEY_PRODUCT_QUANTITY+" FROM " + TABLE_CARTS + " WHERE "+KEY_USER_ID +"='"+cart.getUserId()+"' AND "+ KEY_PRODUCT_ID+"="+cart.getProdToken();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		//cursor.close();
		String CartID="0";
		if (cursor.moveToFirst()) {
			do {
				CartID = cursor.getString(0);

			} while (cursor.moveToNext());
		}
		// return count
		return Integer.parseInt(CartID);
	}



}
